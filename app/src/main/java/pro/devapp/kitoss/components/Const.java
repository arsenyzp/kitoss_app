package pro.devapp.kitoss.components;

/**
 * Список констант
 */
public interface Const {
    String API_URL = "http://37.46.135.13:3000/";
    String SOCKET_URL = "http://37.46.135.13:3000/";
    String GENDER_MALE = "male";
    String GENDER_FEMALE = "female";
    String[] GENDERS = new String[]{GENDER_MALE, GENDER_FEMALE};

    String PREFERENCE_TOKEN = "utoken";
    String PREFERENCE_GCM_TOKEN = "gcm_token";

    int STATE_SUCCESS_RESPONSE = 200;
    int STATE_ERROR_RESPONSE = -1;
    int STATE_SERVER_ERROR = 500;

    int STATE_SUCCESS_AUTH = 201;
    int STATE_SUCCESS_REGISTER = 202;
    int STATE_SUCCESS_RESEND_PASSWORD = 203;
    int STATE_SUCCESS_UPLOAD = 204;
    int STATE_SUCCESS_ADD_TO_FRIENDS = 205;
    int STATE_SUCCESS_REMOVE_FROM_FRIENDS = 206;
    int STATE_SUCCESS_DECLINE_FRIEND_REQUEST = 207;
    int STATE_SUCCESS_CANCEL_FRIEND_REQUEST = 208;

    int SOCKET_ACTION_NEW_FRIEND_REQUEST = 1208; // новый запрос в друзья
    int SOCKET_ACTION_ACCEPT_REQUEST = 1209; // принятие запроса на дружбу
    int SOCKET_ACTION_DECLINE_REQUEST = 1210; // отпклонение запроса на дружбу
    int SOCKET_ACTION_CANCEL_FRIEND_REQUEST = 1211; // отмена запроса на дружбу
    int SOCKET_ACTION_REMOVE_FRIEND = 1212; // удаление из друзей
    int SOCKET_ACTION_NEW_MESSAGE = 1220; // новое сообщение
    int SOCKET_ACTION_NEW_PUBLIC_MESSAGE = 1221; // новое сообщение
    int SOCKET_ACTION_TYPING = 199; // печатает
    int SOCKET_ACTION_UPDATE_BALANCE = 501; // обновление баланса
    int SOCKET_ACTION_REMOVE_PUBLIC_MESSAGE = 1501; // удаление публичного сообщения
    int SOCKET_ACTION_NEW_EVENT = 1502; // новое событие в ленте
    int SOCKET_ACTION_AUTH = 1503; // новое событие в ленте

    int STATE_ACCESS_DINEDED = 403;

    String PREFERENCE_SENT_TOKEN_TO_SERVER = "sentTokenToServer";

    String[] days = {"-", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
    int min_year = 1970;
    int max_year = 2010;

    // статусы друзей
    String FRIEND_STATUS_FRIEND = "friend"; // друг
    String FRIEND_STATUS_WANT_FRIEND = "want_friend"; // хочет дружить со мной
    String FRIEND_STATUS_WISH_FRIEND = "wish_friend"; // хочу дружить с ним (или подписан на него)
    String FRIEND_STATUS_NOTKNOW = "notknow"; // не знаю его

    int REQUEST_CODE_ASK_PERMISSIONS = 71;

    String PREFERENCE_VIEW_MODE = "list_view_mode";

    String FILTER_ONLINE = "online";
    String FILTER_OFFLINE = "offline";
    String FILTER_REQUEST = "request";
    String FILTER_FAVORITE = "favorite";

    String DATETIME_FORMAT = "dd.MM.yyyy hh:mm:ss";
    String DATE_FORMAT = "dd.MM.yyyy";


    String PREFERENCE_FIRST_RUN = "pref_first_run";
    String PREFERENCE_AUTOLOGIN = "pref_auto_login";
    String PREFERENCE_SOUND = "pref_sound";
    String PREFERENCE_VIBRO = "pref_vibro";
    String PREFERENCE_RECEIVE_PHOTO = "pref_receive_photo";

    String ROLE_ADMIN = "admin";
    String ROLE_MANAGER = "manager";
    String ROLE_MODER = "moder";
    String ROLE_MODER_CHAT = "moder_chat";
    String ROLE_MODER_HISTORY = "moder_history";
    String ROLE_USER = "user";

}
