package pro.devapp.kitoss.components;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.Toast;
import org.json.JSONObject;
import java.net.URISyntaxException;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.LoginActivity;
import pro.devapp.kitoss.interfaces.SocketEventsInterface;
import pro.devapp.kitoss.models.NotificationModel;
import pro.devapp.kitoss.models.PrivatChatModel;
import pro.devapp.kitoss.models.PublicChatModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.PushRegisterModel;
import pro.devapp.kitoss.models.UserModel;

/**
 * Базовый набор методов для экранов
 */
public class BaseActivity extends AppCompatActivity implements SocketEventsInterface {

    protected SoundPool soundPool;
    protected int message_sound;
    protected int message_chat_sound;
    protected int request_sound;
    protected int push_sound;
    public  String token;
    public UserModel userModel;

    private static String private_token = "";
    private static UserModel private_userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(BaseActivity.private_token == null || BaseActivity.private_token.equals("")){
            BaseActivity.private_token =((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
        }
        if(BaseActivity.private_userModel == null){
            updateMyProfileData();
        }
        token = BaseActivity.private_token;
        userModel = BaseActivity.private_userModel;
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        message_sound = soundPool.load(this, R.raw.message, 1);
        message_chat_sound = soundPool.load(this, R.raw.chat_message, 1);
        request_sound = soundPool.load(this, R.raw.request, 1);
        push_sound = soundPool.load(this, R.raw.push, 1);
    }

    /**
     * Обновление данных своего профиля
     */
    protected void updateMyProfileData(){
        BaseActivity.private_userModel = UserModel.getByToken(BaseActivity.private_token);
    }

    @Override
    public void onUserLeaveHint() {
        super.onUserLeaveHint();
        ((App)getApplication()).socketClose();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // если авторизированы подключаемся к соккету
        if(!token.equals("")){
            try {
                ((App)getApplication()).socketInit();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else {
            ((App)getApplication()).socketClose();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((App)getApplication()).setHandler(_handler);
    }

    /**
     * Кнопка назад и задать название
     * @param title
     */
    protected void initHomeBtn(String title){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(title);
    }

    /**
     * Нзавание
     * @param title
     */
    protected void setTitle(String title){
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Показать индикатор ожидания
     */
    public void showWait(){
        RelativeLayout wait_overlay = (RelativeLayout) findViewById(R.id.wait_overlay);
        if(wait_overlay !=null)
            wait_overlay.setVisibility(View.VISIBLE);
    }

    /**
     * Скрыть индикатор ожидания
     */
    public void hideWait(){
        RelativeLayout wait_overlay = (RelativeLayout) findViewById(R.id.wait_overlay);
        if(wait_overlay !=null)
            wait_overlay.setVisibility(View.GONE);
    }

    boolean logout_start = false;

    /**
     * Выход
     */
    public void logout(){
        if(logout_start){
            return;
        }
        logout_start = true;

        // удаляем токен для push на сервере
        String gtoken = ((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_GCM_TOKEN);
        PushRegisterModel.removeDevice(BaseActivity.this, token, gtoken);
        ((App)getApplication()).getAppPrefs().saveStringPreference(Const.PREFERENCE_SENT_TOKEN_TO_SERVER, "");

        // очищаем уведомления
        NotificationHelper.cancelNotification(this);
        ((App)getApplication()).socketClose();
        ((App)getApplication()).getAppPrefs().saveStringPreference(Const.PREFERENCE_TOKEN, "");

        // очищаем флаг автологина
        ((App)getApplication()).getAppPrefs().saveBooleanPreference(Const.PREFERENCE_AUTOLOGIN, true);

        // очищаем флаг первого запуска
        ((App)getApplication()).getAppPrefs().saveBooleanPreference(Const.PREFERENCE_FIRST_RUN, true);

        // удаляем приватные сообщения
        PrivatChatModel.deleteAll();

        token = "";
        BaseActivity.private_token = "";
        BaseActivity.private_userModel = null;
        userModel = null;

        Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
        intent.putExtra("after_logout", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    /**
     * Добавить в друзья
     * @param user_id
     */
    public void addToFriends(String user_id, Handler handler){
        String token = ((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
        PublicUserModel.addToMyFriend(token, user_id, handler);
    }

    /**
     * Удалить из друзей
     * @param user_id
     */
    public void removeFromFriends(String user_id, Handler handler){
        String token = ((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
        PublicUserModel.removeFromMyFriends(token, user_id, handler);
    }

    /**
     * Принять запрос дружбы
     * @param user_id
     */
    public void acceptFriendRequest(String user_id, Handler handler){
        String token = ((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
        PublicUserModel.acceptFriendRequest(token, user_id, handler);
    }

    /**
     * Отклонить запрос дружбы
     * @param user_id
     */
    public void declineFriendRequest(String user_id, Handler handler){
        String token = ((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
        PublicUserModel.declineFriendRequest(token, user_id, handler);
    }

    /**
     * Отменить мой запрос дружбы
     * @param user_id
     */
    public void removeMyFriendRequest(String user_id, Handler handler){
        String token = ((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
        PublicUserModel.removeMyFriendRequest(token, user_id, handler);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * Скрываем клавиатуру
     */
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Показываем клавиатуру
     */
    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.showSoftInput(view, 0);
    }

    /**
     * Воспроизведение звука
     * @param SoundId
     */
    public void playSound(int SoundId){
        if(!((App)getApplicationContext()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_SOUND)){
            return;
        }
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        switch( audioManager.getRingerMode() ){
            case AudioManager.RINGER_MODE_NORMAL:
                float curVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                float leftVolume = curVolume / maxVolume;
                float rightVolume = curVolume / maxVolume;
                soundPool.play(SoundId, leftVolume, rightVolume, 0, 0, 1.0f);
                break;
            case AudioManager.RINGER_MODE_SILENT:
                soundPool.play(SoundId, 0.1f, 0.1f, 0, 0, 1.0f);
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                String vs = Context.VIBRATOR_SERVICE;
                Vibrator mVibrator = (Vibrator)getSystemService(vs);
                if(mVibrator.hasVibrator()){
                    mVibrator.vibrate(500l);
                }
                break;
        }
    }

//    public void refreshPrivateDate(){
//        final String token = ((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
//        // свои сообщения
//        PrivatChatModel.loadMyMessages(token, (App)getApplication());
//        // public chats
//        PublicChatModel.loadHistoryMessages(token, (App)getApplication());
//        // обновление друзей
//        PublicUserModel.getFriends(0, 1000, token, new Handler(){
//
//        });
//    }

    public void share(){
        String token = ((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
        UserModel model = UserModel.getByToken(token);
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Kitoss");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Добавляйся ко мне в KITOSS. При регистрации вводи промокод: " +  model.promo_cod + " и получи 2 китса бесплатно www.kitoss.com");
        Intent intent = Intent.createChooser(sharingIntent, "Выберите");
        startActivity(intent);
    }

    final Handler _handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    Toast.makeText(BaseActivity.this, getString(R.string.info_access_dinieded), Toast.LENGTH_SHORT).show();
                    logout();
                    break;
                case Const.SOCKET_ACTION_NEW_FRIEND_REQUEST:
                    onSocketNewFriendsRequest((PublicUserModel) msg.obj);
                    break;
                case Const.SOCKET_ACTION_ACCEPT_REQUEST:
                    onSocketAcceptFriendRequest((PublicUserModel) msg.obj);
                    break;
                case Const.SOCKET_ACTION_DECLINE_REQUEST:
                    onSocketDeclineFriendRequest((PublicUserModel) msg.obj);
                    break;
                case Const.SOCKET_ACTION_CANCEL_FRIEND_REQUEST:
                    onSocketCancelFriendRequest((PublicUserModel) msg.obj);
                    break;
                case Const.SOCKET_ACTION_REMOVE_FRIEND:
                    onSocketRemoveFromFriends((PublicUserModel) msg.obj);
                    break;
                case Const.SOCKET_ACTION_NEW_MESSAGE:
                    onSocketNewMessage((PrivatChatModel) msg.obj);
                    break;
                case Const.SOCKET_ACTION_NEW_PUBLIC_MESSAGE:
                    onSocketNewPublicMessage((PublicChatModel) msg.obj);
                    break;
                case Const.SOCKET_ACTION_REMOVE_PUBLIC_MESSAGE:
                    onSocketRemovePublicMessage((PublicChatModel) msg.obj);
                    break;
                case Const.SOCKET_ACTION_TYPING:
                    onSocketTyping((JSONObject) msg.obj);
                    break;
                case Const.SOCKET_ACTION_UPDATE_BALANCE:
                    onSocketUpdateBalance((UserModel) msg.obj);
                    break;
                case Const.SOCKET_ACTION_NEW_EVENT:
                    onSocketNewEvent();
                    break;
            }
        }
    };

    /**
     * Новая заявка в друзья
     */
    @Override
    public void onSocketNewFriendsRequest(PublicUserModel publicUserModel) {
        NotificationHelper.sendNotification(getString(R.string.notification_new_friend_request), getString(R.string.notification_new_friend_request_text, publicUserModel.first_name), getApplicationContext(), NotificationModel.TYPE_FRIENDS);
    }

    /**
     * Вас удалили из друщей
     */
    @Override
    public void onSocketRemoveFromFriends(PublicUserModel publicUserModel) {
        NotificationHelper.sendNotification(getString(R.string.notification_friend_removes), getString(R.string.notification_friend_removes_text, publicUserModel.first_name), getApplicationContext(), NotificationModel.TYPE_FRIENDS);
    }

    /**
     * Пользователь отменил свою заявку на дружбу
     */
    @Override
    public void onSocketCancelFriendRequest(PublicUserModel publicUserModel) {
        NotificationHelper.sendNotification(getString(R.string.notification_friend_cancel), getString(R.string.notification_friend_cancel_text, publicUserModel.first_name), getApplicationContext(), NotificationModel.TYPE_FRIENDS);
    }

    /**
     * Вашу заяку отклонили
     */
    @Override
    public void onSocketDeclineFriendRequest(PublicUserModel publicUserModel) {
        NotificationHelper.sendNotification(getString(R.string.notification_decline_friend_request), getString(R.string.notification_decline_friend_request_text, publicUserModel.first_name), getApplicationContext(), NotificationModel.TYPE_FRIENDS);
    }

    /**
     * Вашу заявку приняли
     */
    @Override
    public void onSocketAcceptFriendRequest(PublicUserModel publicUserModel) {
        NotificationHelper.sendNotification(getString(R.string.notification_accept_friend_request), getString(R.string.notification_accept_friend_request_text, publicUserModel.first_name), getApplicationContext(), NotificationModel.TYPE_FRIENDS);
    }

    /**
     * Новое сообщение
     */
    @Override
    public void onSocketNewMessage(PrivatChatModel privatChatModel) {
        if(privatChatModel == null){
            return;
        }
        Log.d(BaseActivity.class.getName(), "onNewMessage");
        // если это моё сообщение, то отмечаем его прочитанным
        if(privatChatModel.sender_id.equals(userModel.id)){
            privatChatModel.is_read = 1;
            privatChatModel.insert();
            return;
        }

        PublicUserModel publicUserModel = PublicUserModel.getById(privatChatModel.sender_id);

        if(userModel != null && publicUserModel != null && privatChatModel.is_read < 1 && !privatChatModel.sender_id.equals(userModel.id)){
            String username = (publicUserModel == null) ? "" : publicUserModel.first_name;
            NotificationHelper.cancelNotification(NotificationModel.TYPE_MESSAGES, this);
            NotificationHelper.sendNotification(getString(R.string.notification_new_message), getString(R.string.notification_new_message_text, username), getApplicationContext(), NotificationModel.TYPE_MESSAGES);
        }
    }

    /**
     * Пополнение баланса
     */
    @Override
    public void onSocketUpdateBalance(UserModel userModel) {
        this.userModel = userModel;
    }

    /**
     * Новый подарок
     */
    @Override
    public void onSocketNewGift() {

    }

    /**
     * Новое публичное сообщение
     *
     * @param publicChatModel
     */
    @Override
    public void onSocketNewPublicMessage(PublicChatModel publicChatModel) {

    }

    /**
     * Удалено публичное сообщение
     */
    @Override
    public void onSocketRemovePublicMessage(PublicChatModel publicChatModel) {

    }

    /**
     * Печатает
     */
    @Override
    public void onSocketTyping(JSONObject jsonObject) {

    }

    /**
     * Новое событие в ленте
     */
    @Override
    public void onSocketNewEvent() {

    }
}
