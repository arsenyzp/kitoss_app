package pro.devapp.kitoss.components;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.util.Calendar;

import pro.devapp.kitoss.R;

/**
 * Функции для работы с данными пльзователей
 */
public class UserHelper {
    public static int getOld(String birth_date){
        if(birth_date == null){
            return 100;
        }

        String[] part_date = birth_date.split("[.]");
        if(part_date.length != 3){
            return 101;
        }
        Calendar c = Calendar.getInstance();
        int res = c.get(Calendar.YEAR) - Integer.valueOf(part_date[2]);
        if(res < 0){
            res = 0;
        }
        return res;
    }

    public static String getGenderShortName(String gender, Context context){
        if(gender.equals("male")){
            return context.getString(R.string.label_gender_male);
        } else if(gender.equals("female")){
            return context.getString(R.string.label_gender_female);
        }
        return "-";
    }

    public static Drawable getGenderDrawable(String gender, Context context){
        if(gender.equals("male")){
            return context.getResources().getDrawable(R.drawable.i_g_male);
        } else {
            return context.getResources().getDrawable(R.drawable.i_g_female);
        }
    }
}
