package pro.devapp.kitoss.components;

/**
 * Названия для дат
 */
public class RuDateNames {

    public static int plural(int n){
        return n%10==1 && n%100!=11 ? 0 : (n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
    }

    public static String getDays(int days){
        String[] names = new String[]{
                "день",
                "дня",
                "дней",
                "дней",
                "дней",
                "дней",
        };
        return names[plural(days)];
    }

    public static String getMonths(int days){
        String[] names = new String[]{
                "месяц",
                "месяца",
                "месяцев",
                "месяцев",
                "месяцев",
                "месяцев",
        };
        return names[plural(days)];
    }

    public static String getYears(int days){
        String[] names = new String[]{
                "год",
                "года",
                "лет",
                "лет",
                "лет",
                "лет",
        };
        return names[plural(days)];
    }
}
