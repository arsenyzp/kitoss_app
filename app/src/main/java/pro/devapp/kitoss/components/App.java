package pro.devapp.kitoss.components;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.multidex.MultiDex;
import android.util.Log;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vk.sdk.VKSdk;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Modifier;
import java.net.URISyntaxException;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import pro.devapp.kitoss.models.EventsModel;
import pro.devapp.kitoss.models.PrivatChatModel;
import pro.devapp.kitoss.models.PublicChatModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.ResponseModel;
import pro.devapp.kitoss.models.UserModel;

/**
 *
 */
public class App extends Application {

    /**
     * Object for accessing application preferences
     */
    private ApplicationPreferences appPrefs;

    private static Socket socket;

    private Handler handler;

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());
        VKSdk.initialize(this);
        RealmConfiguration config = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(config);
        Realm.compactRealm(config);
        appPrefs = new ApplicationPreferences(this);

    }


    public void clear(){
        RealmConfiguration config = new RealmConfiguration.Builder(this).build();
        Realm.compactRealm(config);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void setHandler(Handler handler){
        this.handler = handler;
    }

    public Handler getHandler(){
        return this.handler;
    }

    public ApplicationPreferences getAppPrefs() {
        return appPrefs;
    }

    public void socketInit() throws URISyntaxException {
        if(socket != null && socket.connected()){
            return;
        }
        socket = IO.socket(Const.SOCKET_URL);
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "EVENT_CONNECT");
                JSONObject obj = new JSONObject();
                try {
                    obj.put("token", getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN));
                    Log.d(App.class.getName(), obj.toString());
                    if(socket != null){
                        socket.emit("subscribe", obj);
                    }
                } catch (JSONException e) {
                    Log.d(App.class.getName(), e.toString());
                }
            }
        });

        /**
         * Успешное Подключение
         */
        socket.on("auth", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "auth");
                Log.d(App.class.getName(), args[0].toString());
                if(handler != null)
                    handler.obtainMessage(1, Const.SOCKET_ACTION_AUTH, 1, null).sendToTarget();
            }
        });

        /**
         * Ошибка авторизации
         */
        socket.on("fail", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "EVENT FAIL");
                if(handler != null)
                    handler.obtainMessage(1, Const.STATE_ACCESS_DINEDED, 1, null).sendToTarget();
            }

        });

        /**
         * Закрытие подключения
         */
        socket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "EVENT_DISCONNECT");
            }

        });

        /**
         * Событие нового запроса на добавления в друзья
         */
        socket.on("add_to_friends", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "add_to_friends");
                Log.d(App.class.getName(), args[0].toString());
                try {
                    ResponseModel responseModel = getData(ResponseModel.class, new JSONObject(args[0].toString()));
                    PublicUserModel publicUserModel = responseModel.getData(PublicUserModel.class);
                    publicUserModel.update();
                    if(handler != null)
                        handler.obtainMessage(1, Const.SOCKET_ACTION_NEW_FRIEND_REQUEST, 1, publicUserModel).sendToTarget();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * Событие удаления из друзей
         */
        socket.on("remove_friends", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "remove_from_friends");
                Log.d(App.class.getName(), args[0].toString());
                try {
                    ResponseModel responseModel = getData(ResponseModel.class, new JSONObject(args[0].toString()));
                    PublicUserModel publicUserModel = responseModel.getData(PublicUserModel.class);
                    publicUserModel.update();
                    if(handler != null)
                        handler.obtainMessage(1, Const.SOCKET_ACTION_REMOVE_FRIEND, 1, publicUserModel).sendToTarget();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * Событие отмены подачи заявки
         */
        socket.on("cancel_request", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "cancel_request");
                Log.d(App.class.getName(), args[0].toString());
                try {
                    ResponseModel responseModel = getData(ResponseModel.class, new JSONObject(args[0].toString()));
                    PublicUserModel publicUserModel = responseModel.getData(PublicUserModel.class);
                    publicUserModel.update();
                    if(handler != null)
                        handler.obtainMessage(1, Const.SOCKET_ACTION_CANCEL_FRIEND_REQUEST, 1, publicUserModel).sendToTarget();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * Событие принятия запроса добавления в друзья
         */
        socket.on("accept_request", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "accept_request_friends");
                Log.d(App.class.getName(), args[0].toString());
                try {
                    ResponseModel responseModel = getData(ResponseModel.class, new JSONObject(args[0].toString()));
                    PublicUserModel publicUserModel = responseModel.getData(PublicUserModel.class);
                    publicUserModel.update();
                    if(handler != null)
                        handler.obtainMessage(1, Const.SOCKET_ACTION_ACCEPT_REQUEST, 1, publicUserModel).sendToTarget();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * Событие отклонение заявки добавления в друзья
         */
        socket.on("decline_request", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "decline_request_friends");
                Log.d(App.class.getName(), args[0].toString());
                try {
                    ResponseModel responseModel = getData(ResponseModel.class, new JSONObject(args[0].toString()));
                    PublicUserModel publicUserModel = responseModel.getData(PublicUserModel.class);
                    publicUserModel.update();
                    if(handler != null)
                        handler.obtainMessage(1, Const.SOCKET_ACTION_DECLINE_REQUEST, 1, publicUserModel).sendToTarget();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * Событие нового сообщения
         */
        socket.on("new_message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "new_message");
                Log.d(App.class.getName(), args[0].toString());
                try {
                    ResponseModel responseModel = getData(ResponseModel.class, new JSONObject(args[0].toString()));
                    PrivatChatModel privatChatModel = responseModel.getData(PrivatChatModel.class);

                    // отправляем уведомление, только если этого сообщения небыло в базе
                    Realm realm = Realm.getDefaultInstance();
                    PrivatChatModel model = realm.where(PrivatChatModel.class).equalTo("mid", privatChatModel.mid).or().equalTo("uiid", privatChatModel.uiid).findFirst();

                    if(model != null){
                        realm.beginTransaction();
                        model.time = privatChatModel.time;
                        model.mid = privatChatModel.mid;
                        model.is_delivery = 1;
                        realm.insertOrUpdate(model);
                        realm.commitTransaction();
                        realm.close();
                    } else {
                        PublicUserModel publicUserModel = PublicUserModel.getById(privatChatModel.publicUserModel.id);
                        if(publicUserModel == null){
                            privatChatModel.publicUserModel.update();
                        }

                        realm.beginTransaction();
                        privatChatModel.publicUserModel = PublicUserModel.getById(privatChatModel.publicUserModel.id);
                        privatChatModel.is_delivery = 1;

                        realm.insertOrUpdate(privatChatModel);
                        realm.commitTransaction();

                        if(handler != null)
                            handler.obtainMessage(1, Const.SOCKET_ACTION_NEW_MESSAGE, 1, privatChatModel).sendToTarget();

                        realm.close();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * Событие нового публичного сообщения
         */
        socket.on("new_public_message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "new_public_message");
                Log.d(App.class.getName(), args[0].toString());
                try {
                    ResponseModel responseModel = getData(ResponseModel.class, new JSONObject(args[0].toString()));
                    PublicChatModel publicChatModel = responseModel.getData(PublicChatModel.class);
                    publicChatModel.publicUserModel.update();
                    publicChatModel.publicUserModel = PublicUserModel.getById(publicChatModel.publicUserModel.id);
                    publicChatModel.is_delivery = 1;
                    if(handler != null)
                        handler.obtainMessage(1, Const.SOCKET_ACTION_NEW_PUBLIC_MESSAGE, 1, publicChatModel).sendToTarget();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * Событие опонент печатает в приват чате
         */
        socket.on("typing", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "typing");
                Log.d(App.class.getName(), args[0].toString());
                    try {
                        if(handler != null)
                            handler.obtainMessage(1, Const.SOCKET_ACTION_TYPING, 1, new JSONObject(args[0].toString())).sendToTarget();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
            }
        });

        /**
         * Событие новые данные пользователей
         */
        socket.on("getUserInfo", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "getUserInfo");
                Log.d(App.class.getName(), args[0].toString());
                try {
                    ResponseModel responseModel = getData(ResponseModel.class, new JSONObject(args[0].toString()));
                    PublicUserModel.UserList userList = responseModel.getData(PublicUserModel.UserList.class);
                    if (userList.users.size() > 0) {
                        for(PublicUserModel publicUserModel : userList.users){
                            publicUserModel.update();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * Событие изменение баланса
         */
        socket.on("update_balance", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "update_balance");
                Log.d(App.class.getName(), args[0].toString());
                try {
                    ResponseModel responseModel = getData(ResponseModel.class, new JSONObject(args[0].toString()));
                    UserModel userModel = responseModel.getData(UserModel.class);
                    userModel.update();
                    if(handler != null)
                        handler.obtainMessage(1, Const.SOCKET_ACTION_UPDATE_BALANCE, 1, userModel).sendToTarget();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * Событие удалениея сообщения модероатором
         */
        socket.on("remove_public_message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "remove_public_message");
                Log.d(App.class.getName(), args[0].toString());
                try {
                    ResponseModel responseModel = getData(ResponseModel.class, new JSONObject(args[0].toString()));
                    PublicChatModel publicChatModel = responseModel.getData(PublicChatModel.class);
                    if(handler != null)
                        handler.obtainMessage(1, Const.SOCKET_ACTION_REMOVE_PUBLIC_MESSAGE, 1, publicChatModel).sendToTarget();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * Событие новое событие в ленте
         */
        socket.on("new_event", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "new_event");
                Log.d(App.class.getName(), args[0].toString());
                try {
                    ResponseModel responseModel = getData(ResponseModel.class, new JSONObject(args[0].toString()));
                    EventsModel eventsModel = responseModel.getData(EventsModel.class);
                    if (eventsModel != null) {
                        eventsModel.user = PublicUserModel.getById(eventsModel.user.id);
                        eventsModel.owner = UserModel.getByToken(getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN)).id;
                        eventsModel.insert();
                        if(handler != null)
                            handler.obtainMessage(1, Const.SOCKET_ACTION_NEW_EVENT, 1, args[0].toString()).sendToTarget();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * Ошибка в работе соккета
         */
        socket.on("socket_error", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "socket_error");
                socket.close();
                socket.connect();
            }
        });

        /**
         * Ошибка авторизации
         */
        socket.on("auth_error", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(App.class.getName(), "auth_error");
                socket.close();
                if(handler != null)
                    handler.obtainMessage(1, Const.STATE_ACCESS_DINEDED, 1, null).sendToTarget();
            }
        });

        socket.connect();
    }

    public static <T> T getData(Class<T> classOfT, JSONObject data) {
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC).create();
        return gson.fromJson(data.toString(), classOfT);
    }

    /**
     * Проверка подключения к интернету
     * @return
     */
    public boolean checkConnection(){
        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork.isConnectedOrConnecting();
    }

    public void socketClose(){
        if(socket != null){
            socket.disconnect();
            socket.close();
            socket = null;
        }
    }

    public void SocketEmit(String event, JSONObject args){
        Log.d("SocketApp", "EMIT " + event);
        if(socket == null || !socket.connected()){
            Log.d("SocketApp", "Reconnect");
            try {
                socketInit();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        socket.emit(event, args);
    }
}
