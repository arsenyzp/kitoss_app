package pro.devapp.kitoss.components;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Обработка удаления текста в EditText со смайлами
 */
public class SmileTextWatcher implements TextWatcher{
    int old_length = 0;
    int selectionCursor = 0;
    private EditText message;

    int smile_size = 25;
    Activity activity;

    public SmileTextWatcher(EditText message, int smile_size, Activity activity){
        this.message = message;
        this.smile_size = smile_size;
        this.activity = activity;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        old_length = charSequence.length();
        selectionCursor = this.message.getSelectionStart();
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        // только в случае удаления
        if(old_length > editable.length()){
            // только если длиннее чем смайл
            if(editable.length() < 4){
                return;
            }
            int start = message.getSelectionStart();
            int last_smile_start = editable.subSequence(0, start).toString().lastIndexOf(" :smile");
            int last_smile_end = editable.subSequence(0, start).toString().lastIndexOf(": ");
            if(last_smile_start >= 0 && last_smile_end < last_smile_start){
                CharSequence left_str = editable.subSequence(0, last_smile_start+1);
                CharSequence right_str = "";
                if(start < (editable.length() -1)){
                    right_str = editable.subSequence(start, editable.length());
                }
                String new_str = left_str.toString() + right_str.toString();
                message.setSelection(left_str.length());
                Smiles.setSmiles(new_str, message, smile_size, activity);
            }
        }
    }
}
