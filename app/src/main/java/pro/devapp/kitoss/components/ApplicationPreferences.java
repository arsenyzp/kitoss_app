package pro.devapp.kitoss.components;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * ApplicationPreferences
 */
public class ApplicationPreferences {

    public static final String CURRENT_VERSION_CODE = "currentVersionCode";

    public static final String SID = "ssid";
    public static final String USER_ID = "suser_id";

    /**
     * preference name
     */
    public static final String PREFS_NAME = "demoAppPrefs";

    /**
     * used for modifying values in a SharedPreferences prefs
     */
    private SharedPreferences.Editor prefsEditor;

    /**
     * reference to preference
     */
    private SharedPreferences prefs;

    /**
     * the context
     */
    private Context context;

    public ApplicationPreferences(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        prefsEditor = prefs.edit();
    }

    public int getIntPreference(String key) {
        return prefs.getInt(key, 0);
    }

    public String getStringPreference(String key) {
        return prefs.getString(key, "");
    }

    public boolean getBooleanPreference(String key) {
        return prefs.getBoolean(key, false);
    }

    public boolean getBooleanPreference(String key, Boolean def) {
        return prefs.getBoolean(key, def);
    }

    public void setCurrentVersionCode(int versionCode) {
        prefsEditor.putInt(CURRENT_VERSION_CODE, versionCode);
        prefsEditor.commit();
    }

    public void saveStringPreference(String key, String value){
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public void saveBooleanPreference(String key, boolean value) {
        prefsEditor.putBoolean(key, value);
        prefsEditor.commit();
    }

    public void saveLongPreference(String key, long value) {
        prefsEditor.putLong(key, value);
        prefsEditor.commit();
    }

    public long getLongPreference(String key) {
        return prefs.getLong(key, 0l);
    }

    public void saveDoublePreference(String key, double value) {
        prefsEditor.putString(key, String.valueOf(value));
        prefsEditor.commit();
    }

    public double getDoublePreference(String key) {
        return Double.valueOf(prefs.getString(key, "0"));
    }

    public void saveInt(String key, int value){
        prefsEditor.putInt(key, value);
        prefsEditor.commit();
    }
}
