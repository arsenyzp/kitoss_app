package pro.devapp.kitoss.components;

import io.realm.Realm;

/**
 * Singelton for Realm
 */
public class RealmInstance {
    public static Realm realm;

    public static Realm getRealm(){
        //return Realm.getDefaultInstance();
        if(realm == null){
            realm = Realm.getDefaultInstance();
        }
        return realm;
    }
}
