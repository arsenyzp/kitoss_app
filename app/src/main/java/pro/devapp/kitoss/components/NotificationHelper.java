package pro.devapp.kitoss.components;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import java.util.List;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.AppActivity;
import pro.devapp.kitoss.models.NotificationModel;

/**
 * Notification
 */
public class NotificationHelper {

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    public static void sendNotificationGCM(String title, String message, Context ctx, String type) {
        Intent intent = new Intent(ctx, AppActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(ctx)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setLights(Color.RED, 3000, 3000)
                .setTicker(title)
                .setContentIntent(pendingIntent);

        //If enabled sound
        if(((App)ctx.getApplicationContext()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_SOUND)){
            notificationBuilder.setSound(sound);
        } else {
            notificationBuilder.setSound(null);
        }

        //If enabled vibration
        if(((App)ctx.getApplicationContext()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_VIBRO)){
            notificationBuilder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
        }

        NotificationManager notificationManager =
                (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationModel notificationModel = new NotificationModel();
        notificationModel.type = type;
        notificationModel.id = (int)System.currentTimeMillis();
        notificationModel.insert();

        notificationManager.notify(notificationModel.id, notificationBuilder.build());
    }

    public static void sendNotification(String title, String message, Context ctx, String type) {
        Intent intent = new Intent(ctx, AppActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        //Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Uri sound = Uri.parse("android.resource://"+ctx.getPackageName()+ "/" + R.raw.request);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(ctx)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setLights(Color.RED, 3000, 3000)
                .setTicker(title)
                .setContentIntent(pendingIntent);

        //If enabled sound
        if(((App)ctx.getApplicationContext()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_SOUND)){
            notificationBuilder.setSound(sound);
        } else {
            notificationBuilder.setSound(null);
        }

        //If enabled vibration
        if(((App)ctx.getApplicationContext()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_VIBRO)){
            notificationBuilder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
        }

        NotificationManager notificationManager =
                (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationModel notificationModel = new NotificationModel();
        notificationModel.type = type;
        notificationModel.id = (int)System.currentTimeMillis();
        notificationModel.insert();

        notificationManager.notify(notificationModel.id, notificationBuilder.build());
    }

    public static void cancelNotification(final String type, final Context ctx){
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                List<NotificationModel> models = NotificationModel.getAll(type);
                NotificationManager notificationManager =
                        (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
                for (NotificationModel notificationModel : models){
                    notificationManager.cancel(notificationModel.id);
                    notificationModel.delete();
                }
                return null;
            }
        };
        task.execute();
    }

    public static void cancelNotification(final Context ctx){
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                List<NotificationModel> models = NotificationModel.getAll();
                NotificationManager notificationManager =
                        (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
                for (NotificationModel notificationModel : models){
                    notificationManager.cancel(notificationModel.id);
                    notificationModel.delete();
                }
                return null;
            }
        };
        task.execute();
    }

}
