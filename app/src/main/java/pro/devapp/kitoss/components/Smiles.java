package pro.devapp.kitoss.components;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.models.SmilesHistoryModel;

/**
 * Smiles
 */
public class Smiles {
    public static String SmileToText(int Smile){
        switch (Smile){
            case R.id.smile1:
                SmilesHistoryModel.insert(Smile);
                return " :smile1: ";
            case R.id.smile2:
                SmilesHistoryModel.insert(Smile);
                return " :smile2: ";
            case R.id.smile3:
                SmilesHistoryModel.insert(Smile);
                return " :smile3: ";
            case R.id.smile4:
                SmilesHistoryModel.insert(Smile);
                return " :smile4: ";
            case R.id.smile5:
                SmilesHistoryModel.insert(Smile);
                return " :smile5: ";
            case R.id.smile6:
                SmilesHistoryModel.insert(Smile);
                return " :smile6: ";
            case R.id.smile7:
                SmilesHistoryModel.insert(Smile);
                return " :smile7: ";
            case R.id.smile8:
                SmilesHistoryModel.insert(Smile);
                return " :smile8: ";
            case R.id.smile9:
                SmilesHistoryModel.insert(Smile);
                return " :smile9: ";
            case R.id.smile10:
                SmilesHistoryModel.insert(Smile);
                return " :smile10: ";
            case R.id.smile11:
                SmilesHistoryModel.insert(Smile);
                return " :smile11: ";
            case R.id.smile12:
                SmilesHistoryModel.insert(Smile);
                return " :smile12: ";
            case R.id.smile13:
                SmilesHistoryModel.insert(Smile);
                return " :smile13: ";
            case R.id.smile14:
                SmilesHistoryModel.insert(Smile);
                return " :smile14: ";
            case R.id.smile15:
                SmilesHistoryModel.insert(Smile);
                return " :smile15: ";
            case R.id.smile16:
                SmilesHistoryModel.insert(Smile);
                return " :smile16: ";
            case R.id.smile17:
                SmilesHistoryModel.insert(Smile);
                return " :smile17: ";
            case R.id.smile18:
                SmilesHistoryModel.insert(Smile);
                return " :smile18: ";
            case R.id.smile19:
                SmilesHistoryModel.insert(Smile);
                return " :smile19: ";
            case R.id.smile20:
                SmilesHistoryModel.insert(Smile);
                return " :smile20: ";
            case R.id.smile21:
                SmilesHistoryModel.insert(Smile);
                return " :smile21: ";
            case R.id.smile22:
                SmilesHistoryModel.insert(Smile);
                return " :smile22: ";
            case R.id.smile23:
                SmilesHistoryModel.insert(Smile);
                return " :smile23: ";
            case R.id.smile24:
                SmilesHistoryModel.insert(Smile);
                return " :smile24: ";
            case R.id.smile25:
                SmilesHistoryModel.insert(Smile);
                return " :smile25: ";
            case R.id.smile26:
                SmilesHistoryModel.insert(Smile);
                return " :smile26: ";
            case R.id.smile27:
                SmilesHistoryModel.insert(Smile);
                return " :smile27: ";
            case R.id.smile28:
                SmilesHistoryModel.insert(Smile);
                return " :smile28: ";
            case R.id.smile29:
                SmilesHistoryModel.insert(Smile);
                return " :smile29: ";
            case R.id.smile30:
                SmilesHistoryModel.insert(Smile);
                return " :smile30: ";
            case R.id.smile31:
                SmilesHistoryModel.insert(Smile);
                return " :smile31: ";
            case R.id.smile32:
                SmilesHistoryModel.insert(Smile);
                return " :smile32: ";
            case R.id.smile33:
                SmilesHistoryModel.insert(Smile);
                return " :smile33: ";
            case R.id.smile34:
                SmilesHistoryModel.insert(Smile);
                return " :smile34: ";
            case R.id.smile35:
                SmilesHistoryModel.insert(Smile);
                return " :smile35: ";
            case R.id.smile36:
                SmilesHistoryModel.insert(Smile);
                return " :smile36: ";
            case R.id.smile37:
                SmilesHistoryModel.insert(Smile);
                return " :smile37: ";
            case R.id.smile38:
                SmilesHistoryModel.insert(Smile);
                return " :smile38: ";
            case R.id.smile39:
                SmilesHistoryModel.insert(Smile);
                return " :smile39: ";
            case R.id.smile40:
                SmilesHistoryModel.insert(Smile);
                return " :smile40: ";
            case R.id.smile41:
                SmilesHistoryModel.insert(Smile);
                return " :smile41: ";
            case R.id.smile42:
                SmilesHistoryModel.insert(Smile);
                return " :smile42: ";
            case R.id.smile43:
                SmilesHistoryModel.insert(Smile);
                return " :smile43: ";
            case R.id.smile44:
                SmilesHistoryModel.insert(Smile);
                return " :smile44: ";
            case R.id.smile45:
                SmilesHistoryModel.insert(Smile);
                return " :smile45: ";
            case R.id.smile46:
                SmilesHistoryModel.insert(Smile);
                return " :smile46: ";
            case R.id.smile47:
                SmilesHistoryModel.insert(Smile);
                return " :smile47: ";
            case R.id.smile48:
                SmilesHistoryModel.insert(Smile);
                return " :smile48: ";
            case R.id.smile49:
                SmilesHistoryModel.insert(Smile);
                return " :smile49: ";
            case R.id.smile50:
                SmilesHistoryModel.insert(Smile);
                return " :smile50: ";
            case R.id.smile51:
                SmilesHistoryModel.insert(Smile);
                return " :smile51: ";
            case R.id.smile52:
                SmilesHistoryModel.insert(Smile);
                return " :smile52: ";
//            case R.id.smile53:
//                SmilesHistoryModel.insert(Smile);
//                return " :smile53: ";
//            case R.id.smile54:
//                SmilesHistoryModel.insert(Smile);
//                return " :smile54: ";
//            case R.id.smile55:
//                SmilesHistoryModel.insert(Smile);
//                return " :smile55: ";
//            case R.id.smile56:
//                SmilesHistoryModel.insert(Smile);
//                return " :smile56: ";
//            case R.id.smile57:
//                SmilesHistoryModel.insert(Smile);
//                return " :smile57: ";
//            case R.id.smile58:
//                SmilesHistoryModel.insert(Smile);
//                return " :smile58: ";
//            case R.id.smile59:
//                SmilesHistoryModel.insert(Smile);
//                return " :smile59: ";
//            case R.id.smile60:
//                SmilesHistoryModel.insert(Smile);
//                return " :smile60: ";
            default:
                List<SmilesHistoryModel> smilesHistoryList = SmilesHistoryModel.getSmiles();
                switch (Smile){
                    case R.id.a_smile1:
                        return SmileToText(smilesHistoryList.get(0).smile_id);
                    case R.id.a_smile2:
                        return SmileToText(smilesHistoryList.get(1).smile_id);
                    case R.id.a_smile3:
                        return SmileToText(smilesHistoryList.get(2).smile_id);
                    case R.id.a_smile4:
                        return SmileToText(smilesHistoryList.get(3).smile_id);
                    case R.id.a_smile5:
                        return SmileToText(smilesHistoryList.get(4).smile_id);
                }
                return "";
        }
    }

    public static final HashMap<String, Integer> smileicons= new HashMap<String, Integer>();
    static {
        smileicons.put(":smile1:", R.drawable.s1);
        smileicons.put(":smile2:", R.drawable.s2);
        smileicons.put(":smile3:", R.drawable.s3);
        smileicons.put(":smile4:", R.drawable.s4);
        smileicons.put(":smile5:", R.drawable.s5);
        smileicons.put(":smile6:", R.drawable.s6);
        smileicons.put(":smile7:", R.drawable.s7);
        smileicons.put(":smile8:", R.drawable.s8);
        smileicons.put(":smile9:", R.drawable.s9);
        smileicons.put(":smile10:", R.drawable.s10);
        smileicons.put(":smile11:", R.drawable.s11);
        smileicons.put(":smile12:", R.drawable.s12);
        smileicons.put(":smile13:", R.drawable.s13);
        smileicons.put(":smile14:", R.drawable.s14);
        smileicons.put(":smile15:", R.drawable.s15);
        smileicons.put(":smile16:", R.drawable.s16);
        smileicons.put(":smile17:", R.drawable.s17);
        smileicons.put(":smile18:", R.drawable.s18);
        smileicons.put(":smile19:", R.drawable.s19);
        smileicons.put(":smile20:", R.drawable.s20);
        smileicons.put(":smile21:", R.drawable.s21);
        smileicons.put(":smile22:", R.drawable.s22);
        smileicons.put(":smile23:", R.drawable.s23);
        smileicons.put(":smile24:", R.drawable.s24);
        smileicons.put(":smile25:", R.drawable.s25);
        smileicons.put(":smile26:", R.drawable.s26);
        smileicons.put(":smile27:", R.drawable.s27);
        smileicons.put(":smile28:", R.drawable.s28);
        smileicons.put(":smile29:", R.drawable.s29);
        smileicons.put(":smile30:", R.drawable.s30);
        smileicons.put(":smile31:", R.drawable.s31);
        smileicons.put(":smile32:", R.drawable.s32);
        smileicons.put(":smile33:", R.drawable.s33);
        smileicons.put(":smile34:", R.drawable.s34);
        smileicons.put(":smile35:", R.drawable.s35);
        smileicons.put(":smile36:", R.drawable.s36);
        smileicons.put(":smile37:", R.drawable.s37);
        smileicons.put(":smile38:", R.drawable.s38);
        smileicons.put(":smile39:", R.drawable.s39);
        smileicons.put(":smile40:", R.drawable.s40);
        smileicons.put(":smile41:", R.drawable.s41);
        smileicons.put(":smile42:", R.drawable.s42);
        smileicons.put(":smile43:", R.drawable.s43);
        smileicons.put(":smile44:", R.drawable.s44);
        smileicons.put(":smile45:", R.drawable.s45);
        smileicons.put(":smile46:", R.drawable.s46);
        smileicons.put(":smile47:", R.drawable.s47);
        smileicons.put(":smile48:", R.drawable.s48);
        smileicons.put(":smile49:", R.drawable.s49);
        smileicons.put(":smile50:", R.drawable.s50);
        smileicons.put(":smile51:", R.drawable.s51);
        smileicons.put(":smile52:", R.drawable.s52);
//        smileicons.put(":smile53:", R.drawable.s53);
//        smileicons.put(":smile54:", R.drawable.s54);
//        smileicons.put(":smile55:", R.drawable.s55);
//        smileicons.put(":smile56:", R.drawable.s56);
//        smileicons.put(":smile57:", R.drawable.s57);
//        smileicons.put(":smile58:", R.drawable.s58);
//        smileicons.put(":smile59:", R.drawable.s59);
//        smileicons.put(":smile60:", R.drawable.s60);
    }

    public static int TextToSmile(String Smile){
        switch (Smile){
            case ":smile1:":
                return R.drawable.s1;
            case ":smile2:":
                return R.drawable.s2;
            case ":smile3:":
                return R.drawable.s3;
            case ":smile4:":
                return R.drawable.s4;
            case ":smile5:":
                return R.drawable.s5;
            case ":smile6:":
                return R.drawable.s6;
            case ":smile7:":
                return R.drawable.s7;
            case ":smile8:":
                return R.drawable.s8;
            case ":smile9:":
                return R.drawable.s9;
            case ":smile10:":
                return R.drawable.s10;
            default:
                return R.drawable.s1;
        }
    }

    /**
     * Replace text to smiles in TextView
     * @param text
     * @param textView
     * @param smile_size
     * @param activity
     */
    public static void setSmiles(String text, TextView textView, int smile_size, Activity activity){

        int index;
        int reduced_length = 0;
        SpannableStringBuilder builder = new SpannableStringBuilder(text);
        if(!text.contains(":smile")){
            textView.setText(builder);
            return;
        }
        for (index = 0; index < builder.length(); index++) {
            for (Map.Entry<String, Integer> entry : Smiles.smileicons.entrySet()) {
                int length = entry.getKey().length();
                if (index + length > builder.length())
                    continue;
                if (builder.subSequence(index, index + length).toString()
                        .equals(entry.getKey())) {

                    Drawable drawable = activity.getResources().getDrawable(entry.getValue());
                    float w = (smile_size * drawable.getIntrinsicWidth())/drawable.getIntrinsicHeight();

                    drawable.setBounds(0, 0, (int)w, smile_size);

                    builder.setSpan(new ImageSpan(drawable), index, index + length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    reduced_length += length - 1;
                    break;
                }
            }
        }
        textView.setText(builder);
    }

    /**
     * Replace text to smiles in EditText
     * @param text
     * @param textView
     * @param smile_size
     * @param activity
     */
    public static void setSmiles(String text, EditText textView, int smile_size, Activity activity){

        int index;
        int reduced_length = 0;
        int selectionCursor = textView.getSelectionStart();
        SpannableStringBuilder builder = new SpannableStringBuilder(text);
        if(!text.contains(":smile")){
            textView.setText(builder);
            textView.setSelection(selectionCursor);
            return;
        }
        for (index = 0; index < builder.length(); index++) {
            for (Map.Entry<String, Integer> entry : Smiles.smileicons.entrySet()) {
                int length = entry.getKey().length();
                if (index + length > builder.length())
                    continue;
                if (builder.subSequence(index, index + length).toString()
                        .equals(entry.getKey())) {

                    Drawable drawable = activity.getResources().getDrawable(entry.getValue());
                    float w = (smile_size * drawable.getIntrinsicWidth())/drawable.getIntrinsicHeight();

                    drawable.setBounds(0, 0, (int)w, smile_size);

                    builder.setSpan(new ImageSpan(drawable), index, index + length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    reduced_length += length - 1;
                    break;
                }
            }
        }
        textView.setText(builder);
        textView.setSelection(selectionCursor);
    }

    public static SpannableStringBuilder getSpannable(String text, int smile_size, Activity activity){

        int index;
        SpannableStringBuilder builder = new SpannableStringBuilder(text);
        if(!text.contains(":smile")){
            return  builder;
        }
        for (index = 0; index < builder.length(); index++) {
            for (Map.Entry<String, Integer> entry : Smiles.smileicons.entrySet()) {
                int length = entry.getKey().length();
                if (index + length > builder.length())
                    continue;
                if (builder.subSequence(index, index + length).toString()
                        .equals(entry.getKey())) {

                    Drawable drawable = activity.getResources().getDrawable(entry.getValue());
                    float w = (smile_size * drawable.getIntrinsicWidth())/drawable.getIntrinsicHeight();

                    drawable.setBounds(0, 0, (int)w, smile_size);

                    builder.setSpan(new ImageSpan(drawable), index, index + length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    break;
                }
            }
        }
        return builder;
    }
}
