package pro.devapp.kitoss.payment;

/**
 * Created by devapp on 01/08/16.
 */
public class Base64DecoderException extends Exception {
    public Base64DecoderException() {
        super();
    }

    public Base64DecoderException(String s) {
        super(s);
    }

    private static final long serialVersionUID = 1L;
}