package pro.devapp.kitoss.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.fragments.histories.HistoriesInterface;
import pro.devapp.kitoss.fragments.histories.MyHistoriesFragment;
import pro.devapp.kitoss.fragments.histories.PublicHistoriesFragment;
import pro.devapp.kitoss.interfaces.SmilesFragment;

/**
 * Список историй
 */
public class HistoriesFragment extends Fragment implements HistoriesInterface, SmilesFragment{

    private ViewPager viewPager;
    private ViewPagerAdapter adapter;

    /**
     * @return A new instance of fragment HistoriesFragment.
     */
    public static HistoriesFragment newInstance() {
        HistoriesFragment fragment = new HistoriesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        adapter = new ViewPagerAdapter(getFragmentManager());
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_histories, container, false);

        // добавляем вкладки
        if(adapter.getCount() == 0){
            PublicHistoriesFragment fragment1 = PublicHistoriesFragment.newInstance();
            MyHistoriesFragment fragment2 = MyHistoriesFragment.newInstance();

            adapter.addFragment(fragment1, "Все");
            adapter.addFragment(fragment2, "Мои");
        }

        viewPager = (ViewPager) v.findViewById(R.id.view_pager);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(5);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.lightBlue));

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(adapter.getTabView(i));
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int i) {
               //((FeedChat)adapter.getItem(i)).update();
                ((BaseActivity)getActivity()).hideKeyboard();
                adapter.setPostition(i);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return v;
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private int postition = 0;

        public int getPostition(){
            return postition;
        }

        public void setPostition(int postition){
            this.postition = postition;
        }

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        /**
         * Обновление списка в страницах
         */
        public void update(){
            for(Fragment f : mFragmentList){
                ((HistoriesInterface)f).update();
            }
        }

        /**
         * Клик по смайлу
         * @param v
         */
        public void onClickSmile(View v){
            Fragment f = getItem(postition);
            if(f instanceof MyHistoriesFragment){
                ((MyHistoriesFragment)f).onClickSmile(v);
            }
        }

        public boolean onBackPressed(){
            Fragment f = getItem(postition);
            if(f instanceof HistoriesInterface){
                return ((HistoriesInterface)f).onBackPressed();
            }
            return false;
        }

        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View badge_view = View.inflate(getContext(), R.layout.tab_layout_with_badge, null);
            TextView badge_title = (TextView) badge_view.findViewById(R.id.text);
            badge_title.setText(mFragmentTitleList.get(position));
            return badge_view;
        }
    }

    /**
     * Обновление списка
     */
    @Override
    public void update(){
        adapter.update();
    }

    @Override
    public void new_history() {

    }

    /**
     * Клик назад
     */
    @Override
    public boolean onBackPressed() {
        return adapter.onBackPressed();
    }

    /**
     * Добавление смайликов
     * @param v
     */
    public void onClickSmile(View v){
        adapter.onClickSmile(v);
    }
}
