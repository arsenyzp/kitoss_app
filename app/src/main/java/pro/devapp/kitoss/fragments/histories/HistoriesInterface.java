package pro.devapp.kitoss.fragments.histories;

/**
 * Интерфейс для экранов истории
 */
public interface HistoriesInterface {
    /**
     * Обновление списка
     */
    public void update();

    /**
     * Новая история
     */
    public void new_history();

    /**
     * Клик назад
     */
    public boolean onBackPressed();
}
