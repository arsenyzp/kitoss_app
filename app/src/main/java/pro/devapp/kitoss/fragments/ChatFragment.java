package pro.devapp.kitoss.fragments;

import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.fragments.chat.FeedChat;
import pro.devapp.kitoss.fragments.chat.PublicChatFragment;
import pro.devapp.kitoss.interfaces.SmilesFragment;
import pro.devapp.kitoss.models.PublicChatModel;
import pro.devapp.kitoss.models.RegionModel;
import pro.devapp.kitoss.models.UserModel;

/**
 * Чат общий
 */
public class ChatFragment extends Fragment implements FeedChat, SmilesFragment {

    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private int message_chat_sound;
    private SoundPool soundPool;
    //private Timer timer;

    private UserModel userModel;

    /**
     * @return A new instance of fragment ChatFragment.
     */
    public static ChatFragment newInstance() {
        ChatFragment fragment = new ChatFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        adapter = new ViewPagerAdapter(getFragmentManager());
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        message_chat_sound = soundPool.load(getActivity(), R.raw.chat_message, 1);
        userModel = UserModel.getByToken(((BaseActivity)getActivity()).token);
    }

    @Override
    public void onResume() {
        super.onResume();
        // запускаем сервис обновления инфы о пользователях
//        timer = new Timer();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                JSONObject jsonObject = new JSONObject();
//                try {
//                    jsonObject.put("ids", PublicChatModel.getLastUserIds());
//                    ((App)getActivity().getApplication()).SocketEmit("getUserInfo", jsonObject);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, 0, 3000);
    }

    @Override
    public void onPause() {
        super.onPause();
        // останавливем сервис обновления инфы о пользователях
        //timer.cancel();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_chat, container, false);

        // добавляем вкладки для доступных категорий
        if(adapter.getCount() == 0){
            PublicChatFragment fragment1 = PublicChatFragment.newInstance(userModel.region);
            PublicChatFragment fragment2 = PublicChatFragment.newInstance("all");

            adapter.addFragment(fragment1, RegionModel.getRegionName(userModel.region));
            adapter.addFragment(fragment2, "Общий");
        }

        viewPager = (ViewPager) v.findViewById(R.id.view_pager);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(5);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.lightBlue));

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(adapter.getTabView(i));
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int i) {
                ((PublicChatFragment)adapter.getItem(i)).restart();
                ((BaseActivity)getActivity()).hideKeyboard();
                adapter.setPostition(i);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return v;
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private int postition = 0;

        public int getPostition(){
            return postition;
        }

        public void setPostition(int postition){
            this.postition = postition;
        }

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        /**
         * Обновление списка в страницах
         */
        public void new_public_message(PublicChatModel publicChatModel){
            for(Fragment f : mFragmentList){
                ((FeedChat)f).new_public_message(publicChatModel);
            }
        }

        public void removePublicMessage(PublicChatModel publicChatModel) {
            for(Fragment f : mFragmentList){
                ((FeedChat)f).removePublicMessage(publicChatModel);
            }
        }

        /**
         * Клик по смайлу
         * @param v
         */
        public void onClickSmile(View v){
            Fragment f = getItem(postition);
            if(f instanceof PublicChatFragment){
                ((PublicChatFragment)f).onClickSmile(v);
            }
        }

        public boolean onBackPressed(){
            Fragment f = getItem(postition);
            if(f instanceof FeedChat){
                return ((FeedChat)f).onBackPressed();
            }
            return false;
        }

        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View badge_view = View.inflate(getContext(), R.layout.tab_layout_with_badge, null);
            TextView badge_title = (TextView) badge_view.findViewById(R.id.text);
            badge_title.setText(mFragmentTitleList.get(position));
            return badge_view;
        }
    }

    /**
     * Обновление списка
     */
    @Override
    public void update(){
       // adapter.new_public_message();
    }

    /**
     * Клик назад
     */
    @Override
    public boolean onBackPressed() {
        return adapter.onBackPressed();
    }

    /**
     * Добавление смайликов
     * @param v
     */
    public void onClickSmile(View v){
        adapter.onClickSmile(v);
    }

    /**
     * Новое сообщение
     */
    @Override
    public void new_public_message(PublicChatModel publicChatModel) {
        // звук
        if(adapter.getPostition() == 0 && publicChatModel.region.equals(userModel.region)){
            ((BaseActivity)getActivity()).playSound(message_chat_sound);
        } else if(publicChatModel.region.equals("all")){
            ((BaseActivity)getActivity()).playSound(message_chat_sound);
        }
        adapter.new_public_message(publicChatModel);
    }

    /**
     * Удалено сообщение в чате
     */
    @Override
    public void removePublicMessage(PublicChatModel publicChatModel) {
        adapter.removePublicMessage(publicChatModel);
    }

}
