package pro.devapp.kitoss.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.ProfileActivity;

/**
 * Меню для аватарки
 */
public class AvatarMenuFragment extends DialogFragment {

    public static AvatarMenuFragment newInstance() {
        AvatarMenuFragment fragment = new AvatarMenuFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, 0);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.Dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_menu_avatar, container, false);

        // загрузить аватар
        View change_avatar = v.findViewById(R.id.change_avatar);
        change_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProfileActivity)getActivity()).onActivityResult(ProfileActivity.SELECT_PICTURE_AVATAR_CODE, Activity.RESULT_OK, new Intent());
                dismiss();
            }
        });

        // загрузить фон
        View change_bg = v.findViewById(R.id.change_bg);
        change_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProfileActivity)getActivity()).onActivityResult(ProfileActivity.SELECT_PICTURE_BG_CODE, Activity.RESULT_OK, new Intent());
                dismiss();
            }
        });

        // удалить фон
        View delete_bg = v.findViewById(R.id.delete_bg);
        delete_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProfileActivity)getActivity()).onActivityResult(ProfileActivity.DELETE_PICTURE_BG_CODE, Activity.RESULT_OK, new Intent());
                dismiss();
            }
        });

        // удалить аватар
        View delete_avatar = v.findViewById(R.id.delete_avatar);
        delete_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProfileActivity)getActivity()).onActivityResult(ProfileActivity.DELETE_PICTURE_AVATAR_CODE, Activity.RESULT_OK, new Intent());
                dismiss();
            }
        });

        return  v;
    }
}
