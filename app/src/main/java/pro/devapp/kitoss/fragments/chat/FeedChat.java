package pro.devapp.kitoss.fragments.chat;

import pro.devapp.kitoss.models.PublicChatModel;

/**
 * Методы для экрана public чата
 */
public interface FeedChat {

    /**
     * Новое сообщение в чате
     */
    public void new_public_message(PublicChatModel publicChatModel);

    /**
     * Удалено сообщение в чате
     */
    public void removePublicMessage(PublicChatModel publicChatModel);

    /**
     * Клик назад
     */
    public boolean onBackPressed();
}
