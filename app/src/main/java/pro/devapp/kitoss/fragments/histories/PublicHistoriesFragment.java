package pro.devapp.kitoss.fragments.histories;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.AppActivity;
import pro.devapp.kitoss.adapters.HistoriesAdapter;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RecyclerItemClickListener;
import pro.devapp.kitoss.components.Smiles;
import pro.devapp.kitoss.models.HistoriesModel;
import pro.devapp.kitoss.models.PublicChatModel;
import pro.devapp.kitoss.ui.KeyboardlessEditText2;

/**
 * Список историй
 */
public class PublicHistoriesFragment extends Fragment implements HistoriesInterface{
    private RecyclerView recyclerView;
    private HistoriesAdapter adapter;
    private SwipyRefreshLayout swipeRefreshLayout;
    private KeyboardlessEditText2 message;

    private int offset = 0;
    private int limit = 30;

    int smile_size = 25;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PublicChatFragment.
     */
    public static PublicHistoriesFragment newInstance() {
        PublicHistoriesFragment fragment = new PublicHistoriesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        adapter = new HistoriesAdapter(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppActivity)getActivity()).hideKeyboard();
        ((AppActivity)getActivity()).showBottomBar();
        update();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_public_histories, container, false);

        // размер смайла
        smile_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, getResources().getDisplayMetrics());

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        // list
        recyclerView = (RecyclerView) v.findViewById(R.id.list);
        recyclerView.setAdapter(adapter);

        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(30);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener(){
            @Override
            public void onItemClick(View view, int position) {
                if(
                        ((BaseActivity)getActivity()).userModel.role.equals(Const.ROLE_ADMIN) ||
                                ((BaseActivity)getActivity()).userModel.role.equals(Const.ROLE_MANAGER) ||
                                ((BaseActivity)getActivity()).userModel.role.equals(Const.ROLE_MODER)
                        ){
                    final HistoriesModel historiesModel = adapter.getItem(position);

                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
                    View v = View.inflate(getActivity(), R.layout.popup_histories_admin_menu, null);
                    builder.setView(v);
                    final Dialog dialog = builder.show();

                    View histories_accept = v.findViewById(R.id.histories_accept);
                    if(historiesModel.status.equals("new")){
                        histories_accept.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                HistoriesModel.accept(((BaseActivity)getActivity()).token, historiesModel.id, adminHandler);
                                dialog.dismiss();
                            }
                        });
                    } else {
                        histories_accept.setVisibility(View.GONE);
                    }

                    View histories_decline = v.findViewById(R.id.histories_decline);
                    histories_decline.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            HistoriesModel.decline(((BaseActivity)getActivity()).token, historiesModel.id, adminHandler);
                            dialog.dismiss();
                        }
                    });



                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        swipeRefreshLayout = (SwipyRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                offset += limit;
                update();
            }
        });

        return v;
    }

    /**
     * Обновить список
     */
    @Override
    public void update() {
        HistoriesModel.loadHistories(((BaseActivity) getActivity()).token, offset, limit, new HistoriesModel.ResponseListener() {
            @Override
            public void onSuccess(HistoriesModel historiesModel) {
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onSuccess(List<HistoriesModel> historiesModels) {
                swipeRefreshLayout.setRefreshing(false);
                adapter.add(historiesModels);
            }

            @Override
            public void onError(String error) {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void new_history() {

    }


    /**
     * Добавление смайликов
     * @param v
     */
    public void onClickSmile(View v){
        String smile = Smiles.SmileToText(v.getId());
        int selectionCursor = message.getSelectionStart();
        Smiles.setSmiles(message.getText().insert(selectionCursor, smile).toString(), message, smile_size, getActivity());
    }


    Handler adminHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    ((BaseActivity)getActivity()).logout();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    update();
                    Toast.makeText(getActivity(), "Запрос обработан", Toast.LENGTH_LONG).show();
                    break;
                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:
                    Toast.makeText(getActivity(), "Ошибка обработки запроса", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };

}
