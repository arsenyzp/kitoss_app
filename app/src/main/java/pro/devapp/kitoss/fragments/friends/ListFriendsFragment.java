package pro.devapp.kitoss.fragments.friends;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.AppActivity;
import pro.devapp.kitoss.adapters.UsersAdapter;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.fragments.FriendsFragment;

/**
 * Список друзей
 */
public class ListFriendsFragment extends Fragment {

    private RecyclerView recyclerView;
    private UsersAdapter adapter;

    private String type = Const.FILTER_ONLINE;

    /**
     *
     * @param type
     * @return A new instance of fragment ListFriendsFragment.
     */
    public static ListFriendsFragment newInstance(String type) {
        ListFriendsFragment fragment = new ListFriendsFragment();
        fragment.type = type;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        adapter = new UsersAdapter(getActivity(), handler);
    }


    @Override
    public void onStart() {
        super.onStart();
        update();
    }

    /**
     * Обновление данных на экране
     */
    public void update(){
        if(adapter != null)
            adapter.update(type);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list_friends, container, false);
        // list
        recyclerView = (RecyclerView) v.findViewById(R.id.list);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return v;
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {

            String token = ((App)getActivity().getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    ((BaseActivity)getActivity()).hideWait();
                    ((BaseActivity)getActivity()).logout();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    //((BaseActivity)getActivity()).hideWait();
                    adapter.update(type);
                    break;
                case Const.STATE_SUCCESS_ADD_TO_FRIENDS:
                    //((BaseActivity)getActivity()).hideWait();
                    // обновляем бейджи в bottomBar
                    if(getActivity() instanceof AppActivity){
                        ((AppActivity)getActivity()).setBadgeFriend();
                    }
                    // обновляем бейдж в верхней вкладке
                    if(getTargetFragment() instanceof FriendsFragment){
                        ((FriendsFragment)getTargetFragment()).update();
                    }
                    adapter.update(type);
                    Toast.makeText(getActivity(), getString(R.string.info_add_friend), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_REMOVE_FROM_FRIENDS:
                    //((BaseActivity)getActivity()).hideWait();
                    adapter.update(type);
                    Toast.makeText(getActivity(), getString(R.string.info_remove_friend), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_DECLINE_FRIEND_REQUEST:
                    //((BaseActivity)getActivity()).hideWait();
                    // обновляем бейджи
                    if(getActivity() instanceof AppActivity){
                        ((AppActivity)getActivity()).setBadgeFriend();
                    }
                    adapter.update(type);
                    Toast.makeText(getActivity(), getString(R.string.info_success_decline_friend_request), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_CANCEL_FRIEND_REQUEST:
                    //((BaseActivity)getActivity()).hideWait();
                    adapter.update(type);
                    Toast.makeText(getActivity(), getString(R.string.info_success_cancel_friend_request), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:
                    ((BaseActivity)getActivity()).hideWait();
                    Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}
