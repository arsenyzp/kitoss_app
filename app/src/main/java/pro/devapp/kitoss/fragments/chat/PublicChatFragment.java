package pro.devapp.kitoss.fragments.chat;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.AppActivity;
import pro.devapp.kitoss.activity.GiftsListActivity;
import pro.devapp.kitoss.activity.ProfileActivity;
import pro.devapp.kitoss.activity.PublicProfileActivity;
import pro.devapp.kitoss.activity.UserChatActivity;
import pro.devapp.kitoss.adapters.PublicChatAdapter;
import pro.devapp.kitoss.components.AdminHelper;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RecyclerItemClickListener;
import pro.devapp.kitoss.components.SmileTextWatcher;
import pro.devapp.kitoss.components.Smiles;
import pro.devapp.kitoss.models.PublicChatModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.SmilesHistoryModel;
import pro.devapp.kitoss.models.UserModel;
import pro.devapp.kitoss.ui.KeyboardlessEditText2;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PublicChatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PublicChatFragment extends Fragment implements FeedChat {

    private RecyclerView recyclerView;
    private PublicChatAdapter adapter;
    private SwipyRefreshLayout swipeRefreshLayout;
    private View smiles;
    private ImageView button_smile;
    private KeyboardlessEditText2 message;

    private String region;

    private int offset = 0;
    private int limit = 60;

    int smile_size = 25;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PublicChatFragment.
     */
    public static PublicChatFragment newInstance(String region) {
        PublicChatFragment fragment = new PublicChatFragment();
        fragment.region = region;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        adapter = new PublicChatAdapter(getActivity(), region);
    }

    @Override
    public void onResume() {
        super.onResume();

        hideSmiles();
        ((AppActivity)getActivity()).hideKeyboard();
        ((AppActivity)getActivity()).showBottomBar();
        // update my profile
        ((BaseActivity)getActivity()).userModel.refreshFromServer(handler);
        update();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public boolean onBackPressed() {
        if(smiles != null && smiles.getVisibility() == View.VISIBLE){
            // скрываем смайлы
            hideSmiles();
            ((AppActivity)getActivity()).showBottomBar();
            return true;
        }
        return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_public_chat, container, false);

        // смайлики
        smiles = v.findViewById(R.id.smiles);
        // размер смайла
        smile_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, getResources().getDisplayMetrics());

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setStackFromEnd(true);
        layoutManager.setReverseLayout(true);
        // list
        recyclerView = (RecyclerView) v.findViewById(R.id.list);
        recyclerView.setAdapter(adapter);

        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(30);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        swipeRefreshLayout = (SwipyRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                offset += limit;
                update();
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener(){

            @Override
            public void onItemClick(View view, int position) {
                final PublicChatModel publicChatModel = adapter.getItem(position);
                if(publicChatModel.publicUserModel.id.equals(((BaseActivity)getActivity()).userModel.id)){
                    return;
                }
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
                View v = View.inflate(getActivity(), R.layout.popup_publoc_chat_menu, null);
                builder.setView(v);
                final Dialog dialog = builder.show();

                View send_private_message = v.findViewById(R.id.send_private_message);
                View send_gift = v.findViewById(R.id.send_gift);
                View answer = v.findViewById(R.id.answer);
                View go_to_profile = v.findViewById(R.id.go_to_profile);
                View delete_message = v.findViewById(R.id.delete_message);
                View block_user = v.findViewById(R.id.block_user);

                // отправить сообщение
                send_private_message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent intent = new Intent(getActivity(), UserChatActivity.class);
                        intent.putExtra("uid", publicChatModel.sender_id);
                        startActivity(intent);
                    }
                });

                // отправить подарок
                send_gift.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent intent = new Intent(getActivity(), GiftsListActivity.class);
                        intent.putExtra("uid", publicChatModel.sender_id);
                        startActivity(intent);
                    }
                });

                // цитировать
                answer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        message.setText(publicChatModel.publicUserModel.first_name + ", ");
                    }
                });

                // перейти в профиль
                go_to_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        goToUserProfile(publicChatModel.sender_id);
                    }
                });

                if(
                    ((BaseActivity)getActivity()).userModel.role.equals(Const.ROLE_ADMIN) ||
                    ((BaseActivity)getActivity()).userModel.role.equals(Const.ROLE_MANAGER) ||
                    ((BaseActivity)getActivity()).userModel.role.equals(Const.ROLE_MODER) ||
                    ((BaseActivity)getActivity()).userModel.role.equals(Const.ROLE_MODER_CHAT)
                        ){
                    delete_message.setVisibility(View.VISIBLE);
                    block_user.setVisibility(View.VISIBLE);

                    // удалить сообщение
                    delete_message.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            AdminHelper.remove_public_message(publicChatModel.mid, ((BaseActivity)getActivity()).token, adminHandler);
                        }
                    });

                    // заблокировать пользователя
                    block_user.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            AdminHelper.block_user_in_public_chat(publicChatModel.mid, ((BaseActivity)getActivity()).token, adminHandler);
                        }
                    });
                } else {
                    delete_message.setVisibility(View.GONE);
                    block_user.setVisibility(View.GONE);
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {
            }
        }));

        // show smile
        button_smile = (ImageButton) v.findViewById(R.id.button_smile);
        button_smile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(smiles.getVisibility() != View.VISIBLE){
                    // показываем смайлы
                    showSmiles(v);
                } else {
                    // скрываем смайлы
                    hideSmiles();
                    ((AppActivity)getActivity()).showKeyboard();
                }
            }
        });

        message = (KeyboardlessEditText2) v.findViewById(R.id.message);

        // обработка удаления смайликов
        SmileTextWatcher smileTextWatcher = new SmileTextWatcher(message, smile_size, getActivity());
        message.addTextChangedListener(smileTextWatcher);

        /**
         * Клик по полю для ввода сообщения - показ клавиатуры
         */
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((AppActivity)getActivity()).hideBottomBar();
                hideSmiles();
                ((AppActivity)getActivity()).showKeyboard();
                message.requestFocus();
            }
        });
//        message.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                int visib = smiles.getVisibility();
//                if(visib == View.GONE && motionEvent.getAction() == MotionEvent.ACTION_UP){
//                    ((AppActivity)getActivity()).hideBottomBar();
//                    hideSmiles();
//                    ((AppActivity)getActivity()).showKeyboard();
//                    message.requestFocus();
//                }
//                return false;
//            }
//        });

        // send message
        View button_send = v.findViewById(R.id.button_send);
        button_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = message.getText().toString();
                msg = msg.replaceAll("[\r\n]+", "\n");
                if(msg.equals("")){
                    message.setError(getString(R.string.error_empty_message));
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    message.setError(null);
                                }
                            });
                        }
                    }, 3000l);
                } else {

                    if(smiles != null && smiles.getVisibility() == View.VISIBLE){
                        // скрываем смайлы
                        hideSmiles();
                    }
                    ((AppActivity)getActivity()).hideKeyboard();
                    ((AppActivity)getActivity()).showBottomBar();

                    PublicChatModel model = new PublicChatModel();
                    model.message = msg;
                    model.sender_id =  ((BaseActivity)getActivity()).userModel.id;
                    model.is_read = 1;
                    model.time = System.currentTimeMillis();
                    model.uiid = System.currentTimeMillis() + "_" +  ((BaseActivity)getActivity()).userModel.id;
                    model.mid = model.uiid;
                    model.region = region;
                    model.publicUserModel = PublicUserModel.getById(model.sender_id);
                    try {
                        sendMessage(model);
                    } catch (JSONException e) {
                        Log.d(PublicChatFragment.class.getName(), e.toString());
                    }
                    adapter.add(model);
                    scrollTop();
                    message.setText("");
                }
            }
        });

        return v;
    }

    public void restart(){
        offset = 0;
        update();
    }
    /**
     * Обновить список
     */
    public void update() {

        swipeRefreshLayout.setRefreshing(true);
        PublicChatModel.loadHistoryMessages(((BaseActivity) getActivity()).token, offset, limit, region, new PublicChatModel.ResponseListener() {
                    @Override
                    public void onSuccess(List<PublicChatModel> items) {
                        if (adapter != null) {
                            if(offset == 0){
                                adapter.clear();
                            }
                            adapter.add(items);
                            if (items.size() == 0) {
                                Toast.makeText(getActivity(), "Больше нет сообщений", Toast.LENGTH_LONG).show();
                            }
                        }
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onError(String message) {
                        Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) getActivity()).hideWait();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
    }

    /**
     * Новое сообщение в чате
     */
    @Override
    public void new_public_message(PublicChatModel publicChatModel) {
        if(!publicChatModel.publicUserModel.id.equals(((BaseActivity)getActivity()).userModel.id) && publicChatModel.region.equals(region)){
            adapter.add(publicChatModel);
            scrollTop();
        }
    }

    /**
     * Удалено сообщение в чате
     */
    @Override
    public void removePublicMessage(PublicChatModel publicChatModel) {
        adapter.remove(publicChatModel);
    }

    /**
     * Прокрутка чата
     */
    public void scrollTop(){
        if(recyclerView != null && !recyclerView.isAnimating()){
            recyclerView.smoothScrollToPosition(recyclerView.getAdapter().getItemCount());
        }
    }

    /**
     * Показать смайлы
     */
    public void showSmiles(View v){
        if(v == null){
            return;
        }

        // скрываем клавиатуру
        ((AppActivity)getActivity()).hideKeyboard();
        // hide bottom bar
        ((AppActivity)getActivity()).hideBottomBar();

        // добавляем часто используемые смайлы
        List<SmilesHistoryModel> smilesHistoryList = SmilesHistoryModel.getSmiles();
        // часто используемые смайлы
        for (int i = 0; i < smilesHistoryList.size() && i < SmilesHistoryModel.smile.length; i++){
            SmilesHistoryModel smilesHistoryModel = smilesHistoryList.get(i);
            ImageView smile = (ImageView)v.findViewById(smilesHistoryModel.smile_id);
            if(smile != null){
                ImageView a_smile = (ImageView)v.findViewById(SmilesHistoryModel.smile[i]);
                a_smile.setImageDrawable(smile.getDrawable());
                a_smile.setVisibility(View.VISIBLE);
            }
        }
        if(smiles != null){
            smiles.setVisibility(View.VISIBLE);
        }

        /**
         * Кнопка удаления
         */
        View backspace = v.findViewById(R.id.backspace);
        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int start = message.getSelectionStart();
                if(start<1){
                    return;
                }

                String left_str = message.getEditableText().subSequence(0, start-1).toString();
                CharSequence right_str = "";
                if(start < (message.getEditableText().length() -1)){
                    right_str = message.getEditableText().subSequence(start, message.getEditableText().length());
                }
                String new_str = left_str + right_str;

                if(left_str.length() > 2){
                    start = left_str.length()-1;
                    int last_smile_start = new_str.subSequence(0, start).toString().lastIndexOf(" :smile");
                    int last_smile_end = new_str.subSequence(0, start).toString().lastIndexOf(": ");
                    if(last_smile_start >= 0 && last_smile_end < last_smile_start){
                        left_str = new_str.substring(0, last_smile_start+1);
                        new_str = left_str + right_str;
                    }
                }

                message.setText(new_str);
                message.setSelection(left_str.length());

                Smiles.setSmiles(new_str, message, smile_size, getActivity());
            }
        });

        // меняем занчёк на кнопке
        if(button_smile != null){
            button_smile.setImageResource(R.drawable.ic_keyboard);
        }
    }

    /**
     * Скрыть смайлы
     */
    public void hideSmiles(){
        if(smiles != null){
            smiles.setVisibility(View.GONE);
        }
        // меняем занчёк на кнопке
        if(button_smile != null){
            button_smile.setImageResource(R.drawable.ic_smile);
        }
    }

    /**
     * Добавление смайликов
     * @param v
     */
    public void onClickSmile(View v){
        String smile = Smiles.SmileToText(v.getId());
        int selectionCursor = message.getSelectionStart();
        Smiles.setSmiles(message.getText().insert(selectionCursor, smile).toString(), message, smile_size, getActivity());
    }

    /**
     * Отправка сообщения
     * @param model
     * @throws JSONException
     */
    public void sendMessage(PublicChatModel model) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("message", model.message);
        args.put("sender_id", model.sender_id);
        args.put("time", model.time);
        args.put("uiid", model.uiid);
        args.put("region", model.region);

        ((App)getActivity().getApplication()).SocketEmit("send_public_message", args);
    }

    /**
     * Переход к профилю пользователя
     */
    public void goToUserProfile(String user_id){
        if(((BaseActivity)getActivity()).userModel.id.equals(user_id)){
            Intent intent = new Intent(getActivity(), ProfileActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getActivity(), PublicProfileActivity.class);
            intent.putExtra("uid",  user_id);
            startActivity(intent);
        }
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    ((BaseActivity)getActivity()).logout();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    ((BaseActivity)getActivity()).userModel = UserModel.getByToken( ((BaseActivity)getActivity()).token);
                    break;
                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:

                    break;
            }
        }
    };

    Handler adminHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    ((BaseActivity)getActivity()).logout();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    update();
                    Toast.makeText(getActivity(), "Запрос обработан", Toast.LENGTH_LONG).show();
                    break;
                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:
                    Toast.makeText(getActivity(), "Ошибка обработки запроса", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };
}
