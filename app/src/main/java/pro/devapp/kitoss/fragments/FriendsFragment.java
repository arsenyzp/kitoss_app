package pro.devapp.kitoss.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.InviteContactActivity;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.NotificationHelper;
import pro.devapp.kitoss.fragments.friends.ListFriendsFragment;
import pro.devapp.kitoss.models.NotificationModel;
import pro.devapp.kitoss.models.PublicUserModel;

/**
 * Список друзей
 */
public class FriendsFragment extends Fragment {

    ViewPager viewPager;
    TextView badge_count_request;
    TextView badge_count_offline;
    TextView badge_count_online;

    private int offset = 0;
    //TODO нужно решить как сделать подгрузку друзей
    private int limit = 500;

    ViewPagerAdapter adapter;

    /**
     *
     * @return A new instance of fragment FriendsFragment.
     */
    public static FriendsFragment newInstance() {
        FriendsFragment fragment = new FriendsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        adapter = new ViewPagerAdapter(getFragmentManager());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_friends, container, false);

        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                Intent intent = new Intent(getActivity(), InviteContactActivity.class);
                startActivity(intent);
            }
        });

        // добавляем вкладки
        if(adapter.getCount() == 0){
            ListFriendsFragment fragment1 = ListFriendsFragment.newInstance(Const.FILTER_ONLINE);
            ListFriendsFragment fragment2 = ListFriendsFragment.newInstance(Const.FILTER_OFFLINE);
            ListFriendsFragment fragment3 = ListFriendsFragment.newInstance(Const.FILTER_REQUEST);

            adapter.addFragment(fragment1, getString(R.string.menu_friends_online));
            adapter.addFragment(fragment2, getString(R.string.menu_friends_offline));
            adapter.addFragment(fragment3, getString(R.string.menu_friends_request));
        }

        viewPager = (ViewPager) v.findViewById(R.id.view_pager);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(5);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.lightBlue));

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(adapter.getTabView(i));
        }

        // Badge
        TabLayout.Tab tabLayout2 = tabLayout.getTabAt(0);
        badge_count_online = (TextView) tabLayout2.getCustomView().findViewById(R.id.badge);

        TabLayout.Tab tabLayout3 = tabLayout.getTabAt(1);
        badge_count_offline = (TextView) tabLayout3.getCustomView().findViewById(R.id.badge);

        TabLayout.Tab tabLayout1 = tabLayout.getTabAt(2);
        badge_count_request = (TextView) tabLayout1.getCustomView().findViewById(R.id.badge);

        updateBadge();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ((ListFriendsFragment)adapter.getItem(position)).update();
                switch (position){
                    // заявки
                    case 2:
                        // очищаем уведомления о новых заявках
                        NotificationHelper.cancelNotification(NotificationModel.TYPE_FRIENDS, getActivity());
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        String token = ((App)getActivity().getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
        // обновляем модель с сервера
        PublicUserModel.getFriends(offset, limit, token, handler);
        NotificationHelper.cancelNotification(NotificationModel.TYPE_MESSAGES, getContext());
        NotificationHelper.cancelNotification(NotificationModel.TYPE_NOTIFICATIONS, getContext());
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<ListFriendsFragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(ListFriendsFragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View badge_view = View.inflate(getContext(), R.layout.tab_layout_with_badge, null);
            TextView badge_title = (TextView) badge_view.findViewById(R.id.text);
            badge_title.setText(mFragmentTitleList.get(position));
            badge_count_request = (TextView) badge_view.findViewById(R.id.badge);
            badge_count_request.setVisibility(View.GONE);
            return badge_view;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        /**
         * Обновление списка в страницах
         */
        public void update(){
            for(ListFriendsFragment f : mFragmentList){
                f.update();
            }
        }
    }

    /**
     * Обновление индикатора запросов в друзья
     */
    public void updateBadge(){
        int friend_online = PublicUserModel.getCountFriendOnline();
        if(friend_online > 0 && badge_count_online != null){
            badge_count_online.setText(String.valueOf(friend_online));
            badge_count_online.setVisibility(View.VISIBLE);
        }

        int friend_offline = PublicUserModel.getCountFriendOffline();
        if(friend_offline > 0 && badge_count_offline != null){
            badge_count_offline.setText(String.valueOf(friend_offline));
            badge_count_offline.setVisibility(View.VISIBLE);
        }

        int friend_request = PublicUserModel.getCountFriendRequest();
        if(friend_request > 0 && badge_count_request != null){
            badge_count_request.setText(String.valueOf(friend_request));
            badge_count_request.setVisibility(View.VISIBLE);
        }
    }

    public void update(){
        updateBadge();
        adapter.update();
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {

            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    //hideWait();
                    ((BaseActivity)getActivity()).logout();
                    break;
                case Const.SOCKET_ACTION_NEW_FRIEND_REQUEST:
                case Const.SOCKET_ACTION_CANCEL_FRIEND_REQUEST:
                case Const.SOCKET_ACTION_ACCEPT_REQUEST:
                case Const.SOCKET_ACTION_DECLINE_REQUEST:
                case Const.SOCKET_ACTION_REMOVE_FRIEND:
                    //((AppActivity)getActivity()).setBadgeFriend();
                    update();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    //hideWait();
                    update();
                    break;
                case Const.STATE_SUCCESS_ADD_TO_FRIENDS:
                    //hideWait();
                    update();
                    Toast.makeText(getContext(), getString(R.string.info_add_friend), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_REMOVE_FROM_FRIENDS:
                    //hideWait();
                    update();
                    Toast.makeText(getContext(), getString(R.string.info_remove_friend), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_DECLINE_FRIEND_REQUEST:
                    //hideWait();
                    update();
                    Toast.makeText(getContext(), getString(R.string.info_success_decline_friend_request), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_CANCEL_FRIEND_REQUEST:
                    //hideWait();
                    //adapter.update();
                    Toast.makeText(getContext(), getString(R.string.info_success_cancel_friend_request), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:
                    //hideWait();
                    Toast.makeText(getContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

}
