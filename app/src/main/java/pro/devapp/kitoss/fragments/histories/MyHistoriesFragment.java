package pro.devapp.kitoss.fragments.histories;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import java.util.List;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.AppActivity;
import pro.devapp.kitoss.adapters.MyHistoriesAdapter;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.SmileTextWatcher;
import pro.devapp.kitoss.components.Smiles;
import pro.devapp.kitoss.models.MyHistoriesModel;
import pro.devapp.kitoss.models.SmilesHistoryModel;
import pro.devapp.kitoss.ui.KeyboardlessEditText2;

/**
 * Фрагмент моих историй
 */
public class MyHistoriesFragment extends Fragment implements HistoriesInterface{

    private RecyclerView recyclerView;
    private MyHistoriesAdapter adapter;
    private SwipyRefreshLayout swipeRefreshLayout;
    private View smiles;
    private ImageView button_smile;
    private KeyboardlessEditText2 message;

    private int offset = 0;
    private int limit = 30;

    int smile_size = 25;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PublicChatFragment.
     */
    public static MyHistoriesFragment newInstance() {
        MyHistoriesFragment fragment = new MyHistoriesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        adapter = new MyHistoriesAdapter(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        hideSmiles();
        ((AppActivity)getActivity()).hideKeyboard();
        ((AppActivity)getActivity()).showBottomBar();

        ((BaseActivity)getActivity()).userModel.refreshFromServer(handler);
        update();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public boolean onBackPressed() {
        if(smiles != null && smiles.getVisibility() == View.VISIBLE){
            // скрываем смайлы
            hideSmiles();
            ((AppActivity)getActivity()).showBottomBar();
            return true;
        }
        return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_my_histories, container, false);

        // смайлики
        smiles = v.findViewById(R.id.smiles);
        // размер смайла
        smile_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, getResources().getDisplayMetrics());

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        // list
        recyclerView = (RecyclerView) v.findViewById(R.id.list);
        recyclerView.setAdapter(adapter);

        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(30);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        swipeRefreshLayout = (SwipyRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                final int old_message_count = recyclerView.getAdapter().getItemCount();
                offset += limit;
                MyHistoriesModel.loadHistories(((BaseActivity)getActivity()).token, offset, limit, new Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        switch (msg.arg1){
                            case Const.STATE_ACCESS_DINEDED:
                                ((BaseActivity)getActivity()).logout();
                                break;
                            case Const.STATE_SUCCESS_RESPONSE:
                                adapter.update(offset, limit);
                                int new_messag_count = recyclerView.getAdapter().getItemCount() - old_message_count;
                                if(new_messag_count == 0){
                                    Toast.makeText(getActivity(), "Больше нет историй", Toast.LENGTH_LONG).show();
                                }

                                if(new_messag_count == limit) {
                                    recyclerView.smoothScrollToPosition(offset + 5);
                                } else if(new_messag_count > 0) {
                                    recyclerView.smoothScrollToPosition(offset + new_messag_count);
                                }
                                swipeRefreshLayout.setRefreshing(false);
                                break;
                            case Const.STATE_ERROR_RESPONSE:
                            case Const.STATE_SERVER_ERROR:
                                Toast.makeText(getActivity(), "Ошибка сервера", Toast.LENGTH_LONG).show();
                                swipeRefreshLayout.setRefreshing(false);
                                break;
                        }
                    }
                });
            }
        });

        // show smile
        button_smile = (ImageButton) v.findViewById(R.id.button_smile);
        button_smile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(smiles.getVisibility() != View.VISIBLE){
                    // показываем смайлы
                    showSmiles(v);
                } else {
                    // скрываем смайлы
                    hideSmiles();
                    ((AppActivity)getActivity()).showKeyboard();
                }
            }
        });

        message = (KeyboardlessEditText2) v.findViewById(R.id.message);

        // обработка удаления смайликов
        SmileTextWatcher smileTextWatcher = new SmileTextWatcher(message, smile_size, getActivity());
        message.addTextChangedListener(smileTextWatcher);

        /**
         * Клик по полю для ввода сообщения - показ клавиатуры
         */
        message.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(smiles.getVisibility() != View.VISIBLE){
                    ((AppActivity)getActivity()).hideBottomBar();
                    ((AppActivity)getActivity()).showKeyboard();
                    message.requestFocus();
                }
                return false;
            }
        });

        // send
        View button_send = v.findViewById(R.id.button_send);
        button_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = message.getText().toString();
                msg = msg.replaceAll("[\r\n]+", "\n");
                if(msg.equals("")){
                    message.setError(getString(R.string.error_empty_message));
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    message.setError(null);
                                }
                            });
                        }
                    }, 3000l);
                } else {
                    ((BaseActivity)getActivity()).showWait();
                    if(smiles != null && smiles.getVisibility() == View.VISIBLE){
                        // скрываем смайлы
                        hideSmiles();
                    }
                    ((AppActivity)getActivity()).hideKeyboard();
                    ((AppActivity)getActivity()).showBottomBar();

                    MyHistoriesModel.sendHistory(((BaseActivity)getActivity()).token, msg, new Handler(){
                        @Override
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            switch (msg.arg1){
                                case Const.STATE_ACCESS_DINEDED:
                                    ((BaseActivity)getActivity()).logout();
                                    break;
                                case Const.STATE_SUCCESS_RESPONSE:
                                    adapter.update(offset, limit);
                                    scrollTop();
                                    message.setText("");
                                    break;
                                case Const.STATE_ERROR_RESPONSE:
                                case Const.STATE_SERVER_ERROR:
                                    Toast.makeText(getActivity(), "Ошибка сервера", Toast.LENGTH_LONG).show();
                                    break;
                            }
                            ((BaseActivity)getActivity()).hideWait();
                        }
                    });
                }
            }
        });

        MyHistoriesModel.loadHistories(((BaseActivity)getActivity()).token, offset, limit, new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.arg1){
                    case Const.STATE_ACCESS_DINEDED:
                        ((BaseActivity)getActivity()).logout();
                        break;
                    case Const.STATE_SUCCESS_RESPONSE:
                        adapter.update(offset, limit);
                        break;
                    case Const.STATE_ERROR_RESPONSE:
                    case Const.STATE_SERVER_ERROR:
                        Toast.makeText(getActivity(), "Ошибка сервера", Toast.LENGTH_LONG).show();
                        swipeRefreshLayout.setRefreshing(false);
                        break;
                }
            }
        });

        return v;
    }

    /**
     * Обновить список
     */
    @Override
    public void update() {
        if(adapter != null)
            adapter.update(offset, limit);
    }

    @Override
    public void new_history() {

    }

    /**
     * Прокрутка чата
     */
    public void scrollTop(){
        if(recyclerView != null && !recyclerView.isAnimating()){
            recyclerView.smoothScrollToPosition(0);
        }
    }

    /**
     * Показать смайлы
     */
    public void showSmiles(View v){
        if(v == null){
            return;
        }

        // скрываем клавиатуру
        ((AppActivity)getActivity()).hideKeyboard();
        // hide bottom bar
        ((AppActivity)getActivity()).hideBottomBar();

        // добавляем часто используемые смайлы
        List<SmilesHistoryModel> smilesHistoryList = SmilesHistoryModel.getSmiles();
        // часто используемые смайлы
        for (int i = 0; i < smilesHistoryList.size() && i < SmilesHistoryModel.smile.length; i++){
            SmilesHistoryModel smilesHistoryModel = smilesHistoryList.get(i);
            ImageView smile = (ImageView)v.findViewById(smilesHistoryModel.smile_id);
            if(smile != null){
                ImageView a_smile = (ImageView)v.findViewById(SmilesHistoryModel.smile[i]);
                a_smile.setImageDrawable(smile.getDrawable());
                a_smile.setVisibility(View.VISIBLE);
            }
        }
        if(smiles != null){
            smiles.setVisibility(View.VISIBLE);
        }

        /**
         * Кнопка удаления
         */
        View backspace = v.findViewById(R.id.backspace);
        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int start = message.getSelectionStart();
                if(start<1){
                    return;
                }

                String left_str = message.getEditableText().subSequence(0, start-1).toString();
                CharSequence right_str = "";
                if(start < (message.getEditableText().length() -1)){
                    right_str = message.getEditableText().subSequence(start, message.getEditableText().length());
                }
                String new_str = left_str + right_str;

                if(left_str.length() > 2){
                    start = left_str.length()-1;
                    int last_smile_start = new_str.subSequence(0, start).toString().lastIndexOf(" :smile");
                    int last_smile_end = new_str.subSequence(0, start).toString().lastIndexOf(": ");
                    if(last_smile_start >= 0 && last_smile_end < last_smile_start){
                        left_str = new_str.substring(0, last_smile_start+1);
                        new_str = left_str + right_str;
                    }
                }

                message.setText(new_str);
                message.setSelection(left_str.length());

                Smiles.setSmiles(new_str, message, smile_size, getActivity());
            }
        });

        // меняем занчёк на кнопке
        if(button_smile != null){
            button_smile.setImageResource(R.drawable.ic_keyboard);
        }
    }

    /**
     * Скрыть смайлы
     */
    public void hideSmiles(){
        if(smiles != null){
            smiles.setVisibility(View.GONE);
        }
        // меняем занчёк на кнопке
        if(button_smile != null){
            button_smile.setImageResource(R.drawable.ic_smile);
        }
    }

    /**
     * Добавление смайликов
     * @param v
     */
    public void onClickSmile(View v){
        String smile = Smiles.SmileToText(v.getId());
        int selectionCursor = message.getSelectionStart();
        Smiles.setSmiles(message.getText().insert(selectionCursor, smile).toString(), message, smile_size, getActivity());
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    ((BaseActivity)getActivity()).logout();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    break;
                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:

                    break;
            }
        }
    };

}
