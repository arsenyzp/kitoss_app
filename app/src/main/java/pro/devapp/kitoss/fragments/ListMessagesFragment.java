package pro.devapp.kitoss.fragments;


import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.InviteContactActivity;
import pro.devapp.kitoss.adapters.MessagesAdapter;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.models.UserModel;

/**
 * Список сообщений
 */
public class ListMessagesFragment extends Fragment {

    private RecyclerView recyclerView;
    private MessagesAdapter adapter;
    private static int winHeight = 0;

    /**
     * Get instance
     */
    public static ListMessagesFragment newInstance() {
        ListMessagesFragment fragment = new ListMessagesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(winHeight == 0){
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            winHeight = size.y;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list_messages, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.list);
        adapter = new MessagesAdapter(getActivity());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        Button share = (Button) v.findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), InviteContactActivity.class);
                getActivity().startActivity(intent);
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        update();
    }

    public void showFriendRequestButton(Boolean show){
        View v = getView();
        if(v != null){
            Button share = (Button) v.findViewById(R.id.share);
            if(show){
                share.setVisibility(View.VISIBLE);
            } else {
                share.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Обновление списка
     */
    public void update(){
        adapter.update(((BaseActivity)getActivity()).userModel.id);
        showFriendRequestButton(adapter.getItemCount() < 5);
    }
}
