package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Истории
 */
public interface Histories {

    @FormUrlEncoded
    @POST("api/histories")
    Call<ResponseModel> get(
            @Header("Authorization") String token,
            @Field("offset") int offset,
            @Field("limit") int limit
    );

    @FormUrlEncoded
    @POST("api/my_histories")
    Call<ResponseModel> getMy(
            @Header("Authorization") String token,
            @Field("offset") int offset,
            @Field("limit") int limit
    );

    @FormUrlEncoded
    @POST("api/add_history")
    Call<ResponseModel> send(
            @Header("Authorization") String token,
            @Field("text") String text
    );

    @FormUrlEncoded
    @POST("api/like")
    Call<ResponseModel> like(
            @Header("Authorization") String token,
            @Field("id") String id
    );

}
