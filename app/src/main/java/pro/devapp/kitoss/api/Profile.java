package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Api профиля
 */
public interface Profile {

    /**
     * Обновление профиля
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("api/profile")
    Call<ResponseModel> save(
            @Header("Authorization") String token,
            @Field("first_name") String first_name,
            @Field("gender") String gender,
            @Field("birth_date") String birth_date,
            @Field("city") String city,
            @Field("where_go") String where_go,
            @Field("my_status") String my_status,
            @Field("about_me") String about_me
            );

    /**
     * Получение профиля
     * @param token
     * @return
     */
    @GET("api/profile")
    Call<ResponseModel> get(@Header("Authorization") String token);

    /**
     * Обновление профиля
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("api/set_region")
    Call<ResponseModel> set_region(
            @Header("Authorization") String token,
            @Field("region") String region
    );

    /**
     * Обновление статуса
     * @param token
     * @return
     */
    @FormUrlEncoded
    @POST("api/set_status")
    Call<ResponseModel> set_status(
            @Header("Authorization") String token,
            @Field("my_status") String my_status
    );

    /**
     * Сохранить промо код
     * @param token
     * @param promo_cod
     * @return
     */
    @FormUrlEncoded
    @POST("api/save_promo")
    Call<ResponseModel> save_promo(
            @Header("Authorization") String token,
            @Field("promo_cod") String promo_cod
    );
}
