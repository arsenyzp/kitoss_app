package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Подгрузка public сообщений
 */
public interface PublicMessages {

    @FormUrlEncoded
    @POST("api/public_messages")
    Call<ResponseModel> messages(
            @Header("Authorization") String token,
            @Field("offset") int offset,
            @Field("limit") int limit,
            @Field("region") String region
    );
}
