package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Api административных функций
 */
public interface Admins {

    /**
     * Принять историю
     * @param token
     * @param id
     * @return
     */
    @FormUrlEncoded
    @POST("api/accept_history")
    Call<ResponseModel> accept_history(
            @Header("Authorization") String token,
            @Field("id") String id
    );

    /**
     * Отклонить историю
     * @param token
     * @param id
     * @return
     */
    @FormUrlEncoded
    @POST("api/decline_history")
    Call<ResponseModel> decline_history(
            @Header("Authorization") String token,
            @Field("id") String id
    );

    /**
     * Удалить публичное сообщение
     * @param token
     * @param id
     * @return
     */
    @FormUrlEncoded
    @POST("api/remove_public_message")
    Call<ResponseModel> remove_public_message(
            @Header("Authorization") String token,
            @Field("id") String id
    );

    /**
     * Заблокровать пользователю эфир
     * @param token
     * @param id
     * @return
     */
    @FormUrlEncoded
    @POST("api/block_user_in_public_chat")
    Call<ResponseModel> block_user_in_public_chat(
            @Header("Authorization") String token,
            @Field("id") String id
    );

    /**
     * Удалить аватар
     * @param token
     * @param id
     * @return
     */
    @FormUrlEncoded
    @POST("api/delete_avatar")
    Call<ResponseModel> delete_avatar(
            @Header("Authorization") String token,
            @Field("id") String id
    );

    /**
     * Удалить фон
     * @param token
     * @param id
     * @return
     */
    @FormUrlEncoded
    @POST("api/delete_bg")
    Call<ResponseModel> delete_bg(
            @Header("Authorization") String token,
            @Field("id") String id
    );
}
