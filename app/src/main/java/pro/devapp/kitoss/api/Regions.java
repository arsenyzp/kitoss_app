package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Api получения списка регионов
 */
public interface Regions {
    /**
     * Получение всего списка
     * @return
     */
    @GET("api/regions")
    Call<ResponseModel> get();
}
