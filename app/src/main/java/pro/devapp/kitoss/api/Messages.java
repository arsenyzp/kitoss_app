package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Запрос своих сообщений
 */
public interface Messages {
    @FormUrlEncoded
    @POST("api/private_messages")
    Call<ResponseModel> messages(
            @Header("Authorization") String token,
            @Field("last_id") String last_id
    );

    @POST("api/last_messages")
    Call<ResponseModel> last_messages(
            @Header("Authorization") String token
    );

    @FormUrlEncoded
    @POST("api/messages_by_user")
    Call<ResponseModel> messages_by_user(
            @Header("Authorization") String token,
            @Field("oponent_id") String oponent_id
    );
}
