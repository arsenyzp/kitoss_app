package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Клиентская авторизация через G+
 */
public interface GPAuth {
    @FormUrlEncoded
    @POST("api/gpauth")
    Call<ResponseModel> gpauth(@Field("email") String email, @Field("id") String id, @Field("name") String name);
}
