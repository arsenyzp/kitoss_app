package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Авторизация через G+
 */
public interface GAuth {
    @FormUrlEncoded
    @POST("api/gauth")
    Call<ResponseModel> gauth(@Field("token") String token);
}
