package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Регистрация устройства для push уведомлений
 */
public interface RegisterDevicePush {
    @FormUrlEncoded
    @POST("api/device")
    Call<ResponseModel> add_device(@Header("Authorization") String token, @Field("gtoken") String gtoken);

    @FormUrlEncoded
    @POST("api/remove_device")
    Call<ResponseModel> remove_device(@Header("Authorization") String token, @Field("gtoken") String gtoken);
}
