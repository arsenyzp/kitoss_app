package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Регистрация пользователя
 */
public interface RegisterUser {
    @FormUrlEncoded
    @POST("api/register")
    Call<ResponseModel> register(
            @Field("phone") String phone,
            @Field("first_name") String first_name,
            @Field("gender") String gender,
            @Field("promo_cod") String promo_cod,
            @Field("birth_date") String birth_date
            );
}