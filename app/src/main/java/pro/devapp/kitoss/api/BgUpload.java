package pro.devapp.kitoss.api;

import okhttp3.MultipartBody;
import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Api загрузки фоны
 */
public interface BgUpload {
    @Multipart
    @POST("api/bg")
    Call<ResponseModel> upload(@Header("Authorization") String token, @Part MultipartBody.Part file);

    @DELETE("api/bg")
    Call<ResponseModel> delete(@Header("Authorization") String token);
}
