package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Авторизация через вк
 */
public interface VkAuth {
    @FormUrlEncoded
    @POST("api/vkauth")
    Call<ResponseModel> vkauth(@Field("token") String token, @Field("user_id") String user_id);
}
