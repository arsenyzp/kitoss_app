package pro.devapp.kitoss.api;

import okhttp3.RequestBody;
import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Загрузка файла
 */
public interface UploadImage {

    @Multipart
    @POST("api/upload")
    Call<ResponseModel> upload(@Header("Authorization") String token, @Part("file\"; filename=\"pp.jpeg\" ") RequestBody file);
}
