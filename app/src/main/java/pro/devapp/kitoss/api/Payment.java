package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Платежи
 */
public interface Payment {

    /**
     * Сохранить платёж
     * @param token
     * @param sku
     * @return
     */
    @FormUrlEncoded
    @POST("api/gpayment")
    Call<ResponseModel> gpayment(
            @Header("Authorization") String token,
            @Field("sku") String sku
    );
}
