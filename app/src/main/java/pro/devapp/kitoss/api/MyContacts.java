package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Api проверки наличия контактов в китосе
 */

public interface MyContacts {

    @FormUrlEncoded
    @POST("api/check_contacts")
    Call<ResponseModel> check(
            @Header("Authorization") String token,
            @Field("numbers") String[] numbers,
            @Field("ids") int[] ids
    );

    @FormUrlEncoded
    @POST("api/send_invite")
    Call<ResponseModel> send_invite(
            @Header("Authorization") String token,
            @Field("phone") String phone
    );

}
