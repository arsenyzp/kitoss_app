package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Игра
 */
public interface Game {

    /**
     * Получить список юзеров для игры
     * @param token
     * @return
     */
    @POST("api/game/get_users")
    Call<ResponseModel> get_users(
            @Header("Authorization") String token
    );

    /**
     * Получить статистику для игры
     * @param token
     * @return
     */
    @POST("api/game/get_statistic")
    Call<ResponseModel> get_statistic(
            @Header("Authorization") String token
    );

    /**
     * Сохранить результат
     * @param token
     * @param user_id
     * @param result
     * @return
     */
    @FormUrlEncoded
    @POST("api/game/set_result")
    Call<ResponseModel> set_result(
            @Header("Authorization") String token,
            @Field("user_id") String user_id,
            @Field("result") int result
    );
}
