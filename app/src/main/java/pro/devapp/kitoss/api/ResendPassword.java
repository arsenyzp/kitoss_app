package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Повторная отправка или восстановление пароля
 */

public interface ResendPassword {
    @FormUrlEncoded
    @POST("api/resend")
    Call<ResponseModel> resend(@Field("phone") String phone);
}
