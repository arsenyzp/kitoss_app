package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Api список вариантов куда пойти
 */
public interface WhereGo {
    /**
     * Получение всего списка
     * @return
     */
    @GET("api/where_go")
    Call<ResponseModel> get();
}
