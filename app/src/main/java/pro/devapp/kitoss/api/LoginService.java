package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Авторизация
 */
public interface LoginService {
    @FormUrlEncoded
    @POST("api/login")
    Call<ResponseModel> login(@Field("phone") String phone, @Field("password") String password);
}
