package pro.devapp.kitoss.api;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Авторизация через ок
 */
public interface OkAuth {
    @FormUrlEncoded
    @POST("api/okauth")
    Call<ResponseModel> okauth(@Field("access_token") String access_token, @Field("session_secret_key") String session_secret_key);
}
