package pro.devapp.kitoss.api;

import java.util.ArrayList;

import pro.devapp.kitoss.models.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Api поиска пользователей
 */
public interface Users {

    /**
     * Добавление в друзья
     * @param token
     * @param user_id
     * @return
     */
    @FormUrlEncoded
    @POST("api/add_friend")
    Call<ResponseModel> addToFriend(
            @Header("Authorization") String token,
            @Field("user_id") String user_id
    );

    /**
     * Удалить из друзей
     * @param token
     * @param user_id
     * @return
     */
    @FormUrlEncoded
    @POST("api/remove_friend")
    Call<ResponseModel> removeFriend(
            @Header("Authorization") String token,
            @Field("user_id") String user_id
    );

    /**
     * Принять дружбу
     * @param token
     * @param user_id
     * @return
     */
    @FormUrlEncoded
    @POST("api/accept_friend")
    Call<ResponseModel> acceptFriend(
            @Header("Authorization") String token,
            @Field("user_id") String user_id
    );


    /**
     * Отклонить дружбу
     * @param token
     * @param user_id
     * @return
     */
    @FormUrlEncoded
    @POST("api/decline_friend")
    Call<ResponseModel> declineFriend(
            @Header("Authorization") String token,
            @Field("user_id") String user_id
    );

    /**
     * Удалить свою заявку на дружбу
     * @param token
     * @param user_id
     * @return
     */
    @FormUrlEncoded
    @POST("api/remove_friend_request")
    Call<ResponseModel> removeFriendRequest(
            @Header("Authorization") String token,
            @Field("user_id") String user_id
    );

    /**
     * Поиск
     * @param token
     * @param q
     * @param offset
     * @param limit
     * @return
     */
    @FormUrlEncoded
    @POST("api/search")
    Call<ResponseModel> search(
            @Header("Authorization") String token,
            @Field("q") String q,
            @Field("offset") int offset,
            @Field("limit") int limit
    );

    /**
     * Поиск
     * @param token
     * @param q
     * @param phone
     * @param age_from
     * @param age_to
     * @param gender
     * @param region
     * @param only_online
     * @param offset
     * @param limit
     * @return
     */
    @FormUrlEncoded
    @POST("api/search")
    Call<ResponseModel> search(
            @Header("Authorization") String token,
            @Field("q") String q,
            @Field("phone") String phone,
            @Field("age_from") int age_from,
            @Field("age_to") int age_to,
            @Field("gender") String gender,
            @Field("region") String region,
            @Field("only_online") Boolean only_online,
            @Field("offset") int offset,
            @Field("limit") int limit
    );

    /**
     * Мои друзья
     * @param token
     * @param offset
     * @param limit
     * @return
     */
    @FormUrlEncoded
    @POST("api/friends")
    Call<ResponseModel> friends(
            @Header("Authorization") String token,
            @Field("offset") int offset,
            @Field("limit") int limit
    );

    /**
     * Избранное
     * @param token
     * @param favorite_ids
     * @return
     */
    @FormUrlEncoded
    @POST("api/favorites")
    Call<ResponseModel> favorites(
            @Header("Authorization") String token,
            @Field("favorite_ids") ArrayList<String> favorite_ids
    );

    /**
     * Чёрный список
     * @param token
     * @return
     */
    @POST("api/black_list")
    Call<ResponseModel> black(
            @Header("Authorization") String token
    );

    /**
     * Добавить в чёрный список
     * @param token
     * @param user_id
     * @return
     */
    @FormUrlEncoded
    @POST("api/add_to_black_list")
    Call<ResponseModel> add_to_black_list(
            @Header("Authorization") String token,
            @Field("user_id") String user_id
    );

    /**
     * Удалить из чёрного списка
     * @param token
     * @param user_id
     * @return
     */
    @FormUrlEncoded
    @POST("api/remove_from_black_list")
    Call<ResponseModel> remove_from_black_list(
            @Header("Authorization") String token,
            @Field("user_id") String user_id
    );

    /**
     * События
     * @param token
     * @param offset
     * @param limit
     * @return
     */
    @FormUrlEncoded
    @POST("api/events")
    Call<ResponseModel> events(
            @Header("Authorization") String token,
            @Field("offset") int offset,
            @Field("limit") int limit
    );

    /**
     * Отправить подарок
     * @param token
     * @param user_id
     * @param gift_name
     * @param message
     * @return
     */
    @FormUrlEncoded
    @POST("api/send_gift")
    Call<ResponseModel> send_gift(
            @Header("Authorization") String token,
            @Field("user_id") String user_id,
            @Field("gift_name") String gift_name,
            @Field("message") String message
    );

    /**
     * Подарки пользователя
     * @param token
     * @param user_id
     * @return
     */
    @FormUrlEncoded
    @POST("api/user_gifts")
    Call<ResponseModel> user_gifts(
            @Header("Authorization") String token,
            @Field("user_id") String user_id
    );

    /**
     * Пожаловаться на пользователя
     * @param token
     * @param user_id
     * @param text
     * @return
     */
    @FormUrlEncoded
    @POST("api/complain")
    Call<ResponseModel> complain(
            @Header("Authorization") String token,
            @Field("user_id") String user_id,
            @Field("text") String text
    );
}
