package pro.devapp.kitoss.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Edit text with franklin_gothic_medium_cond
 */
public class EditTextRoboto extends EditText {


    public EditTextRoboto(Context context)
    {
        super( context);
        init();
    }


    public EditTextRoboto(Context context, AttributeSet attrs)
    {
        super( context, attrs );
        init();
    }


    public EditTextRoboto(Context context, AttributeSet attrs, int defStyle)
    {
        super( context, attrs, defStyle );
        init();
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EditTextRoboto(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super( context, attrs, defStyleAttr, defStyleRes );
        init();
    }

    public void init() {
        // Изменение шрифта
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto_regular.ttf");
        setTypeface(tf);
    }
}
