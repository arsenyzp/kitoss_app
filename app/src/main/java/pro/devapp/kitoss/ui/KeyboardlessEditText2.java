package pro.devapp.kitoss.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.KeyEvent;

import pro.devapp.kitoss.activity.AppActivity;

/**
 * Chat edit text
 */
public class KeyboardlessEditText2 extends EditTextRoboto {

    public KeyboardlessEditText2(Context context) {
        super(context);
    }

    public KeyboardlessEditText2(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KeyboardlessEditText2(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public KeyboardlessEditText2(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super( context, attrs, defStyleAttr, defStyleRes );
    }

    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK &&
                event.getAction() == KeyEvent.ACTION_UP) {
            // Do your thing here
            if(((AppActivity)getContext()).isShowKeyBoard()){
                ((AppActivity)getContext()).onBackPressed();
            }
            return false;
        }
        return super.dispatchKeyEvent(event);
    }
}
