package pro.devapp.kitoss.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Button with roboto_medium
 */
public class ButtonRobotoMedium extends Button {
    public ButtonRobotoMedium(Context context)
    {
        super( context);
        init();
    }
    public ButtonRobotoMedium(Context context, AttributeSet attrs)
    {
        super( context, attrs );
        init();
    }
    public ButtonRobotoMedium(Context context, AttributeSet attrs, int defStyle)
    {
        super( context, attrs, defStyle );
        init();
    }

    public void init() {
        // Изменение шрифта
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto_medium.ttf");
        setTypeface(tf);
    }
}

