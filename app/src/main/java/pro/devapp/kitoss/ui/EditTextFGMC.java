package pro.devapp.kitoss.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Edit text with franklin_gothic_medium_cond
 */
public class EditTextFGMC extends EditText {
    public EditTextFGMC(Context context)
    {
        super( context);
        init();
    }
    public EditTextFGMC(Context context, AttributeSet attrs)
    {
        super( context, attrs );
        init();
    }
    public EditTextFGMC(Context context, AttributeSet attrs, int defStyle)
    {
        super( context, attrs, defStyle );
        init();
    }

    public void init() {
        // Изменение шрифта
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/franklin_gothic_medium_cond.ttf");
        setTypeface(tf);
    }
}
