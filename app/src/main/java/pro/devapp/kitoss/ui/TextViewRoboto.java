package pro.devapp.kitoss.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Text view with franklin_gothic_medium_cond
 */
public class TextViewRoboto extends TextView {
    public TextViewRoboto(Context context)
    {
        super( context);
        init();
    }
    public TextViewRoboto(Context context, AttributeSet attrs)
    {
        super( context, attrs );
        init();
    }
    public TextViewRoboto(Context context, AttributeSet attrs, int defStyle)
    {
        super( context, attrs, defStyle );
        init();
    }

    public void init() {
        // Изменение шрифта
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto_regular.ttf");
        setTypeface(tf);
    }
}

