package pro.devapp.kitoss.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.interfaces.PlayListener;
import pro.devapp.kitoss.models.GameModel;
import pro.devapp.kitoss.models.PublicUserModel;

/**
 * Рулетка
 */
public class GameCanvas extends View {

    private Context context;
    private int ava_size = 60;

    public GameCanvas(Context context) {
        super(context);
        this.context = context;
    }

    public GameCanvas(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public GameCanvas(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GameCanvas(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
    }

    private Bitmap[] bitmaps;
    private String[] user_ids;
    private int count_avatars = 8;
    private int wait_load = count_avatars;
    private List<Target> targets = new ArrayList<>();

    public void loadAvatars(GameModel.Resp resp){
        count_avatars = wait_load = resp.items.size();
        bitmaps = new Bitmap[wait_load];
        user_ids = new String [wait_load];
        ava_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, getResources().getDisplayMetrics());

        for(int i = 0; i < count_avatars; i++)
        {
            final int pos = i;
            Log.d("LOADING", "Start: " + pos);
            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    // loaded bitmap is here (bitmap)
                    Log.i("LOADING", "Ok " + pos);
                    bitmaps[pos] = getCroppedBitmap(bitmap, ava_size);
                    wait_load--;
                    drawAvatars();
                    targets.remove(this);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    Log.d("LOADING", "Error: " + pos);
                    bitmaps[pos] = getCroppedBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.gift_cup), ava_size);
                    wait_load--;
                    drawAvatars();
                    targets.remove(this);
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    Log.d("LOADING", "Loading: " + pos);
                }
            };
            targets.add(target);
            Picasso.with(context)
                    .load(Const.API_URL + resp.items.get(pos).avatar)
                    .error(R.drawable.gift_cup)
                    .into(target);
            user_ids[pos] = resp.items.get(pos).id;
        }
        Log.d("LOADING", "Size: "+targets.size());
    }

    public void drawAvatars(){
        Log.d("LOADING", "Loaded: " + wait_load);
        if(wait_load != 0){
            return;
        }

        setBackgroundResource(R.drawable.fortune);
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.height = getWidth();
        setLayoutParams(layoutParams);

        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(wait_load == 0){
            int viewWidth = getWidth();
            int viewHeight = getHeight();
            canvas.translate(viewWidth/2, viewHeight/2);

//            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 23, getResources().getDisplayMetrics());
//            int py = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, getResources().getDisplayMetrics());

            int px = viewWidth/17;
            int py = viewWidth/27;



            canvas.rotate(-135);
            for(int i = 0; i < count_avatars; i++) {
                Matrix matrix = new Matrix();
                matrix.setRotate(135, px, py);
                matrix.postTranslate(bitmaps[i].getHeight(), bitmaps[i].getWidth());
                canvas.drawBitmap(bitmaps[i], matrix, null);
                canvas.rotate(45);
            }
        }
    }

    public void run(final PlayListener playListener){
        Random r = new Random();
        final int sector = r.nextInt(8 - 1) + 1;
        int angle = (sector)*(360/8)+360;
        RotateAnimation rotateAnimation = new RotateAnimation(0, angle,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setDuration(500);
        rotateAnimation.setRepeatCount(5);
        rotateAnimation.setFillAfter(true);
        rotateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                playListener.onPlayStart();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                playListener.onPlayEnd(user_ids[8 - sector], 8 - sector);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        startAnimation(rotateAnimation);
    }

    public Bitmap getCroppedBitmap(Bitmap bmp, int radius) {
        Bitmap sbmp;
        if (bmp.getWidth() != radius || bmp.getHeight() != radius) {
            float smallest = Math.min(bmp.getWidth(), bmp.getHeight());
            float factor = smallest / radius;
            sbmp = Bitmap.createScaledBitmap(bmp, (int)(bmp.getWidth() / factor), (int)(bmp.getHeight() / factor), false);
        } else {
            sbmp = bmp;
        }

        Bitmap output = Bitmap.createBitmap(radius, radius,
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        final Paint stroke = new Paint();

        final Rect rect = new Rect(0, 0, radius, radius);

        paint.setAntiAlias(true);
        stroke.setAntiAlias(true);

        paint.setFilterBitmap(true);
        stroke.setFilterBitmap(true);

        paint.setDither(true);
        stroke.setDither(true);

        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(Color.parseColor("#ffffff"));
        stroke.setColor(Color.parseColor("#ffffff"));
        stroke.setStyle(Paint.Style.STROKE);
        stroke.setStrokeWidth(4f);
        canvas.drawCircle(radius / 2 + 0.7f,
                radius / 2 + 0.7f, radius / 2 + 0.1f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(sbmp, rect, rect, paint);

        canvas.drawCircle(radius / 2 + 0.7f,
                radius / 2 + 0.7f, radius / 2 - stroke.getStrokeWidth()/2 +0.1f, stroke);

        return output;
    }
}
