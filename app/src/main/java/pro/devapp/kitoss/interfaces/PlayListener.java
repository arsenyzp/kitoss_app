package pro.devapp.kitoss.interfaces;

public interface PlayListener{
    public void onPlayStart();

    public void onPlayEnd(String user_id, int result);
}
