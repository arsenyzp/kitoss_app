package pro.devapp.kitoss.interfaces;


import android.support.v4.app.Fragment;
import android.view.View;

import pro.devapp.kitoss.fragments.chat.PublicChatFragment;

/**
 * Интерфейс для фрагментов со смайликами
 */
public interface SmilesFragment {

    /**
     * Обновление списка
     */
    public void update();

    /**
     * Клик назад
     */
    public boolean onBackPressed();

    /**
     * Клик по смайлу
     * @param v
     */
    public void onClickSmile(View v);
}
