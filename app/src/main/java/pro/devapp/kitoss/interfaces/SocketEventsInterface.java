package pro.devapp.kitoss.interfaces;

import org.json.JSONObject;

import pro.devapp.kitoss.models.PrivatChatModel;
import pro.devapp.kitoss.models.PublicChatModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.UserModel;

/**
 * События в соккете
 */
public interface SocketEventsInterface {

    /**
     * Новая заявка в друзья
     */
    public void onSocketNewFriendsRequest(PublicUserModel publicUserModel);

    /**
     * Вас удалили из друщей
     */
    public void onSocketRemoveFromFriends(PublicUserModel publicUserModel);

    /**
     * Пользователь отменил свою заявку на дружбу
     */
    public void onSocketCancelFriendRequest(PublicUserModel publicUserModel);

    /**
     * Вашу заяку отклонили
     */
    public void onSocketDeclineFriendRequest(PublicUserModel publicUserModel);

    /**
     * Вашу заявку приняли
     */
    public void onSocketAcceptFriendRequest(PublicUserModel publicUserModel);

    /**
     * Новое сообщение
     */
    public void onSocketNewMessage(PrivatChatModel privatChatModel);

    /**
     * Новое публичное сообщение
     */
    public void onSocketNewPublicMessage(PublicChatModel publicChatModel);

    /**
     * Пополнение баланса
     */
    public void onSocketUpdateBalance(UserModel userModel);

    /**
     * Новый подарок
     */
    public void onSocketNewGift();

    void onSocketRemovePublicMessage(PublicChatModel publicChatModel);

    /**
     * Печатает
     */
    public void onSocketTyping(JSONObject jsonObject);

    /**
     * Новое событие в ленте
     */
    public void onSocketNewEvent();

}
