package pro.devapp.kitoss.interfaces;

/**
 * Скрытие и показ клавиатуры
 */
public interface KeyBoardDetectInterface {

    public void onKeyBoardShow();

    public void onKeyBoardHide();

}
