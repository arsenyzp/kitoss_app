package pro.devapp.kitoss.adapters;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.PublicProfileActivity;
import pro.devapp.kitoss.activity.UserChatActivity;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.Smiles;
import pro.devapp.kitoss.components.UserHelper;
import pro.devapp.kitoss.models.PublicChatModel;
import pro.devapp.kitoss.models.PublicUserModel;

/**
 * Адаптер списка пользователей
 */
public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder>{
    List<PublicUserModel> users = new ArrayList<>();
    private Activity activity;
    private Handler handler;

    public boolean view_is_list = true;
    public int ava_h = 0;
    public int ava_h_margin = 0;

    int smile_size = 25;
    int gender_size = 25;

    public UsersAdapter(Activity activity, Handler handler){
        this.activity = activity;
        this.handler = handler;
        smile_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, activity.getResources().getDisplayMetrics());
        gender_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, activity.getResources().getDisplayMetrics());
    }

    /**
     * Обновляем пользователей из базы данных
     */
    public void update(){
        users = (List<PublicUserModel>)PublicUserModel.fetchResultCursor();
        notifyDataSetChanged();
    }

    public void add(List<PublicUserModel> publicChatModel){
        ((List<PublicUserModel>)users).addAll(publicChatModel);
//        users.clear();
       // users = publicChatModel;
        notifyDataSetChanged();
    }

    /**
     * Обновляем списо с определённым условием
     * @param type
     */
    public void update(String type){
        switch (type){
            case Const.FILTER_ONLINE:
                updateOnlineFriends();
                break;
            case Const.FILTER_OFFLINE:
                updateOfflineFriends();
                break;
            case Const.FILTER_REQUEST:
                updateRequestFriends();
                break;
        }
    }

    /**
     * Обновляем пользователей из базы данных определённого status
     */
    public void updateOnlineFriends(){
        users = (List<PublicUserModel>)PublicUserModel.fetchResultCursor(Const.FRIEND_STATUS_FRIEND, true);
        notifyDataSetChanged();
    }

    /**
     * Друзья оффлайн
     */
    public void updateOfflineFriends(){
        users = (List<PublicUserModel>)PublicUserModel.fetchResultCursor(Const.FRIEND_STATUS_FRIEND, false);
        notifyDataSetChanged();
    }

    /**
     * Запросы в друзья
     */
    public void updateRequestFriends(){
        users = (List<PublicUserModel>)PublicUserModel.fetchResultCursor(Const.FRIEND_STATUS_WANT_FRIEND);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final PublicUserModel userModel = users.get(position);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, PublicProfileActivity.class);
                intent.putExtra("uid", userModel.id);
                activity.startActivity(intent);
            }
        };

        holder.ava.setOnClickListener(listener);
        holder.layoutList.setOnClickListener(listener);
        holder.layoutGrid.setOnClickListener(listener);

        if(userModel.is_online > 0){
            holder.offline.setVisibility(View.GONE);
            holder.online.setVisibility(View.VISIBLE);
        } else {
            holder.online.setVisibility(View.GONE);
            holder.offline.setVisibility(View.VISIBLE);
        }

        // переключение между типами списка
        if(ava_h != 0){
            ViewGroup.LayoutParams layoutParams = holder.ava.getLayoutParams();
            ViewGroup.LayoutParams layoutParams_offline = holder.offline.getLayoutParams();
            ViewGroup.LayoutParams layoutParams_online = holder.online.getLayoutParams();
            if(view_is_list){
                holder.layoutList.setVisibility(View.VISIBLE);
                holder.delimiter.setVisibility(View.VISIBLE);
                holder.item_menu.setVisibility(View.VISIBLE);
                holder.layoutGrid.setVisibility(View.GONE);
                layoutParams.width = ava_h;
                layoutParams.height = ava_h;


                if (layoutParams_offline instanceof ViewGroup.MarginLayoutParams) {
                    ((ViewGroup.MarginLayoutParams)layoutParams_offline).setMargins((int)(ava_h_margin), (int)(ava_h_margin), 0, 0);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        ((ViewGroup.MarginLayoutParams)layoutParams_offline).setMarginStart((int)(ava_h_margin));
                    }
                }
                if (layoutParams_online instanceof ViewGroup.MarginLayoutParams) {
                    ((ViewGroup.MarginLayoutParams)layoutParams_online).setMargins((int)(ava_h_margin), (int)(ava_h_margin), 0, 0);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        ((ViewGroup.MarginLayoutParams)layoutParams_online).setMarginStart((int)(ava_h_margin));
                    }
                }

            } else {
                holder.layoutList.setVisibility(View.GONE);
                holder.delimiter.setVisibility(View.GONE);
                holder.item_menu.setVisibility(View.GONE);
                holder.layoutGrid.setVisibility(View.VISIBLE);

                float zoom = 1.8f;

                layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                layoutParams.height = (int)(ava_h*zoom);

                double m = (int)(ava_h_margin*zoom*(zoom*0.7));

                if (layoutParams_offline instanceof ViewGroup.MarginLayoutParams) {
                    ((ViewGroup.MarginLayoutParams)layoutParams_offline).setMargins((int)(m), (int)(ava_h_margin*zoom), 0, 0);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        ((ViewGroup.MarginLayoutParams)layoutParams_offline).setMarginStart((int)(m));
                    }
                }
                if (layoutParams_online instanceof ViewGroup.MarginLayoutParams) {
                    ((ViewGroup.MarginLayoutParams)layoutParams_online).setMargins((int)(m), (int)(ava_h_margin*zoom), 0, 0);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        ((ViewGroup.MarginLayoutParams)layoutParams_online).setMarginStart((int)(m));
                    }
                }
            }
            holder.ava.setLayoutParams(layoutParams);
            holder.offline.setLayoutParams(layoutParams_offline);
            holder.online.setLayoutParams(layoutParams_online);
        }

        holder.name.setText(userModel.first_name);
        int old = UserHelper.getOld(userModel.birth_date);
        holder.age.setText(String.valueOf(old));

        Drawable img = UserHelper.getGenderDrawable(userModel.gender, activity);
        img.setBounds( 0, 0, gender_size, gender_size );
        holder.age.setCompoundDrawables(null, null, img, null);

        holder.name_grid.setText(userModel.first_name);
        holder.age_grid.setText(String.valueOf(old));
        holder.age_grid.setCompoundDrawables(null, null, img, null);

        final String ava = userModel.avatar;

        Picasso.with(activity)
                .load(Const.API_URL + ava)
                .placeholder(R.drawable.ava)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.ava, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(activity)
                                .load(Const.API_URL + ava)
                                .placeholder(R.drawable.ava)
                                .error(R.drawable.ava)
                                .into(holder.ava);
                    }
                });

        Smiles.setSmiles(userModel.my_status, holder.status, smile_size, activity);

        // меню
        holder.item_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PopupMenu popupMenu = new PopupMenu(activity, view);
                final Menu menu = popupMenu.getMenu();
                popupMenu.getMenuInflater().inflate(R.menu.user_search_item, menu);

                MenuItem item = popupMenu.getMenu().getItem(0);
                MenuItem item_rr = popupMenu.getMenu().getItem(1);


                String friend_status_for_me = userModel.friend_status;

                switch (friend_status_for_me){
                    // друг
                    case Const.FRIEND_STATUS_FRIEND:
                        item.setTitle(activity.getString(R.string.menu_item_remove_from_friend));
                        item_rr.setVisible(false);
                        break;
                    // хочу дружить с ним
                    case Const.FRIEND_STATUS_WISH_FRIEND:
                        item.setTitle(activity.getString(R.string.menu_item_remove_friend_request));
                        item_rr.setVisible(false);
                        break;
                    // хочет дружить со мной
                    case Const.FRIEND_STATUS_WANT_FRIEND:
                        item.setTitle(activity.getString(R.string.menu_item_accept_request));
                        item_rr.setVisible(true);
                        break;
                    // не знаю его
                    case Const.FRIEND_STATUS_NOTKNOW:
                    default:
                        item.setTitle(activity.getString(R.string.menu_item_add_to_friend));
                        item_rr.setVisible(false);
                        break;
                }

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String friend_status_for_me = userModel.friend_status;
                        String user_id = userModel.id;
                        switch (item.getItemId()) {
                            case R.id.action_add_or_remove_friends: // добавить/удалить из друзей
                                switch (friend_status_for_me){
                                    // друг
                                    case Const.FRIEND_STATUS_FRIEND:
                                        // удаляем из друзей
                                        ((BaseActivity)activity).removeFromFriends(user_id, handler);
                                        item.setTitle(activity.getString(R.string.menu_item_add_to_friend));
                                        PublicUserModel.setFriendStatus(user_id, Const.FRIEND_STATUS_NOTKNOW);
                                        break;
                                    // хочу дружить с ним
                                    case Const.FRIEND_STATUS_WISH_FRIEND:
                                        // отменяем свой запрос/отписываемся от юзера
                                        ((BaseActivity)activity).removeMyFriendRequest(user_id, handler);
                                        item.setTitle(activity.getString(R.string.menu_item_add_to_friend));
                                        PublicUserModel.setFriendStatus(user_id, Const.FRIEND_STATUS_NOTKNOW);
                                        break;
                                    // хочет дружить со мной
                                    case Const.FRIEND_STATUS_WANT_FRIEND:
                                        // принимаем заяку
                                        ((BaseActivity)activity).acceptFriendRequest(user_id, handler);
                                        item.setTitle(activity.getString(R.string.menu_item_remove_friend_request));
                                        PublicUserModel.setFriendStatus(user_id, Const.FRIEND_STATUS_FRIEND);
                                        break;
                                    // не знаю его
                                    case Const.FRIEND_STATUS_NOTKNOW:
                                    default:
                                        // отправляем запрос на дружбу
                                        ((BaseActivity)activity).addToFriends(user_id, handler);
                                        item.setTitle(activity.getString(R.string.menu_item_remove_friend_request));
                                        PublicUserModel.setFriendStatus(user_id, Const.FRIEND_STATUS_WISH_FRIEND);
                                        break;
                                }

                                break;
                            case R.id.action_decline_request: // отклонить запрос на дружбу
                                ((BaseActivity)activity).declineFriendRequest(user_id, handler);
                                item.setTitle(activity.getString(R.string.menu_item_add_to_friend));
                                PublicUserModel.setFriendStatus(user_id, Const.FRIEND_STATUS_NOTKNOW);
                                break;
                            case R.id.action_send_message: // отправить сообщение
                                Intent intent = new Intent(activity, UserChatActivity.class);
                                intent.putExtra("uid", user_id);
                                activity.startActivity(intent);
                                break;
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if(users == null){
            return 0;
        }
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case
        public ImageView ava;
        public TextView name;
        public TextView age;
        public TextView name_grid;
        public TextView age_grid;
        public TextView status;
        public ImageView item_menu;
        public View delimiter;
        public RelativeLayout layoutList;
        public LinearLayout layoutGrid;
        public ImageView online;
        public ImageView offline;

        public ViewHolder(View v) {
            super(v);
            ava = (ImageView) v.findViewById(R.id.ava);
            name = (TextView) v.findViewById(R.id.name);
            age = (TextView) v.findViewById(R.id.age);
            age_grid = (TextView) v.findViewById(R.id.age_grid);
            name_grid = (TextView) v.findViewById(R.id.name_grid);
            status = (TextView) v.findViewById(R.id.status);
            item_menu = (ImageView) v.findViewById(R.id.item_menu);
            layoutList = (RelativeLayout) v.findViewById(R.id.relativeLayout);
            layoutGrid = (LinearLayout) v.findViewById(R.id.relativeLayoutGrid);
            delimiter = v.findViewById(R.id.delimiter);
            online = (ImageView) v.findViewById(R.id.online);
            offline = (ImageView) v.findViewById(R.id.offline);
        }
    }
}
