package pro.devapp.kitoss.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.util.AsyncListUtil;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.PublicProfileActivity;
import pro.devapp.kitoss.activity.UserChatActivity;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.Smiles;
import pro.devapp.kitoss.components.UserHelper;
import pro.devapp.kitoss.models.PrivatChatModel;
import pro.devapp.kitoss.models.PublicUserModel;

/**
 * Адаптер списка сообщений
 */
public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder>{
    List<PrivatChatModel> messages;
    private Activity activity;
    int smile_size = 25;
    int gender_size = 25;

    public MessagesAdapter(Activity activity){
        this.activity = activity;
        smile_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, activity.getResources().getDisplayMetrics());
        gender_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, activity.getResources().getDisplayMetrics());
    }

    public PrivatChatModel getItem(int position){
        return messages.get(position);
    }

    /**
     * Обновляем пользователей из базы данных
     */
    public void update(final String user_id){
        messages = PrivatChatModel.getLatests(user_id);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_message, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final PrivatChatModel message = messages.get(position);

        final PublicUserModel publicUserModel;
        if(message.sender_id.equals(((BaseActivity)activity).userModel.id)){
            publicUserModel = PublicUserModel.getById(message.recipient_id);
//            message.is_read = 1;
        } else {
            publicUserModel = PublicUserModel.getById(message.sender_id);
        }

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, PublicProfileActivity.class);
                intent.putExtra("uid", publicUserModel.id);
                activity.startActivity(intent);
            }
        };

        holder.ava.setOnClickListener(listener);

        View.OnClickListener listener_message = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(publicUserModel == null){
                    return;
                }
                Intent intent = new Intent(activity, UserChatActivity.class);
                intent.putExtra("uid", publicUserModel.id);
                activity.startActivity(intent);
            }
        };

        holder.message_place.setOnClickListener(listener_message);

        if(publicUserModel != null){
            holder.name.setText(publicUserModel.first_name);

            int old = UserHelper.getOld(publicUserModel.birth_date);
            holder.age.setText(String.valueOf(old));

            Drawable img = UserHelper.getGenderDrawable(publicUserModel.gender, activity);
            img.setBounds( 0, 0, gender_size, gender_size );
            holder.age.setCompoundDrawables(null, null, img, null);

            if(publicUserModel.is_online > 0){
                holder.offline.setVisibility(View.GONE);
                holder.online.setVisibility(View.VISIBLE);
            } else {
                holder.online.setVisibility(View.GONE);
                holder.offline.setVisibility(View.VISIBLE);
            }

            final String ava = publicUserModel.avatar;

            Picasso.with(activity)
                    .load(Const.API_URL + ava)
                    .placeholder(R.drawable.ava)
                    //.networkPolicy(NetworkPolicy.OFFLINE)
                    .into(holder.ava, new Callback() {
                        @Override
                        public void onSuccess() {

                        }
                        @Override
                        public void onError() {
                            //Try again online if cache failed
                            Picasso.with(activity)
                                    .load(Const.API_URL + ava)
                                    .placeholder(R.drawable.ava)
                                    .error(R.drawable.ava)
                                    .into(holder.ava);
                        }
                    });
        }

        String messag = message.message.replaceAll("\r", " ").replaceAll("\n", " ");
        if(messag.contains(":image:")){
            holder.message.setText("**Фото**");
        } else if(messag.indexOf("gift::") == 0){
            holder.message.setText("**Подарок**");
        } else if(messag.indexOf("bonuses::") == 0){
            holder.message.setText("**Бонус**");
        } else {
            Smiles.setSmiles(messag, holder.message, smile_size, activity);
        }
        holder.date.setReferenceTime(message.time);

        if(message.is_read != 1 && !message.sender_id.equals(((BaseActivity)activity).userModel.id)){
            holder.message_place.setBackgroundColor(activity.getResources().getColor(R.color.white_red));
        } else {
            holder.message_place.setBackgroundColor(activity.getResources().getColor(R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        if(messages == null){
            return 0;
        }
        return messages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case
        public ImageView ava;
        public TextView name;
        public TextView age;
        public TextView message;
        public RelativeTimeTextView date;
        public ImageView online;
        public ImageView offline;
        public RelativeLayout message_place;

        public ViewHolder(View v) {
            super(v);
            ava = (ImageView) v.findViewById(R.id.ava);
            name = (TextView) v.findViewById(R.id.name);
            age = (TextView) v.findViewById(R.id.age);
            message = (TextView) v.findViewById(R.id.message);
            date = (RelativeTimeTextView) v.findViewById(R.id.date);
            online = (ImageView) v.findViewById(R.id.online);
            offline = (ImageView) v.findViewById(R.id.offline);
            message_place = (RelativeLayout) v.findViewById(R.id.message_place);
        }
    }
}
