package pro.devapp.kitoss.adapters;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.util.List;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.models.MyHistoriesModel;

/**
 * Адаптер для моих историй
 */
public class MyHistoriesAdapter extends RecyclerView.Adapter<MyHistoriesAdapter.ViewHolder>{
    private List<MyHistoriesModel> items;
    private Activity activity;
    private int smile_size = 25;

    public MyHistoriesAdapter(Activity activity){
        this.activity = activity;
        smile_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, activity.getResources().getDisplayMetrics());
    }

    public void update(final int offset, final int limit){
        items = MyHistoriesModel.get(0, offset +limit, smile_size, activity);
        notifyDataSetChanged();
    }

    public void updateAsync(final int offset, final int limit){
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            int old_size = 0;

            @Override
            protected Void doInBackground(Void... voids) {
                old_size = getItemCount();
                items = MyHistoriesModel.get(0, offset +limit, smile_size, activity);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                notifyDataSetChanged();
            }
        };
        task.execute();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_history, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final MyHistoriesModel item = items.get(position);

        holder.number.setText("#" + String.valueOf(item.number));
        holder.text.setText(item.spannableText);

        holder.status.setText(MyHistoriesModel.getStatusName(item.status));

        holder.time.setReferenceTime(item.time * 1000l);
    }

    @Override
    public int getItemCount() {
        if(items == null){
            return 0;
        }
        return items.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case
        public TextView number;
        public TextView text;
        public RelativeTimeTextView time;
        public TextView status;


        public ViewHolder(View v) {
            super(v);

            number = (TextView) v.findViewById(R.id.number);
            text = (TextView) v.findViewById(R.id.text);
            time = (RelativeTimeTextView) v.findViewById(R.id.time);
            status = (TextView) v.findViewById(R.id.status);

        }
    }
}
