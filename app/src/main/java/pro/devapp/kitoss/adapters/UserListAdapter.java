package pro.devapp.kitoss.adapters;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.realm.RealmResults;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.BlackListActivity;
import pro.devapp.kitoss.activity.FavoritesActivity;
import pro.devapp.kitoss.activity.PublicProfileActivity;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.Smiles;
import pro.devapp.kitoss.components.UserHelper;
import pro.devapp.kitoss.models.BlackListModel;
import pro.devapp.kitoss.models.FavoriteModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.UserModel;

/**
 * Адаптер black списка пользователей
 */
public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder>{
    List<PublicUserModel> users;
    private Activity activity;

    int smile_size = 25;
    int gender_size = 25;

    public UserListAdapter(Activity activity){
        this.activity = activity;
        smile_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, activity.getResources().getDisplayMetrics());
        gender_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, activity.getResources().getDisplayMetrics());
    }

    /**
     * Обновляем список
     * @param userModel
     */
    public void updateBlackList(UserModel userModel){
        users = PublicUserModel.fetchBlackCursor(userModel.id);
        notifyDataSetChanged();
    }

    /**
     * Обновляем список
     * @param userModel
     */
    public void updateFavoriteList(UserModel userModel){
        users = PublicUserModel.fetchFavoriteCursor(userModel.id);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_black_user, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final PublicUserModel userModel = users.get(position);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(activity, PublicProfileActivity.class);
                intent.putExtra("uid", userModel.id);
                activity.startActivity(intent);
            }
        };

        holder.ava.setOnClickListener(listener);

        if(userModel.is_online > 0){
            holder.offline.setVisibility(View.GONE);
            holder.online.setVisibility(View.VISIBLE);
        } else {
            holder.online.setVisibility(View.GONE);
            holder.offline.setVisibility(View.VISIBLE);
        }


        holder.name.setText(userModel.first_name);
        int old = UserHelper.getOld(userModel.birth_date);
        holder.age.setText(String.valueOf(old));

        Drawable img = UserHelper.getGenderDrawable(userModel.gender, activity);
        img.setBounds( 0, 0, gender_size, gender_size );
        holder.age.setCompoundDrawables(null, null, img, null);

        final String ava = userModel.avatar;

        Picasso.with(activity)
                .load(Const.API_URL + ava)
                .placeholder(R.drawable.ava)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.ava, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(activity)
                                .load(Const.API_URL + ava)
                                .placeholder(R.drawable.ava)
                                .error(R.drawable.ava)
                                .into(holder.ava);
                    }
                });

        Smiles.setSmiles(userModel.my_status, holder.status, smile_size, activity);

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(activity instanceof BlackListActivity){
                    BlackListModel.remove(userModel.id, ((BaseActivity)activity).userModel.id, ((BaseActivity)activity).token, blackListHandler);
                }
                if(activity instanceof FavoritesActivity){
                    FavoriteModel.remove(userModel.id, ((BaseActivity)activity).userModel.id);
                }
                updateBlackList(((BaseActivity)activity).userModel);
            }
        });

    }

    /**
     * Добавление/удаление из чёрного списка на сервере
     */
    Handler blackListHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1) {
                case Const.STATE_ACCESS_DINEDED:
                    ((BaseActivity)activity).logout();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    break;
                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:
                    Toast.makeText(activity, activity.getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    public int getItemCount() {
        if(users == null){
            return 0;
        }
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case
        public ImageView ava;
        public TextView name;
        public TextView age;
        public TextView status;
        public View delimiter;
        public ImageView online;
        public ImageView offline;
        public ImageButton delete;

        public ViewHolder(View v) {
            super(v);
            ava = (ImageView) v.findViewById(R.id.ava);
            name = (TextView) v.findViewById(R.id.name);
            age = (TextView) v.findViewById(R.id.age);
            status = (TextView) v.findViewById(R.id.status);
            delimiter = v.findViewById(R.id.delimiter);
            online = (ImageView) v.findViewById(R.id.online);
            offline = (ImageView) v.findViewById(R.id.offline);
            delete = (ImageButton) v.findViewById(R.id.delete);
        }
    }
}
