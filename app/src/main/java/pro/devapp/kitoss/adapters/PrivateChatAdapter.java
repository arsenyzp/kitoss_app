package pro.devapp.kitoss.adapters;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmResults;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.UserChatActivity;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.Smiles;
import pro.devapp.kitoss.models.GiftModel;
import pro.devapp.kitoss.models.PrivatChatModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.ui.ChatRelativeTimeTextView;

/**
 * Сообщения приватного чата
 */
public class PrivateChatAdapter extends RecyclerView.Adapter<PrivateChatAdapter.ViewHolder>{

    private List<PrivatChatModel> items;
    private Activity activity;
    private String user_id;
    private String oponent_id;
    private PublicUserModel oponentModel;
    int smile_size = 25;

    private boolean show_files = false;

    public PrivateChatAdapter(Activity activity, String user_id, String oponent_id, PublicUserModel oponentModel){
        this.activity = activity;
        this.user_id = user_id;
        this.oponent_id = oponent_id;
        this.oponentModel = oponentModel;
        smile_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, activity.getResources().getDisplayMetrics());

        show_files = ((App)activity.getApplicationContext()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_SOUND);
        if(!show_files){
            show_files = oponentModel.friend_status.equals(Const.FRIEND_STATUS_FRIEND);
        }
    }

    public void update(int offset, int limit){
        //TODO
        //int old_size = getItemCount();
        setItems(offset, limit);
        notifyDataSetChanged();
        //notifyItemRangeInserted(old_size, getItemCount());
    }

    private void setItems(int offset, int limit){
        if(items == null || items.size() == 0){
            items = PrivatChatModel.fetchResultCursor(user_id, oponent_id, 0 , 0);
        } else {
            items = PrivatChatModel.fetchResultCursor(user_id, oponent_id);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat_message, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        PrivatChatModel privatChatModel = items.get(position);


        final String text = privatChatModel.message;
        String recipient_id = privatChatModel.recipient_id;

        final ImageView imageView;
        final TextView textView;
        final RelativeLayout place;
        if(!recipient_id.equals(user_id)){
            textView = holder.text_r;
            imageView = holder.image_r;
            holder.time_r.setReferenceTime(privatChatModel.time);
            holder.left_place.setVisibility(View.GONE);
            holder.right_place.setVisibility(View.VISIBLE);
            place = holder.right_place;

            //success_delivery
            if(privatChatModel.is_delivery > 0){
                holder.success_delivery.setVisibility(View.VISIBLE);
            } else {
                holder.success_delivery.setVisibility(View.INVISIBLE);
            }

        } else {
            textView = holder.text_l;
            imageView = holder.image_l;
            holder.time_l.setReferenceTime(privatChatModel.time);
            holder.left_place.setVisibility(View.VISIBLE);
            holder.right_place.setVisibility(View.GONE);
            place = holder.left_place;
        }

        //DETECT GIFTS
        if(text.indexOf("gift::")==0 && text.split("::gift").length > 0){
            String gift_name = text.replace("gift::", "");
            gift_name = gift_name.split("::gift")[0];

            imageView.setVisibility(View.VISIBLE);
            imageView.setImageDrawable(activity.getResources().getDrawable(GiftModel.getDrawable(gift_name)));
            textView.setVisibility(View.VISIBLE);
            textView.setText(text.replace("gift::"+gift_name+"::gift", ""));

            place.setBackgroundColor(activity.getResources().getColor(R.color.white_red));
            return;
        }
        else {
            place.setBackgroundColor(Color.parseColor("#00000000"));
        }

        if(text.contains(":image:")){
            imageView.setVisibility(View.VISIBLE);
            textView.setVisibility(View.GONE);

            if(!show_files && recipient_id.equals(user_id)){
                imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_account_plus));
                return;
            }

            final String img_url = text.replace(":image:","");
            // показ полного фото
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((UserChatActivity)activity).showFullPhoto(img_url);
                }
            });
            Picasso.with(activity)
                    .load(img_url)
                    .placeholder(R.drawable.ic_account_plus)
                    //.networkPolicy(NetworkPolicy.OFFLINE)
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {

                        }
                        @Override
                        public void onError() {
                            //Try again online if cache failed
                            Picasso.with(activity)
                                    .load(img_url)
                                    .placeholder(R.drawable.ic_account_plus)
                                    .error(R.drawable.ic_account_plus)
                                    .into(imageView, new Callback() {
                                        @Override
                                        public void onSuccess() {

                                        }
                                        @Override
                                        public void onError() {
                                            textView.setVisibility(View.VISIBLE);
                                            imageView.setVisibility(View.GONE);
                                            Smiles.setSmiles(text, textView, smile_size, activity);
                                        }
                                    });
                        }
                    });
        } else {
            imageView.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
            if(text.contains("bonuses::") && text.contains("::bonuses")){
                place.setBackgroundColor(activity.getResources().getColor(R.color.white_red));
                textView.setText(text.replace("bonuses::", "").replace("::bonuses", ""));
            } else {
                place.setBackgroundColor(Color.parseColor("#00000000"));
                Smiles.setSmiles(text, textView, smile_size, activity);
            }
        }

    }

    @Override
    public int getItemCount() {
        if(items == null){
            return 0;
        }
        return items.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case
        public TextView text_l;
        public TextView text_r;
        public ChatRelativeTimeTextView time_l;
        public ChatRelativeTimeTextView time_r;
        public RelativeLayout left_place;
        public RelativeLayout right_place;
        public ImageView ava;
        public ImageView online;
        public ImageView offline;
        public ImageView image_l;
        public ImageView image_r;
        public ImageView success_delivery;


        public ViewHolder(View v) {
            super(v);
            text_l = (TextView) v.findViewById(R.id.text_l);
            text_r = (TextView) v.findViewById(R.id.text_r);
            time_l = (ChatRelativeTimeTextView) v.findViewById(R.id.time_l);
            time_r = (ChatRelativeTimeTextView) v.findViewById(R.id.time_r);
            left_place = (RelativeLayout) v.findViewById(R.id.left_place);
            right_place = (RelativeLayout) v.findViewById(R.id.right_place);
            ava = (ImageView) v.findViewById(R.id.ava);
            online = (ImageView) v.findViewById(R.id.online);
            offline = (ImageView) v.findViewById(R.id.offline);
            image_l = (ImageView) v.findViewById(R.id.image_l);
            image_r = (ImageView) v.findViewById(R.id.image_r);
            success_delivery = (ImageView) v.findViewById(R.id.success_delivery);
            // переход к профилю
            ava.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((UserChatActivity)activity).goToUserProfile();
                }
            });
            // аватарка
            final String avatar = oponentModel.avatar;
            Picasso.with(activity)
                    .load(Const.API_URL + avatar)
                    .fit()
                    .placeholder(R.drawable.ava)
                    //.networkPolicy(NetworkPolicy.OFFLINE)
                    .into(ava, new Callback() {
                        @Override
                        public void onSuccess() {

                        }
                        @Override
                        public void onError() {
                            //Try again online if cache failed
                            Picasso.with(activity)
                                    .load(Const.API_URL + avatar)
                                    .fit()
                                    .placeholder(R.drawable.ava)
                                    .error(R.drawable.ava)
                                    .into(ava);
                        }
                    });
            // статус
            if(oponentModel.is_online > 0){
                online.setVisibility(View.VISIBLE);
                offline.setVisibility(View.GONE);
            } else {
                online.setVisibility(View.GONE);
                offline.setVisibility(View.VISIBLE);
            }
        }
    }
}
