package pro.devapp.kitoss.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import java.util.List;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.ProfileActivity;
import pro.devapp.kitoss.activity.PublicProfileActivity;
import pro.devapp.kitoss.activity.UserChatActivity;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.Smiles;
import pro.devapp.kitoss.models.EventsModel;
import pro.devapp.kitoss.models.GiftModel;


/**
 * Адаптер событий
 */
public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder>{
    private List<EventsModel> items;
    private Activity activity;
    private int smile_size = 25;

    public EventsAdapter(Activity activity){
        this.activity = activity;
        smile_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, activity.getResources().getDisplayMetrics());
    }

    public void update(final int offset, final int limit){
        items = EventsModel.get(0, offset +limit, smile_size, activity);
        notifyDataSetChanged();
    }

    public void updateAsync(final int offset, final int limit){
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            int old_size = 0;

            @Override
            protected Void doInBackground(Void... voids) {
                old_size = getItemCount();
                items = EventsModel.get(0, offset +limit, smile_size, activity);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                notifyDataSetChanged();
            }
        };
        task.execute();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final EventsModel item = items.get(position);

        // статус
        if(item.user.is_online > 0){
            holder.offline.setVisibility(View.GONE);
            holder.online.setVisibility(View.VISIBLE);
        } else {
            holder.online.setVisibility(View.GONE);
            holder.offline.setVisibility(View.VISIBLE);
        }

        holder.name.setText(item.user.first_name);

        // аватарка
        Picasso.with(activity)
                .load(Const.API_URL + item.user.avatar)
                .fit()
                .placeholder(R.drawable.ava)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.ava, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(activity)
                                .load(Const.API_URL + item.user.avatar)
                                .fit()
                                .placeholder(R.drawable.ava)
                                .error(R.drawable.ava)
                                .into(holder.ava);
                    }
                });

        holder.ava.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, PublicProfileActivity.class);
                if(item.user.id.equals(((BaseActivity)activity).userModel.id)){
                    intent = new Intent(activity, ProfileActivity.class);
                }
                intent.putExtra("uid", item.user.id);
                activity.startActivity(intent);
            }
        });

        holder.event_time.setReferenceTime(item.time * 1000l);

        switch (item.type){
            case "status":
                holder.event_image.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_bullhorn_event_red));
                if(item.user.gender.equals("male")){
                    holder.event_name.setText("Обновил статус");
                } else if(item.user.gender.equals("female")){
                    holder.event_name.setText("Обновила статус");
                }
                holder.image.setVisibility(View.GONE);
                holder.gift.setVisibility(View.GONE);
                holder.status.setVisibility(View.VISIBLE);

                Smiles.setSmiles(item.status, holder.status, smile_size, activity);

                break;
            case "gift":
                if(item.user.gender.equals("male")){
                    holder.event_name.setText("Получил подарок");
                } else if(item.user.gender.equals("female")){
                    holder.event_name.setText("Получила подарок");
                }
                holder.event_image.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_gift_event_red));
                holder.status.setVisibility(View.GONE);
                holder.image.setVisibility(View.GONE);
                holder.gift.setVisibility(View.VISIBLE);
                holder.gift.setImageDrawable(activity.getResources().getDrawable(GiftModel.getDrawable(item.gift)));
                break;
            case "photo":
                if(item.user.gender.equals("male")){
                    holder.event_name.setText("Обновил фото");
                } else if(item.user.gender.equals("female")){
                    holder.event_name.setText("Обновила фото");
                }
                holder.event_image.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_camera_event_red));
                holder.status.setVisibility(View.GONE);
                holder.gift.setVisibility(View.GONE);
                holder.image.setVisibility(View.VISIBLE);
                Picasso.with(activity)
                        .load(Const.API_URL + item.image)
                        .fit()
                        .placeholder(R.drawable.ic_account_plus_blue)
                        //.networkPolicy(NetworkPolicy.OFFLINE)
                        .into(holder.image, new Callback() {
                            @Override
                            public void onSuccess() {

                            }
                            @Override
                            public void onError() {
                                //Try again online if cache failed
                                Picasso.with(activity)
                                        .load(Const.API_URL + item.image)
                                        .fit()
                                        .placeholder(R.drawable.ic_account_plus_blue)
                                        .error(R.drawable.ic_account_plus_blue)
                                        .into(holder.image);
                            }
                        });
                break;
        }
    }

    @Override
    public int getItemCount() {
        if(items == null){
            return 0;
        }
        return items.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_event, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case
        public ImageView ava;
        public ImageView online;
        public ImageView offline;
        public TextView name;

        public ImageView event_image;
        public TextView event_name;
        public RelativeTimeTextView event_time;

        public TextView status;
        public ImageView image;
        public ImageView gift;


        public ViewHolder(View v) {
            super(v);

            ava = (ImageView) v.findViewById(R.id.ava);
            online = (ImageView) v.findViewById(R.id.online);
            offline = (ImageView) v.findViewById(R.id.offline);
            name = (TextView) v.findViewById(R.id.name);

            status = (TextView) v.findViewById(R.id.status);
            image = (ImageView) v.findViewById(R.id.image);
            gift = (ImageView) v.findViewById(R.id.gift);

            event_image = (ImageView) v.findViewById(R.id.event_image);
            event_name = (TextView) v.findViewById(R.id.event_name);
            event_time = (RelativeTimeTextView) v.findViewById(R.id.event_time);

        }
    }
}
