package pro.devapp.kitoss.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.models.GiftModel;

/**
 * Gifts
 */
public class GiftsAdapter extends RecyclerView.Adapter<GiftsAdapter.ViewHolder> {
    private List<GiftModel> items;
    private Activity activity;

    public GiftsAdapter(Activity activity){
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_gift, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public void update(){
        items = GiftModel.get();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final GiftModel item = items.get(position);
        holder.image.setImageDrawable(activity.getResources().getDrawable(GiftModel.getDrawable(item.name)));
        holder.price.setText(activity.getResources().getString(R.string.price_text, item.price+""));
    }

    @Override
    public int getItemCount() {
        if(items == null){
            return 0;
        }
        return items.size();
    }

    public GiftModel getItem(int position){
        return items.get(position);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case
        public ImageView image;
        public TextView price;

        public ViewHolder(View v) {
            super(v);
            image = (ImageView) v.findViewById(R.id.image);
            price = (TextView) v.findViewById(R.id.price);
        }
    }
}
