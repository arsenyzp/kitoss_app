package pro.devapp.kitoss.adapters;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.util.ArrayList;
import java.util.List;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.Smiles;
import pro.devapp.kitoss.models.HistoriesModel;
import pro.devapp.kitoss.models.PublicChatModel;

/**
 * Адаптер для историй
 */
public class HistoriesAdapter extends RecyclerView.Adapter<HistoriesAdapter.ViewHolder>{
    private List<HistoriesModel> items = new ArrayList<>();
    private Activity activity;
    private int smile_size = 25;

    public HistoriesAdapter(Activity activity){
        this.activity = activity;
        smile_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, activity.getResources().getDisplayMetrics());
    }

    public void clear(){
        items.clear();
    }

    public void remove(final HistoriesModel model){
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            int j = -1;
            @Override
            protected Void doInBackground(Void... voids) {
                for(int i = 0; i < items.size(); i++){
                    HistoriesModel model = items.get(i);
                    if(model.id.equals(model.id)){
                        j = i;
                        break;
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if(j >= 0){
                    items.remove(j);
                    notifyItemRemoved(j);
                }
            }
        };
        task.execute();
    }

    public void add(final HistoriesModel publicChatModel){
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            Boolean is = false;
            @Override
            protected Void doInBackground(Void... voids) {
                for(HistoriesModel model : items){
                    if(model.id.equals(publicChatModel.id)){
                        is = true;
                        break;
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if(!is){
                    items.add(publicChatModel);
                    notifyDataSetChanged();
                }
            }
        };
        task.execute();
    }

    public void add(List<HistoriesModel> publicChatModel){
        publicChatModel.addAll(items);
        items.clear();
        items = publicChatModel;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final HistoriesModel item = items.get(position);

        holder.number.setText("#" + String.valueOf(item.number));

        Smiles.setSmiles(item.text, holder.text, smile_size, activity);
        //holder.text.setText(item.spannableText);

        holder.time.setReferenceTime(item.time * 1000l);

        if(item.is_like > 0){
            holder.like.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_heart_red));
        } else {
            holder.like.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_heart_grey));
        }
        holder.likes_count.setText(String.valueOf(item.likes_count));

        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HistoriesModel.like(((BaseActivity) activity).token, item.id, new HistoriesModel.ResponseListener() {
                    @Override
                    public void onSuccess(HistoriesModel historiesModel) {
                        items.remove(position);
                        items.add(position, historiesModel);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onSuccess(List<HistoriesModel> historiesModels) {

                    }

                    @Override
                    public void onError(String error) {

                    }
                });
            }
        });

        if(item.status.equals("new")){
            holder.cardView.setBackgroundColor(activity.getResources().getColor(R.color.grey));
        } else {
            holder.cardView.setBackgroundColor(activity.getResources().getColor(R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        if(items == null){
            return 0;
        }
        return items.size();
    }

    public HistoriesModel getItem(int position){
        return items.get(position);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case
        public TextView number;
        public TextView text;
        public RelativeTimeTextView time;
        public ImageButton like;
        public TextView likes_count;
        public CardView cardView;


        public ViewHolder(View v) {
            super(v);

            number = (TextView) v.findViewById(R.id.number);
            text = (TextView) v.findViewById(R.id.text);
            time = (RelativeTimeTextView) v.findViewById(R.id.time);
            like = (ImageButton) v.findViewById(R.id.like);
            likes_count = (TextView) v.findViewById(R.id.likes_count);
            cardView = (CardView) v.findViewById(R.id.cardView);
        }
    }
}
