package pro.devapp.kitoss.adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.activity.InviteDescriptionActivity;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.ContactHelper;
import pro.devapp.kitoss.components.DialogHelper;
import pro.devapp.kitoss.models.ContactModel;

/**
 * Адаптер для контактов
 */
public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder>{

    Cursor phones;
    private Activity context;
    private ArrayList<Integer> ids = new ArrayList<>(); // писок id которые есть в сервисе
    private String promo = "";

    public ContactsAdapter(Activity context, String promo){
        this.context = context;
        this.promo = promo;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contact, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public void update(){
        //"data4 <> ''"
        phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        ids = ContactModel.getExists();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        phones.moveToPosition(position);

        if(position == 0){
            holder.header_banner.setVisibility(View.VISIBLE);
            holder.promo.setText(context.getString(R.string.promocod_info, promo));
        } else {
            holder.header_banner.setVisibility(View.GONE);
        }

        holder.name.setText(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));

        String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));
        if(number == null){
            number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }
        holder.number.setText(number);

        int cid = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID));
        Log.d(ContactsAdapter.class.getName(), String.valueOf(cid));

        String mThumbnailUri = phones.getString(phones.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));
        if(mThumbnailUri != null && !mThumbnailUri.equals("")){
            Bitmap mThumbnail = ContactHelper.loadContactPhotoThumbnail(mThumbnailUri, context);
            holder.view.setImageBitmap(mThumbnail);
        } else {
            holder.view.setImageResource(R.drawable.ava);
        }

        // если контакт есть в сервисе
        if(ids.indexOf(cid)>-1){
            holder.send_invite.setVisibility(View.GONE);
            holder.status.setVisibility(View.VISIBLE);
        } else {
            holder.send_invite.setVisibility(View.VISIBLE);
            holder.status.setVisibility(View.GONE);
            String num = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));
            if(num == null){
                num = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            }

            final String finalNum = num;
            holder.send_invite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogHelper dialog = new DialogHelper();
                    dialog.Confirm(
                            context,
                            context.getString(R.string.menu_friends_invite_title),
                            context.getString(R.string.menu_friends_invite_text),
                            context.getString(R.string.menu_friends_invite_no),
                            context.getString(R.string.menu_friends_invite_yes),
                            new Runnable() {
                        @Override
                        public void run() {
                            final String token = ((App)context.getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
                            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                                Boolean res = false;
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    res = ContactModel.sendInvite(finalNum, token);
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Void aVoid) {
                                    super.onPostExecute(aVoid);
                                    if(res){
                                        Toast.makeText(context, context.getString(R.string.menu_friends_invite_ok), Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(context, context.getString(R.string.menu_friends_invite_error), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            };
                            task.execute();
                        }
                    }, new Runnable() {
                        @Override
                        public void run() {

                        }
                    });
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(phones == null){
            return 0;
        }
        return phones.getCount();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case
        public TextView name;
        public TextView number;
        public TextView status;
        public ImageView view;
        public ImageView send_invite;
        public CardView header_banner;
        public TextView promo;
        public TextView promo_link;
        public View share;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.name);
            number = (TextView) v.findViewById(R.id.number);
            promo = (TextView) v.findViewById(R.id.promo);
            status = (TextView) v.findViewById(R.id.status);
            view = (ImageView) v.findViewById(R.id.view);
            send_invite = (ImageView) v.findViewById(R.id.send_invite);
            header_banner = (CardView) v.findViewById(R.id.header_banner);

            promo_link = (TextView) v.findViewById(R.id.promo_link);
            promo_link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, InviteDescriptionActivity.class);
                    context.startActivity(intent);
                }
            });

            share = v.findViewById(R.id.share);
            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((BaseActivity)context).share();
                }
            });

        }
    }
}
