package pro.devapp.kitoss.adapters;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.Smiles;
import pro.devapp.kitoss.components.UserHelper;
import pro.devapp.kitoss.models.PublicChatModel;
import pro.devapp.kitoss.models.UserModel;

/**
 * Адаптер для общих чатов
 */
public class PublicChatAdapter extends RecyclerView.Adapter<PublicChatAdapter.ViewHolder>{
    private List<PublicChatModel> items = new ArrayList<>();
    private Activity activity;
    private String region;
    private int smile_size = 25;
    int gender_size = 25;
    private UserModel userModel;


    public PublicChatAdapter(Activity activity, String region){
        this.activity = activity;
        this.region = region;
        smile_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, activity.getResources().getDisplayMetrics());
        gender_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, activity.getResources().getDisplayMetrics());
        userModel = ((BaseActivity)activity).userModel;
    }

    public void clear(){
        items.clear();
    }

    public void remove(final PublicChatModel publicChatModel){
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            int j = -1;
            @Override
            protected Void doInBackground(Void... voids) {
                for(int i = 0; i < items.size(); i++){
                    PublicChatModel model = items.get(i);
                    if(model.uiid.equals(publicChatModel.uiid)){
                        j = i;
                        break;
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if(j >= 0){
                    items.remove(j);
                    notifyItemRemoved(j);
                }
            }
        };
        task.execute();
    }

    public void add(final PublicChatModel publicChatModel){
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            Boolean is = false;
            @Override
            protected Void doInBackground(Void... voids) {
                for(PublicChatModel model : items){
                    if(model.uiid.equals(publicChatModel.uiid)){
                        is = true;
                        break;
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if(!is){
                    items.add(publicChatModel);
                    notifyDataSetChanged();
                }
            }
        };
        task.execute();
    }

    public void add(List<PublicChatModel> publicChatModel){
        publicChatModel.addAll(items);
        items.clear();
        items = publicChatModel;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_public_chat, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final PublicChatModel item = items.get(position);
        //holder.message.setText(item.spannableMessage);
        Smiles.setSmiles(item.message, holder.message, smile_size, activity);

//        if(item.publicUserModel == null){
//            if(item.sender_id.equals(userModel.id)){
//                item.publicUserModel = userModel.getPublicModel();
//            } else {
//                item.publicUserModel = PublicUserModel.getById(item.sender_id);
//            }
//            if(item.publicUserModel == null){
//                return;
//            }
//        }
//
        holder.name.setText(item.publicUserModel.first_name);

        int old = UserHelper.getOld(item.publicUserModel.birth_date);
        holder.age.setText(String.valueOf(old));

        Drawable img = UserHelper.getGenderDrawable(item.publicUserModel.gender, activity);
        img.setBounds( 0, 0, gender_size, gender_size );
        holder.age.setCompoundDrawables(null, null, img, null);

        // статус
        if(item.publicUserModel.is_online > 0){
            holder.offline.setVisibility(View.GONE);
            holder.online.setVisibility(View.VISIBLE);
        } else {
            holder.online.setVisibility(View.GONE);
            holder.offline.setVisibility(View.VISIBLE);
        }

        // аватарка
        Picasso.with(activity)
                .load(Const.API_URL + item.publicUserModel.avatar)
                .fit()
                .placeholder(R.drawable.ava)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.ava, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(activity)
                                .load(Const.API_URL + item.publicUserModel.avatar)
                                .fit()
                                .placeholder(R.drawable.ava)
                                .error(R.drawable.ava)
                                .into(holder.ava);
                    }
                });

        // обновляем только если изменился юзер
//        if(holder.ava.getTag() == null || !holder.ava.getTag().equals(item.sender_id)){
//            //переход к профилю
//            holder.ava.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(item.sender_id.equals(userModel.id)){
//                        ((AppActivity)activity).goToMyProfile();
//                    } else {
//                        ((AppActivity)activity).goToUserProfile(item.sender_id);
//                    }
//                }
//            });
//            holder.ava.setTag(item.sender_id);
//        }
    }

    @Override
    public int getItemCount() {
        if(items == null){
            return 0;
        }
        return items.size();
    }

    public PublicChatModel getItem(int position) {
        return items.get(position);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case
        public ImageView ava;
        public ImageView online;
        public ImageView offline;
        public TextView name;
        public TextView age;
        public TextView message;
        public ImageView success_delivery;


        public ViewHolder(View v) {
            super(v);

            ava = (ImageView) v.findViewById(R.id.ava);
            online = (ImageView) v.findViewById(R.id.online);
            offline = (ImageView) v.findViewById(R.id.offline);
            name = (TextView) v.findViewById(R.id.name);
            age = (TextView) v.findViewById(R.id.age);
            message = (TextView) v.findViewById(R.id.message);
            success_delivery = (ImageView) v.findViewById(R.id.success_delivery);

        }
    }
}
