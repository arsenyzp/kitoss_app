package pro.devapp.kitoss.models;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pro.devapp.kitoss.api.RegisterDevicePush;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Регистрация устройства для push уведомлений
 */
public class PushRegisterModel{

    /**
     * Регистрация устройства
     * @param ctx
     * @param gtoken
     */
    public static void regDevice(final Activity ctx, String token, String gtoken){
        // если уже отправляли
        if(((App)ctx.getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_SENT_TOKEN_TO_SERVER).equals("OK")){
            return;
        }

        RegisterDevicePush service = RetrofitInstance.getRetrofit().create(RegisterDevicePush.class);
        Call<ResponseModel> call = service.add_device(token, gtoken);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                   Log.d(PushRegisterModel.class.getName(), "Error send gtoken");
                } else  {
                    if(m.status != 200){
                        Log.d(AuthModel.class.getName(), m.errors.toString());
                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();
                        Log.d(PushRegisterModel.class.getName() , "Error from server " + gson.toJson(m));
                    } else {
                        ((App)ctx.getApplication()).getAppPrefs().saveStringPreference(Const.PREFERENCE_SENT_TOKEN_TO_SERVER, "OK");
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PushRegisterModel.class.getName() , "onFailure");
            }
        });
    }

    /**
     * Удаление устройства
     * @param ctx
     * @param gtoken
     */
    public static void removeDevice(final Activity ctx, String token, String gtoken){
        RegisterDevicePush service = RetrofitInstance.getRetrofit().create(RegisterDevicePush.class);
        Call<ResponseModel> call = service.remove_device(token, gtoken);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    Log.d(PushRegisterModel.class.getName(), "Error send gtoken");
                } else  {
                    if(m.status != 200){
                        Log.d(AuthModel.class.getName(), m.errors.toString());
                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();
                        Log.d(PushRegisterModel.class.getName() , "Error from server " + gson.toJson(m));
                    } else {
                        ((App)ctx.getApplication()).getAppPrefs().saveStringPreference(Const.PREFERENCE_SENT_TOKEN_TO_SERVER, "");
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PushRegisterModel.class.getName() , "onFailure");
            }
        });
    }
}
