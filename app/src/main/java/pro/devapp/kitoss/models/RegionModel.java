package pro.devapp.kitoss.models;

import android.os.Handler;
import android.util.Log;
import com.google.gson.annotations.Expose;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import pro.devapp.kitoss.api.Regions;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Доступные регионы
 */

public class RegionModel extends RealmObject{

    @Expose
    @PrimaryKey
    public String cod;

    @Expose
    public String name;

    @Expose
    public String name_en;

    public RegionModel(){

    }

    /**
     * обновление/сохранение
     */
    public void update(){
        Realm realm = Realm.getDefaultInstance();
        RegionModel model = realm.where(RegionModel.class).equalTo("cod", cod).findFirst();

        if(model != null){
            // если такой пользователь уже есть, только обновляем профиль

            realm.beginTransaction();
            model.name = name;
            model.name_en = name_en;
            realm.insertOrUpdate(model);
            realm.commitTransaction();
            realm.close();
        } else {
            // сохраняем новый профиль
            realm.beginTransaction();
            realm.insertOrUpdate(this);
            realm.commitTransaction();
            realm.close();
        }
    }

    /**
     * Обновление данных с сервера
     * @param handler
     */
    public static void refreshFromServer(final Handler handler){

        Regions service = RetrofitInstance.getRetrofit().create(Regions.class);

        Call<ResponseModel> call = service.get();
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null || resp.items.size() == 0){
                            Log.d(RegionModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            for (RegionModel model : resp.items){
                                Log.d(RegionModel.class.getName(), model.name);
                                model.update();
                            }
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, "").sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(RegionModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    public class Resp{
        public List<RegionModel> items;
    }

    /**
     * Получить название региона
     * @param cod
     * @return
     */
    public static String getRegionName(String cod){
        String result = "";
        Realm realm = Realm.getDefaultInstance();
        RegionModel model = realm.where(RegionModel.class).equalTo("cod", cod).findFirst();
        if(model != null){
            result = model.name;
        }
        realm.close();
        return result;
    }

    public static List<RegionModel> getAll(){
        Realm realm = Realm.getDefaultInstance();
        List<RegionModel> list = realm.where(RegionModel.class).findAll();
        if(list != null){
            list = realm.copyFromRealm(list);
        }
        realm.close();
        return list;
    }
}
