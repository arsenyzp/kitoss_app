package pro.devapp.kitoss.models;

import android.os.Handler;
import android.util.Log;
import com.google.gson.annotations.Expose;
import java.util.List;
import pro.devapp.kitoss.api.Game;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Модель истории игры
 */
public class GameModel {

    /**
     * Получить пользователей для игры
     * @param token
     * @param handler
     */
    public static void get_users(String token, final Handler handler){
        Game service = RetrofitInstance.getRetrofit().create(Game.class);
        Call<ResponseModel> call = service.get_users(token);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicUserModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            for (PublicUserModel model : resp.items){
                                Log.d(PublicUserModel.class.getName(), model.first_name);
                                model.update();
                            }
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, resp).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(HistoriesModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Получить статистику для игры
     * @param token
     * @param handler
     */
    public static void get_statistic(String token, final Handler handler){
        Game service = RetrofitInstance.getRetrofit().create(Game.class);
        Call<ResponseModel> call = service.get_statistic(token);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                    } else {
                        RespStatistic resp = m.getData(RespStatistic.class);
                        if(resp == null){
                            Log.d(PublicUserModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            for (PublicUserModel model : resp.prev_week){
                                Log.d(PublicUserModel.class.getName(), model.first_name);
                                model.update();
                            }
                            for (PublicUserModel model : resp.current_week){
                                Log.d(PublicUserModel.class.getName(), model.first_name);
                                model.update();
                            }
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, resp).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(HistoriesModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Сохранить результат
     * @param token
     * @param user_id
     * @param result
     * @param handler
     */
    public static void set_result(String token, String user_id, int result, final Handler handler){
        Game service = RetrofitInstance.getRetrofit().create(Game.class);
        Call<ResponseModel> call = service.set_result(token, user_id, result);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicUserModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, resp).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(HistoriesModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Получить колличество бонусов в зависимотсти от позиции колеса
     * @param position
     * @return
     */
    public static int getBonusByPosition(int position){
        switch (position){
            case 1:
                return 30;
            case 2:
                return 5;
            case 3:
                return 40;
            case 4:
                return 15;
            case 5:
                return 100;
            case 6:
                return 10;
            case 7:
                return 50;
            case 8:
                return 25;
            default:
                return 0;
        }
    }

    public class Resp{
        @Expose
        public List<PublicUserModel> items;
        @Expose
        public int timeout = 0;
    }

    public class MyStat{
        @Expose
        public int my_game_rate = 0;

        @Expose
        public int my_stat = 0;
    }

    public class RespStatistic{
        @Expose
        public List<PublicUserModel> prev_week;

        @Expose
        public List<PublicUserModel> current_week;

        @Expose
        public MyStat my_stat = null;
    }
}
