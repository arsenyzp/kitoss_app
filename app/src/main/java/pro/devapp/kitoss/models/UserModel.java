package pro.devapp.kitoss.models;

import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import com.google.gson.annotations.Expose;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pro.devapp.kitoss.api.AvatarUpload;
import pro.devapp.kitoss.api.BgUpload;
import pro.devapp.kitoss.api.Profile;
import pro.devapp.kitoss.api.Users;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Модель пользователя
 */

public class UserModel extends RealmObject {

    @Expose
    @PrimaryKey
    public String id;

    @Expose
    public String first_name="";

    @Expose
    public String gender="";

    @Expose
    public String phone="";

    @Expose
    public String status;

    @Expose
    public String role;

    @Expose
    public String avatar="";

    @Expose
    public String background="";

    @Expose
    public int balance=0;

    @Expose
    public int bonuses=0;

    @Expose
    public String promo_cod="";

    @Expose
    public String birth_date="";

    @Expose
    public String country="";

    @Expose
    public String city="";

    @Expose
    public String region="";

    @Expose
    public String my_status="";

    @Expose
    public String about_me="";

    @Expose
    public String g_account_id="";

    @Expose
    public String where_go="";

    @Expose
    public String token;

    @Expose
    public Boolean showed_promo;

    public UserModel(){

    }

    /**
     * обновление/сохранение
     */
    public void update(){

        Realm realm = Realm.getDefaultInstance();
        UserModel userModel = realm.where(UserModel.class).equalTo("id", id).findFirst();

        if(userModel != null) {
            // если такой пользователь уже есть, только обновляем профиль
            realm.beginTransaction();
            userModel.token = token;
            userModel.first_name = first_name;
            userModel.gender = gender;
            userModel.phone = phone;
            userModel.status = status;
            userModel.avatar = avatar;
            userModel.background = background;
            userModel.balance = balance;
            userModel.promo_cod = promo_cod;
            userModel.birth_date = birth_date;
            userModel.country = country;
            userModel.city = city;
            userModel.region = region;
            userModel.g_account_id = g_account_id;
            userModel.my_status = my_status;
            userModel.about_me = about_me;
            userModel.where_go = where_go;
            userModel.role = role;
            userModel.bonuses = bonuses;
            userModel.showed_promo = showed_promo;
            realm.insertOrUpdate(userModel);
            realm.commitTransaction();
            realm.close();
        } else {
            // сохраняем новый профиль
            realm.beginTransaction();
            realm.insertOrUpdate(this);
            realm.commitTransaction();
            realm.close();
        }
    }

    /**
     * Получение профиля по токену
     * @param token
     * @return
     */
    public static UserModel getByToken(String token){
        Realm realm = Realm.getDefaultInstance();
        UserModel userModel = realm.where(UserModel.class).equalTo("token", token).findFirst();
        if(userModel != null){
            userModel = realm.copyFromRealm(userModel);
        }
        realm.close();
        return userModel;
    }

    /**
     * Обновление данных с сервера
     * получение профиля
     * @param handler
     */
    public void refreshFromServer(final Handler handler){

        Profile service = RetrofitInstance.getRetrofit().create(Profile.class);

        Call<ResponseModel> call = service.get(token);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                processResponse(response, handler);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(AuthModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Обработка ответа сервера
     * @param response
     * @param handler
     */
    private void processResponse(Response<ResponseModel> response, Handler handler){
        ResponseModel m = response.body();
        if(m == null){
            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
        } else  {
            if(m.status == 403){
                // доступ запрещён
                handler.obtainMessage(1, Const.STATE_ACCESS_DINEDED, 1, m.errors.toString()).sendToTarget();
            } else if(m.status != 200){
                // другая ошибка
                handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
            } else {
                UserModel userModel = m.getData(UserModel.class);
                if(userModel == null || userModel.token == null){
                    // ошибка сервера
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                } else {
                    // успешное получение данных
                    Log.d(AuthModel.class.getName() , userModel.token);
                    // сохраняем
                    userModel.update();
                    // обновляем текущий экземпляр
                    first_name = userModel.first_name;
                    gender = userModel.gender;
                    phone = userModel.phone;
                    status = userModel.status;
                    avatar = userModel.avatar;
                    background = userModel.background;
                    balance = userModel.balance;
                    promo_cod = userModel.promo_cod;
                    where_go = userModel.where_go;
                    birth_date = userModel.birth_date;
                    country = userModel.country;
                    where_go = userModel.where_go;
                    city = userModel.city;
                    region = userModel.region;
                    g_account_id = userModel.g_account_id;
                    handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, userModel).sendToTarget();
                }
            }
        }
    }

    /**
     * Сохранить на сервер
     * @param handler
     */
    public void saveOnServer(final Handler handler){

        Profile service = RetrofitInstance.getRetrofit().create(Profile.class);

        Call<ResponseModel> call = service.save(token, first_name, gender, birth_date, city, where_go, my_status, about_me);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                processResponse(response, handler);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(AuthModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Загрузить на сервер аватар
     * @param fileUri
     * @param handler
     */
    public void uploadAvatar(Uri fileUri, final Handler handler) {

        // create upload service client
        AvatarUpload service = RetrofitInstance.getRetrofit().create(AvatarUpload.class);

        // use the FileUtils to get the actual file by uri
        File file = new File(fileUri.getPath());

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("image/jpeg"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        // finally, execute the request
        Call<ResponseModel> call = service.upload(token, body);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call,
                                   Response<ResponseModel> response) {
                Log.v("Upload", "success");
                handler.obtainMessage(1, Const.STATE_SUCCESS_UPLOAD, 1, "Upload success").sendToTarget();
                processResponse(response, handler);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
                handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, "Upload error: " + t.getMessage()).sendToTarget();
            }
        });
    }

    /**
     * Загрузить фон на сервер
     * @param fileUri
     * @param handler
     */
    public void uploadBg(Uri fileUri, final Handler handler) {

        // create upload service client
        BgUpload service = RetrofitInstance.getRetrofit().create(BgUpload.class);

        // use the FileUtils to get the actual file by uri
        File file = new File(fileUri.getPath());

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("image/jpeg"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        // finally, execute the request
        Call<ResponseModel> call = service.upload(token, body);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call,
                                   Response<ResponseModel> response) {
                Log.v("Upload", "success");
                handler.obtainMessage(1, Const.STATE_SUCCESS_UPLOAD, 1, "Upload success").sendToTarget();
                processResponse(response, handler);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
                handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, "Upload error: " + t.getMessage()).sendToTarget();
            }
        });
    }

    /**
     * Удалить аватарку
     * @param handler
     */
    public void deleteAva(final Handler handler){

        // create upload service client
        AvatarUpload service = RetrofitInstance.getRetrofit().create(AvatarUpload.class);

        // finally, execute the request
        Call<ResponseModel> call = service.delete(token);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call,
                                   Response<ResponseModel> response) {
                Log.v("Delete", "success");
                handler.obtainMessage(1, Const.STATE_SUCCESS_UPLOAD, 1, "Upload success").sendToTarget();
                processResponse(response, handler);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.e("Delete error:", t.getMessage());
                handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, "Upload error: " + t.getMessage()).sendToTarget();
            }
        });
    }

    /**
     * Удалить фон
     * @param handler
     */
    public void deleteBg(final Handler handler){

        // create upload service client
        BgUpload service = RetrofitInstance.getRetrofit().create(BgUpload.class);

        // finally, execute the request
        Call<ResponseModel> call = service.delete(token);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call,
                                   Response<ResponseModel> response) {
                Log.v("Delete", "success");
                handler.obtainMessage(1, Const.STATE_SUCCESS_UPLOAD, 1, "Upload success").sendToTarget();
                processResponse(response, handler);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.e("Delete error:", t.getMessage());
                handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, "Upload error: " + t.getMessage()).sendToTarget();
            }
        });
    }

    /**
     * Установить регион
     * @param handler
     */
    public void setRegion(String region, final Handler handler){

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        this.region = region;
        realm.insertOrUpdate(this);
        realm.commitTransaction();
        realm.close();

        // create upload service client
        Profile service = RetrofitInstance.getRetrofit().create(Profile.class);

        // finally, execute the request
        Call<ResponseModel> call = service.set_region(token, region);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call,
                                   Response<ResponseModel> response) {

                //TODO проверить json ответ
                Log.v("Set region", "success");
                handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, "Set region success").sendToTarget();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.e("Set region error:", t.getMessage());
                handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, "Set region error: " + t.getMessage()).sendToTarget();
            }
        });
    }

    /**
     * Установить регион
     * @param handler
     */
    public void setStatus(final Handler handler){

        // create upload service client
        Profile service = RetrofitInstance.getRetrofit().create(Profile.class);

        // finally, execute the request
        Call<ResponseModel> call = service.set_status(token, my_status);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call,
                                   Response<ResponseModel> response) {

                //TODO проверить json ответ
                Log.v("Set status", "success");
                handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, "Set status success").sendToTarget();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.e("Set region error:", t.getMessage());
                handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, "Set region error: " + t.getMessage()).sendToTarget();
            }
        });
    }

    /**
     * Сохранить промокод
     * @param promo_cod
     * @param token
     * @param handler
     */
    public static void save_promo(String promo_cod, String token, final Handler handler){
        Profile service = RetrofitInstance.getRetrofit().create(Profile.class);
        Call<ResponseModel> call = service.save_promo(token, promo_cod);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                    } else {
                        handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, null).sendToTarget();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(HistoriesModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    public PublicUserModel getPublicModel(){
        PublicUserModel publicUserModel = new PublicUserModel();
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        publicUserModel.is_online = 1;
        publicUserModel.friend_status = Const.FRIEND_STATUS_NOTKNOW;
        publicUserModel.about_me = about_me;
        publicUserModel.avatar = avatar;
        publicUserModel.avatar_full = avatar;
        publicUserModel.background = background;
        publicUserModel.birth_date = birth_date;
        publicUserModel.city = city;
        publicUserModel.country = country;
        publicUserModel.deleted = true;
        publicUserModel.first_name = first_name;
        publicUserModel.last_name = "";
        publicUserModel.middle_name = "";
        publicUserModel.gender = gender;
        publicUserModel.status = status;
        publicUserModel.region = region;
        publicUserModel.my_status = my_status;
        publicUserModel.where_go = where_go;
        publicUserModel.friend_status = "";
        publicUserModel.id = id;
        publicUserModel.bonuses = bonuses;
        publicUserModel.modified = 0;

        realm.insertOrUpdate(publicUserModel);
        realm.commitTransaction();

        publicUserModel = PublicUserModel.getById(publicUserModel.id);

        realm.close();

        return publicUserModel;
    }
}
