package pro.devapp.kitoss.models;

import android.os.Handler;
import android.util.Log;
import com.google.gson.annotations.Expose;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import pro.devapp.kitoss.api.WhereGo;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Модель вариантов куда пойти
 */

public class WhereGoModel extends RealmObject{
    @Expose
    @PrimaryKey
    public String id;

    @Expose
    public String text;

    public WhereGoModel(){

    }

    public static WhereGoModel getById(String id){
        Realm realm = Realm.getDefaultInstance();
        WhereGoModel model = realm.where(WhereGoModel.class).equalTo("id", id).findFirst();
        if(model != null){
            model = realm.copyFromRealm(model);
        }
        realm.close();
        return model;
    }

    /**
     * Весь список
     * @return
     */
    public static List<WhereGoModel> getAll(){
        Realm realm = Realm.getDefaultInstance();
        List<WhereGoModel> list = realm.where(WhereGoModel.class).findAll();
        if(list != null){
            list = realm.copyFromRealm(list);
        }
        realm.close();
        return list;
    }

    /**
     * обновление/сохранение
     */
    public void update(){
        Realm realm = Realm.getDefaultInstance();
        WhereGoModel model = realm.where(WhereGoModel.class).equalTo("id", id).findFirst();

        if(model != null){
            // если такой пользователь уже есть, только обновляем профиль

            realm.beginTransaction();
            model.text = text;

            realm.insertOrUpdate(model);
            realm.commitTransaction();
            realm.close();
        } else {
            // сохраняем новый профиль
            realm.beginTransaction();
            realm.insertOrUpdate(this);
            realm.commitTransaction();
            realm.close();
        }
    }

    /**
     * Обновление данных с сервера
     * @param handler
     */
    public static void refreshFromServer(final Handler handler){

        WhereGo service = RetrofitInstance.getRetrofit().create(WhereGo.class);

        Call<ResponseModel> call = service.get();
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null || resp.items.size() == 0){
                            Log.d(WhereGoModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            for (WhereGoModel model : resp.items){
                                Log.d(WhereGoModel.class.getName(), model.text);
                                model.update();
                            }
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, "").sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(WhereGoModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    public class Resp{
        public List<WhereGoModel> items;
    }

}
