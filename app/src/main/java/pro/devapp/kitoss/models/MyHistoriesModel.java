package pro.devapp.kitoss.models;

import android.app.Activity;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.util.Log;

import com.google.gson.annotations.Expose;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.Sort;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import pro.devapp.kitoss.api.Histories;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import pro.devapp.kitoss.components.Smiles;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Модель моих историй
 */

public class MyHistoriesModel extends RealmObject {

    @Expose
    @PrimaryKey
    public String id;

    @Expose
    public int number;

    @Expose
    public String text;

    @Expose
    public int time;

    @Expose
    public String status;

    @Expose
    public int likes_count;

    @Expose
    public String user_id;

    @Ignore
    public SpannableStringBuilder spannableText;

    public MyHistoriesModel(){}

    public static List<MyHistoriesModel> get(int offset, int limit, int smile_size, Activity activity){

        Realm realm = Realm.getDefaultInstance();

        List<MyHistoriesModel> list = realm.where(MyHistoriesModel.class).equalTo("user_id", ((BaseActivity)activity).userModel.id).findAllSorted("time", Sort.DESCENDING);
        if(list != null){
            list = realm.copyFromRealm(list);
        }
        realm.close();

        for(int i = 0; i < list.size(); i++){
            MyHistoriesModel model = list.get(i);

            if(model.spannableText == null){
                model.spannableText = Smiles.getSpannable(model.text, smile_size, activity);
            }

            list.set(i, model);
        }
        return list;
    }

    /**
     * Загрузка историй с сервера
     * @param handler
     */
    public static void loadHistories(final String token, int offset, int limit, final Handler handler){

        Histories service = RetrofitInstance.getRetrofit().create(Histories.class);

        Call<ResponseModel> call = service.getMy(token, offset, limit);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicChatModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            if(resp.items != null && resp.items.size() > 0){
                                UserModel userModel = UserModel.getByToken(token);
                                for (MyHistoriesModel model : resp.items){
                                    model.user_id = userModel.id;
                                    model.insert();
                                }
                            }
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, null).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(MyHistoriesModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Отправка истории
     * @param handler
     */
    public static void sendHistory(final String token, String text, final Handler handler){

        Histories service = RetrofitInstance.getRetrofit().create(Histories.class);

        Call<ResponseModel> call = service.send(token, text);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                    } else {
                        MyHistoriesModel model = m.getData(MyHistoriesModel.class);
                        if(model == null){
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            UserModel userModel = UserModel.getByToken(token);
                            model.user_id = userModel.id;
                            model.insert();
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, null).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(MyHistoriesModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    public void insert(){

        Realm realm = Realm.getDefaultInstance();
        MyHistoriesModel model = realm.where(MyHistoriesModel.class).equalTo("id", id).findFirst();

        if(model != null){
            // если такой пользователь уже есть, только обновляем профиль

            realm.beginTransaction();
            model.text = text;
            model.time = time;
            model.likes_count = likes_count;
            model.number = number;
            model.status = status;
            realm.insertOrUpdate(model);
            realm.commitTransaction();
            realm.close();
        } else {
            // сохраняем новый профиль
            realm.beginTransaction();
            realm.insertOrUpdate(this);
            realm.commitTransaction();
            realm.close();
        }
    }

    public static String getStatusName(String status){
        switch (status){
            case "new":
                return "На модерации";
            case "accept":
                return "Опубликовано";
            case "deleted":
                return "Отклонено";
            default:
                return "";
        }
    }

    public class Resp{
        @Expose
        public List<MyHistoriesModel> items;
    }
}
