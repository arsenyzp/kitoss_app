package pro.devapp.kitoss.models;

import android.os.Handler;
import android.util.Log;
import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import pro.devapp.kitoss.api.Users;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import pro.devapp.kitoss.components.RuDateNames;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Модель юзеров
 */

public class PublicUserModel extends RealmObject{

    @Expose
    @PrimaryKey
    public String id;

    @Expose
    public String first_name="";

    @Expose
    public String last_name="";

    @Expose
    public String middle_name="";

    @Expose
    public String gender="";

    @Expose
    public String status;

    @Expose
    public String avatar="";

    @Expose
    public String avatar_full="";

    @Expose
    public String background="";

    @Expose
    public String birth_date="";

    @Expose
    public String country="";

    @Expose
    public String city="";

    @Expose
    public String region="";

    @Expose
    public String my_status="";

    @Expose
    public String about_me="";

    @Expose
    public String where_go="";

    @Expose
    public int is_online=0;

    @Expose
    public int bonuses=0;

    @Expose
    public int modified=0;

    @Expose
    public int join_at=0;

    @Expose
    public Boolean deleted=false;

    /**
     * Статус друга
     * друг
     * ожидает принятия
     * незнакомец
     */
    @Expose
    public String friend_status="";

    @Ignore
    public int score=0;

    @Ignore
    Call<ResponseModel> call;

    /**
     * Обновление данных с сервера
     * @param handler
     */
    public static void get(final int offset, int limit, String token, final Handler handler){

        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        Call<ResponseModel> call = service.search(token, "", offset, limit);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        // удаляем старые записи
                        if(offset == 0){
                            reset();
                        }

                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicUserModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            for (PublicUserModel model : resp.items){
                                Log.d(PublicUserModel.class.getName(), model.first_name);
                                model.update();
                            }
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, "").sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PublicUserModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }


    /**
     * обновление/сохранение
     */
    public void update(){

        Realm realm = Realm.getDefaultInstance();
        PublicUserModel userModel = realm.where(PublicUserModel.class).equalTo("id", id).findFirst();

        if(userModel != null){
            // если такой пользователь уже есть, только обновляем профиль

            realm.beginTransaction();
            userModel.first_name = first_name;
            userModel.last_name = last_name;
            userModel.middle_name = middle_name;
            userModel.gender = gender;
            userModel.status = status;
            userModel.avatar = avatar;
            userModel.background = background;
            userModel.birth_date = birth_date;
            userModel.country = country;
            userModel.city = city;
            userModel.region = region;
            userModel.my_status = my_status;
            userModel.about_me = about_me;
            userModel.where_go = where_go;
            userModel.friend_status = friend_status;
            userModel.is_online = is_online;
            userModel.modified = modified;
            userModel.deleted = false;
            userModel.bonuses = bonuses;

            realm.insertOrUpdate(userModel);
            realm.commitTransaction();
            realm.close();
        } else {
            // сохраняем новый профиль
            realm.beginTransaction();
            realm.insertOrUpdate(this);
            realm.commitTransaction();
            realm.close();
        }
    }

    /**
     * Return cursor for result set for all users
     * @return
     */
    public static List<PublicUserModel> fetchResultCursor() {
        Realm realm = Realm.getDefaultInstance();
        List<PublicUserModel> results = realm
                .where(PublicUserModel.class)
                .equalTo("deleted", false)
                .findAllSorted("modified", Sort.DESCENDING);
        realm.close();
        return results;
    }

    /**
     * Return cursor for result set for all users with some status
     * @return
     */
    public static List<PublicUserModel> fetchResultCursor(String status) {
        Realm realm = Realm.getDefaultInstance();
        List<PublicUserModel> results = realm
                .where(PublicUserModel.class)
                .equalTo("friend_status", status)
                .findAllSorted("modified", Sort.DESCENDING);
        realm.close();
        return results;
    }

    /**
     * Return cursor for result set for all users with some status
     * @return
     */
    public static List<PublicUserModel> fetchResultCursor(String status, boolean is_online) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm
                .where(PublicUserModel.class)
                .equalTo("friend_status", status);

        if(is_online){
            query.equalTo("is_online", 1);
        } else {
            query.equalTo("is_online", 0);
        }
        List<PublicUserModel> results = query.findAllSorted("modified", Sort.DESCENDING);
        realm.close();
        return results;
    }

    /**
     * Избранные
     * @param owner_id
     * @return
     */
    public static List<PublicUserModel> fetchFavoriteCursor(String owner_id) {

        String[] favorite_ids = FavoriteModel.getIdsArray(owner_id);

        Realm realm = Realm.getDefaultInstance();
        RealmQuery<PublicUserModel> query = realm
                .where(PublicUserModel.class);

        query.equalTo("id", "-");
        for (String id : favorite_ids) {
            query = query.or().equalTo("id", id);
        }

        List<PublicUserModel> results = query.findAllSorted("modified", Sort.DESCENDING);
        realm.close();
        return results;
    }

    /**
     * Игнор
     * @param owner_id
     * @return
     */
    public static List<PublicUserModel> fetchBlackCursor(String owner_id) {
        String[] black_ids = BlackListModel.getIdsArray(owner_id);

        Realm realm = Realm.getDefaultInstance();
        RealmQuery<PublicUserModel> query = realm
                .where(PublicUserModel.class);

        query.equalTo("id", "-");
        for (String id : black_ids) {
            query = query.or().equalTo("id", id);
        }

        return query.findAllSorted("modified", Sort.DESCENDING);
    }


    public class Resp{
        @Expose
        public List<PublicUserModel> items;
    }

    /**
     * Очистить список (вызывается при первом запросе для списка)
     */
    public static void reset(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<PublicUserModel> models = realm.where(PublicUserModel.class).equalTo("deleted", false).findAll();
        realm.beginTransaction();
        for (int i = 0; i < models.size(); i++){
            PublicUserModel model = models.get(i);
            model.deleted = true;
            realm.insertOrUpdate(model);
        }
        realm.commitTransaction();
        realm.close();
    }

    /**
     * Добавление в друзья
     */
    public static void addToMyFriend(String token, String user_id, final Handler handler){

        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        Call<ResponseModel> call = service.addToFriend(token, user_id);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        PublicUserModel userModel = m.getData(PublicUserModel.class);
                        userModel.update();
                        handler.obtainMessage(1, Const.STATE_SUCCESS_ADD_TO_FRIENDS, 1, "").sendToTarget();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PublicUserModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Удалить из друзей
     */
    public static void removeFromMyFriends(String token, String user_id, final Handler handler){
        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        Call<ResponseModel> call = service.removeFriend(token, user_id);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        PublicUserModel userModel = m.getData(PublicUserModel.class);
                        userModel.update();
                        handler.obtainMessage(1, Const.STATE_SUCCESS_REMOVE_FROM_FRIENDS, 1, "").sendToTarget();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PublicUserModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Принять заявку в друзья
     */
    public static void acceptFriendRequest(String token, String user_id, final Handler handler){
        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        Call<ResponseModel> call = service.acceptFriend(token, user_id);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        PublicUserModel userModel = m.getData(PublicUserModel.class);
                        userModel.update();
                        handler.obtainMessage(1, Const.STATE_SUCCESS_ADD_TO_FRIENDS, 1, "").sendToTarget();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PublicUserModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Отклонить заявку в друзья
     */
    public static void declineFriendRequest(String token, String user_id, final Handler handler){
        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        Call<ResponseModel> call = service.declineFriend(token, user_id);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        PublicUserModel userModel = m.getData(PublicUserModel.class);
                        userModel.update();
                        handler.obtainMessage(1, Const.STATE_SUCCESS_DECLINE_FRIEND_REQUEST, 1, "").sendToTarget();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PublicUserModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Удалить свою заявку в друзья
     */
    public static void removeMyFriendRequest(String token, String user_id, final Handler handler){
        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        Call<ResponseModel> call = service.removeFriendRequest(token, user_id);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        PublicUserModel userModel = m.getData(PublicUserModel.class);
                        userModel.update();
                        handler.obtainMessage(1, Const.STATE_SUCCESS_CANCEL_FRIEND_REQUEST, 1, "").sendToTarget();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PublicUserModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Ручное обновление статуса пользователя
     * @param user_id
     * @param status
     */
    public static void setFriendStatus(String user_id, String status){
        Realm realm = Realm.getDefaultInstance();
        PublicUserModel model = realm.where(PublicUserModel.class).equalTo("id", user_id).findFirst();

        realm.beginTransaction();
        model.status = status;
        realm.insertOrUpdate(model);
        realm.commitTransaction();

        realm.close();
    }

    /**
     * Получить пользователя по ид
     * @param user_id
     * @return
     */
    public static PublicUserModel getById(String user_id){
        Realm realm = Realm.getDefaultInstance();
        PublicUserModel result = realm.where(PublicUserModel.class).equalTo("id", user_id).findFirst();
        if(result != null){
            result = realm.copyFromRealm(result);
        }
        realm.close();
        return result;
    }

    /**
     * Тескт куда желает пойти
     * @return
     */
    public String getWhereGo(){
        if(where_go.equals("")){
            return  "";
        }
        WhereGoModel whereGoModel = WhereGoModel.getById(where_go);
        return whereGoModel.text;
    }

    /**
     * Поиск
     * @param token
     * @param offset
     * @param limit
     * @param handler
     */
    public void search(String token, final int offset, int limit, final Handler handler){

        final Users service = RetrofitInstance.getRetrofit().create(Users.class);

        call = service.search(
                token,
                FilterUsersModel.name,
                FilterUsersModel.phone,
                FilterUsersModel.age_from,
                FilterUsersModel.age_to,
                FilterUsersModel.gender,
                FilterUsersModel.region,
                FilterUsersModel.only_online,
                offset,
                limit
        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        // удаляем старые записи
                        if(offset == 0){
                            reset();
                        }
                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicUserModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            if(resp.items.size() != 0){
                                for (PublicUserModel model : resp.items){
                                    Log.d(PublicUserModel.class.getName(), model.first_name);
                                    model.update();
                                }
                            }
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, "").sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                if(t.getMessage().equals("Canceled")){
                    Log.d(PublicUserModel.class.getName() , "PublicUserModel->search Canceled");
                    return;
                }
                Log.d(PublicUserModel.class.getName() , "PublicUserModel->search onFailure "+t.getMessage());
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Поиск
     * @param token
     * @param offset
     * @param limit
     * @param listener
     */
    public void search(String token, final int offset, final int limit, final ResponseListener listener){

        final Users service = RetrofitInstance.getRetrofit().create(Users.class);

        call = service.search(
                token,
                FilterUsersModel.name,
                FilterUsersModel.phone,
                FilterUsersModel.age_from,
                FilterUsersModel.age_to,
                FilterUsersModel.gender,
                FilterUsersModel.region,
                FilterUsersModel.only_online,
                offset,
                limit
        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    listener.onError("Empty result");
                } else  {
                    if(m.status != 200){
                        listener.onError(m.errors.toString());
                    } else {
                        // удаляем старые записи
                        if(offset == 0){
                            reset();
                        }
                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicUserModel.class.getName(), resp.toString());
                            // ошибка сервера
                            listener.onError(m.errors.toString());
                        } else {
                            // успешное получение данных
                            if(resp.items.size() != 0){
                                for (PublicUserModel model : resp.items){
                                    Log.d(PublicUserModel.class.getName(), model.first_name);
                                    model.update();
                                }
                            }
                            listener.onSuccess(resp.items);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                if(t.getMessage().equals("Canceled")){
                    Log.d(PublicUserModel.class.getName() , "PublicUserModel->search Canceled");
                    return;
                }
                Log.d(PublicUserModel.class.getName() , "PublicUserModel->search onFailure "+t.getMessage());
                listener.onError("Server Error");
            }
        });
    }

    public void cancelSearch(){
        if(call != null)
            call.cancel();
    }

    public Date getJoinDate() {
        return new Date(join_at*1000L);
    }

    /**
     * Cтрока с описанием длительности регистрации в сервисе
     * @return
     */
    public String getDurationJoin() {
        //1 minute = 60 seconds
        //1 hour = 60 x 60 = 3600
        //1 day = 3600 x 24 = 86400
        //1 month = 86400 x 30 = 2592000
        //1 year = 2592000 x 365 = 946080000

        //milliseconds
        long different = new Date().getTime() - getJoinDate().getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedMonth = 0;
        long elapsedYear = 0;

        long elapsedDays = different / daysInMilli;
       // different = different % daysInMilli;

        if (elapsedDays >= 30) {
            elapsedMonth = (elapsedDays - elapsedDays % 30) / 30;
            elapsedDays = elapsedDays % 30;
        }

        if (elapsedMonth >= 12) {
            elapsedYear = (elapsedMonth - elapsedMonth % 12) / 12;
            elapsedDays = elapsedMonth % 12;
        }

      //  long elapsedHours = different / hoursInMilli;

//        if (elapsedYear > 0) {
//            return elapsedYear + " лет " + elapsedMonth + " месяцев ";
//        } else if (elapsedMonth > 0) {
//            return elapsedMonth + " месяцев " + elapsedDays + " дней ";
//        } else if (elapsedDays > 0) {
//            return elapsedDays + " дней " + elapsedHours + " часов ";
//        } else {
//            return elapsedHours + " часов ";
//        }

        if (elapsedYear > 0) {
            return (int)elapsedYear + " " + RuDateNames.getYears((int)elapsedYear);
        } else if (elapsedMonth > 0) {
            return (int)elapsedMonth + " " + RuDateNames.getMonths((int)elapsedMonth);
        } else if (elapsedDays > 0) {
            return (int)elapsedDays + " " + RuDateNames.getDays((int)elapsedDays);
        } else {
            return  " 1 день ";
        }
    }

    /**
     * Обновление избранных с сервера
     * @param handler
     */
    public static void getFavorites(ArrayList<String> favorite_ids, String token, final Handler handler){

        if(favorite_ids.size() == 0){
            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, "").sendToTarget();
            return;
        }

        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        if(favorite_ids.size() < 2){
            favorite_ids.add("");
        }
        Call<ResponseModel> call = service.favorites(token, favorite_ids);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicUserModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            for (PublicUserModel model : resp.items){
                                Log.d(PublicUserModel.class.getName(), model.first_name);
                                model.update();
                            }
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, "").sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PublicUserModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Обновление друзей с сервера
     * @param handler
     */
    public static void getFriends(final int offset, int limit, String token, final Handler handler){
        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        Call<ResponseModel> call = service.friends(token, offset, limit);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    if(handler!=null) {
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                    }
                } else  {
                    if(m.status != 200){
                        if(handler!=null) {
                            handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                        }
                    } else {
                        // удаляем старые записи
                        if(offset == 0){
                            reset();
                        }

                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicUserModel.class.getName(), resp.toString());
                            // ошибка сервера
                            if(handler!=null) {
                                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                            }
                        } else {
                            // успешное получение данных
                            for (PublicUserModel model : resp.items){
                                Log.d(PublicUserModel.class.getName(), model.first_name);
                                model.update();
                            }
                            if(handler!=null) {
                                handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, "").sendToTarget();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PublicUserModel.class.getName() , "onFailure");
                if(handler!=null) {
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
                }
            }
        });
    }

    /**
     * Пожаловаться на профиль юзера
     * @param user_id
     * @param text
     * @param token
     * @param handler
     */
    public static void complain(String user_id, String text, String token, final Handler handler){
        Users service = RetrofitInstance.getRetrofit().create(Users.class);
        Call<ResponseModel> call = service.complain(token, user_id, text);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                    } else {
                        handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, null).sendToTarget();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(HistoriesModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Подсчёт колличества запросов в друзья
     * @return
     */
    public static int getCountFriendRequest(){
        Realm realm = Realm.getDefaultInstance();
        int result = (int) realm
                .where(PublicUserModel.class)
                .equalTo("friend_status", Const.FRIEND_STATUS_WANT_FRIEND)
                .equalTo("deleted", false)
                .count();
        realm.close();
        return result;
    }

    /**
     * Подсчёт колличества друзей онлайн
     * @return
     */
    public static int getCountFriendOnline(){
        Realm realm = Realm.getDefaultInstance();
        int result = (int) realm
                .where(PublicUserModel.class)
                .equalTo("friend_status", Const.FRIEND_STATUS_WANT_FRIEND)
                .equalTo("deleted", false)
                .equalTo("is_online", 1)
                .count();
        realm.close();
        return result;
    }

    /**
     * Подсчёт колличества друзей оффлайн
     * @return
     */
    public static int getCountFriendOffline(){
        Realm realm = Realm.getDefaultInstance();
        int result = (int) realm
                .where(PublicUserModel.class)
                .equalTo("friend_status", Const.FRIEND_STATUS_WANT_FRIEND)
                .equalTo("deleted", false)
                .equalTo("is_online", 0)
                .count();
        realm.close();
        return result;
    }

    public class UserList{
        public List<PublicUserModel> users;
    }

    public static abstract class ResponseListener{
        public abstract void onSuccess(List<PublicUserModel> items);
        public abstract void onError(String message);
    }
}
