package pro.devapp.kitoss.models;


import android.os.Handler;
import android.util.Log;
import java.util.Arrays;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.api.Users;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Модель моих подарков
 */
public class GiftModel extends RealmObject{

    @PrimaryKey
    public int id = 0;

    public String name="";

    public int price;

    @Ignore
    public static int[] gifts_ids = new int[]{
            R.drawable.gift_ambulance,
            R.drawable.gift_beach_chair,
            R.drawable.gift_bear,
            R.drawable.gift_bicycle,
            R.drawable.gift_bouquet,
            R.drawable.gift_cabriolet_red,
            R.drawable.gift_cake,
            R.drawable.gift_cake_2,
            R.drawable.gift_cake_3,
            R.drawable.gift_cake_4,
            R.drawable.gift_cake_5,
            R.drawable.gift_cappuccino2,
            R.drawable.gift_cherry,
            R.drawable.gift_childhood_dream,
            R.drawable.gift_classic,
            R.drawable.gift_coffin,
            R.drawable.gift_cool,
            R.drawable.gift_death,
            R.drawable.gift_diamond_gem_ruby,
            R.drawable.gift_dog,
            R.drawable.gift_donuts,
            R.drawable.gift_excavator,
            R.drawable.gift_fire_escape,
            R.drawable.gift_flip_flops,
            R.drawable.gift_flippers,
            R.drawable.gift_green,
            R.drawable.gift_hambruger,
            R.drawable.gift_heart,
            R.drawable.gift_heart_2,
            R.drawable.gift_helicopter_medical,
            R.drawable.gift_icon_skull,
            R.drawable.gift_lily_flower_plant,
            R.drawable.gift_love_valentines_day_5,
            R.drawable.gift_love_valentines_day_12,
            R.drawable.gift_minicar,
            R.drawable.gift_money,
            R.drawable.gift_money_jar_savings,
            R.drawable.gift_music,
            R.drawable.gift_palm_beach_travel_vacation_leisure,
            R.drawable.gift_ping_pong,
            R.drawable.gift_pink,
            R.drawable.gift_quad_bike_blue,
            R.drawable.gift_rolice,
            R.drawable.gift_rose,
            R.drawable.gift_rose_2,
            R.drawable.gift_skull,
            R.drawable.gift_squire,
            R.drawable.gift_strawberry,
            R.drawable.gift_teachers_day,
            R.drawable.gift_treasure,
            R.drawable.gift_umbrella,
            R.drawable.gift_water_melon,

            R.drawable.gift_gold_medal,
            R.drawable.gift_bronze_medal,
            R.drawable.gift_silver_medal,
    };

    @Ignore
    public static String[] gifts_names = new String[]{
            "gift_ambulance",
            "gift_beach_chair",
            "gift_bear",
            "gift_bicycle",
            "gift_bouquet",
            "gift_cabriolet_red",
            "gift_cake",
            "gift_cake_2",
            "gift_cake_3",
            "gift_cake_4",
            "gift_cake_5",
            "gift_cappuccino2",
            "gift_cherry",
            "gift_childhood_dream",
            "gift_classic",
            "gift_coffin",
            "gift_cool",
            "gift_death",
            "gift_diamond_gem_ruby",
            "gift_dog",
            "gift_donuts",
            "gift_excavator",
            "gift_fire_escape",
            "gift_flip_flops",
            "gift_flippers",
            "gift_green",
            "gift_hambruger",
            "gift_heart",
            "gift_heart_2",
            "gift_helicopter_medical",
            "gift_icon_skull",
            "gift_lily_flower_plant",
            "gift_love_valentines_day_5",
            "gift_love_valentines_day_12",
            "gift_minicar",
            "gift_money",
            "gift_money_jar_savings",
            "gift_music",
            "gift_palm_beach_travel_vacation_leisure",
            "gift_ping_pong",
            "gift_pink",
            "gift_quad_bike_blue",
            "gift_rolice",
            "gift_rose",
            "gift_rose_2",
            "gift_skull",
            "gift_squire",
            "gift_strawberry",
            "gift_teachers_day",
            "gift_treasure",
            "gift_umbrella",
            "gift_water_melon",

            "gift_gold_medal",
            "gift_bronze_medal",
            "gift_silver_medal",
    };

    @Ignore
    public static int[] gifts_prices = new int[]{
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
            10,
    };

    /**
     * Запись конфигурации в базу
     */
    public static void init(){
        Realm realm = Realm.getDefaultInstance();
        if(realm.where(GiftModel.class).count() == 0){
            realm.beginTransaction();
            for (int i = 0; i < gifts_names.length; i++) {
                // TODO это сделанно для скрытых подарков...
                if(i >= gifts_prices.length){
                    continue;
                }
                GiftModel model = new GiftModel();
                model.id = gifts_ids[i];
                model.name = gifts_names[i];
                model.price = gifts_prices[i];
                realm.insertOrUpdate(model);
            }
            realm.commitTransaction();
        }
        realm.close();
    }

    /**
     * Отправить подарок
     * @param user_id
     * @param token
     * @param name
     * @param handler
     */
    public static void sendGift(String user_id, String token, String name, String message, final Handler handler){

        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        Call<ResponseModel> call = service.send_gift(token, user_id, name, message);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                    } else {
                        handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, null).sendToTarget();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(HistoriesModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Возвращает картинку товара
     * @param name
     * @return
     */
    public static int getDrawable(String name){
        if(Arrays.asList(gifts_names).indexOf(name) >= 0){
            return gifts_ids[Arrays.asList(gifts_names).indexOf(name)];
        } else {
            return gifts_ids[0];
        }

    }

    /**
     * Модель подарка по названию
     * @param name
     * @return
     */
    public static GiftModel findByName(String name){
        Realm realm = Realm.getDefaultInstance();
        GiftModel model = realm.where(GiftModel.class).equalTo("name", name).findFirst();
        if(model != null){
            model = realm.copyFromRealm(model);
        }
        realm.close();
        return model;
    }

    /**
     * Список подарков
     * @return
     */
    public static List<GiftModel> get(){
        Realm realm = Realm.getDefaultInstance();
        List<GiftModel> list = realm.where(GiftModel.class).findAll();
        if(list != null){
            list = realm.copyFromRealm(list);
        }
        realm.close();
        return list;
    }
}
