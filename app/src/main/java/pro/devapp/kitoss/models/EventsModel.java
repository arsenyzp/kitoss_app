package pro.devapp.kitoss.models;

import android.app.Activity;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.util.Log;
import com.google.gson.annotations.Expose;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.Sort;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import pro.devapp.kitoss.api.Users;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import pro.devapp.kitoss.components.Smiles;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Список событий
 */

public class EventsModel extends RealmObject{

    @Expose
    @PrimaryKey
    public String id;

    @Expose
    public PublicUserModel user;

    @Expose
    public String owner;

    @Expose
    public String status = "";

    @Expose
    public String type;

    @Expose
    public String image = "";

    @Expose
    public String gift = "";

    @Expose
    public int time;

    @Ignore
    public SpannableStringBuilder spannableText;

    public EventsModel(){

    }

    /**
     * Загрузка историй с сервера
     * @param handler
     */
    public static void loadEvents(final String token, int offset, int limit, final Handler handler){

        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        Call<ResponseModel> call = service.events(token, offset, limit);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicChatModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            UserModel userModel = UserModel.getByToken(token);
                            if(resp.items != null && resp.items.size() > 0){
                                for (EventsModel model : resp.items){
                                    model.user.update();
                                    model.user = PublicUserModel.getById(model.user.id);
                                    model.owner = userModel.id;
                                    Log.d(PublicChatModel.class.getName(),  model.user.id + "");
                                    model.insert();
                                }
                            }
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, null).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(HistoriesModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    public static List<EventsModel> get(int offset, int limit, int smile_size, Activity activity){

        Realm realm = Realm.getDefaultInstance();

        List<EventsModel> list = realm.where(EventsModel.class).equalTo("owner", ((BaseActivity)activity).userModel.id).findAllSorted("time", Sort.DESCENDING);
        if(list != null){
            list = realm.copyFromRealm(list);
        }
        realm.close();

        for(int i = 0; i < list.size(); i++){
            EventsModel model = list.get(i);
            if(model.spannableText == null){
                model.spannableText = Smiles.getSpannable(model.status, smile_size, activity);
            }

            list.set(i, model);
        }
        return list;
    }

    public void insert(){

        Realm realm = Realm.getDefaultInstance();
        EventsModel model = realm.where(EventsModel.class).equalTo("id", id).equalTo("owner", owner).findFirst();

        if(model != null){
            // если такой пользователь уже есть, только обновляем профиль

            realm.beginTransaction();
            model.status = status;
            model.image = image;
            model.gift = gift;
            model.time = time;
            model.type = type;
            realm.insertOrUpdate(model);
            realm.commitTransaction();
            realm.close();
        } else {
            // сохраняем новый профиль
            realm.beginTransaction();
            realm.insertOrUpdate(this);
            realm.commitTransaction();
            realm.close();
        }

    }

    public class Resp{
        @Expose
        public List<EventsModel> items;
    }
}
