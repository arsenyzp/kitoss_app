package pro.devapp.kitoss.models;

import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Список избранных пользователей
 */

public class FavoriteModel extends RealmObject{
    //ид владельца
    @Expose
    public String owner_id;

    @Expose
    public String user_id;

    public FavoriteModel(){

    }

    /**
     * Получить ид всех юзеров которые есть в избранном
     * @param owner_id
     * @return
     */
    public static ArrayList<String> getIds(String owner_id){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<FavoriteModel> models = realm.where(FavoriteModel.class).equalTo("owner_id", owner_id).findAll();
        ArrayList<String> ids = new ArrayList<>();
        for(int i = 0; i < models.size(); i++){
            ids.add(models.get(i).user_id);
        }
        realm.close();
        return ids;
    }

    public static String[] getIdsArray(String owner_id){
        ArrayList<String> ids = FavoriteModel.getIds(owner_id);
        return ids.toArray(new String[ids.size()]);
    }

    public static boolean check(String user_id, String owner_id){
        Realm realm = Realm.getDefaultInstance();
        Boolean result = realm.where(FavoriteModel.class).equalTo("user_id", user_id).equalTo("owner_id", owner_id).count() > 0;
        realm.close();
        return result;
    }

    /**
     * Добавить в избранное
     * @param user_id
     * @param owner_id
     */
    public static void add(String user_id, String owner_id){
        if(check(user_id, owner_id)){
            return;
        }
        FavoriteModel m = new FavoriteModel();
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        m.user_id = user_id;
        m.owner_id = owner_id;
        realm.insertOrUpdate(m);
        realm.commitTransaction();
        realm.close();
    }

    /**
     * Удалить из избранного
     * @param user_id
     * @param owner_id
     */
    public static void remove(String user_id, String owner_id){
        Realm realm = Realm.getDefaultInstance();
        FavoriteModel model = realm.where(FavoriteModel.class).equalTo("user_id", user_id).equalTo("owner_id", owner_id).findFirst();
        realm.beginTransaction();
        model.deleteFromRealm();
        realm.commitTransaction();
        realm.close();
    }
}
