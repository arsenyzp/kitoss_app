package pro.devapp.kitoss.models;

import android.os.Handler;
import android.util.Log;
import com.google.gson.annotations.Expose;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.Sort;
import io.realm.annotations.PrimaryKey;
import pro.devapp.kitoss.api.Users;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Модель моих подарков
 */

public class MyGiftModel extends RealmObject{

    @Expose
    @PrimaryKey
    public String id="";

    @Expose
    public String sender="";

    @Expose
    public String recipient="";

    @Expose
    public String name="";

    @Expose
    public int time;

    public static List<MyGiftModel> getByUser(String recipient){
        Realm realm = Realm.getDefaultInstance();

        List<MyGiftModel> list = realm.where(MyGiftModel.class).equalTo("recipient", recipient).findAllSorted("time", Sort.DESCENDING);
        if(list != null){
            list = realm.copyFromRealm(list);
        }
        realm.close();

        return list;
    }

    public static int getCountByUser(String recipient){
        Realm realm = Realm.getDefaultInstance();
        int result = (int)realm.where(MyGiftModel.class).equalTo("recipient", recipient).count();
        realm.close();
        return result;
    }

    public void insert(){

        Realm realm = Realm.getDefaultInstance();
        MyGiftModel model = realm.where(MyGiftModel.class).equalTo("id", id).findFirst();

        if(model != null){
            realm.beginTransaction();
            model.name = name;
            realm.insertOrUpdate(model);
            realm.commitTransaction();
            realm.close();
        } else {
            // сохраняем новый профиль
            realm.beginTransaction();
            realm.insertOrUpdate(this);
            realm.commitTransaction();
            realm.close();
        }
    }

    /**
     * Загрузка подарков с сервера
     * @param handler
     */
    public static void loadGifts(String token, String user_id, final Handler handler){

        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        Call<ResponseModel> call = service.user_gifts(token, user_id);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicChatModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            if(resp.items != null && resp.items.size() > 0){
                                for (MyGiftModel model : resp.items){
                                    model.insert();
                                }
                            }
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, null).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(HistoriesModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    public class Resp{
        @Expose
        public List<MyGiftModel> items;
    }
}
