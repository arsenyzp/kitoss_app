package pro.devapp.kitoss.models;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

/**
 * Базовая модель для ответа api
 */
public class ResponseModel {
    public int status;
    public String message;
    public Object data;
    public Object errors;

    public <T> T getData(Class<T> classOfT) {
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC).create();
        Log.d(ResponseModel.class.getName(), gson.toJson(data));
        return gson.fromJson(gson.toJson(data), classOfT);
    }
}
