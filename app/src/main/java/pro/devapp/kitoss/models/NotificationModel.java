package pro.devapp.kitoss.models;

import java.util.List;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Модель уведомлений
 */

public class NotificationModel extends RealmObject {

    public static final String TYPE_FRIENDS = "friends";
    public static final String TYPE_MESSAGES = "messages";
    public static final String TYPE_NOTIFICATIONS = "notifications";
    /**
     * Id
     */
    @PrimaryKey
    public int id;

    /**
     * Тип уведомления
     */
    public String type = "";

    public NotificationModel(){

    }

    /**
     * обновление/сохранение
     */
    public void insert(){
        Realm realm = Realm.getDefaultInstance();
        NotificationModel model = realm.where(NotificationModel.class).equalTo("id", id).findFirst();

        if(model != null){
            realm.beginTransaction();
            model.type = type;
            realm.insertOrUpdate(model);
            realm.commitTransaction();
            realm.close();
        } else {
            // сохраняем новый профиль
            realm.beginTransaction();
            realm.insertOrUpdate(this);
            realm.commitTransaction();
            realm.close();
        }
    }

    public void delete(){
        Realm realm = Realm.getDefaultInstance();
        NotificationModel model = realm.where(NotificationModel.class).equalTo("id", id).findFirst();
        realm.beginTransaction();
        model.deleteFromRealm();
        realm.commitTransaction();
        realm.close();
    }

    public static List<NotificationModel> getAll(){
        Realm realm = Realm.getDefaultInstance();
        List<NotificationModel> list = realm.where(NotificationModel.class).findAll();
        if(list != null){
            list = realm.copyFromRealm(list);
        }
        realm.close();
        return list;
    }

    public static List<NotificationModel> getAll(String type){
        Realm realm = Realm.getDefaultInstance();
        List<NotificationModel> list = realm.where(NotificationModel.class).equalTo("type", type).findAll();
        if(list != null){
            list = realm.copyFromRealm(list);
        }
        realm.close();
        return list;
    }
}
