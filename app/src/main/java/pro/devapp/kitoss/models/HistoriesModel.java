package pro.devapp.kitoss.models;

import android.app.Activity;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.util.Log;
import com.google.gson.annotations.Expose;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.Sort;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import pro.devapp.kitoss.api.Admins;
import pro.devapp.kitoss.api.Histories;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import pro.devapp.kitoss.components.Smiles;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Модель историй
 */

public class HistoriesModel {

    @Expose
    @PrimaryKey
    public String id;

    @Expose
    public int number;

    @Expose
    public String text;

    @Expose
    public String status;

    @Expose
    public int time;

    @Expose
    public int is_like = 0;

    @Expose
    public int likes_count;

    @Ignore
    public SpannableStringBuilder spannableText;

    /**
     * Загрузка историй с сервера
     * @param listener
     */
    public static void loadHistories(String token, int offset, int limit, final ResponseListener listener){

        Histories service = RetrofitInstance.getRetrofit().create(Histories.class);

        Call<ResponseModel> call = service.get(token, offset, limit);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    listener.onError("Empty result");
                } else  {
                    if(m.status != 200){
                        listener.onError(m.errors.toString());
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicChatModel.class.getName(), resp.toString());
                            // ошибка сервера
                            listener.onError(m.errors.toString());
                        } else {
                            // успешное получение данных
//                            if(resp.items != null && resp.items.size() > 0){
//                                for (HistoriesModel model : resp.items){
//                                    model.insert();
//                                }
//                            }
                            listener.onSuccess(resp.items);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(HistoriesModel.class.getName() , "onFailure");
                listener.onError("Server error");
            }
        });
    }

    /**
     * Установка лайка на сервере
     * @param listener
     */
    public static void like(String token, String id, final ResponseListener listener){

        Histories service = RetrofitInstance.getRetrofit().create(Histories.class);

        Call<ResponseModel> call = service.like(token, id);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    listener.onError("Empty result");
                } else  {
                    if(m.status != 200){
                        listener.onError(m.errors.toString());
                    } else {
                        HistoriesModel model = m.getData(HistoriesModel.class);
                        if(model == null){
                            // ошибка сервера
                            listener.onError(m.errors.toString());
                        } else {
                            // успешное получение данных
                            listener.onSuccess(model);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(HistoriesModel.class.getName() , "onFailure");
                listener.onError("Server error");
            }
        });
    }

    public class Resp{
        @Expose
        public List<HistoriesModel> items;
    }

    /**
     * Админская функция принятия истории
     * @param handler
     */
    public static void accept(String token, String id, final Handler handler){

        Admins service = RetrofitInstance.getRetrofit().create(Admins.class);

        Call<ResponseModel> call = service.accept_history(token, id);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                    } else {
                        HistoriesModel model = m.getData(HistoriesModel.class);
                        if(model == null){
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, null).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(HistoriesModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Админская функция удаления истории
     * @param handler
     */
    public static void decline(String token, String id, final Handler handler){

        Admins service = RetrofitInstance.getRetrofit().create(Admins.class);

        Call<ResponseModel> call = service.decline_history(token, id);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                    } else {
                        HistoriesModel model = m.getData(HistoriesModel.class);
                        if(model == null){
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, null).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(HistoriesModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    public abstract static class ResponseListener{
        public abstract void onSuccess(HistoriesModel historiesModel);
        public abstract void onSuccess(List<HistoriesModel> historiesModels);
        public abstract void onError(String error);
    }
}
