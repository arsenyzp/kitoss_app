package pro.devapp.kitoss.models;

import android.app.Activity;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import pro.devapp.kitoss.api.MyContacts;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.ContactHelper;
import pro.devapp.kitoss.components.RetrofitInstance;
import retrofit2.Call;

/**
 * Модель контактов в телефоне
 */

public class ContactModel extends RealmObject{

    @PrimaryKey
    public int id;

    public String number;

    public Boolean is_in_kitoss = false;

    public ContactModel(){

    }

    public ContactModel(int id, String number){
        this.id = id;
        this.number = number;
    }

    /**
     * Обновление списка контактов и их адресов
     * @param context
     * @return
     */
    public static boolean update(Activity context){
        // Удаляем все контакты
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(ContactModel.class);
        realm.commitTransaction();

        // Заносим список
        //"data4 <> ''"
        Cursor phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        String[] numbers = new String[phones.getCount()];
        int[] ids = new int[phones.getCount()];
        int i = 0;
        while (phones.moveToNext())
        {
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));
            if(phoneNumber == null){
                phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            }
            int cid = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID));
            //Log.d(ContactModel.class.getName(), String.valueOf(cid));
            if(phoneNumber == null || phoneNumber.length() < 5 || cid < 1){
                continue;
            }
            ContactModel m = new ContactModel(cid, phoneNumber);
            realm.beginTransaction();
            realm.insertOrUpdate(m);
            realm.commitTransaction();

            phoneNumber = phoneNumber.replaceAll("[+]", "");
            numbers[i] = ContactHelper.md5(phoneNumber);
            Log.d(ContactModel.class.getName(), numbers[i]);
            ids[i] = cid;
            i++;
        }
        phones.close();
        realm.close();
        // Делаем запрос на сервер
        MyContacts service = RetrofitInstance.getRetrofit().create(MyContacts.class);
        Call<ResponseModel> call = service.check(((BaseActivity)context).token, numbers, ids);

        try {
            ResponseModel responseModel = call.execute().body();
            if(responseModel == null){
                Log.d(ContactModel.class.getName(), "response is null");
                return false;
            } else  {
                if(responseModel.status != 200){
                    Log.d(ContactModel.class.getName(), "response is " + responseModel.status);
                    return false;
                } else {
                    Log.d(ContactModel.class.getName(), responseModel.status + "");
                    Resp resp = responseModel.getData(Resp.class);
                    Log.d(ContactModel.class.getName(), "Items: " + resp.items.length + "");
                    if(resp == null || resp.items.length == 0){
                        Log.d(ContactModel.class.getName(), resp.toString());
                        // ошибка сервера
                        return false;
                    } else {
                        Realm r = Realm.getDefaultInstance();
                        // успешное получение данных
                        for (int id : resp.items){
                            ContactModel contactModel = r.where(ContactModel.class).equalTo("id", id).findFirst();
                            r.beginTransaction();
                            contactModel.is_in_kitoss = true;
                            r.insertOrUpdate(contactModel);
                            r.commitTransaction();
                        }
                        r.close();
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(ContactModel.class.getName(), e.toString());
        }
        return true;
    }

    public class Resp{
        public int[] items;
    }

    /**
     * Список существующих контактов
     * @return
     */
    public static ArrayList<Integer> getExists(){
        Log.d(ContactModel.class.getName(), "getExists");
        ArrayList<Integer> ids = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        List<ContactModel> models = realm.where(ContactModel.class).equalTo("is_in_kitoss", true).findAll();
        Log.d(ContactModel.class.getName(), "Exists: " + models.size());
        for (ContactModel m : models){
            ids.add(Integer.valueOf(m.id));
        }
        realm.close();
        return ids;
    }

    /**
     * Отправка инвайта
     * @param phone
     */
    public static boolean sendInvite(String phone, String token){
        // Делаем запрос на сервер
        MyContacts service = RetrofitInstance.getRetrofit().create(MyContacts.class);
        Call<ResponseModel> call = service.send_invite(token, phone);

        try {
            ResponseModel responseModel = call.execute().body();
            if(responseModel == null){
                Log.d(ContactModel.class.getName(), "response is null");
                return false;
            } else  {
                if(responseModel.status != 200){
                    Log.d(ContactModel.class.getName(), "response is " + responseModel.status);
                    return false;
                } else {
                    Log.d(ContactModel.class.getName(), responseModel.status + " send success");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(ContactModel.class.getName(), e.toString());
        }
        return true;
    }
}
