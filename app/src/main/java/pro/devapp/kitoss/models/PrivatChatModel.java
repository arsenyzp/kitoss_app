package pro.devapp.kitoss.models;


import android.os.Handler;
import android.util.Log;
import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.annotations.PrimaryKey;
import pro.devapp.kitoss.api.Messages;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RealmInstance;
import pro.devapp.kitoss.components.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Модель сообщений для приватного чата
 */
public class PrivatChatModel extends RealmObject{

    /**
     * id сообщения на сервере
     */
    @Expose
    @PrimaryKey
    public String mid = "";

    /**
     * Локальный id
     */
    @Expose
    public String uiid = "";

    /**
     * Получатель
     */
    @Expose
    public String recipient_id = "";

    /**
     * Отправитель
     */
    @Expose
    public String sender_id = "";

    @Expose
    public String message = "";

    /**
     * Серверное время отправки
     */
    @Expose
    public long time = 0;

    /**
     * Статус сообщения
     */
    public int is_read = 0;

    /**
     * Статус доставки
     */
    public int is_delivery = 0;

    @Expose
    public PublicUserModel publicUserModel;

    public PrivatChatModel(){
        super();
    }

    /**
     * Return cursor for result set for all users with some status
     * @return
     */
    public static  RealmResults<PrivatChatModel> fetchResultCursor(String user_id, String oponent_id) {

        //String where = "(recipient_id = '" + user_id +"' AND sender_id = '" + oponent_id +"') OR (sender_id = '" + user_id +"' AND recipient_id = '" + oponent_id +"')";
        // отмечаем все сообщения как прочитанные
        Realm realm = Realm.getDefaultInstance();
//        List<PrivatChatModel> privatChatModelList = realm
//                .where(PrivatChatModel.class)
//                .notEqualTo("is_read", 1)
//                .equalTo("sender_id", oponent_id)
//                .findAll();
//        realm.beginTransaction();
//        for (int i = 0; i < privatChatModelList.size(); i++ ){
//            PrivatChatModel privatChatModel = privatChatModelList.get(i);
//            privatChatModel.is_read = 1;
//            realm.insertOrUpdate(privatChatModel);
//        }
//        realm.commitTransaction();
        // выбираем список
        RealmResults<PrivatChatModel> results = realm
                .where(PrivatChatModel.class)
                .beginGroup()
                .equalTo("recipient_id", user_id)
                .equalTo("sender_id", oponent_id)
                .endGroup()
                .or()
                .beginGroup()
                .equalTo("recipient_id", oponent_id)
                .equalTo("sender_id", user_id)
                .endGroup()
                .findAllSorted("time", Sort.DESCENDING);

        realm.beginTransaction();
        for (PrivatChatModel privatChatModel : results){
            privatChatModel.is_read = 1;
            realm.insertOrUpdate(privatChatModel);
        }
        realm.commitTransaction();
        realm.close();
        return results;
    }


    /**
     * Return cursor for result set for all users with some status
     * @return
     */
    public static  List<PrivatChatModel> fetchResultCursor(String user_id, String oponent_id, int offset, int limit) {
        // отмечаем все сообщения как прочитанные
        Realm realm = Realm.getDefaultInstance();
        RealmResults<PrivatChatModel> results = realm
                .where(PrivatChatModel.class)
                .beginGroup()
                .equalTo("recipient_id", user_id)
                .equalTo("sender_id", oponent_id)
                .endGroup()
                .or()
                .beginGroup()
                .equalTo("recipient_id", oponent_id)
                .equalTo("sender_id", user_id)
                .endGroup()
                .findAllSorted("time", Sort.DESCENDING);

        realm.beginTransaction();
        for (PrivatChatModel privatChatModel : results){
            privatChatModel.is_read = 1;
            realm.insertOrUpdate(privatChatModel);
        }
        realm.commitTransaction();


        List<PrivatChatModel> res = realm.copyFromRealm(results);
        if(res.size() > 10){
            res = res.subList(0, 10);
        }

        realm.close();
        return res;
    }

    public Boolean insert(){
        Realm realm = Realm.getDefaultInstance();
        PrivatChatModel model = realm.where(PrivatChatModel.class).equalTo("mid", mid).or().equalTo("uiid", uiid).findFirst();
        if(model != null){
            realm.beginTransaction();
            model.time = time;
            if(model.mid == null || model.mid.equals("")){
                model.mid = mid;
            }
            model.is_delivery = is_delivery;
            model.publicUserModel = realm.where(PublicUserModel.class).equalTo("id", publicUserModel.id).findFirst();
            realm.insertOrUpdate(model);
            realm.commitTransaction();
            realm.close();
            return false;
        } else {
            model = this;
            realm.beginTransaction();
            realm.insertOrUpdate(model);
            realm.commitTransaction();
            realm.close();
            return true;
        }
    }

    public static int getCountNew(String my_id){
        Realm realm = Realm.getDefaultInstance();
        int result = (int) realm.where(PrivatChatModel.class).equalTo("is_read", 0).equalTo("recipient_id", my_id).count();
        realm.close();
        return result;
    }

    /**
     * Получение всех последних сообщений (для списка сообщений)
     * @return
     */
    public static List<PrivatChatModel> getLatests(String user_id){
        List<PrivatChatModel> messageModels = RealmInstance.getRealm()
                .where(PrivatChatModel.class)
                .equalTo("sender_id", user_id)
                .or()
                .equalTo("recipient_id", user_id)
                .findAllSorted("time", Sort.DESCENDING);
        List<String> users = new ArrayList<>();
        List<PrivatChatModel> messageModelsRes = new ArrayList<>();
        for (PrivatChatModel message : messageModels){
            String oponent_id = message.sender_id;
            if(oponent_id.equals(user_id)){
                oponent_id = message.recipient_id;
            }
            if(users.indexOf(oponent_id) > -1){
                continue;
            }
            if(PublicUserModel.getById(oponent_id) == null){
                continue;
            }
            users.add(oponent_id);
            messageModelsRes.add(message);
        }
        return messageModelsRes;
    }

    /**
     * Получаем последние ид последнего сообщения
     * @return
     */
    public static String getLastId(String user_id){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<PrivatChatModel> models = realm
                .where(PrivatChatModel.class)
                .equalTo("sender_id", user_id)
                .or()
                .equalTo("recipient_id", user_id)
                .findAllSorted("time", Sort.DESCENDING);
        PrivatChatModel model = models.where().findFirst();
        String id = "";
        if(model != null){
            id = model.mid;
        }
        realm.close();
        return id;
    }

    /**
     * Загрузка истории переписки с сервера
     * @param token
     * @param handler
     */
    public static void loadMyMessages(final String token, final Handler handler){

        Messages service = RetrofitInstance.getRetrofit().create(Messages.class);
        UserModel userModel = UserModel.getByToken(token);
        Call<ResponseModel> call = service.messages(token, getLastId(userModel.id));
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    if(handler != null){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
                    }
                } else  {
                    if(m.status != 200){
                        if(handler != null){
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        }
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null || resp.items.size() == 0){
                            Log.d(PrivatChatModel.class.getName(), resp.toString());
                            // ошибка сервера
                            if(handler != null){
                                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                            }
                        } else {

                            Realm realm = Realm.getDefaultInstance();
                            // успешное получение данных
                            for (PrivatChatModel privatChatModel : resp.items){
//                                privatChatModel.is_delivery = 1;
//                                privatChatModel.is_read = 0;
//                                if(privatChatModel.publicUserModel != null){
//                                    realm.beginTransaction();
//                                    realm.insertOrUpdate(model.publicUserModel);
//                                    realm.commitTransaction();
//                                    privatChatModel.insert();
//                                }

                                PrivatChatModel model = realm.where(PrivatChatModel.class).equalTo("mid", privatChatModel.mid).or().equalTo("uiid", privatChatModel.uiid).findFirst();

                                if(model != null){
                                    realm.beginTransaction();
                                    model.time = privatChatModel.time;
                                    model.mid = privatChatModel.mid;
                                    model.is_delivery = 1;
                                    realm.insertOrUpdate(model);
                                    realm.commitTransaction();
                                } else {
                                    PublicUserModel publicUserModel = PublicUserModel.getById(privatChatModel.publicUserModel.id);
                                    if(publicUserModel == null){
                                        privatChatModel.publicUserModel.update();
                                    }

                                    realm.beginTransaction();
                                    privatChatModel.publicUserModel = PublicUserModel.getById(privatChatModel.publicUserModel.id);
                                    privatChatModel.is_delivery = 1;

                                    realm.insertOrUpdate(privatChatModel);
                                    realm.commitTransaction();

                                    if(handler != null)
                                        handler.obtainMessage(1, Const.SOCKET_ACTION_NEW_MESSAGE, 1, privatChatModel).sendToTarget();


                                }

                            }

                            realm.close();
                            if(handler != null){
                                handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, null).sendToTarget();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PrivatChatModel.class.getName() , "onFailure");
                if(handler != null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
                }
            }
        });
    }

    /**
     * Загрузка диалога с сервера
     * @param token
     * @param handler
     */
    public static void loadMessagesByUser(final String token, String user_id, final Handler handler){

        Messages service = RetrofitInstance.getRetrofit().create(Messages.class);
        UserModel userModel = UserModel.getByToken(token);
        Call<ResponseModel> call = service.messages_by_user(token, user_id);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    if(handler != null){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
                    }
                } else  {
                    if(m.status != 200){
                        if(handler != null){
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        }
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null || resp.items.size() == 0){
                            Log.d(PrivatChatModel.class.getName(), resp.toString());
                            // ошибка сервера
                            if(handler != null){
                                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                            }
                        } else {

                            Realm realm = Realm.getDefaultInstance();
                            // успешное получение данных
                            for (PrivatChatModel privatChatModel : resp.items){
//                                privatChatModel.is_delivery = 1;
//                                privatChatModel.is_read = 0;
//                                if(privatChatModel.publicUserModel != null){
//                                    realm.beginTransaction();
//                                    realm.insertOrUpdate(model.publicUserModel);
//                                    realm.commitTransaction();
//                                    privatChatModel.insert();
//                                }

                                PrivatChatModel model = realm.where(PrivatChatModel.class).equalTo("mid", privatChatModel.mid).or().equalTo("uiid", privatChatModel.uiid).findFirst();

                                if(model != null){
                                    realm.beginTransaction();
                                    model.time = privatChatModel.time;
                                    model.mid = privatChatModel.mid;
                                    model.is_delivery = 1;
                                    realm.insertOrUpdate(model);
                                    realm.commitTransaction();
                                } else {
                                    PublicUserModel publicUserModel = PublicUserModel.getById(privatChatModel.publicUserModel.id);
                                    if(publicUserModel == null){
                                        privatChatModel.publicUserModel.update();
                                    }

                                    realm.beginTransaction();
                                    privatChatModel.publicUserModel = PublicUserModel.getById(privatChatModel.publicUserModel.id);
                                    privatChatModel.is_delivery = 1;

                                    realm.insertOrUpdate(privatChatModel);
                                    realm.commitTransaction();

                                    if(handler != null)
                                        handler.obtainMessage(1, Const.SOCKET_ACTION_NEW_MESSAGE, 1, privatChatModel).sendToTarget();


                                }

                            }

                            realm.close();
                            if(handler != null){
                                handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, null).sendToTarget();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PrivatChatModel.class.getName() , "onFailure");
                if(handler != null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
                }
            }
        });
    }

    /**
     * Загрузка последних сообщений с сервера
     * @param token
     * @param handler
     */
    public static void loadLastMessages(final String token, final Handler handler){

        Messages service = RetrofitInstance.getRetrofit().create(Messages.class);
        UserModel userModel = UserModel.getByToken(token);
        Call<ResponseModel> call = service.last_messages(token);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    if(handler != null){
                        handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
                    }
                } else  {
                    if(m.status != 200){
                        if(handler != null){
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        }
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null || resp.items.size() == 0){
                            Log.d(PrivatChatModel.class.getName(), resp.toString());
                            // ошибка сервера
                            if(handler != null){
                                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                            }
                        } else {

                            Realm realm = Realm.getDefaultInstance();
                            // успешное получение данных
                            for (PrivatChatModel privatChatModel : resp.items){
//                                privatChatModel.is_delivery = 1;
//                                privatChatModel.is_read = 0;
//                                if(privatChatModel.publicUserModel != null){
//                                    realm.beginTransaction();
//                                    realm.insertOrUpdate(model.publicUserModel);
//                                    realm.commitTransaction();
//                                    privatChatModel.insert();
//                                }

                                PrivatChatModel model = realm.where(PrivatChatModel.class).equalTo("mid", privatChatModel.mid).or().equalTo("uiid", privatChatModel.uiid).findFirst();

                                if(model != null){
                                    realm.beginTransaction();
                                    model.time = privatChatModel.time;
                                    model.mid = privatChatModel.mid;
                                    model.is_delivery = 1;
                                    realm.insertOrUpdate(model);
                                    realm.commitTransaction();
                                } else {
                                    PublicUserModel publicUserModel = PublicUserModel.getById(privatChatModel.publicUserModel.id);
                                    if(publicUserModel == null){
                                        privatChatModel.publicUserModel.update();
                                    }

                                    realm.beginTransaction();
                                    privatChatModel.publicUserModel = PublicUserModel.getById(privatChatModel.publicUserModel.id);
                                    privatChatModel.is_delivery = 1;

                                    realm.insertOrUpdate(privatChatModel);
                                    realm.commitTransaction();

                                    if(handler != null)
                                        handler.obtainMessage(1, Const.SOCKET_ACTION_NEW_MESSAGE, 1, privatChatModel).sendToTarget();


                                }

                            }

                            realm.close();
                            if(handler != null){
                                handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, null).sendToTarget();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PrivatChatModel.class.getName() , "onFailure");
                if(handler != null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
                }
            }
        });
    }

    public class Resp{
        @Expose
        public List<PrivatChatModel> items;
    }

    /**
     * Удалить все сообщения
     */
    public static void deleteAll(){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(PrivatChatModel.class);
        realm.commitTransaction();
        realm.close();
    }
}
