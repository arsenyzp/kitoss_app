package pro.devapp.kitoss.models;

import android.app.Activity;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.util.Log;
import com.google.gson.annotations.Expose;
import org.json.JSONArray;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import pro.devapp.kitoss.api.PublicMessages;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import pro.devapp.kitoss.components.Smiles;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Модель сообщений для общих чатов
 */

public class PublicChatModel{

    /**
     * id сообщения на сервере
     */
    @Expose
    public String mid = "";

    /**
     * Локальный id
     */
    @Expose
    public String uiid = "";

    /**
     * Отправитель
     */
    @Expose
    public String sender_id = "";

    @Expose
    public String message = "";

    /**
     * Серверное время отправки
     */
    @Expose
    public long time = 0;

    /**
     * Статус сообщения
     */
    public int is_read = 0;

    /**
     * Статус доставки
     */
    @Expose
    public int is_delivery = 0;

    @Expose
    public PublicUserModel publicUserModel;

    @Ignore
    public SpannableStringBuilder spannableMessage;

    @Expose
    public String region;

    public PublicChatModel(){

    }

    /**
     * Загрузка истории переписки с сервера
     * @param token
     * @param listener
     */
    public static void loadHistoryMessages(String token, int offset, int limit, String region, final ResponseListener listener){

        PublicMessages service = RetrofitInstance.getRetrofit().create(PublicMessages.class);

        Call<ResponseModel> call = service.messages(token, offset, limit, region);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    listener.onError("Empty result");
                } else  {
                    if(m.status != 200){
                        listener.onError(m.errors.toString());
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicChatModel.class.getName(), resp.toString());
                            // ошибка сервера
                            listener.onError(m.errors.toString());
                        } else {
                            if(resp.items != null && resp.items.size() > 0){
                                for (PublicChatModel model : resp.items){
                                    model.publicUserModel.update();
//                                    model.user = PublicUserModel.getById(model.user.id);
//                                    model.owner = userModel.id;
//                                    Log.d(PublicChatModel.class.getName(),  model.user.id + "");
//                                    model.insert();
                                }
                            }
                            // успешное получение данных
                            if(resp.items.size() > 0){
                                listener.onSuccess(resp.items);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PublicChatModel.class.getName() , "onFailure");
                listener.onError("Error request");
            }
        });
    }

    public class Resp{
        @Expose
        public List<PublicChatModel> items;
    }

    public static abstract class ResponseListener{
        public abstract void onSuccess(List<PublicChatModel> items);
        public abstract void onError(String message);
    }
}
