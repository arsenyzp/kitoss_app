package pro.devapp.kitoss.models;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.api.GAuth;
import pro.devapp.kitoss.api.GPAuth;
import pro.devapp.kitoss.api.LoginService;
import pro.devapp.kitoss.api.OkAuth;
import pro.devapp.kitoss.api.RegisterUser;
import pro.devapp.kitoss.api.ResendPassword;
import pro.devapp.kitoss.api.VkAuth;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Авторизация / регистрация / восстановлние пароля
 */
public class AuthModel {

    /**
     * Запрос регистрации
     * @param ctx
     */
    public static void register(
            final Activity ctx,
            String phone,
            String first_name,
            String gender,
            String promo_cod,
            String birth_date,
            final Handler handler
    ){

        RegisterUser service = RetrofitInstance.getRetrofit().create(RegisterUser.class);

        Call<ResponseModel> call = service.register(phone, first_name, gender, promo_cod, birth_date);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
                } else  {
                    if(m.status != 200){
                        Log.d(AuthModel.class.getName(), m.errors.toString());
                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();
                        Log.d("Error from server ", gson.toJson(m));
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, gson.toJson(m)).sendToTarget();
                    } else {
                        // сохраняем модель пользователя
                        UserModel userModel = m.getData(UserModel.class);
                        userModel.update();
                        Log.d(AuthModel.class.getName() , userModel.token);
                        // сохраняем токен
                        ((App)ctx.getApplication()).getAppPrefs().saveStringPreference(Const.PREFERENCE_TOKEN, userModel.token);
                        handler.obtainMessage(1, Const.STATE_SUCCESS_REGISTER, 1, userModel).sendToTarget();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(AuthModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
            }
        });
    }


    /**
     * Повторная отправка пароля
     * @param ctx
     * @param phone
     * @param handler
     */
    public static void resendPassword(final Activity ctx, String phone, final Handler handler){

        ResendPassword service = RetrofitInstance.getRetrofit().create(ResendPassword.class);

        Call<ResponseModel> call = service.resend(phone);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
                } else  {
                    if(m.status != 200){
                        Log.d(AuthModel.class.getName(), m.errors.toString());
                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();
                        Log.d("Error from server ", gson.toJson(m));
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, gson.toJson(m)).sendToTarget();
                    } else {
                        UserModel userModel = m.getData(UserModel.class);
                        Log.d(AuthModel.class.getName() , userModel.token);
                        handler.obtainMessage(1, Const.STATE_SUCCESS_RESEND_PASSWORD, 1, userModel).sendToTarget();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(AuthModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
            }
        });
    }

    /**
     * Логин
     * @param ctx
     * @param phone
     * @param handler
     */
    public static void login(final Activity ctx, String phone, String password, final Handler handler){

        LoginService service = RetrofitInstance.getRetrofit().create(LoginService.class);

        Call<ResponseModel> call = service.login(phone, password);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                Log.d(AuthModel.class.getName(), response.toString());
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
                } else  {
                    if(m.status != 200){
                        Log.d(AuthModel.class.getName(), m.errors.toString());
                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();
                        Log.d("Error from server ", gson.toJson(m));
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, gson.toJson(m)).sendToTarget();
                    } else {
                        /**
                         * После авторизации обновляем профиль
                         */
                        UserModel userModel = m.getData(UserModel.class);
                        if(userModel == null || userModel.token == null){
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            userModel.update();
                            ((App)ctx.getApplication()).getAppPrefs().saveStringPreference(Const.PREFERENCE_TOKEN, userModel.token);
                            Log.d(AuthModel.class.getName() , userModel.token);
                            handler.obtainMessage(1, Const.STATE_SUCCESS_AUTH, 1, userModel).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(AuthModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
            }
        });
    }

    /**
     * Регистрация/Авторизация через G+
     * @param ctx
     * @param token
     * @param handler
     */
    public static void gauth(final Activity ctx, String token, final Handler handler){

        GAuth service = RetrofitInstance.getRetrofit().create(GAuth.class);

        Call<ResponseModel> call = service.gauth(token);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
                } else  {
                    if(m.status != 200){
                        Toast.makeText(ctx, m.errors.toString(), Toast.LENGTH_SHORT).show();
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        UserModel userModel = m.getData(UserModel.class);
                        if(userModel == null || userModel.token == null){
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешная авторизация
                            Log.d(AuthModel.class.getName() , userModel.token);
                            userModel.update();
                            ((App)ctx.getApplication()).getAppPrefs().saveStringPreference(Const.PREFERENCE_TOKEN, userModel.token);
                            Log.d(AuthModel.class.getName() , userModel.token);
                            handler.obtainMessage(1, Const.STATE_SUCCESS_AUTH, 1, userModel).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(AuthModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
            }
        });
    }

    /**
     * Регистрация/Авторизация через G+
     * @param ctx
     * @param name
     * @param handler
     */
    public static void gpauth(final Activity ctx, String email, String id, String name, final Handler handler){

        GPAuth service = RetrofitInstance.getRetrofit().create(GPAuth.class);

        Call<ResponseModel> call = service.gpauth(email, id, name);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
                } else  {
                    if(m.status != 200){
                        Toast.makeText(ctx, m.errors.toString(), Toast.LENGTH_SHORT).show();
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        UserModel userModel = m.getData(UserModel.class);
                        if(userModel == null || userModel.token == null){
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешная авторизация
                            Log.d(AuthModel.class.getName() , userModel.token);
                            userModel.update();
                            ((App)ctx.getApplication()).getAppPrefs().saveStringPreference(Const.PREFERENCE_TOKEN, userModel.token);
                            Log.d(AuthModel.class.getName() , userModel.token);
                            handler.obtainMessage(1, Const.STATE_SUCCESS_AUTH, 1, userModel).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(AuthModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
            }
        });
    }

    /**
     * Авторизация через VK
     * @param ctx
     * @param token
     * @param user_id
     * @param handler
     */
    public static void vkauth(final Activity ctx, String token, String user_id, final Handler handler){
        VkAuth service = RetrofitInstance.getRetrofit().create(VkAuth.class);

        Call<ResponseModel> call = service.vkauth(token, user_id);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
                } else  {
                    if(m.status != 200){
                        Toast.makeText(ctx, m.errors.toString(), Toast.LENGTH_SHORT).show();
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        UserModel userModel = m.getData(UserModel.class);
                        if(userModel == null || userModel.token == null){
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешная авторизация
                            Log.d(AuthModel.class.getName() , userModel.token);
                            userModel.update();
                            ((App)ctx.getApplication()).getAppPrefs().saveStringPreference(Const.PREFERENCE_TOKEN, userModel.token);
                            Log.d(AuthModel.class.getName() , userModel.token);
                            handler.obtainMessage(1, Const.STATE_SUCCESS_AUTH, 1, userModel).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(AuthModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
            }
        });
    }

    /**
     * Авторизация через OK
     * @param ctx
     * @param access_token
     * @param session_secret_key
     * @param handler
     */
    public static void okauth(final Activity ctx, String access_token, String session_secret_key, final Handler handler){
        OkAuth service = RetrofitInstance.getRetrofit().create(OkAuth.class);

        Call<ResponseModel> call = service.okauth(access_token, session_secret_key);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
                } else  {
                    if(m.status != 200){
                        Toast.makeText(ctx, m.errors.toString(), Toast.LENGTH_SHORT).show();
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        UserModel userModel = m.getData(UserModel.class);
                        if(userModel == null || userModel.token == null){
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешная авторизация
                            Log.d(AuthModel.class.getName() , userModel.token);
                            userModel.update();
                            ((App)ctx.getApplication()).getAppPrefs().saveStringPreference(Const.PREFERENCE_TOKEN, userModel.token);
                            Log.d(AuthModel.class.getName() , userModel.token);
                            handler.obtainMessage(1, Const.STATE_SUCCESS_AUTH, 1, userModel).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(AuthModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, ctx.getString(R.string.error_server)).sendToTarget();
            }
        });
    }

}
