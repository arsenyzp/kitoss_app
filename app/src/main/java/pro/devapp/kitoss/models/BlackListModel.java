package pro.devapp.kitoss.models;

import android.os.Handler;
import android.util.Log;
import com.google.gson.annotations.Expose;
import java.util.ArrayList;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmObject;
import pro.devapp.kitoss.api.Users;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RealmInstance;
import pro.devapp.kitoss.components.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Список избранных пользователей
 */

public class BlackListModel extends RealmObject {
    //ид владельца
    @Expose
    public String owner_id;

    @Expose
    public String user_id;

    public BlackListModel(){}

    /**
     * Получить ид всех юзеров которые есть в blackList
     * @param owner_id
     * @return
     */
    public static ArrayList<String> getIds(String owner_id){
        Realm realm = Realm.getDefaultInstance();
        List<BlackListModel> models = realm.where(BlackListModel.class).equalTo("owner_id", owner_id).findAll();
                ArrayList<String> ids = new ArrayList<>();
        for(int i = 0; i < models.size(); i++){
            ids.add(models.get(i).user_id);
        }
        realm.close();
        return ids;
    }

    public static String[] getIdsArray(String owner_id){
        ArrayList<String> ids = BlackListModel.getIds(owner_id);
        return ids.toArray(new String[ids.size()]);
    }

    /**
     * Проверка наличия в списке
     * @param user_id
     * @param owner_id
     * @return
     */
    public static boolean check(String user_id, String owner_id){
        Realm realm = Realm.getDefaultInstance();
        Boolean res = realm.where(BlackListModel.class).equalTo("user_id", user_id).equalTo("owner_id", owner_id).count() > 0;
        realm.close();
        return res;
    }

    /**
     * Добавить в blackList
     * @param user_id
     * @param owner_id
     */
    public static void add(final String user_id, final String owner_id, String token, final Handler handler){
        if(check(user_id, owner_id)){
            return;
        }

        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        Call<ResponseModel> call = service.add_to_black_list(token, user_id);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicUserModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            BlackListModel model = new BlackListModel();
                            model.user_id = user_id;
                            model.owner_id = owner_id;

                            Realm realm = Realm.getDefaultInstance();
                            realm.beginTransaction();
                            realm.insertOrUpdate(model);
                            realm.commitTransaction();
                            realm.close();

                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, m.errors.toString()).sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PublicUserModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }

    /**
     * Удалить из blackList
     * @param user_id
     * @param owner_id
     */
    public static void remove(final String user_id, final String owner_id, String token, final Handler handler){

        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        Call<ResponseModel> call = service.remove_from_black_list(token, user_id);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicUserModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            Realm realm = Realm.getDefaultInstance();
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    BlackListModel model = realm
                                            .where(BlackListModel.class)
                                            .equalTo("user_id", user_id)
                                            .equalTo("owner_id", owner_id)
                                            .findFirst();
                                    if(model!=null){
                                        model.deleteFromRealm();
                                    }
                                    handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, "").sendToTarget();
                                }
                            });
                            realm.close();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PublicUserModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });

    }


    /**
     * Обновление список заблокированных с сервера
     * @param handler
     */
    public static void getBlack(final String token, final Handler handler){
        Users service = RetrofitInstance.getRetrofit().create(Users.class);

        Call<ResponseModel> call = service.black(token);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel m = response.body();
                if(m == null){
                    handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "Empty result").sendToTarget();
                } else  {
                    if(m.status != 200){
                        handler.obtainMessage(1, Const.STATE_ERROR_RESPONSE, 1, m.errors.toString()).sendToTarget();
                    } else {
                        Resp resp = m.getData(Resp.class);
                        if(resp == null){
                            Log.d(PublicUserModel.class.getName(), resp.toString());
                            // ошибка сервера
                            handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, m.errors.toString()).sendToTarget();
                        } else {
                            // успешное получение данных
                            Realm realm = Realm.getDefaultInstance();
                            realm.beginTransaction();
                            realm.delete(BlackListModel.class);
                            realm.commitTransaction();

                            UserModel owner = UserModel.getByToken(token);
                            List<BlackListModel> models = new ArrayList<>();
                            for (PublicUserModel model : resp.items){
                                Log.d(PublicUserModel.class.getName(), model.first_name);
                                model.update();

                                BlackListModel blackListModel = new BlackListModel();
                                blackListModel.owner_id = owner.id;
                                blackListModel.user_id = model.id;
                                models.add(blackListModel);
                            }


                            realm.beginTransaction();
                            realm.insertOrUpdate(models);
                            realm.commitTransaction();
                            realm.close();

                            handler.obtainMessage(1, Const.STATE_SUCCESS_RESPONSE, 1, "").sendToTarget();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.d(PublicUserModel.class.getName() , "onFailure");
                handler.obtainMessage(1, Const.STATE_SERVER_ERROR, 1, "error").sendToTarget();
            }
        });
    }


    public class Resp{
        public List<PublicUserModel> items;
    }

}
