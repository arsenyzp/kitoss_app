package pro.devapp.kitoss.models;

import java.util.List;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.Sort;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import pro.devapp.kitoss.R;

/**
 * Статистика использования смайликов
 */

public class SmilesHistoryModel extends RealmObject{

    @PrimaryKey
    public int smile_id;

    public int count_usage = 0;

    @Ignore
    public static int[] smile = {
            R.id.a_smile1,
            R.id.a_smile2,
            R.id.a_smile3,
            R.id.a_smile4,
            R.id.a_smile5,
    };

    public static void insert(int smile_id){
        Realm realm = Realm.getDefaultInstance();
        SmilesHistoryModel model = realm.where(SmilesHistoryModel.class).equalTo("smile_id", smile_id).findFirst();

        if(model != null){
            realm.beginTransaction();
            model.count_usage = model.count_usage+1;
            realm.insertOrUpdate(model);
            realm.commitTransaction();
            realm.close();
        } else {
            model = new SmilesHistoryModel();
            realm.beginTransaction();
            model.smile_id = smile_id;
            model.count_usage = 1;
            realm.insertOrUpdate(model);
            realm.commitTransaction();
            realm.close();
        }
    }

    public static List<SmilesHistoryModel> getSmiles(){
        Realm realm = Realm.getDefaultInstance();
        List<SmilesHistoryModel> list = realm.where(SmilesHistoryModel.class).findAllSorted("count_usage", Sort.DESCENDING);
        if(list != null){
            list = realm.copyFromRealm(list);
        }
        realm.close();
        return list;
    }
}
