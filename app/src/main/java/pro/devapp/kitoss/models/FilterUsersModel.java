package pro.devapp.kitoss.models;

/**
 * Модель параметров фильтра для поиска пользователей
 */
public class FilterUsersModel {
    public static String name = "";
    public static String phone = "";
    public static String gender = "";
    public static int age_from = 0;
    public static int age_to = 0;
    public static String region = "";
    public static Boolean only_online = false;
}
