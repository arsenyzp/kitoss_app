package pro.devapp.kitoss.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.models.AuthModel;

public class RegisterActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initHomeBtn(getString(R.string.label_register));

        ArrayAdapter<String> adapter_days = new ArrayAdapter<String>(this, R.layout.spinner_item, Const.days);
        adapter_days.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final Spinner birth_day = (Spinner) findViewById(R.id.birth_day);
        birth_day.setAdapter(adapter_days);

        final String[] month = {"-", getString(R.string.month1), getString(R.string.month2), getString(R.string.month3), getString(R.string.month4), getString(R.string.month5), getString(R.string.month6), getString(R.string.month7), getString(R.string.month8), getString(R.string.month9), getString(R.string.month10), getString(R.string.month11), getString(R.string.month12)};
        ArrayAdapter<String> adapter_month = new ArrayAdapter<String>(this, R.layout.spinner_item, month);
        adapter_month.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final Spinner birth_month = (Spinner) findViewById(R.id.birth_month);
        birth_month.setAdapter(adapter_month);

        final String[] years = new String[Const.max_year-Const.min_year];
        years[0] = "-";
        for(int y = Const.min_year+1; y < Const.max_year; y++){
            years[y - Const.min_year] = String.valueOf(y);
        }
        ArrayAdapter<String> adapter_years = new ArrayAdapter<String>(this, R.layout.spinner_item, years);
        adapter_month.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final Spinner birth_year = (Spinner) findViewById(R.id.birth_year);
        birth_year.setAdapter(adapter_years);

        String[] genders = {"-", getString(R.string.male), getString(R.string.female)};
        ArrayAdapter<String> adapter_genders = new ArrayAdapter<String>(this, R.layout.spinner_item, genders);
        adapter_genders.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final Spinner gender = (Spinner) findViewById(R.id.gender);
        gender.setAdapter(adapter_genders);

        final TextView name = (TextView) findViewById(R.id.name);
        final TextView promo = (TextView) findViewById(R.id.promo);
        final TextView phone = (TextView) findViewById(R.id.phone);
        phone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        final RelativeLayout birth_day_place = (RelativeLayout) findViewById(R.id.birth_day_place);
        final RelativeLayout birth_month_place = (RelativeLayout) findViewById(R.id.birth_month_place);
        final RelativeLayout birth_year_place = (RelativeLayout) findViewById(R.id.birth_year_place);
        final RelativeLayout gender_place = (RelativeLayout) findViewById(R.id.gender_place);

        /**
         * Terms of use
         */
        View terms_of_use = findViewById(R.id.terms_of_use);
        terms_of_use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, TermsOfUseActivity.class);
                startActivity(intent);
            }
        });

        /**
         * Регистрация
         */
        Button submit = (Button) findViewById(R.id.btn);
        if(submit != null)
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean error = false;
                // имя
                String value_name = name.getText().toString();
                if(value_name.equals("") || value_name.length() < 3){
                    //name.setError(getString(R.string.error_empty_name));
                    name.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                    error = true;
                } else {
                    //name.setError(null);
                    name.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                }

                // промо
                String value_promo = promo.getText().toString();

                // телефон
                String value_phone = phone.getText().toString();
                value_phone = value_phone.replaceAll("\\D+", "");
                if(value_phone.equals("") || value_phone.length() < 9){
                    //phone.setError(getString(R.string.error_empty_phone));
                    phone.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                    error = true;
                } else if(value_phone.indexOf("8") == 0 || value_phone.indexOf("0") == 0){
                    phone.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                    error = true;
                } else {
                    //phone.setError(null);
                    phone.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                }

                // день рождения
                int value_birth_day = birth_day.getSelectedItemPosition();
                if(value_birth_day < 1){
                    birth_day_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                    error = true;
                } else {
                    birth_day_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                }

                int value_birth_month = birth_month.getSelectedItemPosition();
                if(value_birth_month < 1){
                    birth_month_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                    error = true;
                } else {
                    birth_month_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                }

                int value_birth_year = birth_year.getSelectedItemPosition();
                if(value_birth_year < 1){
                    birth_year_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                    error = true;
                } else {
                    birth_year_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                }

                // пол
                int value_gender = gender.getSelectedItemPosition();
                if(value_gender < 1){
                    gender_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                    error = true;
                } else {
                    gender_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                }

                // если есть ошибки в форме
                if(error){
                    Toast.makeText(RegisterActivity.this, getString(R.string.error_in_form), Toast.LENGTH_SHORT).show();
                    return;
                }
                // формируем строку с датой рождения
                String v_bith_date = Const.days[value_birth_day] + "." + String.valueOf(value_birth_month) + "." + years[value_birth_year];
                // делаем запрос
                showWait();
                AuthModel.register(RegisterActivity.this, value_phone, value_name, Const.GENDERS[value_gender-1], value_promo, v_bith_date, handler);
            }
        });
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_SUCCESS_REGISTER:
                    hideWait();
                    Intent intent = new Intent(RegisterActivity.this, AppActivity.class);
                    intent.putExtra("after_register", true);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    break;
                case Const.STATE_ERROR_RESPONSE:
                    hideWait();
                    try {
                        JSONObject resp = new JSONObject((String)msg.obj);
                        if(!resp.isNull("errors")){
                            JSONObject errors = resp.getJSONObject("errors");
                            if(!errors.isNull("phone")){
                                Toast.makeText(RegisterActivity.this, errors.getString("phone"), Toast.LENGTH_SHORT).show();
                                final TextView phone = (TextView) findViewById(R.id.phone);
                                phone.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                            }
                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                    break;
                case Const.STATE_SERVER_ERROR:
                    hideWait();
                    Toast.makeText(RegisterActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}
