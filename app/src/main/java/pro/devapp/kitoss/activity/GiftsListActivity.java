package pro.devapp.kitoss.activity;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.adapters.GiftsAdapter;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RecyclerItemClickListener;
import pro.devapp.kitoss.models.GiftModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.UserModel;

public class GiftsListActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private GiftsAdapter adapter;
    private PublicUserModel publicUserModel;
    MenuItem balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gifts_list);

        if(getIntent().getExtras() == null){
            finish();
            return;
        }

        publicUserModel = PublicUserModel.getById(getIntent().getExtras().getString("uid"));
        if(publicUserModel == null){
            finish();
            return;
        }

        initHomeBtn(getString(R.string.title_gifts_list));

        recyclerView = (RecyclerView) findViewById(R.id.list);
        adapter = new GiftsAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter.update();

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener(){

            @Override
            public void onItemClick(View view, int position) {
                GiftModel giftModel = adapter.getItem(position);
                if(userModel.balance < giftModel.price){
                    showNoMoneyDialog();
                } else {
                    //showSendDialog(giftModel);

                    Intent intent = new Intent(GiftsListActivity.this, GiftSendActivity.class);
                    intent.putExtra("uid", publicUserModel.id);
                    intent.putExtra("gift_name", giftModel.name);
                    startActivity(intent);
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(balance != null){
            balance.setTitle(getString(R.string.balance_text, userModel.balance + ""));
        }
    }

    /**
     * Верхние меню
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_my_cash_actions, menu);

        balance = menu.findItem(R.id.balance);
        balance.setTitle(getString(R.string.balance_text, userModel.balance + ""));
        return true;
    }

    /**
     * Отпрвка подарка
     * @param giftModel
     */
    private void showSendDialog(final GiftModel giftModel){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.dialog_send_gif_title));
        builder.setMessage(getString(R.string.dialog_send_gif_text));
        builder.setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                final ProgressDialog progressDialog = new ProgressDialog(GiftsListActivity.this);
                progressDialog.setTitle("Отправка");
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
                GiftModel.sendGift(getIntent().getExtras().getString("uid"), token, giftModel.name, "", new Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        switch (msg.arg1) {
                            case Const.STATE_ACCESS_DINEDED:
                                progressDialog.dismiss();
                                logout();
                                break;
                            case Const.STATE_SERVER_ERROR:
                            case Const.STATE_ERROR_RESPONSE:
                                progressDialog.dismiss();
                                Toast.makeText(GiftsListActivity.this, "Ошибка отправки", Toast.LENGTH_LONG).show();
                                break;
                            case Const.STATE_SUCCESS_RESPONSE:
                                progressDialog.dismiss();
                                Toast.makeText(GiftsListActivity.this, "Подарок отправлен", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }
        });
        builder.setNegativeButton(getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Диалог с сообщением о нехватке баланса
     */
    private void showNoMoneyDialog(){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.dialog_not_money_title));
        builder.setMessage(getString(R.string.dialog_not_money_text));
        builder.setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(GiftsListActivity.this, MyCashActivity.class);
                dialogInterface.dismiss();
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onSocketUpdateBalance(UserModel userModel) {
        if(balance != null){
            balance.setTitle(getString(R.string.balance_text, userModel.balance + ""));
        }
    }
}
