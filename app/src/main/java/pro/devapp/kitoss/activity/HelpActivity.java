package pro.devapp.kitoss.activity;

import android.os.Bundle;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.BaseActivity;

public class HelpActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
    }
}
