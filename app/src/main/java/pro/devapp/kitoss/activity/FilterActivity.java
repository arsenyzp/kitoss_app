package pro.devapp.kitoss.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import java.util.Arrays;
import java.util.List;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.models.FilterUsersModel;
import pro.devapp.kitoss.models.RegionModel;

public class FilterActivity extends BaseActivity {

    Spinner gender;
    Spinner age_from;
    Spinner age_to;
    Spinner region;
    EditText name;
    EditText phone;
    CheckBox only_online;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        initHomeBtn(getString(R.string.title_filter));

        name = (EditText) findViewById(R.id.name);
        name.setText(FilterUsersModel.name);

        phone = (EditText) findViewById(R.id.phone);
        phone.setText(FilterUsersModel.phone);

        only_online = (CheckBox) findViewById(R.id.only_online);
        only_online.setChecked(FilterUsersModel.only_online);

        // пол
        final String[] genders = {getString(R.string.hint_gender), getString(R.string.male), getString(R.string.female)};
        ArrayAdapter<String> adapter_genders = new ArrayAdapter<String>(this, R.layout.spinner_item, genders);
        adapter_genders.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender = (Spinner) findViewById(R.id.gender);
        gender.setAdapter(adapter_genders);
        if(!FilterUsersModel.gender.equals("")){
            gender.setSelection(FilterUsersModel.gender.equals("male") ? 1 : 2);
        }


        final String[] years_from = new String[50];
        years_from[0] = getString(R.string.hint_age_from_filter);
        for(int y = 1; y < 50; y++){
            years_from[y] = String.valueOf(y+10);
        }

        final String[] years_to = new String[50];
        years_to[0] = getString(R.string.hint_age_to_filter);
        for(int y = 1; y < 50; y++){
            years_to[y] = String.valueOf(y+15);
        }

        ArrayAdapter<String> adapter_age_from = new ArrayAdapter<String>(this, R.layout.spinner_item, years_from);
        ArrayAdapter<String> adapter_age_to = new ArrayAdapter<String>(this, R.layout.spinner_item, years_to);
        age_from = (Spinner) findViewById(R.id.age_from);
        age_from.setAdapter(adapter_age_from);
        age_from.setSelection(FilterUsersModel.age_from);
        age_to = (Spinner) findViewById(R.id.age_to);
        age_to.setAdapter(adapter_age_to);
        age_to.setSelection(FilterUsersModel.age_to);

        List<RegionModel> regionModels = RegionModel.getAll();
        // названия регионов
        final String[] regions = new String[regionModels.size()+1];
        // коды регионов
        final String[] code_regions = new String[regionModels.size()+1];
        regions[0] = getString(R.string.hint_region_filter);
        code_regions[0] = "";
        for(int i = 1; i < regions.length; i++){
            regions[i] = regionModels.get(i-1).name;
            code_regions[i] = regionModels.get(i-1).cod;
        }
        ArrayAdapter<String> adapter_regions = new ArrayAdapter<String>(this, R.layout.spinner_item, regions);
        region = (Spinner) findViewById(R.id.region);
        region.setAdapter(adapter_regions);
        if(!FilterUsersModel.region.equals("")){
            region.setSelection(Arrays.asList(code_regions).indexOf(FilterUsersModel.region));
        }

        // кнопка поиска
        View btn = findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterUsersModel.name = name.getText().toString();
                FilterUsersModel.phone = phone.getText().toString();
                if(gender.getSelectedItemPosition() == 0){
                    FilterUsersModel.gender = "";
                } else {
                    FilterUsersModel.gender = gender.getSelectedItemPosition() == 1 ? "male" : "female";
                }
                FilterUsersModel.age_from = age_from.getSelectedItemPosition();
                FilterUsersModel.age_to = age_to.getSelectedItemPosition();
                FilterUsersModel.region = code_regions[region.getSelectedItemPosition()];
                FilterUsersModel.only_online = only_online.isChecked();
                finish();
            }
        });
    }
}
