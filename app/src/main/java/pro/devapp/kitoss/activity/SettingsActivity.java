package pro.devapp.kitoss.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;

public class SettingsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initHomeBtn(getString(R.string.title_settings));

        ToggleButton toggleButtonAutologin = (ToggleButton) findViewById(R.id.toggleButtonAutologin);
        toggleButtonAutologin.setChecked(((App)getApplication()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_AUTOLOGIN));
        toggleButtonAutologin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ((App)getApplication()).getAppPrefs().saveBooleanPreference(Const.PREFERENCE_AUTOLOGIN, b);
            }
        });

        ToggleButton toggleButtonSound = (ToggleButton) findViewById(R.id.toggleButtonSound);
        toggleButtonSound.setChecked(((App)getApplication()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_SOUND));
        toggleButtonSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ((App)getApplication()).getAppPrefs().saveBooleanPreference(Const.PREFERENCE_SOUND, b);
            }
        });

        ToggleButton toggleButtonVibro = (ToggleButton) findViewById(R.id.toggleButtonVibro);
        toggleButtonVibro.setChecked(((App)getApplication()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_VIBRO));
        toggleButtonVibro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ((App)getApplication()).getAppPrefs().saveBooleanPreference(Const.PREFERENCE_VIBRO, b);
            }
        });

        ToggleButton toggleButtonReceivePhoto = (ToggleButton) findViewById(R.id.toggleButtonReceivePhoto);
        toggleButtonReceivePhoto.setChecked(((App)getApplication()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_RECEIVE_PHOTO));
        toggleButtonReceivePhoto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ((App)getApplication()).getAppPrefs().saveBooleanPreference(Const.PREFERENCE_RECEIVE_PHOTO, b);
            }
        });
    }

    public void onClickSelectRegion(View v){
        Intent intent = new Intent(this, SettingsRegionActivity.class);
        startActivity(intent);
    }
}
