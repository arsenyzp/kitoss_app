package pro.devapp.kitoss.activity;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.models.GiftModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.UserModel;

public class GiftSendActivity extends BaseActivity {

    private PublicUserModel publicUserModel;
    private MenuItem balance;
    private GiftModel giftModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift_send);

        if(getIntent().getExtras() == null){
            finish();
            return;
        }

        publicUserModel = PublicUserModel.getById(getIntent().getExtras().getString("uid"));
        if(publicUserModel == null){
            finish();
            return;
        }

        String gift_name = getIntent().getExtras().getString("gift_name");
        giftModel = GiftModel.findByName(gift_name);

        initHomeBtn(getString(R.string.gift_for, publicUserModel.first_name));

        ImageView gift = (ImageView) findViewById(R.id.gift);
        gift.setImageDrawable(getResources().getDrawable(GiftModel.getDrawable(gift_name)));

        TextView price = (TextView) findViewById(R.id.price);
        price.setText(getString(R.string.price_text, giftModel.price+""));

        TextView user_name = (TextView) findViewById(R.id.user_name);
        user_name.setText(publicUserModel.first_name);

        final ImageView ava = (ImageView) findViewById(R.id.ava);
        Picasso.with(this)
                .load(Const.API_URL + publicUserModel.avatar)
                .placeholder(R.drawable.ava)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(ava, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(GiftSendActivity.this)
                                .load(Const.API_URL + publicUserModel.avatar)
                                .placeholder(R.drawable.ava)
                                .error(R.drawable.ava)
                                .into(ava);
                    }
                });


        final EditText message = (EditText) findViewById(R.id.message);

        Button btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog progressDialog = new ProgressDialog(GiftSendActivity.this);
                progressDialog.setTitle("Отправка");
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
                GiftModel.sendGift(getIntent().getExtras().getString("uid"), token, giftModel.name, message.getText().toString(), new Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        switch (msg.arg1) {
                            case Const.STATE_ACCESS_DINEDED:
                                progressDialog.dismiss();
                                logout();
                                break;
                            case Const.STATE_SERVER_ERROR:
                            case Const.STATE_ERROR_RESPONSE:
                                progressDialog.dismiss();
                                Toast.makeText(GiftSendActivity.this, "Ошибка отправки", Toast.LENGTH_LONG).show();
                                break;
                            case Const.STATE_SUCCESS_RESPONSE:
                                progressDialog.dismiss();
                                Toast.makeText(GiftSendActivity.this, "Подарок отправлен", Toast.LENGTH_LONG).show();

                                userModel.balance = userModel.balance - giftModel.price;
                                userModel.update();
                                if(balance != null){
                                    balance.setTitle(getString(R.string.balance_text, userModel.balance + ""));
                                }
                                onSocketUpdateBalance(userModel);

                                Intent intent = new Intent(GiftSendActivity.this, PublicProfileActivity.class);
                                intent.putExtra("uid", publicUserModel.id);
                                intent.putExtra("gift", true);
                                startActivity(intent);
                                break;
                        }
                    }
                });
            }
        });
    }

    /**
     * Верхние меню
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_my_cash_actions, menu);

        balance = menu.findItem(R.id.balance);
        balance.setTitle(getString(R.string.balance_text, userModel.balance + ""));
        return true;
    }
}
