package pro.devapp.kitoss.activity;

import android.os.Bundle;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.BaseActivity;

public class TermsOfUseActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initHomeBtn(getString(R.string.label_terms_of_use));
        setContentView(R.layout.activity_terms_of_use);
    }
}
