package pro.devapp.kitoss.activity;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.vending.billing.IInAppBillingService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.api.Admins;
import pro.devapp.kitoss.api.Payment;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.RetrofitInstance;
import pro.devapp.kitoss.models.HistoriesModel;
import pro.devapp.kitoss.models.ResponseModel;
import pro.devapp.kitoss.payment.IabHelper;
import pro.devapp.kitoss.payment.IabResult;
import pro.devapp.kitoss.payment.Inventory;
import pro.devapp.kitoss.payment.Purchase;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GooglePaymentActivity extends BaseActivity {

    // Индефикатор продукта
    String SKU_PREMIUM = "15_kitsov";

    // (произвольный) код для возвращения в приложение данных при покупке из приложения "Play Маркет"
    static final int RC_REQUEST = 10001;

    // класс помощника
    IabHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_payment);

        final String base64EncodedPublicKey =
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4fS6Y2B8evGbbT4881cCnLfA4Q9qzRwNZMdUBxL1sHfm3OWUPK5mn5IN83CgNNMqZIvLNihRPoTQvEmkS2ZJryhHgz/n0c7bntqukC9GcYHjhwOWjr4+Xaed+5+43UDBpUH5o3O0xgBSFeiKkXPylvfPF2ITkQneuJ9iecZHpgLd9y/GpXOAF1MVM+MLk/aQ2QkOud2w1FV3nzRaRYDIQBUbFtzYcyTROgTwrv3SxtF4fGagEEoozRCKO/BfFyeQo24Nb1KrUj2mJnoQqvCh5t9LENqAfDqZ09hxMub3XyWkzFcepANiBFPPH1zHNpXjpPc2UeSKy/1aIiRtjZD/MQIDAQAB";

        switch (getIntent().getExtras().getInt("pay")){
            case 1:
                SKU_PREMIUM = "15_kitsov";
                break;
            case 2:
                SKU_PREMIUM = "30_kitsov";
                break;
            case 3:
                SKU_PREMIUM = "100_kitsov";
                break;
        }

        mHelper = new IabHelper(this, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);

        Log.d(GooglePaymentActivity.class.getName(), "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(GooglePaymentActivity.class.getName(), "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    complain("Problem setting up in-app billing: " + result);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.d(GooglePaymentActivity.class.getName(), "Setup successful. Querying inventory.");
                //mHelper.queryInventoryAsync(mGotInventoryListener);


                try {
                    Bundle bundle = mHelper.getmService().getPurchases(3, getPackageName(), "15_kitsov", base64EncodedPublicKey);
                    String INAPP_CONTINUATION_TOKEN = bundle.getString("INAPP_CONTINUATION_TOKEN");
                    alert(INAPP_CONTINUATION_TOKEN);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }


            }
        });

    }

    // We're being destroyed. It's important to dispose of the helper here!
    @Override
    public void onDestroy() {
        super.onDestroy();

        // very important:
        Log.d(GooglePaymentActivity.class.getName(), "Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(GooglePaymentActivity.class.getName(), "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d(GooglePaymentActivity.class.getName(), "onActivityResult handled by IABUtil.");
        }
    }

    /** Verifies the developer payload of a purchase. */
    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, final Purchase purchase) {
            Log.d(GooglePaymentActivity.class.getName(), "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                complain("Error purchasing: " + result);
                setWaitScreen(false);
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");
                setWaitScreen(false);
                return;
            }

            Log.d(GooglePaymentActivity.class.getName(), "Purchase successful.");

            if (purchase.getSku().equals(SKU_PREMIUM)) {
                // bought 1/4 tank of gas. So consume it.
                Log.d(GooglePaymentActivity.class.getName(), "Purchase is gas. Starting gas consumption.");

                Payment service = RetrofitInstance.getRetrofit().create(Payment.class);
                Call<ResponseModel> call = service.gpayment(token, SKU_PREMIUM);
                call.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        //ResponseModel m = response.body();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alert("Покупка успешна!");
                                setWaitScreen(false);

                                // Note the null is the continueToken you may not get all of the purchased items
                                // in one call - check ownedItems.getString("INAPP_CONTINUATION_TOKEN") for
                                // the next continueToken and re-call with that until you don't get a token

                                Bundle ownedItems = null;
                                try {
                                    ownedItems = mHelper.getmService().getPurchases(3, getPackageName(), "inapp", null);
                                } catch (RemoteException e) {
                                    e.printStackTrace();
                                }

                                // Check response
                                int responseCode = ownedItems.getInt("RESPONSE_CODE");
                                if (responseCode != 0) {
                                    alert("Покупка не использована!");
                                }

                                // Get the list of purchased items
                                ArrayList<String> purchaseDataList =
                                        ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                                for (String purchaseData : purchaseDataList) {
                                    JSONObject o = null;
                                    try {
                                        o = new JSONObject(purchaseData);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    String purchaseToken = o.optString("token", o.optString("purchaseToken"));
                                    // Consume purchaseToken, handling any errors
                                    try {
                                        mHelper.getmService().consumePurchase(3, getPackageName(), purchaseToken);
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                }

                                alert("Покупка использована!");
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                alert("Ошибка сохранения баланса!");
                                setWaitScreen(false);
                            }
                        });
                    }
                });
            }
        }
    };

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(GooglePaymentActivity.class.getName(), "Query inventory finished.");


            String payload = "pro.devapp.kitoss.activity_" + SKU_PREMIUM;

            mHelper.launchPurchaseFlow(GooglePaymentActivity.this, SKU_PREMIUM, RC_REQUEST,
                    mPurchaseFinishedListener, payload);
        }
    };

    // Enables or disables the "please wait" screen.
    void setWaitScreen(boolean set) {

    }

    void complain(String message) {
        Log.e(GooglePaymentActivity.class.getName(), "**** TrivialDrive Error: " + message);
        alert("Error: " + message);
    }

    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Log.d(GooglePaymentActivity.class.getName(), "Showing alert dialog: " + message);
        bld.create().show();
    }
}
