package pro.devapp.kitoss.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import org.json.JSONException;
import org.json.JSONObject;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.GAuthHelper;
import pro.devapp.kitoss.components.NotificationHelper;
import pro.devapp.kitoss.models.AuthModel;
import pro.devapp.kitoss.models.PrivatChatModel;
import pro.devapp.kitoss.models.PublicChatModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.PushRegisterModel;
import pro.devapp.kitoss.models.UserModel;
import ru.ok.android.sdk.Odnoklassniki;
import ru.ok.android.sdk.OkListener;
import ru.ok.android.sdk.util.OkAuthType;
import ru.ok.android.sdk.util.OkScope;

public class LoginActivity extends BaseActivity {

    protected Odnoklassniki odnoklassniki;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final TextView after_register = (TextView) findViewById(R.id.after_register);
        final TextView password = (TextView) findViewById(R.id.password);
        final TextView phone = (TextView) findViewById(R.id.phone);
        phone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        // показать строку повторной отправки
        if(getIntent().getExtras() != null && getIntent().getExtras().getBoolean("after_register", false)){
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    after_register.setVisibility(View.VISIBLE);
                }
            }, 4000l);
        }

        // переход после регистрации - заполняем телефон
        if(getIntent().getExtras() != null && getIntent().getExtras().getBoolean("after_register", false) &&
                !token.equals("")){
            final UserModel userModel = UserModel.getByToken(token);
            phone.setText(userModel.phone);

            if(after_register != null)
                after_register.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AuthModel.resendPassword(LoginActivity.this, userModel.phone, handler);
                    }
                });
        }

        // после восстановления пароля - заполняем телефон
        if(getIntent().getExtras() != null && getIntent().getExtras().getBoolean("after_restore", false)){
            phone.setText(getIntent().getExtras().getString("phone", ""));
        }

        //VK
        View vk_auth = findViewById(R.id.vk_auth);
        vk_auth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VKSdk.login(LoginActivity.this, "");
            }
        });

        //OK
        odnoklassniki = Odnoklassniki.createInstance(this, getString(R.string.ok_app_id), getString(R.string.ok_app_key));
        //TODO не ясно назанчение
        odnoklassniki.checkValidTokens(new OkListener() {
            @Override
            public void onSuccess(JSONObject json) {
                Log.d("LOGIN", json.toString());
            }

            @Override
            public void onError(String error) {
               // Toast.makeText(LoginActivity.this, getString(R.string.auth_canceled), Toast.LENGTH_LONG).show();
            }
        });
        View ok_auth = findViewById(R.id.ok_auth);
        ok_auth.setOnClickListener(new LoginClickListener(OkAuthType.ANY));

        // переход после выхода из приложения
        if(getIntent().getExtras() != null && getIntent().getExtras().getBoolean("after_logout", false)){
            odnoklassniki.clearTokens();
        }

        /**
         * Terms of use
         */
        View terms_of_use = findViewById(R.id.terms_of_use);
        terms_of_use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, TermsOfUseActivity.class);
                startActivity(intent);
            }
        });

        // Отправка формы
        Button submit = (Button) findViewById(R.id.btn);
        if(submit != null)
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Boolean error = false;

                    String value_password = password.getText().toString();

                    // телефон
                    String value_phone = phone.getText().toString();
                    value_phone = value_phone.replaceAll("\\D+", "");
                    if (value_phone.equals("") || value_phone.length() < 9) {
                        //phone.setError(getString(R.string.error_empty_phone));
                        phone.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                        error = true;
                    } else {
                        //phone.setError(null);
                        phone.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                    }

                    if (value_password.equals("") || value_password.length() < 2) {
                        //phone.setError(getString(R.string.error_empty_phone));
                        password.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                        error = true;
                    } else {
                        //phone.setError(null);
                        password.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                    }

                    if(error){
                        Toast.makeText(LoginActivity.this, getString(R.string.error_in_form), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    // делаем запрос
                    showWait();
                    AuthModel.login(LoginActivity.this, value_phone, value_password, handler);
                }
            });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Odnoklassniki.getInstance().isActivityRequestOAuth(requestCode)) {
            Odnoklassniki.getInstance().onAuthActivityResult(requestCode, resultCode, data, getAuthListener());
        } else if (Odnoklassniki.getInstance().isActivityRequestViral(requestCode)) {
            Odnoklassniki.getInstance().onActivityResultResult(requestCode, resultCode, data, getToastListener());
        } else if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                showWait();
                AuthModel.vkauth(LoginActivity.this, res.accessToken, res.userId, handler);
            }
            @Override
            public void onError(VKError error) {
                // User didn't pass Authorization
                Toast.makeText(LoginActivity.this, getString(R.string.auth_canceled), Toast.LENGTH_LONG).show();
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * Ok auth listener
     */
    @NonNull
    private OkListener getToastListener() {
        return new OkListener() {
            @Override
            public void onSuccess(final JSONObject json) {
                //TODO
                // Success OK auth
                Toast.makeText(LoginActivity.this, json.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(LoginActivity.this, String.format("%s: %s", "Error"), Toast.LENGTH_LONG).show();
            }
        };
    }

    /**
     * Listener that is run on OAUTH authorization completion
     */
    @NonNull
    private OkListener getAuthListener() {
        return new OkListener() {
            @Override
            public void onSuccess(final JSONObject json) {
                Log.d("LOGIN", json.toString());
                try {
                    // Send to server
                    showWait();
                    AuthModel.okauth(LoginActivity.this, json.getString("access_token"), json.getString("session_secret_key"), handler);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("LOGIN", e.toString());
                }
            }

            @Override
            public void onError(String error) {
                Log.d("LOGIN", error.toString());
                Toast.makeText(LoginActivity.this,
                        String.format("%s: %s", "Error", error),
                        Toast.LENGTH_SHORT).show();
            }
        };
    }

    /**
     * переход на экран восстановления пароля
     * @param v
     */
    public void onClickRemember(View v){
        Intent intent = new Intent(this, RestoreActivity.class);
        startActivity(intent);
    }

    /**
     * переход на экран регистрации
     * @param v
     */
    public void onClickRegister(View v){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_SUCCESS_AUTH:
                    token = ((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
//                    // свои сообщения
//                    PrivatChatModel.loadMyMessages(token, (App)getApplication());
//                    // public chats
//                    PublicChatModel.loadHistoryMessages(token, new Handler(){});
//                    // обновление друзей
//                    PublicUserModel.getFriends(0, 500, token, new Handler(){});

                    hideWait();
                    // отправляем токен для push
                    String gtoken = ((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_GCM_TOKEN);
                    Log.d("TOKEN", "push token");
                    Log.d("TOKEN", gtoken);
                    PushRegisterModel.regDevice(LoginActivity.this, token, gtoken);

                    Intent intent = new Intent(LoginActivity.this, AppActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    break;
                case Const.STATE_ERROR_RESPONSE:
                    hideWait();
                    try {
                        JSONObject resp = new JSONObject((String)msg.obj);
                        if(!resp.isNull("errors")){
                            JSONObject errors = resp.getJSONObject("errors");
                            if(!errors.isNull("phone")){
                                Toast.makeText(LoginActivity.this, errors.getString("phone"), Toast.LENGTH_SHORT).show();
                                final TextView phone = (TextView) findViewById(R.id.phone);
                                phone.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                            }
                            if(!errors.isNull("password")){
                                Toast.makeText(LoginActivity.this, errors.getString("password"), Toast.LENGTH_SHORT).show();
                                final TextView password = (TextView) findViewById(R.id.password);
                                password.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                            }
                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                    break;
                case Const.STATE_SERVER_ERROR:
                    hideWait();
                    Toast.makeText(LoginActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_RESEND_PASSWORD:
                    hideWait();
                    Toast.makeText(LoginActivity.this, getString(R.string.info_password_success_resend), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    /**
     * Клик по кнопке авторизация через OK
     */
    protected class LoginClickListener implements View.OnClickListener {
        private OkAuthType authType;

        public LoginClickListener(OkAuthType authType) {
            this.authType = authType;
        }

        @Override
        public void onClick(final View view) {
            odnoklassniki.requestAuthorization(LoginActivity.this, getString(R.string.ok_redirect_url), authType);
        }
    }
}
