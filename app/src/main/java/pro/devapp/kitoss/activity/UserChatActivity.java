package pro.devapp.kitoss.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.annotations.Expose;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.adapters.PrivateChatAdapter;
import pro.devapp.kitoss.api.UploadImage;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.ImagePicker;
import pro.devapp.kitoss.components.NotificationHelper;
import pro.devapp.kitoss.components.RetrofitInstance;
import pro.devapp.kitoss.components.SmileTextWatcher;
import pro.devapp.kitoss.components.Smiles;
import pro.devapp.kitoss.fragments.ListMessagesFragment;
import pro.devapp.kitoss.models.BlackListModel;
import pro.devapp.kitoss.models.FavoriteModel;
import pro.devapp.kitoss.models.NotificationModel;
import pro.devapp.kitoss.models.PrivatChatModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.ResponseModel;
import pro.devapp.kitoss.models.SmilesHistoryModel;
import pro.devapp.kitoss.models.UserModel;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.senab.photoview.PhotoViewAttacher;

public class UserChatActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private PrivateChatAdapter adapter;
    private EditText message;
    private View smiles;
    private RelativeLayout photo_overlay;
    private ImageView mImageView;
    private PhotoViewAttacher mAttacher;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageButton button_smile;

    PublicUserModel publicUserModel;

    private int offset = 0;
    private int limit = 20;

    int smile_size = 25;

    private static final int PICK_IMAGE_ID = 234;

    Boolean keyBoardShow = false;
    private int oldDiff = 0;

    private View typing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_chat);

        publicUserModel = PublicUserModel.getById(getIntent().getExtras().getString("uid"));
        initHomeBtn(publicUserModel.first_name);

        // смайлики
        smiles = findViewById(R.id.smiles);
        // размер смайла
        smile_size = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, getResources().getDisplayMetrics());

        /**
         * Фон
         */
        final ImageView user_background = (ImageView) findViewById(R.id.user_background);
        Picasso.with(this)
                .load(Const.API_URL + publicUserModel.background)
                .placeholder(R.drawable.full_bg)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(user_background, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(UserChatActivity.this)
                                .load(Const.API_URL + publicUserModel.background)
                                .placeholder(R.drawable.full_bg)
                                .error(R.drawable.full_bg)
                                .into(user_background);
                    }
                });

        // список сообщений
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
       // layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView = (RecyclerView) findViewById(R.id.list);
        adapter = new PrivateChatAdapter(this, userModel.id, publicUserModel.id, publicUserModel);
        recyclerView.setAdapter(adapter);

        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(30);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                int old_message_count = recyclerView.getAdapter().getItemCount();
                offset += limit;
                adapter.update(offset, limit);
                int new_messag_count = recyclerView.getAdapter().getItemCount() - old_message_count;
                if(new_messag_count == 0){
                    Toast.makeText(UserChatActivity.this, "Больше нет сообщений", Toast.LENGTH_LONG).show();
                }
                if(new_messag_count == limit) {
                    recyclerView.smoothScrollToPosition(offset + 5);
                } else if(new_messag_count > 0) {
                    recyclerView.smoothScrollToPosition(offset + new_messag_count);
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        message = (EditText) findViewById(R.id.message);

        // show smile
        button_smile = (ImageButton)findViewById(R.id.button_smile);
        button_smile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(smiles.getVisibility() != View.VISIBLE){
                    // скрываем клавиатуру
                    hideKeyboard();
                    // показываем смайлы
                    showSmiles();
                } else {
                    // скрываем смайлы
                    hideSmiles();
                    // показываем клавиатуру
                    showKeyboard();
                }
            }
        });
        // send message
        View button_send = findViewById(R.id.button_send);
        button_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = message.getText().toString();
                msg = msg.replaceAll("[\r\n]+", "\n");
                if(msg.equals("")){
                    message.setError(getString(R.string.error_empty_message));
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    message.setError(null);
                                }
                            });
                        }
                    }, 3000l);
                } else {
                    PrivatChatModel privatChatModel = new PrivatChatModel();
                    privatChatModel.message = msg;
                    privatChatModel.sender_id = userModel.id;
                    privatChatModel.recipient_id = publicUserModel.id;
                    privatChatModel.is_read = 1;
                    privatChatModel.time = System.currentTimeMillis();
                    privatChatModel.uiid = publicUserModel.id + "_" +System.currentTimeMillis() + "_" + userModel.id;
                    privatChatModel.publicUserModel = publicUserModel;
                    try {
                        sendMessage(privatChatModel);
                    } catch (JSONException e) {
                        Log.d(UserChatActivity.class.getName(), e.toString());
                    }
                    privatChatModel.insert();
                    adapter.update(offset, limit);
                    scrollTop();
                    message.setText("");
                    playSound(push_sound);
                }
            }
        });

        photo_overlay = (RelativeLayout) findViewById(R.id.photo_overlay);
        mImageView = (ImageView) findViewById(R.id.iv_photo);
        /**
         * Закрыть полноэкранный режим просмотра фото
         */
        View close_photo = findViewById(R.id.close_photo);
        close_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photo_overlay.setVisibility(View.GONE);
            }
        });

        /**
         * Клик по полю для ввода сообщения
         * скрыти смайлов и показ клавиатуры
         */
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSmiles();
            }
        });

        // обработка удаления смайликов
        SmileTextWatcher smileTextWatcher = new SmileTextWatcher(message, smile_size, this);
        message.addTextChangedListener(smileTextWatcher);
        message.addTextChangedListener(new TextWatcher() {
            boolean sended = false;
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!sended){
                    sended = true;
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("recipient_id", publicUserModel.id);
                        jsonObject.put("sender_id", userModel.id);
                        ((App)getApplication()).SocketEmit("typing", jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            sended = false;
                        }
                    }, 4000l);
                }
            }
        });

        // Скрытие и показ клавиатуры
        final View activityRootView = findViewById(R.id.list);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
                if (heightDiff > oldDiff) { // if more than 150 pixels, its probably a keyboard...
                    if(keyBoardShow){
                        return;
                    }
                    keyBoardShow = true;
                    //scrollTop();
                } else {
                    if(!keyBoardShow){
                        return;
                    }
                    keyBoardShow = false;
                    //scrollTop();
                }
                oldDiff = heightDiff;
            }
        });
        // обновляем свой профиль
        userModel.refreshFromServer(handler);

        // сообщения
        PrivatChatModel.loadMessagesByUser(token, publicUserModel.id, new Handler(){
            private boolean is_process = false;
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if(is_process){
                    return;
                }
                is_process = true;
                switch (msg.arg1) {
                    case Const.STATE_ACCESS_DINEDED:
                        logout();
                        break;
                    case Const.STATE_SUCCESS_RESPONSE:
                        adapter.update(offset, limit);
                        scrollTop();
                        break;
                    case Const.STATE_SERVER_ERROR:
                        break;
                }
                is_process = false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        NotificationHelper.cancelNotification(NotificationModel.TYPE_MESSAGES, this);
        NotificationHelper.cancelNotification(NotificationModel.TYPE_NOTIFICATIONS, this);

        adapter.update(offset, limit);
        scrollTop();
    }

    @Override
    public void onBackPressed() {
        if(smiles.getVisibility() == View.VISIBLE){
            hideSmiles();
        } else {
            super.onBackPressed();
        }
    }

    public void scrollTop(){
        if(!recyclerView.isAnimating()){
            //recyclerView.smoothScrollToPosition(recyclerView.getHeight());
            recyclerView.smoothScrollToPosition(0);
        }
    }

    /**
     * Показать смайлы
     */
    public void showSmiles(){
        // добавляем часто используемые смайлы
        List<SmilesHistoryModel> smilesHistoryList = SmilesHistoryModel.getSmiles();
        // часто используемые смайлы
        for (int i = 0; i < smilesHistoryList.size() && i < SmilesHistoryModel.smile.length; i++){
            SmilesHistoryModel smilesHistoryModel = smilesHistoryList.get(i);
            ImageView smile = (ImageView)findViewById(smilesHistoryModel.smile_id);
            ImageView a_smile = (ImageView)findViewById(SmilesHistoryModel.smile[i]);
            a_smile.setImageDrawable(smile.getDrawable());
            a_smile.setVisibility(View.VISIBLE);
        }
        smiles.setVisibility(View.VISIBLE);

        /**
         * Кнопка удаления
         */
        View backspace = findViewById(R.id.backspace);
        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int start = message.getSelectionStart();
                if(start<1){
                    return;
                }

                String left_str = message.getEditableText().subSequence(0, start-1).toString();
                CharSequence right_str = "";
                if(start < (message.getEditableText().length() -1)){
                    right_str = message.getEditableText().subSequence(start, message.getEditableText().length());
                }
                String new_str = left_str + right_str;

                if(left_str.length() > 2){
                    start = left_str.length()-1;
                    int last_smile_start = new_str.subSequence(0, start).toString().lastIndexOf(" :smile");
                    int last_smile_end = new_str.subSequence(0, start).toString().lastIndexOf(": ");
                    if(last_smile_start >= 0 && last_smile_end < last_smile_start){
                        left_str = new_str.substring(0, last_smile_start+1);
                        new_str = left_str + right_str;
                    }
                }

                message.setText(new_str);
                message.setSelection(left_str.length());

                Smiles.setSmiles(new_str, message, smile_size, UserChatActivity.this);
            }
        });
        // меняем занчёк на кнопке
        button_smile.setImageResource(R.drawable.ic_keyboard);
    }

    /**
     * Скрыть смайлы
     */
    public void hideSmiles(){
        if(smiles.getVisibility() == View.VISIBLE){
            smiles.setVisibility(View.GONE);
            // меняем занчёк на кнопке
            button_smile.setImageResource(R.drawable.ic_smile);
        }
    }

    /**
     * Верхние меню и быстрый поиск
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_user_chat_actions, menu);
        return true;
    }

    /**
     * On selecting action bar icons
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case R.id.action_gift:
                Intent intent = new Intent(this, GiftsListActivity.class);
                intent.putExtra("uid", publicUserModel.id);
                startActivity(intent);
                return true;
            case R.id.action_menu:
                createMenu(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Меню
     * @param point
     */
    private void createMenu(MenuItem point){
        // MenuPopupHelper cannot be used without an anchor
        View menuItemView = findViewById(R.id.action_menu);
        final PopupMenu popupMenu = new PopupMenu(this, menuItemView);
        final Menu menu = popupMenu.getMenu();
        popupMenu.getMenuInflater().inflate(R.menu.user_chat_item, menu);

        MenuItem item = popupMenu.getMenu().getItem(0);
        MenuItem item_rr = popupMenu.getMenu().getItem(1);
        final MenuItem item_favorites = popupMenu.getMenu().getItem(3);
        final MenuItem item_black_list = popupMenu.getMenu().getItem(4);

        // название меню избранное
        if(FavoriteModel.check(publicUserModel.id, userModel.id)){
            item_favorites.setTitle(getString(R.string.menu_item_remove_from_favorite));
        } else {
            item_favorites.setTitle(getString(R.string.menu_item_add_to_favorite));
        }
        // название меню в игнор
        if(BlackListModel.check(publicUserModel.id, userModel.id)){
            item_black_list.setTitle(getString(R.string.menu_item_remove_from_black_list));
        } else {
            item_black_list.setTitle(getString(R.string.menu_item_to_black_list));
        }
        // действие с юзером
        switch (publicUserModel.friend_status){
            // друг
            case Const.FRIEND_STATUS_FRIEND:
                item.setTitle(getString(R.string.menu_item_remove_from_friend));
                item_rr.setVisible(false);
                break;
            // хочу дружить с ним
            case Const.FRIEND_STATUS_WISH_FRIEND:
                item.setTitle(getString(R.string.menu_item_remove_friend_request));
                item_rr.setVisible(false);
                break;
            // хочет дружить со мной
            case Const.FRIEND_STATUS_WANT_FRIEND:
                item.setTitle(getString(R.string.menu_item_accept_request));
                item_rr.setVisible(true);
                break;
            // не знаю его
            case Const.FRIEND_STATUS_NOTKNOW:
            default:
                item.setTitle(getString(R.string.menu_item_add_to_friend));
                item_rr.setVisible(false);
                break;
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_add_or_remove_friends: // добавить/удалить из друзей
                        switch (publicUserModel.friend_status){
                            // друг
                            case Const.FRIEND_STATUS_FRIEND:
                                // удаляем из друзей
                                removeFromFriends(publicUserModel.id, handler);
                                item.setTitle(getString(R.string.menu_item_add_to_friend));
                                //PublicUserModel.setFriendStatus(publicUserModel.id, Const.FRIEND_STATUS_NOTKNOW);
                                break;
                            // хочу дружить с ним
                            case Const.FRIEND_STATUS_WISH_FRIEND:
                                // отменяем свой запрос/отписываемся от юзера
                                removeMyFriendRequest(publicUserModel.id, handler);
                                item.setTitle(getString(R.string.menu_item_add_to_friend));
                                //PublicUserModel.setFriendStatus(publicUserModel.id, Const.FRIEND_STATUS_NOTKNOW);
                                break;
                            // хочет дружить со мной
                            case Const.FRIEND_STATUS_WANT_FRIEND:
                                // принимаем заяку
                                acceptFriendRequest(publicUserModel.id, handler);
                                item.setTitle(getString(R.string.menu_item_remove_from_friend));
                                //PublicUserModel.setFriendStatus(publicUserModel.id, Const.FRIEND_STATUS_FRIEND);
                                break;
                            // не знаю его
                            case Const.FRIEND_STATUS_NOTKNOW:
                            default:
                                // отправляем запрос на дружбу
                                addToFriends(publicUserModel.id, handler);
                                item.setTitle(getString(R.string.menu_item_remove_friend_request));
                                //PublicUserModel.setFriendStatus(publicUserModel.id, Const.FRIEND_STATUS_WISH_FRIEND);
                                break;
                        }
                        break;
                    case R.id.action_decline_request: // отклонить запрос
                        declineFriendRequest(publicUserModel.id, handler);
                        item.setTitle(getString(R.string.menu_item_add_to_friend));
//                        PublicUserModel.setFriendStatus(publicUserModel.id, Const.FRIEND_STATUS_NOTKNOW);
//                        publicUserModel = PublicUserModel.getById(publicUserModel.id);
                        break;
                    case R.id.action_send_photo: // отправить фото
                        Intent chooseImageIntent = ImagePicker.getPickImageIntent(UserChatActivity.this);
                        startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
                        break;
                    case R.id.action_add_to_favorite: // добавить в избранное
                        if(FavoriteModel.check(publicUserModel.id, userModel.id)){
                            // удаляем если уже был добавлен
                            FavoriteModel.remove(publicUserModel.id, userModel.id);
                            Toast.makeText(UserChatActivity.this, getString(R.string.success_remove_from_favorites), Toast.LENGTH_SHORT).show();
                            item_favorites.setTitle(getString(R.string.menu_item_add_to_favorite));
                        } else {
                            // добавляем если ещё нет в избранном
                            FavoriteModel.add(publicUserModel.id, userModel.id);
                            Toast.makeText(UserChatActivity.this, getString(R.string.success_add_to_favorites), Toast.LENGTH_SHORT).show();
                            item_favorites.setTitle(getString(R.string.menu_item_remove_from_favorite));
                        }
                        break;
                    case R.id.action_to_black_list:
                        // добавить в игнор
                        if(BlackListModel.check(publicUserModel.id, userModel.id)){
                            // удаляем если уже был добавлен
                            BlackListModel.remove(publicUserModel.id, userModel.id, token, blackListHandler);
                            Toast.makeText(UserChatActivity.this, getString(R.string.success_remove_from_black_list), Toast.LENGTH_SHORT).show();
                            item_black_list.setTitle(getString(R.string.menu_item_to_black_list));
                        } else {
                            // добавляем если ещё нет в списке
                            BlackListModel.add(publicUserModel.id, userModel.id, token, blackListHandler);
                            Toast.makeText(UserChatActivity.this, getString(R.string.success_add_to_black_list), Toast.LENGTH_SHORT).show();
                            item_black_list.setTitle(getString(R.string.menu_item_remove_from_black_list));
                        }
                        break;
                }
                return true;
            }
        });
        popupMenu.show();
    }

    /**
     * Добавление/удаление из чёрного списка на сервере
     */
    Handler blackListHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1) {
                case Const.STATE_ACCESS_DINEDED:
                    hideWait();
                    logout();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    break;
                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:
                    Toast.makeText(UserChatActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    /**
     * Новое сообщение
     * @param privatChatModel
     */
    @Override
    public void onSocketNewMessage(PrivatChatModel privatChatModel) {
        // подтверждение о доставке
        if(privatChatModel.sender_id.equals(userModel.id)){
            adapter.update(offset, limit);
            //scrollTop();
            return;
        }
        // сообщение не из активного чата
        if(!privatChatModel.sender_id.equals(publicUserModel.id)){
            super.onSocketNewMessage(privatChatModel);
            return;
        }

        // новое сообщение
        if(privatChatModel.sender_id.equals(publicUserModel.id)){
            // звук нового сообщения
            adapter.update(offset, limit);
            scrollTop();
            playSound(message_chat_sound);
        }

    }

    /**
     * Печатает
     */
    @Override
    public void onSocketTyping(JSONObject jsonObject) {

        if(typing != null && typing.getVisibility() == View.VISIBLE){
            return;
        }

        try {
            JSONObject data = jsonObject.getJSONObject("data");
            String sender_id = data.getString("sender_id");
            if(publicUserModel.id.equals(sender_id)){
                if(typing == null){
                    typing = findViewById(R.id.typing);
                }
                typing.setVisibility(View.VISIBLE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                typing.setVisibility(View.GONE);
                            }
                        });
                    }
                }, 3000l);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    hideWait();
                    logout();
                    break;
                case Const.STATE_SUCCESS_ADD_TO_FRIENDS:
                    hideWait();
                    Toast.makeText(UserChatActivity.this, getString(R.string.info_add_friend), Toast.LENGTH_SHORT).show();
                    publicUserModel = PublicUserModel.getById(publicUserModel.id);
                    break;
                case Const.STATE_SUCCESS_REMOVE_FROM_FRIENDS:
                    hideWait();
                    Toast.makeText(UserChatActivity.this, getString(R.string.info_remove_friend), Toast.LENGTH_SHORT).show();
                    publicUserModel = PublicUserModel.getById(publicUserModel.id);
                    break;
                case Const.STATE_SUCCESS_DECLINE_FRIEND_REQUEST:
                    hideWait();
                    Toast.makeText(UserChatActivity.this, getString(R.string.info_success_decline_friend_request), Toast.LENGTH_SHORT).show();
                    publicUserModel = PublicUserModel.getById(publicUserModel.id);
                    break;
                case Const.STATE_SUCCESS_CANCEL_FRIEND_REQUEST:
                    hideWait();
                    Toast.makeText(UserChatActivity.this, getString(R.string.info_success_cancel_friend_request), Toast.LENGTH_SHORT).show();
                    publicUserModel = PublicUserModel.getById(publicUserModel.id);
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    userModel = UserModel.getByToken(token);
                    break;
                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:
                    hideWait();
                    Toast.makeText(UserChatActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    /**
     * Добавление смайликов
     * @param v
     */
    public void onClickSmile(View v){
        String smile = Smiles.SmileToText(v.getId());
        int selectionCursor = message.getSelectionStart();
        Smiles.setSmiles(message.getText().insert(selectionCursor, smile).toString(), message, smile_size, this);
    }

    /**
     * Отправка сообщения
     * @param privatChatModel
     * @throws JSONException
     */
    public void sendMessage(PrivatChatModel privatChatModel) throws JSONException {
        JSONObject args = new JSONObject();
        args.put("message", privatChatModel.message);
        args.put("recipient_id", privatChatModel.recipient_id);
        args.put("time", privatChatModel.time);
        args.put("uiid", privatChatModel.uiid);

        ((App)getApplication()).SocketEmit("send_message", args);
    }

    /**
     * Переход к профилю пользователя
     */
    public void goToUserProfile(){
        Intent intent = new Intent(this, PublicProfileActivity.class);
        intent.putExtra("uid", publicUserModel.id);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case PICK_IMAGE_ID:
                Bitmap bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
                try {
                    uploadFile(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(UserChatActivity.this, "Не удалось обработать картинку", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    /**
     * Загрузка файла
     * @param bitmap
     */
    private void uploadFile(Bitmap bitmap) throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        File f = new File(Environment.getExternalStorageDirectory()
                + File.separator + "testimage.jpg");
        f.createNewFile();
        FileOutputStream fo = new FileOutputStream(f);
        fo.write(bytes.toByteArray());
        fo.close();

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("image/jpeg"), f);

        // create upload service client
        UploadImage service = RetrofitInstance.getRetrofit().create(UploadImage.class);


        // finally, execute the request
        Call<ResponseModel> call = service.upload(token, requestFile);
        call.enqueue(new retrofit2.Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call,
                                   Response<ResponseModel> response) {
                Log.v("Upload", "success");
                Toast.makeText(UserChatActivity.this, "Картинка загружена", Toast.LENGTH_LONG).show();
                ResponseModel m = response.body();
                ImgResp imgResp = m.getData(ImgResp.class);

                PrivatChatModel privatChatModel = new PrivatChatModel();
                privatChatModel.message = ":image:" + imgResp.thumb;
                privatChatModel.sender_id = userModel.id;
                privatChatModel.recipient_id = publicUserModel.id;
                privatChatModel.is_read = 1;
                privatChatModel.time = System.currentTimeMillis();
                privatChatModel.uiid = System.currentTimeMillis() + "_" + userModel.id;
                privatChatModel.publicUserModel = publicUserModel;
                privatChatModel.insert();
                adapter.update(offset, limit);
                try {
                    sendMessage(privatChatModel);
                } catch (JSONException e) {
                    Log.d(UserChatActivity.class.getName(), e.toString());
                }
                scrollTop();
                playSound(push_sound);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Log.e("Upload_error", t.getMessage());
                t.printStackTrace();
                Toast.makeText(UserChatActivity.this, "Не удалось загрузить картинку", Toast.LENGTH_LONG).show();
            }
        });
    }

    public class ImgResp {
        @Expose
        public String thumb = "";
        @Expose
        public String image = "";
    }

    /**
     * Показ увеличенного фото
     * @param url
     */
    public void showFullPhoto(String url){
        final String finalUrl = url.replace("thumb", "full");
        Picasso.with(UserChatActivity.this)
                .load(finalUrl)
                .placeholder(R.drawable.ic_account_plus)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(mImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        // Attach a PhotoViewAttacher, which takes care of all of the zooming functionality.
                        // (not needed unless you are going to change the drawable later)
                        mAttacher = new PhotoViewAttacher(mImageView);
                        photo_overlay.setVisibility(View.VISIBLE);
                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(UserChatActivity.this)
                                .load(finalUrl)
                                .placeholder(R.drawable.ic_account_plus)
                                .error(R.drawable.ic_account_plus)
                                .into(mImageView, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        // Attach a PhotoViewAttacher, which takes care of all of the zooming functionality.
                                        // (not needed unless you are going to change the drawable later)
                                        mAttacher = new PhotoViewAttacher(mImageView);
                                        photo_overlay.setVisibility(View.VISIBLE);
                                    }
                                    @Override
                                    public void onError() {
                                        Toast.makeText(UserChatActivity.this, "Не удалось загрузить картинку", Toast.LENGTH_LONG).show();
                                    }
                                });
                    }
                });

    }
}
