package pro.devapp.kitoss.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.models.AuthModel;

public class RestoreActivity extends BaseActivity {

    TextView phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restore);
        initHomeBtn(getString(R.string.label_forgot));

        phone = (TextView) findViewById(R.id.phone);
        phone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        Button submit = (Button) findViewById(R.id.btn);
        if(submit != null)
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Boolean error = false;

                    // телефон
                    String value_phone = phone.getText().toString();
                    value_phone = value_phone.replaceAll("\\D+", "");
                    if (value_phone.equals("") || value_phone.length() < 9) {
                        //phone.setError(getString(R.string.error_empty_phone));
                        phone.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                        error = true;
                    } else {
                        //phone.setError(null);
                        phone.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                    }

                    if(error){
                        Toast.makeText(RestoreActivity.this, getString(R.string.error_in_form), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    // делаем запрос
                    showWait();
                    AuthModel.resendPassword(RestoreActivity.this, value_phone, handler);
                }
            });
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_SUCCESS_RESEND_PASSWORD:
                    hideWait();
                    Intent intent = new Intent(RestoreActivity.this, LoginActivity.class);
                    intent.putExtra("after_restore", true);
                    intent.putExtra("phone", phone.getText().toString());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    break;
                case Const.STATE_ERROR_RESPONSE:
                    hideWait();
                    try {
                        JSONObject resp = new JSONObject((String)msg.obj);
                        if(!resp.isNull("errors")){
                            JSONObject errors = resp.getJSONObject("errors");
                            if(!errors.isNull("phone")){
                                Toast.makeText(RestoreActivity.this, errors.getString("phone"), Toast.LENGTH_SHORT).show();
                                final TextView phone = (TextView) findViewById(R.id.phone);
                                phone.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                            }
                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                    break;
                case Const.STATE_SERVER_ERROR:
                    hideWait();
                    Toast.makeText(RestoreActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}
