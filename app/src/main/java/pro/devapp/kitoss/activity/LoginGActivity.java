package pro.devapp.kitoss.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.vk.sdk.VKSdk;

import org.json.JSONException;
import org.json.JSONObject;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.GAuthHelper;
import pro.devapp.kitoss.models.AuthModel;
import pro.devapp.kitoss.models.PrivatChatModel;
import pro.devapp.kitoss.models.PublicChatModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.PushRegisterModel;
import pro.devapp.kitoss.models.UserModel;

public class LoginGActivity extends BaseActivity {

    private GAuthHelper gAuthHelper;
    private static final int RC_SIGN_IN = 9001;
    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        VKSdk.initialize(this);

        final TextView after_register = (TextView) findViewById(R.id.after_register);
        final TextView password = (TextView) findViewById(R.id.password);
        final TextView phone = (TextView) findViewById(R.id.phone);
        phone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        // переход после выхода из приложения
        if(getIntent().getExtras() != null && getIntent().getExtras().getBoolean("after_logout", false)){

        }

        // показать строку повторной отправки
        if(getIntent().getExtras() != null && getIntent().getExtras().getBoolean("after_register", false)){
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    after_register.setVisibility(View.VISIBLE);
                }
            }, 4000l);
        }

        // переход после регистрации - заполняем телефон
        if(getIntent().getExtras() != null && getIntent().getExtras().getBoolean("after_register", false) &&
                !token.equals("")){
            final UserModel userModel = UserModel.getByToken(token);
            phone.setText(userModel.phone);

            if(after_register != null)
                after_register.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AuthModel.resendPassword(LoginGActivity.this, userModel.phone, handler);
                    }
                });
        }

        // после восстановления пароля - заполняем телефон
        if(getIntent().getExtras() != null && getIntent().getExtras().getBoolean("after_restore", false)){
            phone.setText(getIntent().getExtras().getString("phone", ""));
        }

        // G+ login
        gAuthHelper = new GAuthHelper(this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(LoginGActivity.this)
                .enableAutoManage(LoginGActivity.this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(LoginGActivity.this, "Не возможно произвести авторизацию", Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
//        final View btn = (View) findViewById(R.id.sign_in_button);
//        if(btn != null)
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
//                startActivityForResult(signInIntent, RC_SIGN_IN);
//
//            }
//        });

        /**
         * Terms of use
         */
        View terms_of_use = findViewById(R.id.terms_of_use);
        terms_of_use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginGActivity.this, TermsOfUseActivity.class);
                startActivity(intent);
            }
        });

        // Отправка формы
        Button submit = (Button) findViewById(R.id.btn);
        if(submit != null)
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Boolean error = false;

                    String value_password = password.getText().toString();

                    // телефон
                    String value_phone = phone.getText().toString();
                    value_phone = value_phone.replaceAll("\\D+", "");
                    if (value_phone.equals("") || value_phone.length() < 9) {
                        //phone.setError(getString(R.string.error_empty_phone));
                        phone.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                        error = true;
                    } else {
                        //phone.setError(null);
                        phone.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                    }

                    if (value_password.equals("") || value_password.length() < 2) {
                        //phone.setError(getString(R.string.error_empty_phone));
                        password.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                        error = true;
                    } else {
                        //phone.setError(null);
                        password.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                    }

                    if(error){
                        Toast.makeText(LoginGActivity.this, getString(R.string.error_in_form), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    // делаем запрос
                    showWait();
                    AuthModel.login(LoginGActivity.this, value_phone, value_password, handler);
                }
            });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(LoginGActivity.class.getName(), "handleSignInResult:" + result.isSuccess());

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.d(LoginGActivity.class.getName(), "handleSignInResult:" + acct.getDisplayName());
            Log.d(LoginGActivity.class.getName(), "handleSignInResult:" + acct.getEmail());
            Log.d(LoginGActivity.class.getName(), "handleSignInResult:" + acct.getId());
            // делаем запрос
            showWait();
            AuthModel.gpauth(LoginGActivity.this, acct.getEmail(), acct.getId(), acct.getDisplayName(), handler);
        } else {
            Log.d(LoginGActivity.class.getName(), result.getStatus().toString());
            // Signed out, show unauthenticated UI.
            Toast.makeText(this, getString(R.string.auth_canceled), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * переход на экран восстановления пароля
     * @param v
     */
    public void onClickRemember(View v){
        Intent intent = new Intent(this, RestoreActivity.class);
        startActivity(intent);
    }

    /**
     * переход на экран регистрации
     * @param v
     */
    public void onClickRegister(View v){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_SUCCESS_AUTH:
                    token = ((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
//                    // свои сообщения
//                    PrivatChatModel.loadMyMessages(token, (App)getApplication());
//                    // public chats
//                    PublicChatModel.loadHistoryMessages(token, new Handler(){});
//                    // обновление друзей
//                    PublicUserModel.getFriends(0, 500, token, new Handler(){});

                    hideWait();
                    // отправляем токен для push
                    String gtoken = ((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_GCM_TOKEN);
                    Log.d("TOKEN", "push token");
                    Log.d("TOKEN", gtoken);
                    PushRegisterModel.regDevice(LoginGActivity.this, token, gtoken);

                    Intent intent = new Intent(LoginGActivity.this, AppActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    break;
                case Const.STATE_ERROR_RESPONSE:
                    hideWait();
                    try {
                        JSONObject resp = new JSONObject((String)msg.obj);
                        if(!resp.isNull("errors")){
                            JSONObject errors = resp.getJSONObject("errors");
                            if(!errors.isNull("phone")){
                                Toast.makeText(LoginGActivity.this, errors.getString("phone"), Toast.LENGTH_SHORT).show();
                                final TextView phone = (TextView) findViewById(R.id.phone);
                                phone.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                            }
                            if(!errors.isNull("password")){
                                Toast.makeText(LoginGActivity.this, errors.getString("password"), Toast.LENGTH_SHORT).show();
                                final TextView password = (TextView) findViewById(R.id.password);
                                password.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                            }
                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                    break;
                case Const.STATE_SERVER_ERROR:
                    hideWait();
                    Toast.makeText(LoginGActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_RESEND_PASSWORD:
                    hideWait();
                    Toast.makeText(LoginGActivity.this, getString(R.string.info_password_success_resend), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void gAuth(String acc){
        Log.d(LoginGActivity.class.getName(), "accname " + acc);

        gAuthHelper.getAuthToken(acc, new GAuthHelper.OAuthCallbackListener() {
            @Override
            public void callback(final String authToken) {
                if (authToken==null) {
                    Toast.makeText(LoginGActivity.this, getString(R.string.auth_canceled), Toast.LENGTH_LONG).show();
                } else {
                    // делаем запрос
                    showWait();
                    Log.d(LoginGActivity.class.getName(), authToken);
                    AuthModel.gauth(LoginGActivity.this, authToken, handler);
                }
            }
        });
    }
}
