package pro.devapp.kitoss.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.models.GiftModel;
import pro.devapp.kitoss.models.RegionModel;
import pro.devapp.kitoss.models.WhereGoModel;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // auto login
        if(((App)getApplicationContext()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_FIRST_RUN)){
            if(!((App)getApplicationContext()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_AUTOLOGIN)){
                ((App)getApplication()).getAppPrefs().saveStringPreference(Const.PREFERENCE_TOKEN, "");
            }
        }

        if(checkPlayServices()){

            new Thread(new Runnable() {
                public void run() {
                    //code
                    InstanceID instanceID = InstanceID.getInstance(SplashActivity.this);
                    try {
                        String gtoken = instanceID.getToken(getString(R.string.gcm_sender_id),  GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                        Log.d(SplashActivity.class.getName(), gtoken);
                        ((App)getApplication()).getAppPrefs().saveStringPreference(Const.PREFERENCE_GCM_TOKEN, gtoken);
                    } catch (IOException e) {
                        Log.d(SplashActivity.class.getName() + " ERROR ", e.toString());
                    }
                }
            }).start();
        }

        final String token = ((App)getApplication()).getAppPrefs().getStringPreference(Const.PREFERENCE_TOKEN);
        final AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                firstRun();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                // проверяем авторизацию
                Intent intent;
                if(token.equals("")){
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                } else {
                    intent = new Intent(SplashActivity.this, AppActivity.class);
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        };

        // Получаем варианты куда пойти
        WhereGoModel.refreshFromServer(new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.arg1){
                    case Const.STATE_SUCCESS_RESPONSE:
                        Log.d(SplashActivity.class.getName(), "STATE_SUCCESS_RESPONSE");
                        break;
                    case Const.STATE_ERROR_RESPONSE:
                        Log.d(SplashActivity.class.getName(), "STATE_ERROR_RESPONSE");
                        break;
                    case Const.STATE_SERVER_ERROR:
                        Log.d(SplashActivity.class.getName(), "STATE_SERVER_ERROR");
                        break;
                    case Const.STATE_SUCCESS_RESEND_PASSWORD:
                        Log.d(SplashActivity.class.getName(), "STATE_SUCCESS_RESEND_PASSWORD");
                        break;
                }

                RegionModel.refreshFromServer(new Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        switch (msg.arg1){
                            case Const.STATE_SUCCESS_RESPONSE:
                                Log.d(SplashActivity.class.getName(), "STATE_SUCCESS_RESPONSE");
                                break;
                            case Const.STATE_ERROR_RESPONSE:
                                Log.d(SplashActivity.class.getName(), "STATE_ERROR_RESPONSE");
                                break;
                            case Const.STATE_SERVER_ERROR:
                                Log.d(SplashActivity.class.getName(), "STATE_SERVER_ERROR");
                                break;
                            case Const.STATE_SUCCESS_RESEND_PASSWORD:
                                Log.d(SplashActivity.class.getName(), "STATE_SUCCESS_RESEND_PASSWORD");
                                break;
                        }

                        task.execute();
                    }
                });
            }
        });

//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                task.execute();
//            }
//        }, 2000l);
    }


    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 1).show();
            } else {
                Log.d(SplashActivity.class.getName(), "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    private void firstRun(){
        if(!((App)getApplicationContext()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_FIRST_RUN)){
            ((App)getApplicationContext()).getAppPrefs().saveBooleanPreference(Const.PREFERENCE_FIRST_RUN, true);
            ((App)getApplicationContext()).getAppPrefs().saveBooleanPreference(Const.PREFERENCE_SOUND, true);
            ((App)getApplicationContext()).getAppPrefs().saveBooleanPreference(Const.PREFERENCE_VIBRO, true);
            ((App)getApplicationContext()).getAppPrefs().saveBooleanPreference(Const.PREFERENCE_AUTOLOGIN, true);
            ((App)getApplicationContext()).getAppPrefs().saveBooleanPreference(Const.PREFERENCE_RECEIVE_PHOTO, false);
            GiftModel.init();
        }
    }

}
