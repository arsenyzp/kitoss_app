package pro.devapp.kitoss.activity;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.List;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.models.RegionModel;

public class SettingsRegionActivity extends BaseActivity {

    Spinner region;
    List<RegionModel> regionModels; // список моделей вариантов куда пойти

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settigs_region);

        initHomeBtn(getString(R.string.title_settings_region));

        region = (Spinner) findViewById(R.id.region);

        // список регионов
        int selected = 0;
        regionModels = RegionModel.getAll();
        final String[] regions = new String[regionModels.size()];
        for (int i = 0; i < regionModels.size(); i++){
            regions[i] = regionModels.get(i).name;
            if(regionModels.get(i).cod.equals(userModel.region)){
                selected = i;
            }
        }
        ArrayAdapter<String> adapter_regions = new ArrayAdapter<String>(this, R.layout.spinner_item, regions);
        adapter_regions.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        region.setAdapter(adapter_regions);
        region.setSelection(selected);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        int position = region.getSelectedItemPosition();
        userModel.setRegion(regionModels.get(position).cod, new Handler(){});
    }
}
