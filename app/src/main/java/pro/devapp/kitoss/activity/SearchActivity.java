package pro.devapp.kitoss.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.List;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.adapters.MUsersAdapter;
import pro.devapp.kitoss.adapters.UsersAdapter;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.EndlessRecyclerViewScrollListener;
import pro.devapp.kitoss.models.FilterUsersModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.UserModel;

public class SearchActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private MUsersAdapter adapter;

    private UserModel model;
    private PublicUserModel publicUserModel;

    private int offset = 0;
    private int limit = 21;

    private SwipyRefreshLayout swipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        initHomeBtn(getString(R.string.title_search));
        publicUserModel = new PublicUserModel();
        recyclerView = (RecyclerView) findViewById(R.id.list);
        adapter = new MUsersAdapter(this, handler);
        // устанвливаем полседний выбранный вид списка
        adapter.view_is_list = ((App)getApplication()).getAppPrefs().getBooleanPreference(Const.PREFERENCE_VIEW_MODE, true);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        showWait();
        // получение профиля с сервера
        model = UserModel.getByToken(token);
        model.refreshFromServer(handler);

        // размеры для изменения варианта списка
        adapter.ava_h = (int)(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics()));
        adapter.ava_h_margin = (int)(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics()));

        // иництализация списка пользователей
        initList();
        // получение первоначального списка
//        publicUserModel.cancelSearch();
//        publicUserModel.search(token, offset, limit, new PublicUserModel.ResponseListener() {
//                    @Override
//                    public void onSuccess(List<PublicUserModel> items) {
//                        adapter.add(items);
//                        hideWait();
//                    }
//
//                    @Override
//                    public void onError(String message) {
//                        hideWait();
//                        Toast.makeText(SearchActivity.this, "Больше нет пользователей", Toast.LENGTH_LONG).show();
//                    }
//                }
//        );
    }

    /**
     * Инициализация списка пользователей
     */
    private void initList(){
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, getCountColum(), GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);

        swipeRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                final int old_message_count = recyclerView.getAdapter().getItemCount();

                offset += limit;

                // отменяем запрос поиска
                publicUserModel.cancelSearch();
//                publicUserModel.search(token, offset, limit, new Handler() {
//                            @Override
//                            public void handleMessage(Message msg) {
//                                super.handleMessage(msg);
//                                switch (msg.arg1) {
//                                    case Const.STATE_ACCESS_DINEDED:
//                                        hideWait();
//                                        logout();
//                                        break;
//                                    case Const.STATE_SUCCESS_RESPONSE:
//                                        adapter.update();
//                                        int new_messag_count = recyclerView.getAdapter().getItemCount() - old_message_count;
//                                        if(new_messag_count == 0){
//                                            Toast.makeText(SearchActivity.this, "Больше нет пользователей", Toast.LENGTH_LONG).show();
//                                        }
//                                        if(new_messag_count == limit) {
//                                            recyclerView.smoothScrollToPosition(offset + 5);
//                                        } else if(new_messag_count > 0) {
//                                            recyclerView.smoothScrollToPosition(offset + new_messag_count);
//                                        }
//                                        swipeRefreshLayout.setRefreshing(false);
//                                        break;
//                                    case Const.STATE_ERROR_RESPONSE:
//                                    case Const.STATE_SERVER_ERROR:
//                                        swipeRefreshLayout.setRefreshing(false);
//                                        Toast.makeText(SearchActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
//                                        break;
//                                }
//                            }
//                        }
//                );

                publicUserModel.search(token, offset, limit, new PublicUserModel.ResponseListener() {
                            @Override
                            public void onSuccess(List<PublicUserModel> items) {
                                adapter.add(items);
                                int new_messag_count = recyclerView.getAdapter().getItemCount() - old_message_count;
                                if(new_messag_count == 0){
                                    Toast.makeText(SearchActivity.this, "Больше нет пользователей", Toast.LENGTH_LONG).show();
                                }
                                if(new_messag_count == limit) {
                                    recyclerView.smoothScrollToPosition(offset + 5);
                                } else if(new_messag_count > 0) {
                                    recyclerView.smoothScrollToPosition(offset + new_messag_count);
                                }
                                swipeRefreshLayout.setRefreshing(false);
                                hideWait();
                            }

                            @Override
                            public void onError(String message) {
                                hideWait();
                            }
                        }
                );
            }
        });
    }

    /**
     * Верхние меню и быстрый поиск
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_search_actions, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if(null!=searchManager ) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setIconifiedByDefault(false);

        // быстрый поиск по имени
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String q) {
                Log.d(SearchActivity.class.getName(), q);
                FilterUsersModel.name = q;
                publicUserModel.cancelSearch();
                offset = 0;
                adapter.clear();
                publicUserModel.search(token, offset, limit, new PublicUserModel.ResponseListener() {
                            @Override
                            public void onSuccess(List<PublicUserModel> items) {
                                adapter.add(items);
                                hideWait();
                            }

                            @Override
                            public void onError(String message) {
                                Toast.makeText(SearchActivity.this, "Больше нет пользователей", Toast.LENGTH_LONG).show();
                            }
                        }
                );
                return true;
            }
        });
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        publicUserModel.cancelSearch();
    }

    /**
     * Возвращение с экрана фильтра
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            offset = 0;
            adapter.clear();
            showWait();
            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
            publicUserModel.cancelSearch();
            publicUserModel.search(token, offset, limit, new PublicUserModel.ResponseListener() {
                        @Override
                        public void onSuccess(List<PublicUserModel> items) {
                            adapter.add(items);
                            hideWait();
                        }

                        @Override
                        public void onError(String message) {
                            hideWait();
                            Toast.makeText(SearchActivity.this, "Больше нет пользователей", Toast.LENGTH_LONG).show();
                        }
                    }
            );
            initList();
        }
    }

    /**
     * On selecting action bar icons
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case R.id.action_filter:
                // filter
                Intent intent = new Intent(this, FilterActivity.class);
                startActivityForResult(intent, 1);
                return true;
            case R.id.action_view:
                if(adapter.view_is_list){
                    adapter.view_is_list = false;
                    item.setIcon(R.drawable.ic_view_list);
                } else {
                    adapter.view_is_list = true;
                    item.setIcon(R.drawable.ic_view_grid);
                }
                ((App)getApplication()).getAppPrefs().saveBooleanPreference(Const.PREFERENCE_VIEW_MODE, adapter.view_is_list);
                //recyclerView.setLayoutManager(new GridLayoutManager(this, getCountColum(), GridLayoutManager.VERTICAL, false));
                initList();
                recyclerView.requestLayout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Колличество колонок в списке
     * @return
     */
    private int getCountColum(){
        if(adapter.view_is_list){
            return 1;
        }
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int numberOfColumns = 4;
        if(metrics.widthPixels < 650){
            numberOfColumns = 3;
        }
        if(metrics.widthPixels/metrics.densityDpi < 4){
            numberOfColumns = 3;
        }
        return numberOfColumns;
    }

    @Override
    public void onSocketAcceptFriendRequest(PublicUserModel publicUserModel) {
        super.onSocketAcceptFriendRequest(publicUserModel);
        offset = 0;
        adapter.clear();
        publicUserModel.search(token, offset, limit, new PublicUserModel.ResponseListener() {
                    @Override
                    public void onSuccess(List<PublicUserModel> items) {
                        adapter.add(items);
                        hideWait();
                    }

                    @Override
                    public void onError(String message) {
                        hideWait();
                        Toast.makeText(SearchActivity.this, "Больше нет пользователей", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    @Override
    public void onSocketDeclineFriendRequest(PublicUserModel publicUserModel) {
        super.onSocketDeclineFriendRequest(publicUserModel);
        offset = 0;
        adapter.clear();
        publicUserModel.search(token, offset, limit, new PublicUserModel.ResponseListener() {
                    @Override
                    public void onSuccess(List<PublicUserModel> items) {
                        adapter.add(items);
                        hideWait();
                    }

                    @Override
                    public void onError(String message) {
                        hideWait();
                        Toast.makeText(SearchActivity.this, "Больше нет пользователей", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    @Override
    public void onSocketCancelFriendRequest(PublicUserModel publicUserModel) {
        super.onSocketCancelFriendRequest(publicUserModel);
        offset = 0;
        adapter.clear();
        publicUserModel.search(token, offset, limit, new PublicUserModel.ResponseListener() {
                    @Override
                    public void onSuccess(List<PublicUserModel> items) {
                        adapter.add(items);
                        hideWait();
                    }

                    @Override
                    public void onError(String message) {
                        hideWait();
                        Toast.makeText(SearchActivity.this, "Больше нет пользователей", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    @Override
    public void onSocketNewFriendsRequest(PublicUserModel publicUserModel) {
        super.onSocketNewFriendsRequest(publicUserModel);
        offset = 0;
        adapter.clear();
        publicUserModel.search(token, offset, limit, new PublicUserModel.ResponseListener() {
                    @Override
                    public void onSuccess(List<PublicUserModel> items) {
                        adapter.add(items);
                        hideWait();
                    }

                    @Override
                    public void onError(String message) {
                        hideWait();
                        Toast.makeText(SearchActivity.this, "Больше нет пользователей", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    @Override
    public void onSocketRemoveFromFriends(PublicUserModel publicUserModel) {
        super.onSocketRemoveFromFriends(publicUserModel);
        offset = 0;
        adapter.clear();
        publicUserModel.search(token, offset, limit, new PublicUserModel.ResponseListener() {
                    @Override
                    public void onSuccess(List<PublicUserModel> items) {
                        adapter.add(items);
                        hideWait();
                    }

                    @Override
                    public void onError(String message) {
                        hideWait();
                        Toast.makeText(SearchActivity.this, "Больше нет пользователей", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {

            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    hideWait();
                    logout();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    hideWait();
                    break;
                case Const.STATE_SUCCESS_ADD_TO_FRIENDS:
                    hideWait();
                    PublicUserModel.get(offset , limit, token, handler);
                    Toast.makeText(SearchActivity.this, getString(R.string.info_add_friend), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_REMOVE_FROM_FRIENDS:
                    hideWait();
                    PublicUserModel.get(offset , limit, token, handler);
                    Toast.makeText(SearchActivity.this, getString(R.string.info_remove_friend), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_DECLINE_FRIEND_REQUEST:
                    hideWait();
                    PublicUserModel.get(offset , limit, token, handler);
                    Toast.makeText(SearchActivity.this, getString(R.string.info_success_decline_friend_request), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_CANCEL_FRIEND_REQUEST:
                    hideWait();
                    PublicUserModel.get(offset , limit, token, handler);
                    Toast.makeText(SearchActivity.this, getString(R.string.info_success_cancel_friend_request), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:
                    hideWait();
                    Toast.makeText(SearchActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
            }

            offset = 0;
            adapter.clear();
            publicUserModel.search(token, offset, limit, new PublicUserModel.ResponseListener() {
                        @Override
                        public void onSuccess(List<PublicUserModel> items) {
                            adapter.add(items);
                            hideWait();
                        }

                        @Override
                        public void onError(String message) {
                            hideWait();
                            Toast.makeText(SearchActivity.this, "Больше нет пользователей", Toast.LENGTH_LONG).show();
                        }
                    }
            );
        }
    };
}
