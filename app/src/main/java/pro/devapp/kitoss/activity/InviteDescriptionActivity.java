package pro.devapp.kitoss.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.BaseActivity;

public class InviteDescriptionActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_description);

        initHomeBtn(getString(R.string.title_invite_contacts_description));
    }
}
