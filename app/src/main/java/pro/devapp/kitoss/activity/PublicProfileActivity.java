package pro.devapp.kitoss.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.adapters.MyGiftsAdapter;
import pro.devapp.kitoss.components.AdminHelper;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.Smiles;
import pro.devapp.kitoss.components.UserHelper;
import pro.devapp.kitoss.models.FavoriteModel;
import pro.devapp.kitoss.models.GiftModel;
import pro.devapp.kitoss.models.MyGiftModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.RegionModel;
import pro.devapp.kitoss.models.UserModel;
import uk.co.senab.photoview.PhotoViewAttacher;

public class PublicProfileActivity extends BaseActivity {

    PublicUserModel model;

    TextView name;
    TextView gender_old;
    TextView my_status;
    TextView city;
    TextView region;
    TextView want_go;
    TextView inapp;
    TextView about_me;
    TextView text_web_status;
    TextView action_friend_text;
    CircleImageView avatar;
    ImageView full_bg;
    ImageView online;
    ImageView offline;
    RelativeLayout photo_overlay;
    ImageView mImageView;

    PhotoViewAttacher mAttacher;

    List<MyGiftModel> myGiftModels = new ArrayList<>();

    int smile_size = 25;
    int gender_size = 25;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_public_profile);

        // размер смайла
        smile_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, getResources().getDisplayMetrics());
        gender_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());

        avatar = (CircleImageView) findViewById(R.id.avatar);
        full_bg = (ImageView) findViewById(R.id.full_bg);

        photo_overlay = (RelativeLayout) findViewById(R.id.photo_overlay);

        text_web_status = (TextView) findViewById(R.id.text_web_status);
        online = (ImageView) findViewById(R.id.online);
        offline = (ImageView) findViewById(R.id.offline);

        name = (TextView) findViewById(R.id.name);
        gender_old = (TextView) findViewById(R.id.gender_old);
        my_status = (TextView) findViewById(R.id.my_status);
        want_go = (TextView) findViewById(R.id.want_go);
        about_me = (TextView) findViewById(R.id.about_me);
        city = (TextView) findViewById(R.id.city);
        region = (TextView) findViewById(R.id.region);
        inapp = (TextView) findViewById(R.id.inapp);
        action_friend_text = (TextView) findViewById(R.id.action_friend_text);
        mImageView = (ImageView) findViewById(R.id.iv_photo);

        View close = findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        // меню
        View menu = findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showContextMenu(view);
            }
        });

        // кнопка друзей
        FrameLayout action_friend = (FrameLayout) findViewById(R.id.action_friend);
        action_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (model.friend_status){
                    // друг
                    case Const.FRIEND_STATUS_FRIEND:
                        // удаляем из друзей
                        removeFromFriends(model.id, handler);
                        action_friend_text.setText("В друзья");
                        //PublicUserModel.setFriendStatus(model.id, Const.FRIEND_STATUS_NOTKNOW);
                        break;
                    // хочу дружить с ним
                    case Const.FRIEND_STATUS_WISH_FRIEND:
                        // отменяем свой запрос/отписываемся от юзера
                        removeMyFriendRequest(model.id, handler);
                        action_friend_text.setText("В друзья");

                        //PublicUserModel.setFriendStatus(model.id, Const.FRIEND_STATUS_NOTKNOW);
                        break;
                    // хочет дружить со мной
                    case Const.FRIEND_STATUS_WANT_FRIEND:
                        // принимаем заяку
                        acceptFriendRequest(model.id, handler);
                        action_friend_text.setText(getString(R.string.menu_item_remove_friend_request));
                        //PublicUserModel.setFriendStatus(model.id, Const.FRIEND_STATUS_FRIEND);
                        break;
                    // не знаю его
                    case Const.FRIEND_STATUS_NOTKNOW:
                    default:
                        // отправляем запрос на дружбу
                        addToFriends(model.id, handler);
                        action_friend_text.setText(getString(R.string.menu_item_remove_friend_request));
                        //PublicUserModel.setFriendStatus(model.id, Const.FRIEND_STATUS_WISH_FRIEND);
                        break;
                }
            }
        });

        /**
         * Закрыть полноэкранный режим просмотра фото
         */
        View close_photo = findViewById(R.id.close_photo);
        close_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photo_overlay.setVisibility(View.GONE);
            }
        });

        FrameLayout message_action = (FrameLayout) findViewById(R.id.message_action);
        message_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PublicProfileActivity.this, UserChatActivity.class);
                intent.putExtra("uid", model.id);
                startActivity(intent);
            }
        });

        updateDate();
    }

    /**
     * Обновление данных
     */
    private void updateDate(){
        model = PublicUserModel.getById(getIntent().getExtras().getString("uid"));
        MyGiftModel.loadGifts(token, model.id, new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.arg1) {
                    case Const.STATE_ACCESS_DINEDED:
                        hideWait();
                        logout();
                        break;
                    default:
                        showGifts();
                }
            }
        });
        bindMedia();
        bindData();
//        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
//            @Override
//            protected Void doInBackground(Void... voids) {
//                model = PublicUserModel.getById(getIntent().getExtras().getString("uid"));
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(Void aVoid) {
//                super.onPostExecute(aVoid);
//                // запрос списка подарков
//                MyGiftModel.loadGifts(token, model.id, new Handler(){
//                    @Override
//                    public void handleMessage(Message msg) {
//                        super.handleMessage(msg);
//                        switch (msg.arg1) {
//                            case Const.STATE_ACCESS_DINEDED:
//                                hideWait();
//                                logout();
//                                break;
//                            default:
//                                showGifts();
//                        }
//                    }
//                });
//                bindMedia();
//                bindData();
//            }
//        };
//        task.execute();
    }

    /**
     * Отображение подарков
     * в профиле
     */
    public void showGifts(){
        TextView gifts_label = (TextView) findViewById(R.id.gifts_label);
        final int gift_count = MyGiftModel.getCountByUser(model.id);
        myGiftModels = MyGiftModel.getByUser(model.id);
        gifts_label.setText(getString(R.string.gifts_count, gift_count + ""));
        gifts_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopUpGifts(gift_count);
            }
        });

        FrameLayout send_first_gift = (FrameLayout) findViewById(R.id.send_first_gift);
        LinearLayout gifts_block = (LinearLayout) findViewById(R.id.gifts_block);
        if(gift_count > 0){
            send_first_gift.setVisibility(View.GONE);
            gifts_block.setVisibility(View.VISIBLE);

            int gift_variant = gift_count >= 3 ? 3 : gift_count;
            switch (gift_variant){
                case 3:
                    ImageView gift_1 = (ImageView) findViewById(R.id.gift_1);
                    gift_1.setVisibility(View.VISIBLE);
                    gift_1.setImageDrawable(getResources().getDrawable(GiftModel.getDrawable(myGiftModels.get(2).name)));
                case 2:
                    ImageView gift_2 = (ImageView) findViewById(R.id.gift_2);
                    gift_2.setVisibility(View.VISIBLE);
                    gift_2.setImageDrawable(getResources().getDrawable(GiftModel.getDrawable(myGiftModels.get(1).name)));
                case 1:
                    ImageView gift_3 = (ImageView) findViewById(R.id.gift_3);
                    gift_3.setVisibility(View.VISIBLE);
                    gift_3.setImageDrawable(getResources().getDrawable(GiftModel.getDrawable(myGiftModels.get(0).name)));
            }

        } else {
            send_first_gift.setVisibility(View.VISIBLE);
            gifts_block.setVisibility(View.GONE);
        }
    }

    /**
     * Список подарков пользователя
     * @param gift_count
     */
    private void showPopUpGifts(int gift_count){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        // Custom title popup
        View v_title = View.inflate(this, R.layout.popup_gifts_title, null);
        TextView count_gifts = (TextView) v_title.findViewById(R.id.count_gifts);
        count_gifts.setText(getString(R.string.gifts_count, gift_count + ""));
        ImageButton send_gift = (ImageButton) v_title.findViewById(R.id.send_gift);
        send_gift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickSendGift(view);
            }
        });
        builder.setCustomTitle(v_title);

        // Popup content
        View v = View.inflate(this, R.layout.popup_gifts_content, null);

        RecyclerView recyclerView;
        MyGiftsAdapter adapter;

        recyclerView = (RecyclerView) v.findViewById(R.id.list);
        adapter = new MyGiftsAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter.update(model.id);

        builder.setView(v);
        builder.setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Показ верхнего меню
     * @param view
     */
    private void showContextMenu(View view){
        final PopupMenu popupMenu = new PopupMenu(this, view);
        final Menu menu = popupMenu.getMenu();
        popupMenu.getMenuInflater().inflate(R.menu.public_profile, menu);

       // final MenuItem item = popupMenu.getMenu().getItem(0);
        //MenuItem item_rr = popupMenu.getMenu().getItem(1);

        MenuItem delete_bg = popupMenu.getMenu().getItem(1);
        MenuItem delete_avatar = popupMenu.getMenu().getItem(2);

        if(
            !(userModel.role.equals(Const.ROLE_ADMIN) ||
            userModel.role.equals(Const.ROLE_MANAGER) ||
            userModel.role.equals(Const.ROLE_MODER))){
            delete_bg.setVisible(false);
            delete_avatar.setVisible(false);
        }

//        switch (model.friend_status){
//            // друг
//            case Const.FRIEND_STATUS_FRIEND:
//                item.setTitle(getString(R.string.menu_item_remove_from_friend));
//                item_rr.setVisible(false);
//                break;
//            // хочу дружить с ним
//            case Const.FRIEND_STATUS_WISH_FRIEND:
//                item.setTitle(getString(R.string.menu_item_remove_friend_request));
//                item_rr.setVisible(false);
//                break;
//            // хочет дружить со мной
//            case Const.FRIEND_STATUS_WANT_FRIEND:
//                item.setTitle(getString(R.string.menu_item_accept_request));
//                item_rr.setVisible(true);
//                break;
//            // не знаю его
//            case Const.FRIEND_STATUS_NOTKNOW:
//            default:
//                item.setTitle(getString(R.string.menu_item_add_to_friend));
//                item_rr.setVisible(false);
//                break;
//        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                String user_id = model.id;
                switch (item.getItemId()) {
//                    case R.id.action_add_or_remove_friends: // добавить/удалить из друзей
//                        switch (model.friend_status){
//                            // друг
//                            case Const.FRIEND_STATUS_FRIEND:
//                                // удаляем из друзей
//                                removeFromFriends(user_id, handler);
//                                item.setTitle(getString(R.string.menu_item_add_to_friend));
//                                PublicUserModel.setFriendStatus(user_id, Const.FRIEND_STATUS_NOTKNOW);
//                                break;
//                            // хочу дружить с ним
//                            case Const.FRIEND_STATUS_WISH_FRIEND:
//                                // отменяем свой запрос/отписываемся от юзера
//                                removeMyFriendRequest(user_id, handler);
//                                item.setTitle(getString(R.string.menu_item_add_to_friend));
//                                PublicUserModel.setFriendStatus(user_id, Const.FRIEND_STATUS_NOTKNOW);
//                                break;
//                            // хочет дружить со мной
//                            case Const.FRIEND_STATUS_WANT_FRIEND:
//                                // принимаем заяку
//                                acceptFriendRequest(user_id, handler);
//                                item.setTitle(getString(R.string.menu_item_remove_friend_request));
//                                PublicUserModel.setFriendStatus(user_id, Const.FRIEND_STATUS_FRIEND);
//                                break;
//                            // не знаю его
//                            case Const.FRIEND_STATUS_NOTKNOW:
//                            default:
//                                // отправляем запрос на дружбу
//                                addToFriends(user_id, handler);
//                                item.setTitle(getString(R.string.menu_item_remove_friend_request));
//                                PublicUserModel.setFriendStatus(user_id, Const.FRIEND_STATUS_WISH_FRIEND);
//                                break;
//                        }
//                        model = PublicUserModel.getById(model.id);
//                        break;
//                    case R.id.action_decline_request: // отклонить запрос на дружбу
//                        declineFriendRequest(user_id, handler);
//                        item.setTitle(getString(R.string.menu_item_add_to_friend));
//                        PublicUserModel.setFriendStatus(user_id, Const.FRIEND_STATUS_NOTKNOW);
//                        model = PublicUserModel.getById(model.id);
//                        break;
                    case R.id.action_send_message: // отправить сообщение
                        Intent intent = new Intent(PublicProfileActivity.this, UserChatActivity.class);
                        intent.putExtra("uid", model.id);
                        startActivity(intent);
                        break;
                    case R.id.action_delete_bg: // удалить фон
                        showWait();
                        AdminHelper.delete_bg(model.id, token, adminHandler);
                        break;
                    case R.id.action_delete_avatar: // удалить аватарку
                        showWait();
                        AdminHelper.delete_avatar(model.id, token, adminHandler);
                        break;
                    case R.id.action_complain: // пожаловаться
                        showComplainForm();
                        break;
                }
                return false;
            }
        });
        popupMenu.show();
    }

    /**
     * Диалог жалобы на пользователя
     */
    private void showComplainForm(){
        final Dialog dialog = new Dialog(this);
        LayoutInflater inflater = getLayoutInflater();
        final View view = inflater.inflate(R.layout.popup_complain, null);

        Button send = (Button) view.findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText text = (EditText) view.findViewById(R.id.text);
                if(text.getText().length() < 5 || text.getText().length() > 500){
                    text.setError("Текст должен быть от 5 до 500 символов");
                } else {
                    showWait();

                    PublicUserModel.complain(model.id, text.getText().toString(), token, new Handler(){
                        @Override
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            switch (msg.arg1) {
                                case Const.STATE_SUCCESS_RESPONSE:
                                    dialog.dismiss();
                                    Toast.makeText(PublicProfileActivity.this, "Обращение отправлено", Toast.LENGTH_LONG).show();
                                    break;
                                case Const.STATE_SERVER_ERROR:
                                    Toast.makeText(PublicProfileActivity.this, getString(R.string.error_server), Toast.LENGTH_LONG).show();
                            }
                            hideWait();
                        }
                    });

                }
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(view);
        dialog.show();
    }

    /**
     * Заполнение полей данными
     */
    private void bindData(){
        name.setText(model.first_name);

        gender_old.setText(String.valueOf(UserHelper.getOld(model.birth_date)));

        Drawable img = UserHelper.getGenderDrawable(model.gender, this);
        img.setBounds( 0, 0, gender_size, gender_size );
        gender_old.setCompoundDrawables(null, null, img, null);

        // статус
        if(model.my_status.equals("")){
            my_status.setVisibility(View.GONE);
        } else {
            Smiles.setSmiles(model.my_status, my_status, smile_size, this);
        }

        if(model.getWhereGo().equals("")){
            want_go.setVisibility(View.GONE);
        } else {
            want_go.setText(getString(R.string.where_go_template, model.getWhereGo()));
        }

        about_me.setText(model.about_me);
        city.setText(model.city);
        region.setText(RegionModel.getRegionName(model.region));
        inapp.setText(model.getDurationJoin());

        if(model.is_online > 0){
            offline.setVisibility(View.GONE);
            online.setVisibility(View.VISIBLE);
            text_web_status.setText(getString(R.string.web_status_online));
        } else {
            offline.setVisibility(View.VISIBLE);
            online.setVisibility(View.GONE);
            text_web_status.setText(getString(R.string.web_status_offline));
        }

        // установка текста на кнопке
        switch (model.friend_status){
            // друг
            case Const.FRIEND_STATUS_FRIEND:
                action_friend_text.setText("Удалить");
                break;
            // хочу дружить с ним
            case Const.FRIEND_STATUS_WISH_FRIEND:
                action_friend_text.setText(getString(R.string.menu_item_remove_friend_request));
                break;
            // хочет дружить со мной
            case Const.FRIEND_STATUS_WANT_FRIEND:
                action_friend_text.setText(getString(R.string.menu_item_accept_request));
                break;
            // не знаю его
            case Const.FRIEND_STATUS_NOTKNOW:
            default:
                action_friend_text.setText("В друзья");
                break;
        }

        // избранное
        final UserModel userModel = UserModel.getByToken(token);
        final ImageView favorite = (ImageView) findViewById(R.id.favorite);
        // отмечаем как уже добавленный в избранное
        if(FavoriteModel.check(model.id, userModel.id)){
            favorite.setImageResource(R.drawable.ic_star_full_white);
        }
        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(FavoriteModel.check(model.id, userModel.id)){
                    // удаляем если уже был добавлен
                    favorite.setImageResource(R.drawable.ic_star_line);
                    FavoriteModel.remove(model.id, userModel.id);
                } else {
                    // добавляем если ещё нет в избранном
                    favorite.setImageResource(R.drawable.ic_star_full_white);
                    FavoriteModel.add(model.id, userModel.id);
                }
            }
        });
    }

    /**
     * Обновление аватарки и фона
     */
    private void bindMedia(){
        /**
         * аватарка
         */
        Picasso.with(this)
                .load(Const.API_URL + model.avatar)
                .placeholder(R.drawable.ava)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(avatar, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(PublicProfileActivity.this)
                                .load(Const.API_URL + model.avatar)
                                .placeholder(R.drawable.ava)
                                .error(R.drawable.ava)
                                .into(avatar);
                    }
                });

        /**
         * Фон
         */
        Picasso.with(this)
                .load(Const.API_URL + model.background)
                .placeholder(R.drawable.full_bg)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(full_bg, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(PublicProfileActivity.this)
                                .load(Const.API_URL + model.background)
                                .placeholder(R.drawable.full_bg)
                                .error(R.drawable.full_bg)
                                .into(full_bg);
                    }
                });

        /**
         * Показ увеличенной аватарки
         */
        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(PublicProfileActivity.this)
                        .load(Const.API_URL + model.avatar_full)
                        .placeholder(R.drawable.ava)
                        //.networkPolicy(NetworkPolicy.OFFLINE)
                        .into(mImageView, new Callback() {
                            @Override
                            public void onSuccess() {

                            }
                            @Override
                            public void onError() {
                                //Try again online if cache failed
                                Picasso.with(PublicProfileActivity.this)
                                        .load(Const.API_URL + model.avatar_full)
                                        .placeholder(R.drawable.ava)
                                        .error(R.drawable.ava)
                                        .into(mImageView);
                            }
                        });

                // Attach a PhotoViewAttacher, which takes care of all of the zooming functionality.
                // (not needed unless you are going to change the drawable later)
                mAttacher = new PhotoViewAttacher(mImageView);

                photo_overlay.setVisibility(View.VISIBLE);
            }
        });

        /**
         * Показ увеличенной копии фона
         */
        full_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(PublicProfileActivity.this)
                        .load(Const.API_URL + model.background)
                        .placeholder(R.drawable.full_bg)
                        //.networkPolicy(NetworkPolicy.OFFLINE)
                        .into(mImageView, new Callback() {
                            @Override
                            public void onSuccess() {

                            }
                            @Override
                            public void onError() {
                                //Try again online if cache failed
                                Picasso.with(PublicProfileActivity.this)
                                        .load(Const.API_URL + model.background)
                                        .placeholder(R.drawable.full_bg)
                                        .error(R.drawable.full_bg)
                                        .into(mImageView);
                            }
                        });

                // Attach a PhotoViewAttacher, which takes care of all of the zooming functionality.
                // (not needed unless you are going to change the drawable later)
                mAttacher = new PhotoViewAttacher(mImageView);

                photo_overlay.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateDate();
    }

    @Override
    public void onBackPressed() {
        if(photo_overlay.getVisibility() == View.VISIBLE){
            photo_overlay.setVisibility(View.GONE);
            return;
        }
        if(getIntent().getBooleanExtra("gift", false)){
            Intent intent = new Intent(this, AppActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return;
        }
        super.onBackPressed();
    }

    /**
     * Открыть выбор подарков
     * @param v
     */
    public void onClickSendGift(View v){
        Intent intent = new Intent(this, GiftsListActivity.class);
        intent.putExtra("uid", model.id);
        startActivity(intent);
    }

    @Override
    public void onSocketAcceptFriendRequest(PublicUserModel publicUserModel) {
        super.onSocketAcceptFriendRequest(publicUserModel);
        updateDate();
    }

    @Override
    public void onSocketDeclineFriendRequest(PublicUserModel publicUserModel) {
        super.onSocketDeclineFriendRequest(publicUserModel);
        updateDate();
    }

    @Override
    public void onSocketCancelFriendRequest(PublicUserModel publicUserModel) {
        super.onSocketCancelFriendRequest(publicUserModel);
        updateDate();
    }

    @Override
    public void onSocketNewFriendsRequest(PublicUserModel publicUserModel) {
        super.onSocketNewFriendsRequest(publicUserModel);
        updateDate();
    }

    @Override
    public void onSocketRemoveFromFriends(PublicUserModel publicUserModel) {
        super.onSocketRemoveFromFriends(publicUserModel);
        updateDate();
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    hideWait();
                    logout();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    hideWait();
                    updateDate();
                    break;
                case Const.STATE_SUCCESS_ADD_TO_FRIENDS:
                    hideWait();
                    updateDate();
                    Toast.makeText(PublicProfileActivity.this, getString(R.string.info_add_friend), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_REMOVE_FROM_FRIENDS:
                    hideWait();
                    updateDate();
                    Toast.makeText(PublicProfileActivity.this, getString(R.string.info_remove_friend), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_DECLINE_FRIEND_REQUEST:
                    hideWait();
                    updateDate();
                    Toast.makeText(PublicProfileActivity.this, getString(R.string.info_success_decline_friend_request), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_CANCEL_FRIEND_REQUEST:
                    hideWait();
                    updateDate();
                    Toast.makeText(PublicProfileActivity.this, getString(R.string.info_success_cancel_friend_request), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:
                    hideWait();
                    Toast.makeText(PublicProfileActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    Handler adminHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    hideWait();
                    logout();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    hideWait();
                    updateDate();
                    Toast.makeText(PublicProfileActivity.this, "Запрос выполнен", Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:
                    hideWait();
                    Toast.makeText(PublicProfileActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}
