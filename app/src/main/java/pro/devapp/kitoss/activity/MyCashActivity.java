package pro.devapp.kitoss.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.NotificationHelper;
import pro.devapp.kitoss.models.NotificationModel;
import pro.devapp.kitoss.models.UserModel;

public class MyCashActivity extends BaseActivity {

    Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cash);

        initHomeBtn(getString(R.string.title_my_cash));

        NotificationHelper.cancelNotification(NotificationModel.TYPE_NOTIFICATIONS, this);

        Button share = (Button) findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                share();
            }
        });
    }

    public void onClickPaymentVariant(View v){
        Intent intent = new Intent(this, PaymentActivity.class);
        switch (v.getId()){
            case R.id.pay_v_1:
                intent.putExtra("pay", 1);
                break;
            case R.id.pay_v_2:
                intent.putExtra("pay", 2);
                break;
            case R.id.pay_v_3:
                intent.putExtra("pay", 3);
                break;
        }
        startActivity(intent);
    }

    /**
     * Верхние меню
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_my_cash_actions, this.menu);

        MenuItem balance = this.menu.findItem(R.id.balance);
        balance.setTitle(getString(R.string.balance_text, userModel.balance));
        return true;
    }

    @Override
    public void onSocketUpdateBalance(UserModel userModel) {
        super.onSocketUpdateBalance(userModel);
        this.userModel = userModel;
        if(menu != null){
            MenuItem balance = this.menu.findItem(R.id.balance);
            balance.setTitle(getString(R.string.balance_text, userModel.balance + ""));
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                NotificationHelper.cancelNotification(NotificationModel.TYPE_NOTIFICATIONS, MyCashActivity.this);
            }
        }, 2000l);
    }
}
