package pro.devapp.kitoss.activity;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.UserHelper;
import pro.devapp.kitoss.interfaces.PlayListener;
import pro.devapp.kitoss.models.GameModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.RegionModel;
import pro.devapp.kitoss.services.AlarmReceiver;
import pro.devapp.kitoss.ui.GameCanvas;

public class GameActivity extends BaseActivity {

    private final int time = 1800;
    private TextView run_text;
    private GameCanvas gameCanvas;
    private Timer timer;
    private View run;
    private int gender_size = 20;
    // таймер
    private int timeout = time;
    private Boolean enable = false;
    Boolean delay_end = false;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        initHomeBtn(getString(R.string.label_bonus));

        showWait();

        gender_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics());

        gameCanvas = (GameCanvas) findViewById(R.id.game);
        run_text = (TextView) findViewById(R.id.run_text);
        run = findViewById(R.id.run);

        run.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!enable || !delay_end){
                    return;
                }
                enable = false;

                // запускаем таймер
                timer = new Timer();
                timer.schedule(new GameTimerTask(), 0, 1000);
                createAlarm();
                gameCanvas.run(new PlayListener(){

                    @Override
                    public void onPlayStart() {

                    }

                    @Override
                    public void onPlayEnd(String user_id, final int result) {
                        showWait();
                        final PublicUserModel publicUserModel = PublicUserModel.getById(user_id);
                        GameModel.set_result(token, user_id, result, new Handler(){
                            @Override
                            public void handleMessage(Message msg) {
                                super.handleMessage(msg);
                                switch (msg.arg1) {
                                    case Const.STATE_SUCCESS_RESPONSE:
                                        int bonuses = GameModel.getBonusByPosition(result);
                                        GameActivity.this.userModel.bonuses += bonuses;
                                        GameActivity.this.userModel.update();
                                        getGameStat();
                                        alert(publicUserModel, bonuses);
                                        break;
                                    case Const.STATE_SERVER_ERROR:
                                        Toast.makeText(GameActivity.this, getString(R.string.error_server), Toast.LENGTH_LONG).show();
                                }
                                hideWait();
                            }
                        });
                    }
                });
            }
        });

        // переход к правилам
        View go_to_rules = findViewById(R.id.go_to_rules);
        go_to_rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GameActivity.this, GameRulesActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();

        gameCanvas = (GameCanvas) findViewById(R.id.game);
        showWait();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(delay_end){
                    hideWait();
                } else {
                    delay_end = true;
                }
            }
        }, 3000l);
        // загружаем список юзеров для игры
        GameModel.get_users(token, new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.arg1) {
                    case Const.STATE_SUCCESS_RESPONSE:
                        gameCanvas.loadAvatars((GameModel.Resp)msg.obj);
                        timeout = ((GameModel.Resp)msg.obj).timeout;
                        if(timeout <= 0){
                            enable = true;
                            timeout = time;
                        } else {
                            enable = false;
                            // запускаем таймер
                            timer = new Timer();
                            timer.schedule(new GameTimerTask(), 0, 1000);
                        }
                        break;
                    case Const.STATE_SERVER_ERROR:
                        Toast.makeText(GameActivity.this, getString(R.string.error_server), Toast.LENGTH_LONG).show();
                }
                if(delay_end){
                    hideWait();
                } else {
                    delay_end = true;
                }
            }
        });

        getGameStat();
    }

    /**
     * Отображение игровой статистики
     */
    private void getGameStat(){
        // загружаем статистику для игры
        GameModel.get_statistic(token, new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.arg1) {
                    case Const.STATE_SUCCESS_RESPONSE:
                        GameModel.RespStatistic respStatistic = (GameModel.RespStatistic)msg.obj;
                        drawMyStat(respStatistic);
                        drawTopUsers(respStatistic);
                        drawWinnerUsers(respStatistic);
                        break;
                    case Const.STATE_SERVER_ERROR:
                        Toast.makeText(GameActivity.this, getString(R.string.error_server), Toast.LENGTH_LONG).show();
                }
                hideWait();
            }
        });
    }

    /**
     * Верхние меню
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_my_cash_actions, this.menu);

        drawBalance();
        return true;
    }

    /**
     * Отображение бонусов в меню
     */
    private void drawBalance(){
        if(this.menu == null){
            return;
        }
        MenuItem balance = this.menu.findItem(R.id.balance);
        balance.setTitle(getBonusesName(userModel.bonuses));
    }
    /**
     * Показ результата
     * @param publicUserModel
     */
    public void alert(final PublicUserModel publicUserModel, final int bonuus) {
        final Dialog dialog = new Dialog(this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.popup_game_result, null);

        TextView user_name = (TextView) view.findViewById(R.id.user_name);
        user_name.setText(publicUserModel.first_name);

        TextView age = (TextView) view.findViewById(R.id.age);
        int old = UserHelper.getOld(publicUserModel.birth_date);
        age.setText(String.valueOf(old));

        TextView region = (TextView) view.findViewById(R.id.region);
        String region_text = "";
        if(!RegionModel.getRegionName(publicUserModel.region).equals("") && !publicUserModel.city.equals("")){
            region_text = RegionModel.getRegionName(publicUserModel.region) + ", " + publicUserModel.city;
        } else {
            region_text = RegionModel.getRegionName(publicUserModel.region) + " " + publicUserModel.city;
        }
        region.setText(region_text);

        Drawable img = UserHelper.getGenderDrawable(publicUserModel.gender, this);
        img.setBounds( 0, 0, gender_size, gender_size );
        age.setCompoundDrawables(null, null, img, null);

        TextView bonus = (TextView) view.findViewById(R.id.bonuus);
        bonus.setText(getBonusesName(bonuus));

        TextView gift_bonuses = (TextView) view.findViewById(R.id.gift_bonuses);
        gift_bonuses.setText(getBonusesName(bonuus/5));

        View btn_ok = view.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        final ImageView ava = (ImageView) view.findViewById(R.id.ava);
        Picasso.with(this)
                .load(Const.API_URL + publicUserModel.avatar)
                .placeholder(R.drawable.ava)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(ava, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(GameActivity.this)
                                .load(Const.API_URL + publicUserModel.avatar)
                                .placeholder(R.drawable.ava)
                                .error(R.drawable.ava)
                                .into(ava);
                    }
                });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(view);
        dialog.show();
    }

    /**
     * Лидеры недели
     */
    private void drawTopUsers(GameModel.RespStatistic resp){
        LinearLayout top_users = (LinearLayout) findViewById(R.id.top_users);
        top_users.removeAllViews();
        LayoutInflater inflater = getLayoutInflater();

        int game_rate = 1;
        for(final PublicUserModel publicUserModel : resp.current_week){
            View view = inflater.inflate(R.layout.item_game_user, null);

            TextView name = (TextView) view.findViewById(R.id.name);
            name.setText(publicUserModel.first_name);

            TextView score = (TextView) view.findViewById(R.id.score);
            score.setText(getBonusesName(publicUserModel.score));

            TextView age = (TextView) view.findViewById(R.id.age);
            int old = UserHelper.getOld(publicUserModel.birth_date);
            age.setText(String.valueOf(old));

            Drawable img = UserHelper.getGenderDrawable(publicUserModel.gender, this);
            img.setBounds( 0, 0, gender_size, gender_size );
            age.setCompoundDrawables(null, null, img, null);

            TextView status = (TextView) view.findViewById(R.id.status);
            status.setText(getString(R.string.game_rate, game_rate));
            game_rate++;

            View offline = view.findViewById(R.id.offline);
            View online = view.findViewById(R.id.online);
            if(publicUserModel.is_online > 0){
                offline.setVisibility(View.GONE);
                online.setVisibility(View.VISIBLE);
            } else {
                online.setVisibility(View.GONE);
                offline.setVisibility(View.VISIBLE);
            }

            final ImageView ava = (ImageView) view.findViewById(R.id.ava);
            Picasso.with(this)
                    .load(Const.API_URL + publicUserModel.avatar)
                    .placeholder(R.drawable.ava)
                    //.networkPolicy(NetworkPolicy.OFFLINE)
                    .into(ava, new Callback() {
                        @Override
                        public void onSuccess() {

                        }
                        @Override
                        public void onError() {
                            //Try again online if cache failed
                            Picasso.with(GameActivity.this)
                                    .load(Const.API_URL + publicUserModel.avatar)
                                    .placeholder(R.drawable.ava)
                                    .error(R.drawable.ava)
                                    .into(ava);
                        }
                    });

            top_users.addView(view);

            ava.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goToUserProfile(publicUserModel.id);
                }
            });
        }
    }

    /**
     * Победители прошлой недели
     */
    private void drawWinnerUsers(GameModel.RespStatistic resp){
        LinearLayout top_users = (LinearLayout) findViewById(R.id.winner_users);
        top_users.removeAllViews();
        LayoutInflater inflater = getLayoutInflater();

        int game_rate = 1;
        for(final PublicUserModel publicUserModel : resp.prev_week) {
            View view = inflater.inflate(R.layout.item_game_user, null);

            TextView name = (TextView) view.findViewById(R.id.name);
            name.setText(publicUserModel.first_name);

            TextView score = (TextView) view.findViewById(R.id.score);
            score.setText(getBonusesName(publicUserModel.score));

            TextView age = (TextView) view.findViewById(R.id.age);
            int old = UserHelper.getOld(publicUserModel.birth_date);
            age.setText(String.valueOf(old));

            Drawable img = UserHelper.getGenderDrawable(publicUserModel.gender, this);
            img.setBounds( 0, 0, gender_size, gender_size );
            age.setCompoundDrawables(null, null, img, null);

            TextView status = (TextView) view.findViewById(R.id.status);
            status.setText(getString(R.string.game_rate, game_rate));

            Drawable game_rate_img = null;
            switch (game_rate){
                case 1:
                    score.setVisibility(View.GONE);
                    view.findViewById(R.id.cup).setVisibility(View.VISIBLE);
                    game_rate_img = getResources().getDrawable(R.drawable.gift_gold_medal);
                    break;
                case 2:
                    game_rate_img = getResources().getDrawable(R.drawable.gift_silver_medal);
                    score.setText("300 руб.");
                    break;
                case 3:
                    game_rate_img = getResources().getDrawable(R.drawable.gift_bronze_medal);
                    score.setText("100 руб.");
                    break;
                case 4:
                    //game_rate_img = getResources().getDrawable(R.drawable.gift_bronze_medal);
                    score.setText("30 китсов.");
                    break;
                case 5:
                    //game_rate_img = getResources().getDrawable(R.drawable.gift_bronze_medal);
                    score.setText("20 китсов.");
                    break;
            }

            if(game_rate_img != null){
                game_rate_img.setBounds( 0, 0, gender_size, gender_size );
                status.setCompoundDrawables(game_rate_img, null, null, null);
            }

            game_rate++;

            View offline = view.findViewById(R.id.offline);
            View online = view.findViewById(R.id.online);
            if(publicUserModel.is_online > 0){
                offline.setVisibility(View.GONE);
                online.setVisibility(View.VISIBLE);
            } else {
                online.setVisibility(View.GONE);
                offline.setVisibility(View.VISIBLE);
            }

            final ImageView ava = (ImageView) view.findViewById(R.id.ava);
            Picasso.with(this)
                    .load(Const.API_URL + publicUserModel.avatar)
                    .placeholder(R.drawable.ava)
                    //.networkPolicy(NetworkPolicy.OFFLINE)
                    .into(ava, new Callback() {
                        @Override
                        public void onSuccess() {

                        }
                        @Override
                        public void onError() {
                            //Try again online if cache failed
                            Picasso.with(GameActivity.this)
                                    .load(Const.API_URL + publicUserModel.avatar)
                                    .placeholder(R.drawable.ava)
                                    .error(R.drawable.ava)
                                    .into(ava);
                        }
                    });
            ava.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goToUserProfile(publicUserModel.id);
                }
            });

            top_users.addView(view);
        }
    }

    /**
     * Блок своей статистики
     * @param resp
     */
    private void drawMyStat(GameModel.RespStatistic resp){

        final PublicUserModel publicUserModel = userModel.getPublicModel();

        String my_status = "У Вас пока нет бонусов...";

        TextView my_rate = (TextView) findViewById(R.id.my_rate);
        my_rate.setText(my_status);
        publicUserModel.score = 0;
        if(resp.my_stat != null &&  resp.my_stat.my_stat > 0){
            publicUserModel.score = resp.my_stat.my_stat;
            my_status = getString(R.string.game_rate, resp.my_stat.my_game_rate);
            my_rate.setText(getString(R.string.my_game_status_title, resp.my_stat.my_game_rate));
        }

        // обновляем в базе
        userModel.bonuses = resp.my_stat.my_stat;
        userModel.update();

        drawBalance();

        /**
         * Место в общем зачёте
         */
        TextView status = (TextView) findViewById(R.id.my_status);
        status.setText(my_status);

        /**
         * Набрано баллов
         */
        TextView score = (TextView) findViewById(R.id.my_score);
        score.setText(getBonusesName(userModel.bonuses));

        TextView name = (TextView) findViewById(R.id.my_name);
        name.setText(publicUserModel.first_name);

        TextView age = (TextView) findViewById(R.id.my_age);
        int old = UserHelper.getOld(publicUserModel.birth_date);
        age.setText(String.valueOf(old));

        Drawable img = UserHelper.getGenderDrawable(publicUserModel.gender, this);
        img.setBounds( 0, 0, gender_size, gender_size );
        age.setCompoundDrawables(null, null, img, null);

        View offline = findViewById(R.id.offline);
        View online = findViewById(R.id.online);
        if(publicUserModel.is_online > 0){
            offline.setVisibility(View.GONE);
            online.setVisibility(View.VISIBLE);
        } else {
            online.setVisibility(View.GONE);
            offline.setVisibility(View.VISIBLE);
        }

        final ImageView ava = (ImageView) findViewById(R.id.ava);
        Picasso.with(this)
                .load(Const.API_URL + publicUserModel.avatar)
                .placeholder(R.drawable.ava)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(ava, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(GameActivity.this)
                                .load(Const.API_URL + publicUserModel.avatar)
                                .placeholder(R.drawable.ava)
                                .error(R.drawable.ava)
                                .into(ava);
                    }
                });
    }

    /**
     * Переход к профилю пользователя
     */
    public void goToUserProfile(String user_id){
        if(userModel.id.equals(user_id)){
            return;
        }
        Intent intent = new Intent(this, PublicProfileActivity.class);
        intent.putExtra("uid", user_id);
        startActivity(intent);
    }

    /**
     * Подписи к колличеству баллов
     * @param bonuses
     * @return
     */
    private String getBonusesName(int bonuses){
        int[] cases = {2, 0, 1, 1, 1, 2};
        int[] titles = {R.string.bonuses1_text, R.string.bonuses2_text, R.string.bonuses3_text};
        return getString(titles[ (bonuses%100>4 && bonuses%100<20)? 2 : cases[(bonuses%10<5)?bonuses%10:5] ], bonuses);
    }

    /**
     * Отсчёт времени
     */
    public class GameTimerTask extends TimerTask {
        @Override
        public void run() {
            timeout--;
            if(timeout <= 0){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        run_text.setText("СТАРТ");
                        enable = true;
                        timer.cancel();
                        timeout = time;
                    }
                });
            } else {
                final int second = timeout %60;
                final int min = (timeout -second)/60;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        run_text.setText(String.format("%02d", min) + " : " + String.format("%02d", second));
                    }
                });
            }
        }
    }

    /**
     * Добавить напоминание
     */
    private void createAlarm(){
        Intent alarmIntent = new Intent(GameActivity.this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(GameActivity.this, 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.MINUTE, 30);

        manager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }
}
