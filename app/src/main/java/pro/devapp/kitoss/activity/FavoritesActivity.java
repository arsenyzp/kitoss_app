package pro.devapp.kitoss.activity;

import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.adapters.UserListAdapter;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.models.FavoriteModel;
import pro.devapp.kitoss.models.PublicUserModel;

public class FavoritesActivity extends BaseActivity {

    private UserListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        initHomeBtn(getString(R.string.title_favorites));

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        adapter = new UserListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter.updateFavoriteList(userModel);

        /**
         * Делаем запрос к серверу
         */
        PublicUserModel.getFavorites(FavoriteModel.getIds(userModel.id), token, handler);
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {

            super.handleMessage(msg);
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    hideWait();
                    logout();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    hideWait();
                    adapter.updateFavoriteList(userModel);
                    break;
                case Const.STATE_SUCCESS_ADD_TO_FRIENDS:
                    hideWait();
                    adapter.updateFavoriteList(userModel);
                    Toast.makeText(FavoritesActivity.this, getString(R.string.info_add_friend), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_REMOVE_FROM_FRIENDS:
                    hideWait();
                    adapter.updateFavoriteList(userModel);
                    Toast.makeText(FavoritesActivity.this, getString(R.string.info_remove_friend), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_DECLINE_FRIEND_REQUEST:
                    hideWait();
                    adapter.updateFavoriteList(userModel);
                    Toast.makeText(FavoritesActivity.this, getString(R.string.info_success_decline_friend_request), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_CANCEL_FRIEND_REQUEST:
                    hideWait();
                    adapter.updateFavoriteList(userModel);
                    Toast.makeText(FavoritesActivity.this, getString(R.string.info_success_cancel_friend_request), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:
                    hideWait();
                    Toast.makeText(FavoritesActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}
