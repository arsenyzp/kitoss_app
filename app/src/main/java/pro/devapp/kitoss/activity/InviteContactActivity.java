package pro.devapp.kitoss.activity;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.adapters.ContactsAdapter;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.models.ContactModel;
import pro.devapp.kitoss.models.UserModel;

public class InviteContactActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private ContactsAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_contact);

        initHomeBtn(getString(R.string.title_invite_contacts));

        UserModel model = UserModel.getByToken(token);

        recyclerView = (RecyclerView) findViewById(R.id.contact_list);
        adapter = new ContactsAdapter(this, model.promo_cod);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(checkPermission()){
            adapter.update();
            loadContacts();
        }
    }

    private void loadContacts(){
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                // проверка контактов
                ContactModel.update(InviteContactActivity.this);
                adapter.update();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                adapter.notifyDataSetChanged();
            }
        };
        task.execute();
    }

    private Boolean checkPermission(){
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS);
        int hasReadeContactsPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED && hasReadeContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CONTACTS},
                    Const.REQUEST_CODE_ASK_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode != RESULT_OK)
        switch (requestCode){
            case Const.REQUEST_CODE_ASK_PERMISSIONS:
                loadContacts();
                break;
        }
    }
}
