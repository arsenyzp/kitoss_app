package pro.devapp.kitoss.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.api.Payment;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.RetrofitInstance;
import pro.devapp.kitoss.models.ResponseModel;
import pro.devapp.kitoss.models.UserModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity extends BaseActivity implements BillingProcessor.IBillingHandler{

    BillingProcessor bp;
    final String base64EncodedPublicKey =
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4fS6Y2B8evGbbT4881cCnLfA4Q9qzRwNZMdUBxL1sHfm3OWUPK5mn5IN83CgNNMqZIvLNihRPoTQvEmkS2ZJryhHgz/n0c7bntqukC9GcYHjhwOWjr4+Xaed+5+43UDBpUH5o3O0xgBSFeiKkXPylvfPF2ITkQneuJ9iecZHpgLd9y/GpXOAF1MVM+MLk/aQ2QkOud2w1FV3nzRaRYDIQBUbFtzYcyTROgTwrv3SxtF4fGagEEoozRCKO/BfFyeQo24Nb1KrUj2mJnoQqvCh5t9LENqAfDqZ09hxMub3XyWkzFcepANiBFPPH1zHNpXjpPc2UeSKy/1aIiRtjZD/MQIDAQAB";

    String SKU_PREMIUM = "10_kitsov";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        bp = new BillingProcessor(this, base64EncodedPublicKey, this);

        initHomeBtn("Оплата");

        TextView promo_text_g = (TextView) findViewById(R.id.promo_text_g);

        switch (getIntent().getExtras().getInt("pay")){
            case 1:
                promo_text_g.setText("Оплатить 10 китсов");
                SKU_PREMIUM = "10_kitsov";
                break;
            case 2:
                promo_text_g.setText("Оплатить 30 китсов");
                SKU_PREMIUM = "30_kitsov";
                break;
            case 3:
                promo_text_g.setText("Оплатить 100 китсов");
                SKU_PREMIUM = "100_kitsov";
                break;
        }

    }

    @Override
    public void onDestroy() {
        if (bp != null)
            bp.release();

        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        if(bp.consumePurchase(productId)){

            Payment service = RetrofitInstance.getRetrofit().create(Payment.class);
            Call<ResponseModel> call = service.gpayment(token, SKU_PREMIUM);
            call.enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                    //ResponseModel m = response.body();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideWait();
                            alert("Покупка прошла успешно ");
                        }
                    });
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideWait();
                            alert("Ошибка сохранения баланса!");
                        }
                    });
                }
            });
        } else {
            alert("Ошибка покупки ");
            hideWait();
        }
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        alert("Ошибка: " + errorCode);
    }

    @Override
    public void onBillingInitialized() {
        View g_pay = findViewById(R.id.g_pay);
        g_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWait();
                if(!bp.consumePurchase(SKU_PREMIUM)){
                    bp.purchase(PaymentActivity.this, SKU_PREMIUM);
                }
            }
        });
    }

    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Log.d(GooglePaymentActivity.class.getName(), "Showing alert dialog: " + message);
        bld.create().show();
    }

    @Override
    public void onSocketUpdateBalance(UserModel userModel) {
        super.onSocketUpdateBalance(userModel);
        this.userModel = userModel;
    }
}
