package pro.devapp.kitoss.activity;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import pro.devapp.kitoss.R;
import pro.devapp.kitoss.adapters.MyGiftsAdapter;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.SmileTextWatcher;
import pro.devapp.kitoss.components.Smiles;
import pro.devapp.kitoss.fragments.AvatarMenuFragment;
import pro.devapp.kitoss.models.GiftModel;
import pro.devapp.kitoss.models.MyGiftModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.SmilesHistoryModel;
import pro.devapp.kitoss.models.UserModel;
import pro.devapp.kitoss.models.WhereGoModel;

public class ProfileActivity extends BaseActivity {

    UserModel model;
    EditText name;
    EditText city;
    Spinner birth_day;
    Spinner birth_month;
    Spinner birth_year;
    Spinner gender;
    Spinner want_go;
    EditText about_me;
    EditText my_status;
    CircleImageView avatar;
    ImageView full_bg;

    View smiles;

    int smile_size = 25;

    List<WhereGoModel> whereGoModels; // список моделей вариантов куда пойти

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        model = UserModel.getByToken(token);

        // смайлики
        smiles = findViewById(R.id.smiles);
        // размер смайла
        smile_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, getResources().getDisplayMetrics());

        // дата рождения
        ArrayAdapter<String> adapter_days = new ArrayAdapter<String>(this, R.layout.spinner_item, Const.days);
        adapter_days.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        birth_day = (Spinner) findViewById(R.id.birth_day);
        birth_day.setAdapter(adapter_days);

        final String[] month = {getString(R.string.month1), getString(R.string.month2), getString(R.string.month3), getString(R.string.month4), getString(R.string.month5), getString(R.string.month6), getString(R.string.month7), getString(R.string.month8), getString(R.string.month9), getString(R.string.month10), getString(R.string.month11), getString(R.string.month12)};
        ArrayAdapter<String> adapter_month = new ArrayAdapter<String>(this, R.layout.spinner_item, month);
        adapter_month.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        birth_month = (Spinner) findViewById(R.id.birth_month);
        birth_month.setAdapter(adapter_month);

        final String[] years = new String[40];
        for(int y = Const.min_year; y < Const.max_year; y++){
            years[y - Const.min_year] = String.valueOf(y);
        }
        ArrayAdapter<String> adapter_years = new ArrayAdapter<String>(this, R.layout.spinner_item, years);
        adapter_month.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        birth_year = (Spinner) findViewById(R.id.birth_year);
        birth_year.setAdapter(adapter_years);

        // пол
        final String[] genders = {getString(R.string.male), getString(R.string.female)};
        ArrayAdapter<String> adapter_genders = new ArrayAdapter<String>(this, R.layout.spinner_item, genders);
        adapter_genders.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender = (Spinner) findViewById(R.id.gender);
        gender.setAdapter(adapter_genders);

        // куда пойти
        whereGoModels = WhereGoModel.getAll();
        final String[] whereGo = new String[whereGoModels.size()+1];
        whereGo[0] = "-";
        for (int i = 0; i < whereGoModels.size(); i++){
            Log.d(ProfileActivity.class.getName(), whereGoModels.get(i).text);
            whereGo[i+1] = whereGoModels.get(i).text;
        }
        ArrayAdapter<String> adapter_whereGo = new ArrayAdapter<String>(this, R.layout.spinner_item, whereGo);
        adapter_whereGo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        want_go = (Spinner) findViewById(R.id.want_go);
        want_go.setAdapter(adapter_whereGo);

        name = (EditText) findViewById(R.id.name);
        city = (EditText) findViewById(R.id.city);
        my_status = (EditText) findViewById(R.id.my_status);
        about_me = (EditText) findViewById(R.id.about_me);
        avatar = (CircleImageView) findViewById(R.id.avatar);
        full_bg = (ImageView) findViewById(R.id.full_bg);

        final RelativeLayout birth_day_place = (RelativeLayout) findViewById(R.id.birth_day_place);
        final RelativeLayout birth_month_place = (RelativeLayout) findViewById(R.id.birth_month_place);
        final RelativeLayout birth_year_place = (RelativeLayout) findViewById(R.id.birth_year_place);
        final RelativeLayout gender_place = (RelativeLayout) findViewById(R.id.gender_place);

        // меню загрузки аватарки
        ImageView avatar = (ImageView) findViewById(R.id.avatar);
        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                DialogFragment dialogFragment;
                dialogFragment = AvatarMenuFragment.newInstance();
                dialogFragment.show(transaction, "dialog");
            }
        });
        // меню загрузки фона

        //закрыть
        View close = findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        // сохранение
        Button btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                boolean error = false;
                // день рождения
                int value_birth_day = birth_day.getSelectedItemPosition();
                if(value_birth_day < 1){
                    birth_day_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                    error = true;
                } else {
                    birth_day_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                }

                int value_birth_month = birth_month.getSelectedItemPosition();
                if(value_birth_month < 0){
                    birth_month_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                    error = true;
                } else {
                    birth_month_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                }

                int value_birth_year = birth_year.getSelectedItemPosition();
                if(value_birth_year < 0){
                    birth_year_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                    error = true;
                } else {
                    birth_year_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                }

                // пол
                int value_gender = gender.getSelectedItemPosition();
                if(value_gender < 0){
                    gender_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                    error = true;
                } else {
                    gender_place.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                }

                //город
                model.city = city.getText().toString();
                if(model.city.length() < 3){
                    city.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                    error = true;
                } else {
                    city.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                }
                //имя
                model.first_name = name.getText().toString();
                if(model.first_name.length() < 3){
                    name.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                    error = true;
                } else {
                    name.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg));
                }

                // статус
                model.my_status = my_status.getText().toString();
                // о себе
                model.about_me = about_me.getText().toString();

                // если есть ошибки в форме
                if(error){
                    Toast.makeText(ProfileActivity.this, getString(R.string.error_in_form), Toast.LENGTH_SHORT).show();
                    return;
                }


                if(value_birth_day >= Const.days.length){
                    value_birth_day = 0;
                }

                // формируем строку с датой рождения
                model.birth_date = Const.days[value_birth_day] + "." + String.valueOf(value_birth_month+1) + "." + years[value_birth_year];
                model.gender = value_gender == 0 ? "male" : "female";

                // получаем id выбранного варианта куда пойти
                if(want_go.getSelectedItemPosition() > 0){
                    WhereGoModel wgModel = whereGoModels.get(want_go.getSelectedItemPosition()-1);
                    model.where_go = wgModel.id;
                }

                // делаем запрос
                showWait();
                model.saveOnServer(handler);
            }
        });

        // show smile
        final ImageButton button_smile = (ImageButton)findViewById(R.id.button_smile);
        button_smile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(smiles.getVisibility() != View.VISIBLE){
                    // скрываем клавиатуру
                    hideKeyboard();
                    // показываем смайлы
                    showSmiles();
                    // меняем занчёк на кнопке
                    button_smile.setImageResource(R.drawable.ic_keyboard);
                } else {
                    // скрываем смайлы
                    hideSmiles();
                    // показываем клавиатуру
                    showKeyboard();
                    // меняем занчёк на кнопке
                    button_smile.setImageResource(R.drawable.ic_smile);
                }
            }
        });

        // обработка удаления смайликов
        SmileTextWatcher smileTextWatcher = new SmileTextWatcher(my_status, smile_size, this);
        my_status.addTextChangedListener(smileTextWatcher);

        /**
         * Клик по полю для ввода сообщения
         * скрыти смайлов и показ клавиатуры
         */
        my_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(smiles.getVisibility() == View.VISIBLE){
                    smiles.setVisibility(View.GONE);
                }
            }
        });

        // обновляем модель с сервера
        showWait();
        model.refreshFromServer(refreshHandler);
    }

    @Override
    public void onBackPressed() {
        if(smiles.getVisibility() == View.VISIBLE){
            smiles.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Заполнение полей данными
     */
    private void bindData(){
        name.setText(model.first_name);
        city.setText(model.city);
        Smiles.setSmiles(model.my_status, my_status, smile_size, this);
        about_me.setText(model.about_me);

        String[] date_part = model.birth_date.split("[.]");
        if(date_part.length == 3){
            birth_day.setSelection(Integer.valueOf(date_part[0]));
            birth_month.setSelection(Integer.valueOf(date_part[1])-1);
            if(Const.max_year < Integer.valueOf(date_part[2])){
                birth_year.setSelection(0);
            } else {
                birth_year.setSelection(Integer.valueOf(date_part[2]) - Const.min_year);
            }
        }

        if(model.gender.equals("male")){
            gender.setSelection(0);
        } else {
            gender.setSelection(1);
        }

        if(model.where_go != null && !model.where_go.equals("")){
            for(int i = 0; i < whereGoModels.size(); i++){
                if(whereGoModels.get(i).id.equals(model.where_go)){
                    want_go.setSelection(i);
                    break;
                }
            }
        }
    }

    /**
     * Обновление аватарки и фона
     */
    private void bindMedia(){

        Picasso.with(this)
                .load(Const.API_URL + model.avatar)
                .placeholder(R.drawable.ava)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(avatar, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(ProfileActivity.this)
                                .load(Const.API_URL + model.avatar)
                                .placeholder(R.drawable.ava)
                                .error(R.drawable.ava)
                                .into(avatar);
                    }
                });

        Picasso.with(this)
                .load(Const.API_URL + model.background)
                .placeholder(R.drawable.full_bg)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(full_bg, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(ProfileActivity.this)
                                .load(Const.API_URL + model.background)
                                .placeholder(R.drawable.full_bg)
                                .error(R.drawable.full_bg)
                                .into(full_bg);
                    }
                });

        // запрос списка подарков
        MyGiftModel.loadGifts(token, model.id, new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.arg1) {
                    case Const.STATE_ACCESS_DINEDED:
                        hideWait();
                        logout();
                        break;
                    default:
                        showGifts();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindData();
        bindMedia();
    }

    /**
     * Показать смайлы
     */
    public void showSmiles(){
        // добавляем часто используемые смайлы
        List<SmilesHistoryModel> smilesHistoryList = SmilesHistoryModel.getSmiles();
        // часто используемые смайлы
        for (int i = 0; i < smilesHistoryList.size(); i++){
            SmilesHistoryModel smilesHistoryModel = smilesHistoryList.get(i);
            ImageView smile = (ImageView)findViewById(smilesHistoryModel.smile_id);
            ImageView a_smile = (ImageView)findViewById(SmilesHistoryModel.smile[i]);
            a_smile.setImageDrawable(smile.getDrawable());
            a_smile.setVisibility(View.VISIBLE);
        }
        smiles.setVisibility(View.VISIBLE);

        /**
         * Кнопка удаления
         */
        View backspace = findViewById(R.id.backspace);
        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int start = my_status.getSelectionStart();
                if(start<1){
                    return;
                }

                String left_str = my_status.getEditableText().subSequence(0, start-1).toString();
                CharSequence right_str = "";
                if(start < (my_status.getEditableText().length() -1)){
                    right_str = my_status.getEditableText().subSequence(start, my_status.getEditableText().length());
                }
                String new_str = left_str + right_str;

                if(left_str.length() > 2){
                    start = left_str.length()-1;
                    int last_smile_start = new_str.subSequence(0, start).toString().lastIndexOf(" :smile");
                    int last_smile_end = new_str.subSequence(0, start).toString().lastIndexOf(": ");
                    if(last_smile_start >= 0 && last_smile_end < last_smile_start){
                        left_str = new_str.substring(0, last_smile_start+1);
                        new_str = left_str + right_str;
                    }
                }

                my_status.setText(new_str);
                my_status.setSelection(left_str.length());

                Smiles.setSmiles(new_str, my_status, smile_size, ProfileActivity.this);
            }
        });
    }

    /**
     * Скрыть смайлы
     */
    public void hideSmiles(){
        if(smiles.getVisibility() == View.VISIBLE){
            smiles.setVisibility(View.GONE);
        }
    }

    /**
     * Добавление смайликов
     * @param v
     */
    public void onClickSmile(View v){
        String smile = Smiles.SmileToText(v.getId());

        int selectionCursor = my_status.getSelectionStart();
        Smiles.setSmiles(my_status.getText().insert(selectionCursor, smile).toString(), my_status, smile_size, this);
    }

    /**
     * Отображение подарков
     * в профиле
     */
    public void showGifts(){
        TextView gifts_label = (TextView) findViewById(R.id.gifts_label);
        final int gift_count = MyGiftModel.getCountByUser(model.id);
        List<MyGiftModel> myGiftModels = MyGiftModel.getByUser(model.id);
        gifts_label.setText(getString(R.string.gifts_count, gift_count + ""));
        gifts_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopUpGifts(gift_count);
            }
        });

        LinearLayout gifts_block = (LinearLayout) findViewById(R.id.gifts_block);
        if(gift_count > 0){
            gifts_block.setVisibility(View.VISIBLE);
            int gift_variant = gift_count >= 3 ? 3 : gift_count;
            switch (gift_variant){
                case 3:
                    ImageView gift_1 = (ImageView) findViewById(R.id.gift_1);
                    gift_1.setVisibility(View.VISIBLE);
                    gift_1.setImageDrawable(getResources().getDrawable(GiftModel.getDrawable(myGiftModels.get(2).name)));
                case 2:
                    ImageView gift_2 = (ImageView) findViewById(R.id.gift_2);
                    gift_2.setVisibility(View.VISIBLE);
                    gift_2.setImageDrawable(getResources().getDrawable(GiftModel.getDrawable(myGiftModels.get(1).name)));
                case 1:
                    ImageView gift_3 = (ImageView) findViewById(R.id.gift_3);
                    gift_3.setVisibility(View.VISIBLE);
                    gift_3.setImageDrawable(getResources().getDrawable(GiftModel.getDrawable(myGiftModels.get(0).name)));
            }

        } else {
            gifts_block.setVisibility(View.GONE);
        }
    }

    /**
     * Список подарков пользователя
     * @param gift_count
     */
    private void showPopUpGifts(int gift_count){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        // Custom title popup
        View v_title = View.inflate(this, R.layout.popup_my_gifts_title, null);
        TextView count_gifts = (TextView) v_title.findViewById(R.id.count_gifts);
        count_gifts.setText(getString(R.string.gifts_count, gift_count + ""));
        builder.setCustomTitle(v_title);

        // Popup content
        View v = View.inflate(this, R.layout.popup_gifts_content, null);

        RecyclerView recyclerView;
        MyGiftsAdapter adapter;

        recyclerView = (RecyclerView) v.findViewById(R.id.list);
        adapter = new MyGiftsAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter.update(model.id);

        builder.setView(v);
        builder.setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            PublicUserModel publicUserModel;
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    hideWait();
                    logout();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    hideWait();
                    Toast.makeText(ProfileActivity.this, getString(R.string.info_edit_profile_success_save), Toast.LENGTH_SHORT).show();
                    // Обновляем публичный профиль
                    publicUserModel = model.getPublicModel();
                    publicUserModel.update();
                    bindData();
                    updateMyProfileData();
                    break;
                case Const.STATE_SUCCESS_UPLOAD:
                    hideWait();
                    Toast.makeText(ProfileActivity.this, getString(R.string.info_file_success_save), Toast.LENGTH_SHORT).show();
                    // Обновляем публичный профиль
                    publicUserModel = model.getPublicModel();
                    publicUserModel.update();
                    // обновляем автар
                    bindMedia();
                    updateMyProfileData();
                    break;
                case Const.STATE_ERROR_RESPONSE:
                    hideWait();
                    try {
                        JSONObject resp = new JSONObject((String)msg.obj);
                        if(!resp.isNull("errors")){
                            JSONObject errors = resp.getJSONObject("errors");
                            if(!errors.isNull("name")){
                                Toast.makeText(ProfileActivity.this, errors.getString("name"), Toast.LENGTH_SHORT).show();
                                name.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                            }
                            if(!errors.isNull("city")){
                                Toast.makeText(ProfileActivity.this, errors.getString("city"), Toast.LENGTH_SHORT).show();
                                city.setBackgroundDrawable(getResources().getDrawable(R.drawable.edittext_bg_err));
                            }
                        }
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                    break;
                case Const.STATE_SERVER_ERROR:
                    hideWait();
                    Toast.makeText(ProfileActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
                case Const.STATE_SUCCESS_RESEND_PASSWORD:
                    hideWait();
                    Toast.makeText(ProfileActivity.this, getString(R.string.info_password_success_resend), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    Handler refreshHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            PublicUserModel publicUserModel;
            switch (msg.arg1){
                case Const.STATE_ACCESS_DINEDED:
                    hideWait();
                    logout();
                    break;
                case Const.STATE_SUCCESS_RESPONSE:
                    hideWait();
                    // Обновляем публичный профиль
                    publicUserModel = model.getPublicModel();
                    publicUserModel.update();
                    bindData();
                    updateMyProfileData();
                    break;

                case Const.STATE_ERROR_RESPONSE:
                case Const.STATE_SERVER_ERROR:
                    hideWait();
                    Toast.makeText(ProfileActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    final int SELECT_AVATAR_PICTURE_REQUEST_CODE = 1;
    final int SELECT_BG_PICTURE_REQUEST_CODE = 10;
    public static final int SELECT_PICTURE_AVATAR_CODE = 2;
    public static final int SELECT_PICTURE_BG_CODE = 3;

    public static final int DELETE_PICTURE_AVATAR_CODE = 21;
    public static final int DELETE_PICTURE_BG_CODE = 31;

    private Uri outputFileUri;

    private void openImageIntent(int request_code) {

        // Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "Kitoss" + File.separator);
        root.mkdirs();
        final String fname = "tmp_img.jpg";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, getString(R.string.select_picture));

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        startActivityForResult(chooserIntent, request_code);
    }

    Uri outputUri;

    int actionUpload = 0;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode){
                case SELECT_AVATAR_PICTURE_REQUEST_CODE:
                case SELECT_BG_PICTURE_REQUEST_CODE:
                    final boolean isCamera;
                    if (data == null) {
                        isCamera = true;
                    } else {
                        final String action = data.getAction();
                        if (action == null) {
                            isCamera = false;
                        } else {
                            isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        }
                    }

                    Uri selectedImageUri;
                    if (isCamera) {
                        selectedImageUri = outputFileUri;
                    } else {
                        selectedImageUri = data == null ? null : data.getData();
                    }

                    final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "Kitoss" + File.separator);
                    root.mkdirs();
                    final String fname = "tmp_img2.jpg";
                    final File sdImageMainDirectory = new File(root, fname);
                    outputUri = Uri.fromFile(sdImageMainDirectory);

                    actionUpload = requestCode;
                    if(requestCode == SELECT_AVATAR_PICTURE_REQUEST_CODE){
                        Crop.of(selectedImageUri, outputUri).asSquare().start(ProfileActivity.this);
                    } else if(requestCode == SELECT_BG_PICTURE_REQUEST_CODE){
                        Crop.of(selectedImageUri, outputUri).withAspect(9, 16).start(ProfileActivity.this);
                    }
                    break;
                case Crop.REQUEST_CROP:
                    // загружаем на сервер
                    showWait();
                    if(actionUpload == SELECT_AVATAR_PICTURE_REQUEST_CODE){
                        model.uploadAvatar(outputUri, handler);
                    } else if(actionUpload == SELECT_BG_PICTURE_REQUEST_CODE){
                        model.uploadBg(outputUri, handler);
                    }
                    break;
                case SELECT_PICTURE_AVATAR_CODE:
                    openImageIntent(SELECT_AVATAR_PICTURE_REQUEST_CODE);
                    break;
                case SELECT_PICTURE_BG_CODE:
                    openImageIntent(SELECT_BG_PICTURE_REQUEST_CODE);
                    break;

                case DELETE_PICTURE_BG_CODE:
                    model.deleteBg(handler);
                    break;
                case DELETE_PICTURE_AVATAR_CODE:
                    model.deleteAva(handler);
                    break;

            }
        }
    }
}
