package pro.devapp.kitoss.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarBadge;
import com.roughike.bottombar.OnMenuTabClickListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import pro.devapp.kitoss.R;
import pro.devapp.kitoss.components.App;
import pro.devapp.kitoss.components.BaseActivity;
import pro.devapp.kitoss.components.Const;
import pro.devapp.kitoss.components.DialogHelper;
import pro.devapp.kitoss.components.UserHelper;
import pro.devapp.kitoss.fragments.ChatFragment;
import pro.devapp.kitoss.fragments.EventsFragment;
import pro.devapp.kitoss.fragments.FriendsFragment;
import pro.devapp.kitoss.fragments.HistoriesFragment;
import pro.devapp.kitoss.fragments.ListMessagesFragment;
import pro.devapp.kitoss.fragments.chat.FeedChat;
import pro.devapp.kitoss.interfaces.SmilesFragment;
import pro.devapp.kitoss.models.BlackListModel;
import pro.devapp.kitoss.models.PrivatChatModel;
import pro.devapp.kitoss.models.PublicChatModel;
import pro.devapp.kitoss.models.PublicUserModel;
import pro.devapp.kitoss.models.UserModel;

public class AppActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private NavigationView navigationView;
    private BottomBar mBottomBar;
    private ImageView main_avatar;
    private TextView main_name;
    private TextView drawer_my_name;
    private TextView drawer_my_old;
    private TextView drawer_wallet;
    private ImageView drawer_avatar;
    private ImageView drawer_bg;
    private BottomBarBadge newFriends;
    private BottomBarBadge newPrivateMessages;
    private Boolean second_click = false;
    private Boolean keyBoardShow = false;
    private int gender_size = 25;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);

        /**
         * Получаем доп данные
         */

        // свои сообщения
        PrivatChatModel.loadLastMessages(token, new Handler(){
            private boolean is_process = false;
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if(is_process){
                    return;
                }
                is_process = true;
                switch (msg.arg1) {
                    case Const.STATE_ACCESS_DINEDED:
                        logout();
                        break;
                    case Const.STATE_SUCCESS_RESPONSE:
                        setBadgeFriend();
                        setBadgePrivateMessages();
                        Fragment fragment = getSupportFragmentManager().findFragmentByTag("app");
                        if(fragment instanceof ListMessagesFragment){
                            ((ListMessagesFragment)fragment).update();
                        }
                        break;
                    case Const.STATE_SERVER_ERROR:
                        break;
                }
                is_process = false;
            }
        });
//
//        // public chats
//        PublicChatModel.loadHistoryMessages(token, new Handler(){});
//        // обновление друзей
//        PublicUserModel.getFriends(0, 500, token, new Handler(){});
//        // обновляем чёрный список
//        BlackListModel.getBlack(token, new Handler(){});
        // добавляем или обновляем свой профиль
        PublicUserModel myPublicModel = userModel.getPublicModel();
        myPublicModel.update();

        gender_size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());

        // картинка профиля
        main_avatar = (ImageView) findViewById(R.id.main_avatar);
        main_avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                hideKeyboard();
                showBottomBar();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                hideKeyboard();
                showBottomBar();
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        drawer_my_name = (TextView) drawer.findViewById(R.id.my_name);
        drawer_my_old = (TextView) drawer.findViewById(R.id.my_old);
        drawer_wallet = (TextView) drawer.findViewById(R.id.wallet);
        drawer_wallet.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_kits, 0);
        drawer_wallet.setCompoundDrawablePadding(10);

        drawer_avatar = (ImageView) drawer.findViewById(R.id.avatar);
        drawer_bg = (ImageView) drawer.findViewById(R.id.full_bg);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mBottomBar = BottomBar.attach(this, savedInstanceState);
        mBottomBar.noNavBarGoodness();
        mBottomBar.noTabletGoodness();
        //mBottomBar.setTextAppearance(R.style.MyTextAppearance);
        mBottomBar.noTopOffset();
        mBottomBar.setMaxFixedTabs(4);
        //mBottomBar.useFixedMode();
        mBottomBar.setItemsFromMenu(R.menu.bottombar_menu, new OnMenuTabClickListener() {

            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
                second_click = false;
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment old_fragment = getSupportFragmentManager().findFragmentByTag("app");
                if(old_fragment != null)
                    ft.remove(old_fragment);
                switch (menuItemId){
                    case R.id.ic_email:
                        ft.replace(R.id.main_fragment, ListMessagesFragment.newInstance(), "app");
                        mBottomBar.selectTabAtPosition(0, false);
                        mBottomBar.setActiveTabColor(R.color.tab_1);
                        break;
                    case R.id.ic_friends:
                        ft.replace(R.id.main_fragment, FriendsFragment.newInstance(), "app");
                        mBottomBar.selectTabAtPosition(1, false);
                        mBottomBar.setActiveTabColor(R.color.tab_2);
                        break;
                    case R.id.ic_events:
                        ft.replace(R.id.main_fragment, EventsFragment.newInstance(), "app");
                        mBottomBar.selectTabAtPosition(2, false);
                        mBottomBar.setActiveTabColor(R.color.tab_3);
                        break;
                    case R.id.ic_history:
                        ft.replace(R.id.main_fragment, HistoriesFragment.newInstance(), "app");
                        mBottomBar.selectTabAtPosition(3, false);
                        mBottomBar.setActiveTabColor(R.color.tab_4);
                        break;
                    case R.id.ic_bullhorn:
                        ft.replace(R.id.main_fragment, ChatFragment.newInstance(), "app");
                        mBottomBar.selectTabAtPosition(4, false);
                        mBottomBar.setActiveTabColor(R.color.tab_5);
                        break;
                }
                ft.addToBackStack(null);
                ft.commit();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setBadgePrivateMessages();
                        setBadgeFriend();
                    }
                }, 1000l);

            }
            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.ic_history) {
                    // The user reselected item number one, scroll your content to top.
                }
            }
        });

        // Setting colors for different tabs when there's more than three of them.
        // You can set colors for tabs in three different ways as shown below.
        mBottomBar.mapColorForTab(0, ContextCompat.getColor(this, R.color.tab_1));
        mBottomBar.mapColorForTab(1, ContextCompat.getColor(this, R.color.tab_2));
        mBottomBar.mapColorForTab(2, ContextCompat.getColor(this, R.color.tab_3));
        mBottomBar.mapColorForTab(3, ContextCompat.getColor(this, R.color.tab_4));
        mBottomBar.mapColorForTab(4, ContextCompat.getColor(this, R.color.tab_5));

        // кнопка поиск
        View search = findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AppActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });

        // конпка игры
        View gift = findViewById(R.id.gift);
        gift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AppActivity.this, GameActivity.class);
                startActivity(intent);
            }
        });

        initMenu();

        /**
         * показ окна промо кода для нового юзера
         */
        if(!((App)getApplication()).getAppPrefs().getBooleanPreference("first_run_app") && !userModel.showed_promo){
            showPopUpPromo();
        }
    }

    /**
     * установка бейджа на вкладке друзей
     */
    public void setBadgeFriend(){
        int count = PublicUserModel.getCountFriendRequest();
        if(count > 0 &&  mBottomBar.getCurrentTabPosition() != 1){
            if(newFriends == null){
                newFriends = mBottomBar.makeBadgeForTabAt(1, "#FF9800", count);
                newFriends.hide();
                newFriends.setAnimationDuration(200);
                newFriends.setAutoShowAfterUnSelection(true);
                newFriends.setAutoHideOnSelection(true);
            } else {
                newFriends.setCount(count);
            }
            if(!newFriends.isVisible()){
                newFriends.show();
            }
        } else if(newFriends != null && newFriends.isVisible()){
            newFriends.setCount(0);
            newFriends.hide();
        }
    }

    /**
     * установка бейджа на вкладке приватных сообщений
     */
    public void setBadgePrivateMessages(){
        int count = PrivatChatModel.getCountNew(userModel.id);
        if(count > 0 &&  mBottomBar.getCurrentTabPosition() != 0){
            if(newPrivateMessages == null){
                newPrivateMessages = mBottomBar.makeBadgeForTabAt(0, "#FF9800", count);
                newPrivateMessages.hide();
                newPrivateMessages.setAnimationDuration(200);
                newPrivateMessages.setAutoShowAfterUnSelection(true);
                newPrivateMessages.setAutoHideOnSelection(true);
            } else {
                newPrivateMessages.setCount(count);
            }
            if(!newPrivateMessages.isVisible()){
                newPrivateMessages.show();
            }
        } else if(newPrivateMessages != null && newPrivateMessages.isVisible()){
            newPrivateMessages.setCount(0);
            newPrivateMessages.hide();
        }
    }

    /**
     * Левое меню
     */
    public void initMenu(){
        // кнопка редактирования профиля
        View edit = findViewById(R.id.edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(AppActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        View l_favorites = findViewById(R.id.l_favorites);
        l_favorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(AppActivity.this, FavoritesActivity.class);
                startActivity(intent);
            }
        });

        View l_cash = findViewById(R.id.l_cash);
        l_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(AppActivity.this, MyCashActivity.class);
                startActivity(intent);
            }
        });

        View l_black_list = findViewById(R.id.l_black_list);
        l_black_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(AppActivity.this, BlackListActivity.class);
                startActivity(intent);
            }
        });

        View l_settings = findViewById(R.id.l_settings);
        l_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(AppActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });

        View l_help = findViewById(R.id.l_help);
        l_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(AppActivity.this, UserChatActivity.class);
                intent.putExtra("uid", "57652aae5d760a0a5ec79a38");
                startActivity(intent);
            }
        });

        View l_about = findViewById(R.id.l_about);
        l_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(AppActivity.this, AboutActivity.class);
                startActivity(intent);
            }
        });

        View l_exit = findViewById(R.id.l_exit);
        l_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
               // logout();
                finish();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Necessary to restore the BottomBar's state, otherwise we would
        // lose the current tab on orientation change.
        mBottomBar.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        /**
         * Скрываем клавиатуру если показана
         */
        if(keyBoardShow){
            keyBoardShow = false;
            hideKeyboard();
            showBottomBar();
            return;
        }

        if(!is_show_bottom_bar){
            showBottomBar();
        }

        /**
         * Передаём событие в фрагменты
         */
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("app");
        if(fragment instanceof ChatFragment){
            if(((ChatFragment)fragment).onBackPressed()){
                return;
            }
        }

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            Fragment old_fragment = getSupportFragmentManager().findFragmentByTag("app");
//            if(old_fragment == null || getSupportFragmentManager().getBackStackEntryCount() == 0 || second_click){
//                DialogHelper dialogHelper = new DialogHelper();
//                dialogHelper.Confirm(
//                        AppActivity.this,
//                        getString(R.string.confirm_exit_title),
//                        getString(R.string.confirm_exit_text),
//                        getString(R.string.confirm_no),
//                        getString(R.string.confirm_yes),
//                        new Runnable(){
//                            @Override
//                            public void run() {
//                                finish();
//                            }
//                        },
//                        new Runnable(){
//                            @Override
//                            public void run() {
//
//                            }
//                        });
//                return;
//            }

            DialogHelper dialogHelper = new DialogHelper();
            dialogHelper.Confirm(
                    AppActivity.this,
                    getString(R.string.confirm_exit_title),
                    getString(R.string.confirm_exit_text),
                    getString(R.string.confirm_no),
                    getString(R.string.confirm_yes),
                    new Runnable(){
                        @Override
                        public void run() {
                            finish();
                        }
                    },
                    new Runnable(){
                        @Override
                        public void run() {

                        }
                    });
            super.onBackPressed();
            // исходя из последней задачи нужно показывать диалог выхода по первому нажатию
//            second_click = true;
//            old_fragment = getSupportFragmentManager().findFragmentByTag("app");
//            if(old_fragment instanceof ListMessagesFragment){
//                mBottomBar.selectTabAtPosition(0, true);
//            }
//            if(old_fragment instanceof FriendsFragment){
//                mBottomBar.selectTabAtPosition(1, true);
//            }
//            if(old_fragment instanceof EventsFragment){
//                mBottomBar.selectTabAtPosition(2, true);
//            }
//            if(old_fragment instanceof HistoriesFragment){
//                mBottomBar.selectTabAtPosition(3, true);
//            }
//            if(old_fragment instanceof ChatFragment){
//                mBottomBar.selectTabAtPosition(4, true);
//            }
        }
    }

    /**
     * Скрываем клавиатуру
     */
    public void hideKeyboard() {
        if(!keyBoardShow){
            return;
        }
       super.hideKeyboard();
        keyBoardShow = false;
    }

    public boolean isShowKeyBoard(){
        return keyBoardShow;
    }

    /**
     * Показываем клавиатуру
     */
    public void showKeyboard() {
        if(keyBoardShow){
            return;
        }
        super.showKeyboard();
        keyBoardShow = true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()){

        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        subscribe();
        // обновление профиля
        userModel.refreshFromServer(new Handler(){});

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                ((App)getApplication()).clear();
                return null;
            }
        };
        task.execute();

        setBadgeFriend();
        setBadgePrivateMessages();
        // имя профиля
        main_name = (TextView) findViewById(R.id.main_name);
        main_name.setText(userModel.first_name);

        Picasso.with(this)
                .load(Const.API_URL + userModel.avatar)
                .placeholder(R.drawable.ava)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(main_avatar, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(AppActivity.this)
                                .load(Const.API_URL + userModel.avatar)
                                .error(R.drawable.ava)
                                .into(main_avatar);
                    }
                });

        drawer_my_name.setText(userModel.first_name);
        drawer_wallet.setText(userModel.balance + "");

        drawer_my_old.setText(String.valueOf(UserHelper.getOld(userModel.birth_date)));

        Drawable img = UserHelper.getGenderDrawable(userModel.gender, AppActivity.this);
        img.setBounds( 0, 0, gender_size, gender_size );
        drawer_my_old.setCompoundDrawables(null, null, img, null);

        Picasso.with(this)
                .load(Const.API_URL + userModel.avatar)
                .placeholder(R.drawable.ava)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(drawer_avatar, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(AppActivity.this)
                                .load(Const.API_URL + userModel.avatar)
                                .error(R.drawable.ava)
                                .into(drawer_avatar);
                    }
                });


        Picasso.with(this)
                .load(Const.API_URL + userModel.background)
                .placeholder(R.drawable.full_bg)
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(drawer_bg, new Callback() {
                    @Override
                    public void onSuccess() {

                    }
                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(AppActivity.this)
                                .load(Const.API_URL + userModel.background)
                                .placeholder(R.drawable.full_bg)
                                .error(R.drawable.full_bg)
                                .into(drawer_bg);
                    }
                });
    }

    /**
     * Добавление смайликов
     * @param v
     */
    public void onClickSmile(View v){
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("app");
        if(fragment instanceof SmilesFragment){
            ((SmilesFragment)fragment).onClickSmile(v);
        }
    }

    Boolean is_show_bottom_bar = true;
    /**
     * Скрыть нижнее меню
     */
    public void hideBottomBar(){
        is_show_bottom_bar = false;
        mBottomBar.hide();
    }

    /**
     * Показать нижнее меню
     */
    public void showBottomBar(){
        is_show_bottom_bar = true;
        mBottomBar.show();
    }

    /**
     * Go to user profile
     */
    public void goToUserProfile(String user_id){
        Intent intent = new Intent(this, PublicProfileActivity.class);
        intent.putExtra("uid", user_id);
        startActivity(intent);
    }

    /**
     * подписка паблик на чат
     */
    private void subscribe(){
        // подписка на региональный чат
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("region", userModel.region);
            ((App)getApplication()).SocketEmit("subscribe_public_chat", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Go to my profile
     */
    public void goToMyProfile(){
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }


    public void showPopUpPromo(){
        final Dialog dialog = new Dialog(this);
        LayoutInflater inflater = getLayoutInflater();
        final View view = inflater.inflate(R.layout.popup_promo, null);

        Button cancel = (Button) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                ((App)getApplication()).getAppPrefs().saveBooleanPreference("first_run_app", true);
            }
        });

        Button send = (Button) view.findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText text = (EditText) view.findViewById(R.id.text);
                if(text.getText().length() < 3 || text.getText().length() > 10){
                    text.setError("Промокод не может быть пустым");
                } else {
                    showWait();

                    UserModel.save_promo( text.getText().toString(), token, new Handler(){
                        @Override
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            switch (msg.arg1) {
                                case Const.STATE_SUCCESS_RESPONSE:
                                    dialog.dismiss();
                                    Toast.makeText(AppActivity.this, "Промокод сохранён", Toast.LENGTH_LONG).show();
                                    break;
                                case Const.STATE_SERVER_ERROR:
                                    Toast.makeText(AppActivity.this, getString(R.string.error_server), Toast.LENGTH_LONG).show();
                            }
                            hideWait();
                        }
                    });

                }
                ((App)getApplication()).getAppPrefs().saveBooleanPreference("first_run_app", true);
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(view);
        dialog.show();
    }

    /**
     * Новое приватное сообщение
     * @param privatChatModel
     */
    @Override
    public void onSocketNewMessage(PrivatChatModel privatChatModel) {
        if(mBottomBar.getCurrentTabPosition() != 0){
            // не показываем пуши когда открыта вкладка сообщений
            super.onSocketNewMessage(privatChatModel);
        } else {
            playSound(message_chat_sound);
        }
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("app");
        if(fragment instanceof ListMessagesFragment){
            ((ListMessagesFragment)fragment).update();
        }
        setBadgePrivateMessages();
    }

    /**
     * Новая заявка в друзья
     */
    @Override
    public void onSocketNewFriendsRequest(PublicUserModel publicUserModel) {
        super.onSocketNewFriendsRequest(publicUserModel);
        setBadgeFriend();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("app");
        if(fragment instanceof ListMessagesFragment){
            ((ListMessagesFragment)fragment).update();
        }
        if(fragment instanceof FriendsFragment){
            ((FriendsFragment)fragment).update();
        }
    }

    /**
     * Вас удалили из друщей
     */
    @Override
    public void onSocketRemoveFromFriends(PublicUserModel publicUserModel) {
        super.onSocketNewFriendsRequest(publicUserModel);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("app");
        if(fragment instanceof ListMessagesFragment){
            ((ListMessagesFragment)fragment).update();
        }
        if(fragment instanceof FriendsFragment){
            ((FriendsFragment)fragment).update();
        }
    }

    /**
     * Пользователь отменил свою заявку на дружбу
     */
    @Override
    public void onSocketCancelFriendRequest(PublicUserModel publicUserModel) {
        super.onSocketNewFriendsRequest(publicUserModel);
        setBadgeFriend();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("app");
        if(fragment instanceof ListMessagesFragment){
            ((ListMessagesFragment)fragment).update();
        }
        if(fragment instanceof FriendsFragment){
            ((FriendsFragment)fragment).update();
        }
    }

    /**
     * Новое публичное сообщение
     *
     * @param publicChatModel
     */
    @Override
    public void onSocketNewPublicMessage(PublicChatModel publicChatModel) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("app");
        if(fragment instanceof FeedChat){
            ((FeedChat)fragment).new_public_message(publicChatModel);
        }
    }

    /**
     * Удалено публичное сообщение
     */
    @Override
    public void onSocketRemovePublicMessage(PublicChatModel publicChatModel) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("app");
        if(fragment instanceof FeedChat){
            ((FeedChat)fragment).removePublicMessage(publicChatModel);
        }
    }

    /**
     * Пополнение баланса
     */
    @Override
    public void onSocketUpdateBalance(UserModel userModel) {
        super.onSocketUpdateBalance(userModel);
        if(drawer_wallet != null){
            drawer_wallet.setText(userModel.balance + " к");
        }
    }

    /**
     * Новое событие в ленте
     */
    @Override
    public void onSocketNewEvent() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("app");
        if(fragment instanceof EventsFragment){
            ((EventsFragment)fragment).updateAsync();
        }
    }
}
