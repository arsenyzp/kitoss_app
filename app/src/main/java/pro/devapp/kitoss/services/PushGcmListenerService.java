package pro.devapp.kitoss.services;

import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.gcm.GcmListenerService;
import pro.devapp.kitoss.components.NotificationHelper;
import pro.devapp.kitoss.models.NotificationModel;

/**
 * Push listener
 */
public class PushGcmListenerService extends GcmListenerService {

    private static final String TAG = "PushGcmListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("body");
        String title = data.getString("title");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Title: " + title);
        Log.d(TAG, "Message: " + message);


        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        NotificationHelper.sendNotificationGCM(title, message, this, NotificationModel.TYPE_NOTIFICATIONS);
    }
}
